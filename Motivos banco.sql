use chequesdata;
Insert into motivo values (11 , 'Cheque sem fundos - 1ª apresentação');
Insert into motivo values (12 , 'Cheque sem fundos - 2ª apresentação');
Insert into motivo values (13 , 'Conta encerrada');
Insert into motivo values (14 , 'Prática espúria');
Insert into motivo values (20 , 'Cheque sustado ou revogado em virtude de roubo, furto ou extravio de folhas de cheque em branco');
Insert into motivo values (21 , 'Cheque sustado ou revogado');
Insert into motivo values (22 , 'Divergência ou insuficiência de assinatura');
Insert into motivo values (23 , 'Cheques emitidos por entidades e órgãos da administração pública federal direta e indireta, em desacordo com os requisitos constantes do art. 74, § 2º, do Decreto-lei nº 200, de 25 de fevereiro de 1967');
Insert into motivo values (24 , 'Bloqueio judicial ou determinação do Bacen');
Insert into motivo values (25 , 'Cancelamento de talonário pelo participante destinatário');
Insert into motivo values (27 , 'Feriado municipal não previsto');
Insert into motivo values (28 , 'Cheque sustado ou revogado em virtude de roubo, furto ou extravio');
Insert into motivo values (30 , 'Furto ou roubo de cheque');
Insert into motivo values (31 , 'Erro  formal  (sem  data  de  emissão,  com  o  mês  grafado  numericamente,  ausência  de  assinatura  ou  não registro do valor por extenso)');
Insert into motivo values (33 , 'Divergência de endosso');
Insert into motivo values (34 , 'Cheque apresentado por participante que não o indicado no cruzamento em preto, sem o endosso-mandato');
Insert into motivo values (35 , 'Cheque fraudado, emitido sem prévio controle ou responsabilidade do participante (""cheque universal""), ou com adulteração da praça sacada, ou ainda com rasura no preenchimento');
Insert into motivo values (37 , 'Registro inconsistente');
Insert into motivo values (38 , 'Assinatura digital ausente ou inválida');
Insert into motivo values (39 , 'Imagem fora do padrão');
Insert into motivo values (40 , 'Moeda Inválida');
Insert into motivo values (41 , 'Cheque apresentado a participante que não o destinatário');
Insert into motivo values (43 , 'Cheque, devolvido anteriormente pelos motivos 21, 22, 23, 24, 31 e 34, não passível de reapresentação em virtude de persistir o motivo da devolução');
Insert into motivo values (44 , 'Cheque prescrito');
Insert into motivo values (45 , 'Cheque  emitido  por  entidade  obrigada  a  realizar  movimentação  e  utilização  de  recursos  financeiros  do Tesouro Nacional mediante Ordem Bancária');
Insert into motivo values (48 , 'Cheque de valor superior a R$100,00 (cem reais), emitido sem a identificação do beneficiário');
Insert into motivo values (49 , 'Remessa nula, caracterizada pela reapresentação de cheque devolvido pelos motivos 12, 13, 14, 20, 25, 28, 30, 35, 43, 44 e 45.');
Insert into motivo values (59 , 'Informação essencial faltante ou inconsistente não passível de verificação pelo participante remetente e não enquadrada no motivo 31');
Insert into motivo values (60 , 'Instrumento inadequado para a finalidade');
Insert into motivo values (61 , 'Papel não compensável');
Insert into motivo values (70 , 'Sustação ou revogação provisória');
Insert into motivo values (71 , 'Inadimplemento contratual da cooperativa de crédito no acordo de compensação');
Insert into motivo values (72 , 'Contrato de Compensação encerrado');