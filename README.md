SIL � SOFTWARE
SCHEQUES
22/06/2018
VIS�O GERAL
1. Descri��o e hist�rico do projeto
Sistema de gerenciamento de desconto de cheques e processo de empr�stimos com fluxo de caixa. 
2. Escopo do projeto
O SCheques � voltado para empresas de factory para processo de antecipa��o de receb�veis, para vendas em cheques, como tamb�m empr�stimos. O processo encarrega de controlar os procedimentos de desconto, cliente a cliente como os de empr�stimos e antecipa��es.
3. Requisitos de alto n�vel
Os requisitos do sistema s�o divididos em dois tipos: RF � Requisitos funcionais, s�o aqueles que representam alguma funcionalidade ao sistema e RNF � Requisitos n�o funcionais, s�o aqueles representam algum aspecto ao sistema.

a. RF1 � Lotes:
1. O lote de cheque � composto pelos seguintes campos: (I) Cliente, (II) Quantidade de cheques do lote, (III) Valor total do lote de cheques, (IV) Data do lote;
2. Ao ser selecionado o cliente para o lote, a aplica��o deve coletar no cadastro deste, as taxas pr�-fixadas para serem aplicadas no lote;
3. A aplica��o deve permitir ao usu�rio, que a partir do lote, ele possa cadastrar um cliente.
4. A busca dentro do lote pode ser realizada a partir dos seguintes campos: Cliente do lote, Data do lote, n�mero do lote;
5. As buscas feitas que resultar em mais de um resultado deve ser exibido em uma lista para que o usu�rio possa utilizar.
6. Quando um lote for finalizado, o aplicativo deve apresentar um relat�rio contendo o resultado do lote com cada cheque, banco, conta e ag�ncia, al�m do vencimento e valor de cada cheque. Esse relat�rio deve ser montado em ordem de digita��o.
7. Ao ser realizado a conclus�o e a conferencia do lote o usu�rio poder� realizar, com isso o aplicativo deve lan�ar uma transa��o de cr�dito do valor do lote deduzido os juros na conta de transa��o do cliente que se refere o lote.
b. RNF1 � Lotes:
1. A aplica��o, no formul�rio de lote, deve conter os bot�es Gravar, Novo, Excluir, Buscar, Editar, Cancelar e Sair;
2. O Bot�es gravar e Cancelar s� devem ser disponibilizados durante o processo de registro de um novo lote ou de edi��o;
3. Os bot�es: novo, excluir, buscar, editar e sair. S� estar�o dispon�veis se a aplica��o n�o estiver no processo de grava��o de um registro;
4. A aplica��o deve disponibilizar um campo para ser preenchido com os dados para a busca.

c. RF2 � Usu�rio:
1. A aplica��o deve fornecer a possibilidade de cadastro de usu�rio. O usu�rio deve ter os seguintes campos: (I) Nome, (II) Login, (III) Senha, (IV) Ativo;
2. Todos os processos e edi��o, grava��o dos usu�rios devem ser registrados em processo de lote no banco de dados para um processo de auditoria de dados.
3. O usu�rio n�o ativo, n�o poder� ter acesso a dados da aplica��o;
4. O usu�rio pode desativar ou ativar outros usu�rios, mas n�o poder� desativar a ele mesmo;
5. A aplica��o, no primeiro acesso, deve pedir para criar o primeiro usu�rio, em seguida solicitar que o login seja realizado; 
6. A busca dentro do formul�rio usu�rio poder� ser realizada por meio dos campos: Nome  
7. As buscas feitas que resultar em mais de um resultado, deveram ser apresentadas em uma lista para o usu�rio selecionar o dado desejado.
8. Somente o pr�prio usu�rio poder� editar e visualizar os seus dados;

d. RNF2 � Usu�rio:
1. A aplica��o, no formul�rio de usu�rio, deve conter os mesmos requisitos da RNF1.1 a RNF1.4;

e. RNF3 � A Aplica��o:
1. A aplica��o deve ser desenvolvida para uso em desktop, compat�vel com sistema Windows 10;
2. A aplica��o deve ser desenvolvida com recursos multitarefa para rede;
3. A aplica��o deve ser instalada em uma maquina como servidora e nas demais como esta��o;
4. As maquinas esta��o deveram ter no seu arquivo XML de configura��o a informa��o do endere�o da maquina servidora para acesso do banco de dados;
5. A aplica��o n�o deve abrir na esta��o caso o servidor esteja fora do ar;
6. A aplica��o pode oferecer a op��o de recupera��o em nuvem do backup, caso o servidor tenha dado problema e transformar essa esta��o em um novo servidor;
7. Caso a esta��o n�o encontre o servidor na rede, a aplica��o deve exibir as configura��es de rede da esta��o para que possa ser feito o devido ajuste;
8. A aplica��o deve disponibilizar um bot�o que possa testar a conex�o configurada antes de gravar os dados;
9. Os dados de configura��o de rede devem ficar guardado em um arquivo XML;
10. A aplica��o, para ativar seu serial na web, o mesmo s� poder� ser no servidor, e a mesma s� pode ser feita em uma �nica m�quina, n�o sendo permitido o uso de 2 maquinas servidoras com a mesma serial. 
11. O sistema poder� permitir quantas esta��es o cliente desejar com o mesmo serial.
12. A aplica��o deve oferecer ao usu�rio a op��o de impress�o de cheques nos formul�rios que requisita-los.
f. RF3 � Cheques:
1. Os cheques devem conter os seguintes campos: (I) CodBanco, (II) CodAgencia, (III) Conta, (IV) N�mero do cheque, (V) Valor do cheque, (VI) data de vencimento, (VII) Opera��o nos bancos que trabalham com esse requisito.
2. Caso o banco n�o trabalhe com opera��o na conta, o campo deve ser preenchido com �000�. 
3. Al�m destes dados a aplica��o poder� solicitar, caso a conta ainda n�o esteja cadastrada, o CPF ou CNPJ do emitente do cheque, esse campo ser� obrigat�rio, al�m dele, o usu�rio poder� registrar junto a esse CPF ou CNPJ o nome do emitente.
4. O nome do emitente, ao contr�rio do CPF ou CNPJ, ser� opcional e poder� ser posteriormente adicionado em outro momento da aplica��o.
5. Caso a conta j� esteja cadastrada, o sistema s� deve apresentar os dados de CPF/CNPJ do emitente e o nome, caso tenha o mesmo cadastrado.
6. Ao ser informado o Valor do cheque e o vencimento, a aplica��o j� deve informar os seguintes dados:  O n�mero de dias entre a data do lote e o vencimento do cheque, o valor dos juros calculado, o valor do cheque com os juros descontados.
7. A aplica��o deve extrair do campo de c�digo de barras os dados de: (I) Cod do banco, (II) Cod da Ag�ncia, (III) Conta Corrente, (IV) Opera��o (no caso dos bancos que trabalham com esse requisito);
8. Caso o usu�rio n�o tenha leitora de cheque ou haja falha na leitura, a aplica��o deve oferecer a possibilidade da digita��o dos campos coletados do c�digo de barras. Mas o usu�rio tamb�m poder� digitar o c�digo de barras inteiro.
9. A aplica��o, deve analisar os dados do c�digo de barras do cheque e valida-lo, grupo a grupo, caso ele encontre um erro em qualquer um dos grupos, a aplica��o deve identificar qual grupo est� errado e direcionar o usu�rio para corrigi-lo
10. O campo de c�digo de barras s� poder� conter n�meros.
11. Ao ser lan�ado os dados de um cheque, o aplicativo deve verificar se ao CPF/CNPJ vinculado a conta, apresenta em seu hist�rico algum registro de cheque devolvido. 
12. Tamb�m deve ser verificado quanto h� em saldo a depositar tem o emitente do cheque at� o presente momento. 
13. A vincula��o dos cheques passa a ser pela conta junto ao CPF/CNPJ do emitente.
14. Quando for passado uma conta de um cpf ou cnpj negativado, o mesmo deve ser informado.
15. O usu�rio poder� inserir hist�ricos de movimenta��o de um cheque, e deve ser usado os hist�ricos da febrabam.
16. O usu�rio poder� procurar por um cheque posteriormente em outro formul�rio, bastando digitar algum dos seguintes dados: Emitente( Nome, CPF ou CNPJ), combina��o de Banco, Ag�ncia, Conta e n�mero do cheque, sendo que o resultado deve ser apresentado em uma lista para que o usu�rio possa selecionar qual cheque que verificar os dados.
17. O cheque selecionado na consulta deve apresentar um pequeno hist�rico contendo desde a sua digita��o at� sua compensa��o por dep�sito ou por devolu��o ao cliente.
18. O cheque poder� ter as seguintes posi��o: Digitado (Quando o mesmo � digitado no sistema.) , Separado para dep�sito ( Quando o mesmo � separado para enviar ao banco mas ainda n�o tem a posi��o final), Depositado (Quando o mesmo seguiu para dep�sito, este deve estar vinculado a um dep�sito que identifica qual banco est� junto com outros do mesmo lote.), Devolvido pelo banco (Quando o mesmo foi devolvido seja pela primeira ou segunda vez, neste caso deve ser informado a al�nea da devolu��o no processo de hist�rico), Usado para pagar cliente (Quando o mesmo for utilizado para pagamento de algum movimento de um outro cliente, ou at� mesmo depositado na conta deste), Devolvido ao cliente(Quando esse � recuperado pelo cliente seja por qual motivo for). 
19. Caso o vencimento do cheque seja um s�bado, um domingo ou um feriado do processo de calend�rio, o aplicativo dever� efetuar o acr�scimo de dias ao c�lculo conforme a seguinte regra: 
a. Se o vencimento n�o for um dia �til o sistema dever� adicionar um dia a mais e novamente verifica se essa nova data � �til, se n�o, repetir o processo at� o primeiro dia �til que ele validar.
b. O n�mero de dias adicionais deve ser apresentado no c�lculo dos juros.
g. RF4 � Cliente:
1. O aplicativo deve oferecer ao usu�rio a possibilidade de adicionar, bloquear, editar os dados de um cliente.
2. O cadastro de um cliente deve conter os seguintes dados: (I) Nome, (II) Endere�o, (III) Telefone contato, (IV) Cidade, (V) Estado, (VI) Taxa juros, (VII) Taxa Administrativa, (VIII) Juros acumulado. Sendo os campos I, VI a VIII obrigat�rios.
3. O aplicativo deve permitir ao usu�rio que ele possa verificar por meio de um extrato, todas as movimenta��es vinculadas ao cliente, sejam elas por empr�stimos ou descontos.
4. Deve ser poss�vel que o usu�rio possa verificar o saldo que o cliente esteja devendo a empresa por meio de empr�stimo, bem como o saldo de cheque a depositar trocados por esse cliente.
5. Na conta do cliente, o usu�rio poder� realizar lan�amentos tais como: Pagamento de cheque em dinheiro, Pagamento de cheque com dep�sito de outros cheques, associar esses cheques a transa��o, Pagamento de duplicatas com cheque ou com dinheiro. 
6. Quando a transa��o for realizada o saldo da conta do cliente deve zerar.
7. A Aplica��o no extrato do cliente deve conter dois tipos de saldo, sendo ele o saldo da conta por desconto de cheque, este deve ao final do dia est� fechado e saldo de linhas de empr�stimos a realizar, feito por� antecipa��es ou empr�stimos realizados ao cliente.
8. Algumas transa��es s�o apresentadas ao extrato de forma autom�tica como: Valor de desconto de cheque do lote (Trata-se dos cheques descontados pelo cliente vindo do lote fechado), tamb�m os lan�amentos por meio de empr�stimos.
9. Os cheques que vierem de um cliente e forem devolvidos, devem ser apresentados no extrato como um novo empr�stimo, e deve contar os seus juros a partir da data do carimbo da primeira devolu��o.
10. O cliente s� pode ter um �nico cadastro no sistema.
h. RF5 � Empr�stimo:
1. O empr�stimo ou financiamento para ser realizado deve solicitar os seguintes dados: (I) Cliente, (II) Data do inicio do processo, (III) Valor do empr�stimo, (IV) N�mero de parcelar, (V) Intervalo de dias entra as parcelas e ter� como padr�o 30 dias, (VI) Taxa de juros, (VII) Taxa administrativa.
2. O empr�stimo deve ter um bot�o que ir� gerar as parcelas j� com os respectivos valores e vencimento j� incluso os juros do per�odo. 
3. Cada parcela deve ser informada se � uma promiss�ria ou um cheque, e no caso de cheque deve ser repassado os dados do cheque que s�o os mesmos usados no lote de cheques.
4. Os cheques lan�ados no lote de financiamento, deveram ter o mesmo tratamento dos cheques do lote de desconto.
5. O empr�stimo pode ter a modalidade de adiantamento, neste caso o mesmo � definido como uma parcela e n�o ter� vencimento, os juros desta modalidade s� ser� calculado na quita��o do empr�stimo.
6. Todo empr�stimo que tiver cheque caucionado ser� considerado quitado e os cheques transferidos para o saldo de cheques a depositar nas respectivas datas.
7. Um empr�stimo pode ser quitado integral ou parcialmente, quando quitado parcialmente o res�duo poder� ficar na mesma condi��o do processo original, ou criado um novo processo podendo ser vinculado cheques.
8. O empr�stimo que n�o houver cau��o de cheques deve emitir uma promiss�ria para ser assinada pelo cliente.
9. Caso o empr�stimo seja parcelado e n�o tenha uso de cheques e sim de duplicata, o sistema dever� emitir uma duplicata para cada vencimento, j� incluso os juros do per�odo. Este procedimento n�o considera como liquidado, como no caso de cheques caucionando, visto que poder� haver a possibilidade de atraso na data de pagamento.
10. O sistema dever� aplicar a mesma regra de calculo de juros do processo de desconto caso o vencimento da parcela de empr�stimo coincida com um dia n�o �til.
11. Os empr�stimos realizados e suas transa��es devem apresentar no extrato do cliente a qual ele ser referencie.
i. RF6 � Lan�amentos:
1. �  Processo a ser desenvolvido na segunda etapa do projeto.
j. RF7 � Relat�rios:
1. A aplica��o deve oferecer os seguintes relat�rios ao usu�rio:
a. Resumo de cheques descontados contendo as seguintes informa��es: Quantidade de lotes descontados, valor de juros, valor total dos lotes somados, valor total descontados os juros.
b. O mesmo relat�rio da 1.a sendo que agora com o detalhamento de cada lote.
c. Rela��o de clientes contendo os que tem saldo devedor.
d. Lista de contas de clientes que n�o est�o com saldo zerado.
e. Lista de cheques devolvidos ainda n�o regularizado podendo ser ordenado por: Cliente que trouxe, banco do emitente do cheque, por emitente.
f. Lista de clientes com empr�stimos atrasados.
g. Lista de clientes com empr�stimos a realizar.
h. Extrato de um cliente por per�odo.
i. Relat�rio com o resumo da produtividade do dia.
j. O relat�rio de fechamento do dia deve ser enviado para o e-mail que estiver cadastrado nos dados de administra��o.

k. RF8 � Calend�rio:
1. A Aplica��o deve oferecer ao usu�rio a possibilidade de criar um calend�rio de feriados.
2. A aplica��o deve oferecer a possibilidade de o usu�rio definir se uma data deve ser repetir nos anos seguintes ou n�o. Como padr�o a data deve ser do ano corrente.
l. RF9 � Fechamento:
1. � Processo a ser desenvolvido na segunda etapa do projeto.
m. RF10 � Administrativo:
1. A aplica��o deve oferecer ao usu�rio a possibilidade de cadastrar informa��es padr�es que s�o obrigat�rias, s�o elas: Nome, endere�o, telefone e CPF ou CNPJ da empresa que ir� usar o sistema. Ainda deve ser informado um serial para ativa��o que deve ser realizado no primeiro processo de acesso ao sistema. 
2. Nos dados cadastrais da empresa deve ser poss�vel adicionar um e-mail para que seja encaminhado relat�rios.
3. O usu�rio ainda deve informar os dados padr�es que s�o eles: Taxa de juros, Taxa administrativa e por fim se o mesmo aplicar� os juros acumulados.
4. O sistema deve verificar mensalmente on-line se h� atualiza��es al�m da normalidade da mensalidade.
5. Caso haja atualiza��o o processo de download deve ser iniciado automaticamente e ap�s instalado. 
6. Caso haja pendencias o sistema dever� informar ao usu�rio e um prazo para regularizar ser� informado, ap�s o prazo o mesmo dever� parar imediatamente.
7. O sistema de backup deve trabalhar em nuvem por medida de seguran�a, de modo autom�tico.

4. Resultados finais
O processo deve ser conclu�do em etapas e apresentado ao cliente para que este possa dar um feedback para a pr�xima realize. As realizes devem ser ajustadas dentro do prazo estipulado. 
5. Custos e tempo de conclus�o
Cada etapa do projeto dever� ser entregue em reliases funcionais dentro do calend�rio apresentado.  O tempo estipulado para a conclus�o de cada reliase � de 15 a 30 dias dependendo dos requisitos selecionados. O tempo total de desenvolvimento deste projeto para a primeira etapa ser� de 90 dias, podendo estender a 120 dias.
Tempo de trabalho di�rio: 3h dia
Valor da hora trabalhada: R$ 45,84
Quantidade de dias para conclus�o da 1� Etapa : 90 dias
Total de horas: 90x3 = 270horas
6. Processo de mensalidade
Ap�s a entrega da primeira vers�o funcional em atividade, o processo de mensalidade ficar� fixada em um sal�rio e reajustado em base do �ndice de ajuste do m�nimo. O processo de mensalidade dar� direito de uso do sistema pelo cliente, o servi�o de backup em nuvem, al�m do suporte tecnico para bugs e ajustes nas funcionalidades j� implementadas no sistema por 30 dias, renovado mensalmente. Novas funcionalidades fica a avalia��o do processo de tempo de desenvolvimento, ficando esse valor fora do acobertado pela mensalidade.
7. Plano de implementa��o
O processo de implementa��o iniciar� ap�s o aceite deste contrato e dever� ser conclu�do em etapas, sendo a primeira etapa estipulada a sua conclus�o em 90 dias podendo ser estendido a mais 30 dias totalizando 120 dias.
8. Direitos intelectuais e de uso:
Os direitos intelectuais desta aplica��o s�o de propriedade de Laerton Marques de Figueiredo e fica a disponibilidade de uso para o cliente informado neste contrato, ficando os dados contidos neste sistema e contrato privado de sua divulga��o. Os dados contidos no banco de dados s�o de exclusividade do cliente de uso. A divulga��o dos dados deste banco fica proibido, sendo somente permitido por meio de autoriza��o do cliente.
9. Cronograma/agenda de alto n�vel
RequisitoJulhoAgostoSetembroOutubroRF1?RF2?RF3??RF4?RF5?RF62� Etapa2� Etapa2� Etapa2� EtapaRF7??RF8?RF92� Etapa2� Etapa2� Etapa2� EtapaRF10?APROVA��O E AUTORIDADE PARA PROCEDER
Os requisitos acimas elencados foram revisados e aprovados por 
NomeT�tuloDataFrancisca das Chagas dos SantosSecret�ria28/06/2018Aprovamos o projeto como descrito acima e autorizamos a equipe a prosseguir.

Aprovado porDataAprovado porData


