﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchequesWin.Forms
{
    public partial class FormModelo : Form
    {
        public FormModelo(Form mdiForm)
        {
            MdiParent = mdiForm;
            Inicializa();
            
        }

        public FormModelo()
        {
            Inicializa();
          
        }

        public void Inicializa()
        {
            InitializeComponent();
            picIcone.Image = Icon.ToBitmap();
            labTitulo.Text = Text;
            this.KeyPreview = true;
        }
        public FormModelo(string labTitulo)
        {
            InitializeComponent();
            this.labTitulo.Text = labTitulo;
            
        }

        public Panel PanelTop
        {
            get { return panelTop; }
            set { panelTop = value; }
        }

        public Label LabTitulo
        {
            get { return labTitulo; }
            set { labTitulo = value; }
        }

        public Panel PanelLeft
        {
            get { return panelLeft; }
            set { panelLeft = value; }
        }

      

        public PictureBox PicIcone
        {
            get { return picIcone; }
            set { picIcone = value; }
        }

        

        private void FormModelo_Resize(object sender, EventArgs e)
        {
            PanelTop.Width = this.Width;
            PanelLeft.Height = this.Height;
            panelContainer.Width = this.Width;
            panelContainer.Height = this.Height;
            panelBarra.Width = PanelTop.Width;
        }

        private void FormModelo_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);
                    break;
             
            }
        }

        private void FormModelo_Load(object sender, EventArgs e)
        {
            LabTitulo.Text = Text;
            PicIcone.Image = Icon.ToBitmap();
        }

    }
}
