﻿namespace SchequesWin.Forms
{
    partial class FormModelo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormModelo));
            this.panelLeft = new System.Windows.Forms.Panel();
            this.panelTop = new System.Windows.Forms.Panel();
            this.panelBarra = new System.Windows.Forms.Panel();
            this.labTitulo = new System.Windows.Forms.Label();
            this.panelContainer = new System.Windows.Forms.Panel();
            this.picIcone = new System.Windows.Forms.PictureBox();
            this.panelLeft.SuspendLayout();
            this.panelTop.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcone)).BeginInit();
            this.SuspendLayout();
            // 
            // panelLeft
            // 
            this.panelLeft.BackColor = System.Drawing.Color.CornflowerBlue;
            this.panelLeft.Controls.Add(this.picIcone);
            this.panelLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelLeft.Location = new System.Drawing.Point(0, 0);
            this.panelLeft.Name = "panelLeft";
            this.panelLeft.Size = new System.Drawing.Size(75, 451);
            this.panelLeft.TabIndex = 5;
            // 
            // panelTop
            // 
            this.panelTop.BackColor = System.Drawing.Color.MidnightBlue;
            this.panelTop.Controls.Add(this.panelBarra);
            this.panelTop.Controls.Add(this.labTitulo);
            this.panelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTop.Font = new System.Drawing.Font("Arial Rounded MT Bold", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelTop.Location = new System.Drawing.Point(75, 0);
            this.panelTop.Name = "panelTop";
            this.panelTop.Size = new System.Drawing.Size(864, 95);
            this.panelTop.TabIndex = 6;
            // 
            // panelBarra
            // 
            this.panelBarra.BackColor = System.Drawing.Color.RoyalBlue;
            this.panelBarra.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelBarra.Location = new System.Drawing.Point(0, 33);
            this.panelBarra.Name = "panelBarra";
            this.panelBarra.Size = new System.Drawing.Size(864, 62);
            this.panelBarra.TabIndex = 42;
            // 
            // labTitulo
            // 
            this.labTitulo.AutoSize = true;
            this.labTitulo.Font = new System.Drawing.Font("Arial Rounded MT Bold", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labTitulo.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labTitulo.Location = new System.Drawing.Point(5, 7);
            this.labTitulo.Name = "labTitulo";
            this.labTitulo.Size = new System.Drawing.Size(0, 22);
            this.labTitulo.TabIndex = 0;
            // 
            // panelContainer
            // 
            this.panelContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContainer.Font = new System.Drawing.Font("Arial Unicode MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelContainer.Location = new System.Drawing.Point(75, 95);
            this.panelContainer.Name = "panelContainer";
            this.panelContainer.Size = new System.Drawing.Size(864, 356);
            this.panelContainer.TabIndex = 7;
            // 
            // picIcone
            // 
            this.picIcone.BackColor = System.Drawing.Color.White;
            this.picIcone.Image = ((System.Drawing.Image)(resources.GetObject("picIcone.Image")));
            this.picIcone.Location = new System.Drawing.Point(22, 33);
            this.picIcone.Name = "picIcone";
            this.picIcone.Size = new System.Drawing.Size(32, 32);
            this.picIcone.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.picIcone.TabIndex = 2;
            this.picIcone.TabStop = false;
            // 
            // FormModelo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 451);
            this.Controls.Add(this.panelContainer);
            this.Controls.Add(this.panelTop);
            this.Controls.Add(this.panelLeft);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormModelo";
            this.Text = "Cadastro de usuário";
            this.Load += new System.EventHandler(this.FormModelo_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormModelo_KeyDown);
            this.Resize += new System.EventHandler(this.FormModelo_Resize);
            this.panelLeft.ResumeLayout(false);
            this.panelLeft.PerformLayout();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcone)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Panel panelLeft;
        public System.Windows.Forms.PictureBox picIcone;
        public System.Windows.Forms.Panel panelTop;
        public System.Windows.Forms.Panel panelBarra;
        public System.Windows.Forms.Label labTitulo;
        public System.Windows.Forms.Panel panelContainer;
    }
}