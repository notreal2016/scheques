﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheque.Uteis
{
    public class Juros
    {
        /// <summary>
        /// Método efetua calculo de juros simples com uso de taxa ao mês aplicada em tempo em dias
        /// </summary>
        /// <param name="principal">Valor de principal</param>
        /// <param name="taxa">Taxa a ser aplicada ao mês</param>
        /// <param name="dias">Quantidade de dias</param>
        /// <returns>Valor montante do Juros</returns>
        public static decimal JurosSimplesMesAppDia(decimal principal, decimal taxaMes, Int64 dias)
        {
            double p = (double)principal;
            double t = (double)taxaMes;
            var taxaDia = (Math.Pow((1 + t/100d), (1d / 30d)) - 1);
            var result = p * ( taxaDia)* dias;
            return (decimal) Math.Round(result, 2);
        }
        /// <summary>
        /// Método efetual calculo de Jusros compostos com uso de taxa ao mês aplicada em tempo em dias
        /// </summary>
        /// <param name="principal">Valor do principal para o calculo</param>
        /// <param name="taxaMes">Taxa de juros ao mês</param>
        /// <param name="dias">Número de dias a ser aplicada a taxa</param>
        /// <returns></returns>
        public static decimal JurosCompostoMesAppDia(decimal principal, decimal taxaMes, Int64 dias)
        {
            double p = (double)principal;
            double t = (double)taxaMes;
            //var taxaDia = (taxaMes * (1d / 30d));
            var taxaDia = (Math.Pow( (1 + t/100d) , (1d / 30d)) - 1);
            var result = (p * (Math.Pow((1 +(taxaDia)), dias)))-p;
            return (decimal) Math.Round(result,2);
        }

        public static decimal CalculaParcelaPrice(decimal pv, decimal juros, int n)
        {
            double j = (double)juros;
            double p = (double)pv;
            double dividendo = (p*(j/100))* Math.Pow((1 + (j/100)), n);
            double divisor = (Math.Pow((1 + (j / 100)), n)) - 1;
            double result = dividendo / divisor;
            return (decimal) Math.Round(result, 2);
        }

        /// <summary>
        /// Cria uma tabela de parcelas Price
        /// </summary>
        /// <param name="pv">Saldo devedor</param>
        /// <param name="juros">Taxa de juros ao mês</param>
        /// <param name="n">Número de parcelas</param>
        /// <returns>Lista de resultado com a tabela</returns>
        public static List<Decimal[]> CriaTabelaPrice(decimal pv, decimal juros, int n)
        {
            decimal parcela = CalculaParcelaPrice(pv, juros, n);
            decimal saldo = pv;
            List<Decimal[]> resul = new List<decimal[]>();
            for (int i = 0; i < n; i++)
            {
                decimal vJuros = saldo * (juros / 100);
                decimal vAmortiza = parcela - vJuros;
                saldo = Math.Round((saldo - vAmortiza), 2);
                resul.Add(new decimal[] {i + 1, Math.Round(parcela,2),Math.Round(vJuros,2), Math.Round(vAmortiza,2), Math.Round(saldo,2)});
            }

            if (saldo > 0)
            {
                resul[n - 1][1] += saldo; //Ajusta ultima parcela
                resul[n - 1][4] =0;//Ajusta saldo
                resul[n - 1][3] += saldo; //Ajusta ultima amortização
            }
            return resul;
        }
        public static List<Decimal[]> CriaTabelaSAC(decimal pv, decimal juros, int n)
        {
            decimal vAmortizado = Math.Round((pv/n),2);
            decimal ultimavAmortizado = Math.Round((pv - (vAmortizado * (n - 1))),2);

            decimal saldo = pv;
            List<Decimal[]> resul = new List<decimal[]>();
            for (int i = 0; i < n; i++)
            {
                decimal vJuros = saldo * (juros / 100);
                decimal vAmo = (i< n-1)?(vAmortizado): (ultimavAmortizado);
                saldo = Math.Round((saldo - vAmo), 2);
                decimal parcela = vAmo + vJuros;
                resul.Add(new Decimal[] {i + 1, Math.Round(parcela,2),Math.Round(vJuros,2), Math.Round(vAmo,2), Math.Round(saldo,2)});
            }

            if (saldo > 0)
            {
                resul[n - 1][1] += saldo; //Ajusta ultima parcela
                resul[n - 1][4] =0;//Ajusta saldo
                resul[n - 1][3] += saldo; //Ajusta ultima amortização
            }
            return resul;
        }
        
    }
}
