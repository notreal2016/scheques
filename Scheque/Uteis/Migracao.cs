﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Repository;
using Scheque.Service;
using Scheque.Uteis;
using Exception = System.Exception;
using System.Data.OleDb;
using System.Threading;

using MySql.Data.MySqlClient;
using Scheque.Interface;

namespace Scheque.Uteis
{
    public class Migracao
    {
        private static String seting = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\projetos\Base de dados de Cheques\dados1.mdb;";
        private readonly static IUnitOfWork unitOfWork = new UnitOfWork(new ScContext());
        public static long criaEmpresatimo(long idLote)
        {   
            using(IScContext context = new ScContext())
            {
                Lote lote = context.Lotes.Find(idLote); //lr.FindById(idLote);
                Emprestimo emprestimo = new Emprestimo()
                {
                    Adiantamento = false,
                    Quitado = false,
                    TaxaAdministrativa = 0,
                    TaxaJuros = lote.Taxa,
                    Intervalo = 30,
                    NumeroParcelas = lote.Quantidade,
                    Valor = lote.ValorLote,
                    DataContrato = lote.DataLote,
                    IdCliente = lote.IdCliente,
                    Fixo = true,
                    Sac = false,
                    Tipo = TipoEmprestimo.PROMISSORIA,
                    IdAntigo = lote.IdAntigo

                };
                context.Emprestimos.Add(emprestimo);
                context.Lotes.Remove(lote);
                context.SaveChanges(); 
                return emprestimo.Id;
            }
            
        }

        public static void varrerLote()
        {
            ILoteRepository ls = new LoteRepository(new ScContext(), unitOfWork);
            var lotes = ls.FindAlLotes().Where(l => l.IdAntigo >= 65262).ToList(); //.Where(c => c.IdAntigo == 68298)
            IList<Banco> bancos = unitOfWork.BancoRepository.AllBancos();
            List<Conta> cCriadas = new List<Conta>();
            List<Conta> contas = unitOfWork.ContaRepository.AllContas();
            lotes.ForEach(l => {
                cheques(l.IdAntigo, l.Id, bancos, cCriadas, contas);
            });
            Console.ReadLine();
        }

        public static void cheques(long idLote, long id, IList<Banco> bancos, IList<Conta> cCriadas, IList<Conta> contas )
        {
            String sql = "Select * from cheque where codlote = " + idLote + ";";
            String comando = "insert into documento (valor, vencimento, dias, novovencimento, status, idconta, numerocheque, juroscheque, idlote, Discriminator) values ";
            using(ScContext context = new ScContext())
            {
                using (OleDbConnection conex = new OleDbConnection(seting))
                {
                    conex.Open();
                    OleDbCommand command = new OleDbCommand(sql, conex);
                    OleDbDataReader dr = command.ExecuteReader();
                    
                    string banco = "", conta = "", agencia = "", numero = "", operacao = "", compensassao = "", valor = "";
                    bool emprestimo = false;
                    string juros = "";
                    int status = 0;
                    Console.WriteLine("Lote: " + idLote);
                    while (dr.Read())
                    {
                        String cmc7 = dr["codCheque"].ToString();
                        status = Convert.ToInt32(dr["situação"]);
                        valor = dr["valor1"].ToString();
                        if (decimal.Parse(valor) == 0)
                        {
                            return;
                        }

                        if (cmc7.Length < 30)//Processo de emprestimo
                        {
                            if (emprestimo == false)
                            {
                                id = criaEmpresatimo(id);
                                emprestimo = true;
                                comando = "insert into documento (valor, vencimento, dias, novovencimento, status, jurospromissoria, idEmprestimo ,Discriminator) values ";
                            }
                            juros = (string.Format("'{0:N2}'", dr["Juros"])).Replace(".", "").Replace(',', '.');
                            valor = dr["valor1"].ToString().Replace(".", "").Replace(',', '.');
                            comando += string.Format("({0}, '{1:yyyy/MM/dd}', 0, '{1:yyyy/MM/dd}', {2}, {3}, {4}, '{5}' ),",
                                valor,
                                Convert.ToDateTime(dr["DataVencimento"] != DBNull.Value ? dr["DataVencimento"] : "01/01/2000"),
                                (status == 0 ? 0 : 6),
                                juros,
                                id,
                                "Duplicata");
                        }
                        else
                        {
                            

                            banco = ValidaCmc7.ExtraiCodBanco(cmc7);
                            conta = ValidaCmc7.ExtraiNumContaCliente(cmc7);
                            agencia = ValidaCmc7.ExtraiCodAgencia(cmc7);
                            numero = ValidaCmc7.ExtraiNumDocumento(cmc7);
                            operacao = ValidaCmc7.ExtraiCodOperadorConta(cmc7);
                            compensassao = ValidaCmc7.ExtraiCamaraCompensacao(cmc7);
                            Conta contaV = new Conta()
                            {
                                Agencia = agencia,
                                IdBanco = banco,
                                ContaCorrente = conta,
                                Operacao = operacao,
                                Compensassao = compensassao
                            };
                            var nDigitos = bancos.Where(b => b.Id == banco).First().NumeroDigitosConta;
                            var result = contas.Where(c => c.ComparaConta(contaV, nDigitos)).ToList();
                            Conta cc = null;
                            //Conta já esta na base de dados antes da primeira passada
                            if (result.Count > 0)
                            {
                                cc = result[0];
                            }
                            //Conta esta na base de dados mas a lista local não esta atualizada
                            if (cc == null)
                            {
                                var r = cCriadas.Where(c1 => c1.ComparaConta(contaV, nDigitos));
                                cc = r.Count() > 0 ? r.First() : null;
                            }
                            //Conta nova
                            if (cc == null)
                            {
                                cc = new Conta
                                {
                                    Agencia = agencia,
                                    ContaCorrente = conta,
                                    IdBanco = banco,
                                    Operacao = operacao,
                                    DaEmpresa = false,
                                    Compensassao = compensassao
                                };
                                Console.Write("Criado conta" + conta);
                                //cr.Save(cc);
                                context.Contas.Add(cc);
                                context.SaveChanges();
                                cCriadas.Add(cc);
                            }
                            long idConta = cc.Id;
                            juros = (string.Format("{0:N2}", dr["Juros"])).Replace(".", "").Replace(',', '.');
                            valor = dr["valor1"].ToString().Replace(".", "").Replace(',', '.');
                            comando += string.Format("('{0}', '{1:yyyy/MM/dd}', 0, '{1:yyyy/MM/dd}', {2}, {3}, '{4}', {5}, {6}, 'Cheque'),",
                                valor, Convert.ToDateTime(dr["DataVencimento"] != DBNull.Value ? dr["DataVencimento"] : "01/01/2000"), (status == 0 ? 0 : 5), idConta, numero, juros, id);
                        }
                        Console.Write("*");
                    }
                    persiste(comando);

                }
            }
            
            
           

        }

        public static void lotes()
        {
            String SQL = "SELECT Lotes.Numerolote, Lotes.Codcliente, Lotes.Quantidade, Lotes.ValorTotal, Lotes.Automático, Lotes.Valorpago, Lotes.data, Lotes.Juros, Lotes.Tipo, Lotes.CPMF, Lotes.ante, Lotes.alterado, Lotes.duplica, Lotes.adm, Last(Lotes.Numerolote) AS ÚltimoDeNumerolote FROM Lotes INNER JOIN cheque ON Lotes.Numerolote = cheque.Codlote GROUP BY cheque.situação, Lotes.Numerolote, Lotes.Codcliente, Lotes.Quantidade, Lotes.ValorTotal, Lotes.Automático, Lotes.Valorpago, Lotes.data, Lotes.Juros, Lotes.Tipo, Lotes.CPMF, Lotes.ante, Lotes.alterado, Lotes.duplica, Lotes.adm HAVING(((cheque.situação) = 0)) order by lotes.numerolote;";
            String comando = "Insert into Lote (idcliente, Quantidade, ValorLote, Taxa, Administrativa, DataLote, valorDescontado, idAntigo, id) values ";
            using (OleDbConnection conex = new OleDbConnection(seting))
            {
                conex.Open();
                OleDbCommand command = new OleDbCommand(SQL, conex);
                OleDbDataReader dr = command.ExecuteReader();
                long id = 1;
                while (dr.Read())
                {
                    string pago = string.Format("{0:N2}", dr["valorpago"]).Replace(".", "").Replace(',', '.');
                    comando += string.Format("({0}, {1}, {2}, {3}, 0, '{4:yyyy/MM/dd}', '{5:N2}', {6}, {7}),",
                        Convert.ToInt64(dr["codCLiente"]),
                        Convert.ToInt32(dr["Quantidade"]),
                        dr["ValorTotal"].ToString().Replace(',', '.'),
                        dr["Juros"].ToString().Replace(',', '.'),
                        Convert.ToDateTime(dr["data"]),
                        pago,
                        Convert.ToInt64(dr["numerolote"]),
                        id);
                    Console.Write("*" + Convert.ToInt64(dr["numeroLote"]) + "*");
                    id++;
                }
                persiste(comando);
                Console.ReadLine();
            }
        }

        public static void clientes(String sql)
        {
            using (OleDbConnection conex = new OleDbConnection(seting))
            {
                String comando = "Insert into cliente(id , nome , numero, taxa, Administrativa, acumulado, DiasMinimo) values ";
                conex.Open();
                OleDbCommand command = new OleDbCommand(sql, conex);
                OleDbDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    comando += string.Format("({0},'{1}',{2},{3},{4},{5},{6}),", Convert.ToInt64(dr["codCliente"]), dr["nome"], 0, 3, 0, true, 20);
                    Console.Write("*");
                }
                persiste(comando);
                Console.ReadLine();
            }
        }
        public static void negativacao(String sql , int informe = 0)
        {
            using (OleDbConnection conex = new OleDbConnection(seting))
            {
                using(IScContext context = new ScContext())
                {
                    conex.Open();
                    if (informe > 0)
                    {
                        OleDbCommand c2 = new OleDbCommand("SELECT max(código) as t from situação;", conex);
                        var dr1 = c2.ExecuteReader();
                        dr1.Read();
                        long total = long.Parse(dr1["t"].ToString());
                        dr1.Close();
                        long faixa1 = total / 2;

                        if (informe == 1)
                        {
                            sql += " where código <= " + faixa1;
                        }
                        else
                        {
                            sql += " where código > " + faixa1;
                        }
                    }
                    String comando = "Insert into negativado (idConta,idMotivo, Data, Valor) Values ";
                    OleDbCommand command = new OleDbCommand(sql, conex);
                    OleDbDataReader dr = command.ExecuteReader();
                    
                    IList<Banco> bancos = context.Bancos.ToList();
                    string agencia="", banco="", opercao="", cc="", numero="";
                    long i = 0;
                    IList<Conta> contas = context.Contas.ToList();
                   
                    while (dr.Read())
                    {
                        String cmc = dr["cod"].ToString();
                        agencia = ValidaCmc7.ExtraiCodAgencia(cmc);
                        banco = ValidaCmc7.ExtraiCodBanco(cmc);
                        opercao = ValidaCmc7.ExtraiCodOperadorConta(cmc);
                        cc = ValidaCmc7.ExtraiNumContaCliente(cmc);
                        Banco bc = bancos.Where(b => b.Id.Equals(banco)).First();
                        Conta cv = new Conta()
                        {
                            Agencia = agencia,
                            ContaCorrente = cc,
                            IdBanco = banco,
                            Operacao = opercao
                        };
                        
                        IList<Conta> r = contas.Where(c1 => cv.ComparaConta(c1, bc.NumeroDigitosConta)).ToList();
                        Conta conta = r.Count > 0 ? r[0] : null; 
                        if (conta != null)
                        {
                            //Valor preenchido com Zero por obrigação já que a nova base exige valor
                            comando += string.Format("( {0}, {1}, '{2:yyyy/MM/dd}', {3} ),", conta.Id, 12, Convert.ToDateTime(dr["data"]), 0);
                            Console.Write(i++ + " ");
                        }
                        Console.Write("*");
                    }
                    persiste(comando);
                    Console.ReadLine();
                }
               
            }
        }
        public static void processaContas(string sql)
        {
            using (OleDbConnection conex = new OleDbConnection(seting))
            {
                String comando = "Insert into conta (idbanco, agencia, contacorrente, operacao, daempresa) values ";
                conex.Open();
                OleDbCommand command = new OleDbCommand(sql, conex);
                OleDbDataReader dr = command.ExecuteReader();
                
                IContaRepository cr = unitOfWork.ContaRepository;
                IBancoRepository br = unitOfWork.BancoRepository;
                var bancos = br.AllBancos();
                //string agencia, banco, opercao, cc, numero;
                List<Conta> contas = new List<Conta>();
                string cc, agencia, banco, operacao, compensassao;
                
                long i = 0;
                while (dr.Read())
                {
                    String cmc = dr["cod"].ToString();
                    //Extrai os dados do cmc7 do cheque
                    agencia = ValidaCmc7.ExtraiCodAgencia(cmc);
                    banco = ValidaCmc7.ExtraiCodBanco(cmc);
                    operacao = ValidaCmc7.ExtraiCodOperadorConta(cmc);
                    cc = ValidaCmc7.ExtraiNumContaCliente(cmc);
                    compensassao = ValidaCmc7.ExtraiCamaraCompensacao(cmc);
                   //Valida se a conta já existe
                    Conta conta = new Conta()
                    {
                        Agencia = ValidaCmc7.ExtraiCodAgencia(cmc),
                        IdBanco = ValidaCmc7.ExtraiCodBanco(cmc),
                        Operacao = ValidaCmc7.ExtraiCodOperadorConta(cmc),
                        ContaCorrente = ValidaCmc7.ExtraiNumContaCliente(cmc),
                        Compensassao = ValidaCmc7.ExtraiCamaraCompensacao(cmc)
                    };

                    Banco ban = bancos.Where(b => b.Id.Equals(conta.IdBanco)).Count() > 0 ? bancos.Where(b => b.Id.Equals(conta.IdBanco)).First() : null;
                    
                    if (ban != null)
                    {
                        Console.Write("*");
                        Boolean isCadastro = contas.Where(c => c.ComparaConta(conta, ban.NumeroDigitosConta)).Count() > 0;
                        if (!isCadastro)
                        {
                            comando += string.Format("('{0}', '{1}', '{2}', '{3}', {4}),", conta.IdBanco, conta.Agencia, conta.ContaCorrente, conta.Operacao, false);
                            Console.WriteLine(i++);
                            contas.Add(conta);
                        }
                        else
                        {
                            Console.WriteLine(string.Format("A conta {1} da Agencia {2} OP {4} Já cadastrada", conta.IdBanco, conta.ContaCorrente, conta.Agencia, "", conta.Operacao));
                        }
                    }
                    else
                    {
                        Console.WriteLine(string.Format("Banco {0} não foi localizado e a conta {1} da Agencia {2} com ch {3} e OP {4} não foi incluso", conta.IdBanco, conta.ContaCorrente, conta.Agencia, "", conta.Operacao));
                            
                    }
                }
                persiste(comando);
            }

            Console.ReadLine();
        }
        public static void persiste(string comando)
        {
            IScContext context = new ScContext();
            comando = comando.Substring(0, comando.Length - 1);
            using(MySqlConnection conecction = new MySqlConnection(context.ConnectionSeting))
            {
                conecction.Open();
                MySqlTransaction transacao = conecction.BeginTransaction();
                MySqlCommand mycomand = new MySqlCommand(comando, conecction, transacao);
                try
                {
                    mycomand.ExecuteNonQuery();
                    transacao.Commit();
                    Console.WriteLine("Gravado todos os registros");
                }
                catch (Exception e)
                {
                    transacao.Rollback();
                    Console.WriteLine(e.Message);
                }
            }
        }
    }
    
}
