﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Service;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;
using Scheque.Repository;
using Scheque.Uteis;

namespace Scheque.Service
{
    public class EmprestimoService : IEmprestimoService, IDisposable
    {
        private IEmprestimoRepository _empRepository;
        private IClienteRepository _clienteRepository;
        private IChequeRepository _chequeRepository;
        private IBancoRepository _bancoRepository;
        private IContaRepository _contaRepository;
        private IProprietarioRepoitory _proprietarioRepoitory;
        private readonly IUnitOfWork _unitOfWork;
        public EmprestimoService(IUnitOfWork unitOfWork, IEmprestimoRepository er, IChequeRepository cr,
            IClienteRepository clr, IBancoRepository br, IContaRepository Iccr, IProprietarioRepoitory pr)
        {
            _unitOfWork = unitOfWork;
            _empRepository = er;
            _chequeRepository = cr;
            _clienteRepository = clr;
            _bancoRepository = br;
            _contaRepository = Iccr;
            _proprietarioRepoitory = pr;
        }
        /// <summary>
        /// Lista todos os clientes cadastrado em ordem alfabetica
        /// </summary>
        /// <returns></returns>
        public List<Cliente> AllClientes()
        {
            return _clienteRepository.FindAllClientes().ToList();
        }
      
        /// <summary>
        /// Salva os dados no banco de dados
        /// </summary>
        /// <param name="emprestimo"></param>
        public void Save(Emprestimo emprestimo)
        {
            _empRepository.Save(emprestimo);
        }

        /// <summary>
        /// Cria tabla de parcelas a partir do emprestimo repassado. Tabela pode ser do tipo Price ou SAC, sendo price como padrão
        /// </summary>
        /// <param name="emp"> Emprestimo</param>
        /// <param name="sac">Tabela SAC = padão false</param>
        /// <returns></returns>
        public List<AbstracDocumento> CriaPromissorias(Emprestimo emp, bool sac = false, bool diaFixo = true)
        {
            List<AbstracDocumento> resulta = new List<AbstracDocumento>();
            var tabela = (sac)? Juros.CriaTabelaSAC(emp.Valor, emp.TaxaJuros, emp.NumeroParcelas): Juros.CriaTabelaPrice(emp.Valor, emp.TaxaJuros, emp.NumeroParcelas);
            
            int i = 1;
            foreach (var parcela in tabela)
            {
                TimeSpan diasTime = DateTime.Now.AddMonths(i) - DateTime.Now;
                resulta.Add(new Promissoria()
                {
                    Vencimento = (diaFixo)? DateTime.Now.AddMonths(i): DateTime.Now.AddDays((30*i)),
                    Dias = (diaFixo)? diasTime.Days:(30*i),
                    Valor = parcela[1],
                    JurosPromissoria = parcela[2],
                
                });
                i++;
            }
            return resulta;

        }
        /// <summary>
        /// Cria a lista de cheques por meio da tabela SAC ou PRICE
        /// </summary>
        /// <param name="emp">Emprestimo que gerará as parcelas</param>
        /// <param name="sac">Define se aplica a tabela SAC, como padrão false</param>
        /// <param name="diaFixo">Se será criado com dia fixo ou intervalo de dias</param>
        /// <returns></returns>
        public List<AbstracDocumento> CriaCheque(Emprestimo emp, bool sac = false, bool diaFixo = true)
        {
            List<AbstracDocumento> resulta = new List<AbstracDocumento>();
            var tabela = (sac)? Juros.CriaTabelaSAC(emp.Valor, emp.TaxaJuros, emp.NumeroParcelas): Juros.CriaTabelaPrice(emp.Valor, emp.TaxaJuros, emp.NumeroParcelas);
            
            int i = 1;
            foreach (var parcela in tabela)
            {
                TimeSpan diasTime = DateTime.Now.AddMonths(i) - DateTime.Now;
                resulta.Add(new Cheque()
                {
                    Vencimento = (diaFixo)? DateTime.Now.AddMonths(i): DateTime.Now.AddDays((30*i)),
                    Dias = (diaFixo)? diasTime.Days:(30*i),
                    Valor = parcela[1],
                    JurosCheque = parcela[2],
                
                });
                i++;
            }
            return resulta;

        }
        /// <summary>
        /// Cria uma promissor no processo de antecipação
        /// </summary>
        /// <param name="valor">Valor da parcela</param>
        /// <returns>Promissora criada</returns>
        public Promissoria CriaPromissoriaAntecipacao(decimal valor)
        {
            if (valor <= 0)
            {
                throw new Exception("Valor para a parcela deve ser positívo e maior que zero.");
            }

            return  new Promissoria()
            {
                Valor = valor,
                Status = StatusDocumento.DIGITADO,
                
            };
        }

        /// <summary>
        /// Atualizar os valores de uma parcela de um emprestimo do tipo antecipado.
        /// </summary>
        /// <param name="pro">Promissoria a ser atualizada</param>
        /// <param name="vencimento">Vencimento</param>
        /// <returns></returns>
        public Promissoria AtualizaPromissoraAntecipacao(Promissoria pro, DateTime vencimento)
        {
            DateTime data = pro.Emprestimo.DataContrato;
            decimal txJuros = pro.Emprestimo.TaxaJuros;
            TimeSpan t = vencimento - pro.Emprestimo.DataContrato;
            Decimal juros = (pro.Emprestimo.Cliente.Acumulado)
                ? Juros.JurosCompostoMesAppDia(pro.Valor, txJuros, t.Days): Juros.JurosSimplesMesAppDia(pro.Valor,txJuros,t.Days);
            pro.JurosPromissoria = Math.Round(juros, 2);
            pro.Vencimento = vencimento;
            pro.Dias = t.Days;
            return pro;
        }
        /// <summary>
        /// Retorna um emprestimo por meio do id repassado.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Emprestimo FindById(Int64 id)
        {
            return _empRepository.FindById(id);
        }

        /// <summary>
        /// Busca por clientes que contenha o nome repassado no nome
        /// </summary>
        /// <param name="nome">Termo de busca</param>
        /// <returns>Lista de clientes</returns>
        public List<Cliente> FindClienteByNome(string nome)
        {
            if (nome.Trim().Equals(""))
            {
                throw new Exception("Nome do cliente não foi informado.");
            }

            if (ValidaDados.ExistCaracterEspeciais(nome))
            {
                throw new Exception("Nome do cliente não pode conter caracteres especiais.");
            }
            return _clienteRepository.FindByName(nome).ToList();
        }
        /// <summary>
        /// Busca por um banco apartir do cod dele repassado como paramentro
        /// </summary>
        /// <param name="codBanco">Codigo do banco</param>
        /// <returns>Banco localizado</returns>
        public Banco FindBancoByCodBanco(string codBanco)
        {
            return _bancoRepository.FindByCodBanco(codBanco);

        }
        /// <summary>
        /// Busca por uma conta baseado nos dados repassados como parametro
        /// </summary>
        /// <param name="idBanco">Id do Banco</param>
        /// <param name="agcencia">Agencia</param>
        /// <param name="conta">Conta</param>
        /// <returns>Conta localizado ou null quando não localizado</returns>
        public Conta FindContaByDados(string idBanco, string agcencia, string conta, string operacao, int numeroDigitosCC = 6)
        {
            return _contaRepository.FindContaByDados(idBanco, agcencia, conta, operacao, numeroDigitosCC);
        }
        /// <summary>
        /// Metodo busca por um emitente/proprietario apartir do documento repassado
        /// </summary>
        /// <param name="doc">Documento para busca</param>
        /// <returns>Emitente/Proprietário da do documento</returns>
        public Proprietario FindProprietarioByDocumentos(String doc)
        {
            return _proprietarioRepoitory.FindByDocumento(doc);
        }
        /// <summary>
        /// Método busca por todos os bancos cadastrados no bando de dados
        /// </summary>
        /// <returns>Lista de bancos</returns>
        public List<Banco> FindAllBancos()
        {
            return _bancoRepository.AllBancos().ToList();
        }
        /// <summary>
        /// Salva os dados de uma conta no banco de dados
        /// </summary>
        /// <param name="conta">Dados da conta</param>
        public void SaveConta(Conta conta)
        {
            _contaRepository.Save(conta);
        }
        /// <summary>
        /// Localiza todo os emprestimos de um cliente pelo nome repassado
        /// </summary>
        /// <param name="termo">Nome do cliente que será buscado emprestimos</param>
        /// <returns>Lista de emprestimos</returns>
        public List<Emprestimo> FindEmpByNomeCliente(string termo)
        {
            return _empRepository.FindByNomeCliente(termo);
        }

        /// <summary>
        /// Remove o emprestimo do banco de dados
        /// </summary>
        /// <param name="emprestimo">Empresimo a ser removido</param>
        public void Remove(Emprestimo emprestimo)
        {
            _empRepository.Remove(emprestimo.Id);
        }
        /// <summary>
        /// Montas todas as promissorias de um emprestimo
        /// </summary>
        /// <param name="id">Id do emprestimo</param>
        /// <returns></returns>
        public Dictionary<String, object> DadosPromissorias(Int64 id)
        {
            var promissorias = _empRepository.FindById(id).Documentos;
            Dictionary<String , object> retorno = new Dictionary<string, object>()
            {
                {"promissorias", promissorias }
            };
            return retorno;
        }

        #region IDisposable Support
        private bool disposedValue = false; // Para detectar chamadas redundantes

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _unitOfWork.Dispose();
                }

                // TODO: liberar recursos não gerenciados (objetos não gerenciados) e substituir um finalizador abaixo.
                // TODO: definir campos grandes como nulos.

                disposedValue = true;
            }
        }

        // TODO: substituir um finalizador somente se Dispose(bool disposing) acima tiver o código para liberar recursos não gerenciados.
         ~EmprestimoService()
         {
           // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
           Dispose(false);
         }

        // Código adicionado para implementar corretamente o padrão descartável.
        void IDisposable.Dispose()
        {
            // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
            Dispose(true);
            // TODO: remover marca de comentário da linha a seguir se o finalizador for substituído acima.
            // GC.SuppressFinalize(this);
        }
        #endregion


    }
}
