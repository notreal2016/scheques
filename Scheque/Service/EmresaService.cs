﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Repository;
using Scheque.Entidade;

namespace Scheque.Service
{
    /// <summary>
    /// Classe de serviços para os objetetos do tipo empresa
    /// </summary>
    public class EmresaService: IDisposable
    {
        private EmpresaRepository _er;

        public EmresaService(EmpresaRepository er)
        {
            _er = er;
        }
        /// <summary>
        /// Persiste os dados de uma empresa
        /// </summary>
        /// <param name="empresa">Empresa cujos dados serão persistidos</param>
        /// <returns>Empresa após a persistencia dos dados</returns>
        public Empresa Save(Empresa empresa)
        {
            return _er.Save(empresa);
        }
        /// <summary>
        /// Busca por uma empersa pelo id repassado
        /// </summary>
        /// <param name="id">Id da empresa a ser localizada</param>
        /// <returns>Empresa localizada ou null</returns>
        public Empresa FindByID(long id)
        {
            return _er.FindById(id);
        }

        #region IDisposable Support
        private bool disposedValue = false; // Para detectar chamadas redundantes

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _er.Dispose();
                }

                // TODO: liberar recursos não gerenciados (objetos não gerenciados) e substituir um finalizador abaixo.
                // TODO: definir campos grandes como nulos.

                disposedValue = true;
            }
        }

        // TODO: substituir um finalizador somente se Dispose(bool disposing) acima tiver o código para liberar recursos não gerenciados.
        // ~EmresaService()
        // {
        //   // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
        //   Dispose(false);
        // }

        // Código adicionado para implementar corretamente o padrão descartável.
        void IDisposable.Dispose()
        {
            // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
            Dispose(true);
            // TODO: remover marca de comentário da linha a seguir se o finalizador for substituído acima.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
