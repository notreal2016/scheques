﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Service;
using Scheque.Entidade;

namespace Scheque.Service
{
    public class ValidaConta
    {
        /// <summary>
        /// Valida os dados de uma conta.
        /// </summary>
        /// <param name="conta">Conta cujo dados serão validados</param>
        public static void ValidarConta(Conta conta)
        {
            if (!ValidaDados.SoNumeros(conta.Agencia))
            {
                throw new ContaException("Os dados de uma agência deve ter somente números.");
            }

            if (!ValidaDados.SoNumeros(conta.ContaCorrente))
            {
                throw new ContaException("Os dados de uma Conta Corrente deve ter somente números.");
            }

            if (!ValidaDados.SoNumeros(conta.Proprietario.CpfCNPJ))
            {
                throw new ContaException("Os dados de uma CPF ou CNPJ do emitente deve ter somente números.");
            }

            if (conta.Proprietario.CpfCNPJ.Trim().Equals(""))
            {
                throw new ContaException("O CNPJ ou CPF do emitente da conta é obrigatório");
            }

            if (ValidaDados.ExistCaracterEspeciais(conta.Proprietario.Nome))
            {
                throw new ContaException("O nome do emitente do cheque não pode conter caracteres especiais.");
            }
        }
    }
}
