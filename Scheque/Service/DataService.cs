﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheque.Service
{
    class DataService
    {
        /// <summary>
        /// Metodo para converter a data do padrão normal para data por extenso
        /// </summary>
        /// <param name="data">DateTime contendo data para ser convertida</param>
        /// <returns>String contendo data por extenso</returns>
        public static String ConverteData(DateTime data)
        {
            CultureInfo culture = new CultureInfo("pt-br");
            DateTimeFormatInfo dtfi = culture.DateTimeFormat;


            int _dia = data.Day;
            int _ano = data.Year;
            String _mes = culture.TextInfo.ToTitleCase(dtfi.GetMonthName(data.Month));
            String _diaString;
                
            return (_dia + " de " + _mes + " de " + _ano);

            switch (_dia)
            {
                case 1:
                    _diaString = "Primeiro";
                    break;

                case 2:
                    _diaString = "Dois";
                    break;

                case 3:
                    _diaString = "Três";
                    break;

                case 4:
                    _diaString = "Quatro";
                    break;

                case 5:
                    _diaString = "Cinco";
                    break;

                case 6:
                    _diaString = "Seis";
                    break;

                case 7:
                    _diaString = "Sete";
                    break;

                case 8:
                    _diaString = "Oito";
                    break;

                case 9:
                    _diaString = "Nove";
                    break;

                case 10:
                    _diaString = "Dez";
                    break;

                case 11:
                    _diaString = "Onze";
                    break;

                case 12:
                    _diaString = "Doze";
                    break;

                case 13:
                    _diaString = "Treze";
                    break;

                case 14:
                    _diaString = "Quatorze";
                    break;

                case 15:
                    _diaString = "Quinze";
                    break;

                case 16:
                    _diaString = "Dezesseis";
                    break;

                case 17:
                    _diaString = "Dezessete";
                    break;

                case 18:
                    _diaString = "Dezoito";
                    break;

                case 19:
                    _diaString = "Dezenove";
                    break;

                case 20:
                    _diaString = "Vinte";
                    break;

                case 21:
                    _diaString = "Vinte e um";
                    break;

                case 22:
                    _diaString = "Vinte e dois";
                    break;

                case 23:
                    _diaString = "Vinte e três";
                    break;

                case 24:
                    _diaString = "Vinte e quatro";
                    break;

                case 25:
                    _diaString = "Vinte e cinco";
                    break;

                case 26:
                    _diaString = "Vinte e seis";
                    break;

                case 27:
                    _diaString = "Vinte e sete";
                    break;

                case 28:
                    _diaString = "Vinte e oito";
                    break;

                case 29:
                    _diaString = "Vinte e nove";
                    break;

                case 30:
                    _diaString = "Trinta";
                    break;

                case 31:
                    _diaString = "Trinta e um";
                    break;
            }
            return (_diaString + " de " + _mes + " de " + _ano);
        }
    }
}
