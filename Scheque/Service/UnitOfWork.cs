﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;
using Scheque.Repository;

namespace Scheque.Service
{
    public class UnitOfWork : IUnitOfWork
    {
        private IClienteService _clienteService ;
        private ILoteService _loteService;
        private IUsuarioService _usuarioService ;
        private IBancoService _bancoService;
        private IUsuarioRepository _usuarioRepository;
        private IBancoRepository _bancoRepository ;
        private ICalendarioRepository _calendarioRepository ;
        private IChequeRepository _chequeRepository ;
        private IClienteRepository _clienteRepository ;
        private IContaRepository _contaRepository ;
        private IEmpresaRepository _empresaRepository ;
        private IEmprestimoRepository _emprestimoRepository;
        private ILoteRepository _loteRepository ;
        private IPromissoriaRepository _promissoriaRepository ;
        private IProprietarioRepoitory _proprietarioRepoitory ;
        private IRepositoryMovimentoDoc _repositoryMovimentoDoc ;
        private IRepositoryNegativado _repositoryNegativado ;
        private IEmpresaService _empresaService ;
        private IEmprestimoService _emprestimoService ;
        private IProprietarioService _proprietarioService;
        private IScContext _context;

        private Dictionary<IPersistenceBase, IUnitOfWorkRepository> _entidadesAdicionadas;
        private Dictionary<IPersistenceBase, IUnitOfWorkRepository> _entidadesAlteradas;
        private Dictionary<IPersistenceBase, IUnitOfWorkRepository> _entidadesRemovidas;


        public UnitOfWork(IScContext context)
        {
            this._context = context;
            
            _entidadesAdicionadas = new Dictionary<IPersistenceBase, IUnitOfWorkRepository>();
            _entidadesAlteradas = new Dictionary<IPersistenceBase, IUnitOfWorkRepository>();
            _entidadesRemovidas = new Dictionary<IPersistenceBase, IUnitOfWorkRepository>();
            _usuarioRepository = new UsuarioRepository(new ScContext(), this);
            _usuarioService = new UsuarioService(this, UsuarioRepository);
            _bancoRepository = new BancoRepository(new ScContext(), this);
            _calendarioRepository = new CalendarioRepository(new ScContext(), this);
            _chequeRepository = new ChequeRepository(new ScContext(), this);
            _contaRepository = new ContaRepository(new ScContext(), this);
            _clienteRepository = new ClienteRepository(new ScContext(), this);
            _empresaRepository = new EmpresaRepository(new ScContext(), this);
            _emprestimoRepository = new EmprestimoRepository(new ScContext(), this);
            _loteRepository = new LoteRepository(new ScContext(), this);
            _promissoriaRepository = new PromissoriaRepository(new ScContext(), this);
            _proprietarioRepoitory = new ProprietarioRepoitory(new ScContext(), this);
            _repositoryMovimentoDoc = new RepositoryMovimentoDoc(new ScContext(), this);
            _repositoryNegativado = new RepositoryNegativado(new ScContext(), this);
            _bancoService = new BancoService(_bancoRepository);
            _loteService = new LoteService(this, LoteRepository, ClienteRepository, BancoRepository, ContaRepository, CalendarioRepository, ChequeRepository, ProprietarioRepoitory,
                RepositoryMovimentoDoc, RepositoryNegativado);
            _empresaService = new EmpresaService(_empresaRepository);
            _emprestimoService = new EmprestimoService(this, EmprestimoRepository, ChequeRepository, ClienteRepository,
                BancoRepository, ContaRepository, ProprietarioRepoitory);
            _proprietarioService = new ProprietarioService(this,ProprietarioRepoitory);
            _clienteService = new ClienteService(this, ClienteRepository);
        }



        public IBancoRepository BancoRepository => _bancoRepository;
        public ICalendarioRepository CalendarioRepository => _calendarioRepository;
        public IChequeRepository ChequeRepository => _chequeRepository;
        public IClienteRepository ClienteRepository => _clienteRepository;
        public IContaRepository ContaRepository => _contaRepository;
        public IEmpresaRepository EmpresaRepository => _empresaRepository;
        public IEmprestimoRepository EmprestimoRepository => _emprestimoRepository;
        public ILoteRepository LoteRepository => _loteRepository;
        public IPromissoriaRepository PromissoriaRepository => _promissoriaRepository;
        public IProprietarioRepoitory ProprietarioRepoitory => _proprietarioRepoitory;
        public IRepositoryMovimentoDoc RepositoryMovimentoDoc => _repositoryMovimentoDoc;
        public IRepositoryNegativado RepositoryNegativado => _repositoryNegativado;
        public IUsuarioService UsuarioService => _usuarioService;
        public IUsuarioRepository UsuarioRepository => _usuarioRepository;
        public IBancoService BancoService => _bancoService;
        public ILoteService LoteService => _loteService;
        public IEmpresaService EmpresaService => _empresaService;
        public IEmprestimoService EmprestimoService => _emprestimoService;
        public IProprietarioService ProprietarioService => _proprietarioService;
        public IClienteService ClienteService => _clienteService;

        public void RegistraAlterado(IPersistenceBase entidade, IUnitOfWorkRepository respository)
        {
            if (!_entidadesAlteradas.ContainsKey(entidade))
            {
                _entidadesAlteradas.Add(entidade, respository);
            }
        }

        public void RegistraNovo(IPersistenceBase entidade, IUnitOfWorkRepository respository)
        {
            if (!_entidadesAdicionadas.ContainsKey(entidade))
            {
                _entidadesAdicionadas.Add(entidade, respository);
            }
        }

        public void RegistraRemovido(IPersistenceBase entidade, IUnitOfWorkRepository respository)
        {
            if (!_entidadesRemovidas.ContainsKey(entidade))
            {
                _entidadesRemovidas.Add(entidade, respository);
            }
        }

        public void Commit()
        {
            using (TransactionScope escopo = new TransactionScope())
            {
                foreach (IPersistenceBase entidade in this._entidadesAdicionadas.Keys)
                {
                    this._entidadesAdicionadas[entidade].InsertPersiste(entidade);
                }
                foreach (IPersistenceBase entidade in this._entidadesAlteradas.Keys)
                {
                    this._entidadesAlteradas[entidade].UpdatePersiste(entidade);
                }
                foreach (IPersistenceBase entidade in this._entidadesRemovidas.Keys)
                {
                    this._entidadesRemovidas[entidade].DeletePersiste(entidade);
                }
                escopo.Complete();
                this._entidadesAdicionadas = new Dictionary<IPersistenceBase, IUnitOfWorkRepository>();
                this._entidadesAlteradas = new Dictionary<IPersistenceBase, IUnitOfWorkRepository>();
                this._entidadesRemovidas = new Dictionary<IPersistenceBase, IUnitOfWorkRepository>();
            }
        }

        

        #region IDisposable Support
        private bool disposedValue = false; // Para detectar chamadas redundantes

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    /*_bancoRepository.Dispose();
                    _calendarioRepository.Dispose();
                    _chequeRepository.Dispose();
                    _clienteRepository.Dispose();
                    _contaRepository.Dispose();
                    _empresaRepository.Dispose();
                    _emprestimoRepository.Dispose();
                    _loteRepository.Dispose();
                    _promissoriaRepository.Dispose();
                    _repositoryMovimentoDoc.Dispose();
                    _repositoryNegativado.Dispose();
                    _usuarioRepository.Dispose();*/
                    _context.Dispose();

                }

                // TODO: liberar recursos não gerenciados (objetos não gerenciados) e substituir um finalizador abaixo.
                // TODO: definir campos grandes como nulos.

                disposedValue = true;
            }
        }

        // TODO: substituir um finalizador somente se Dispose(bool disposing) acima tiver o código para liberar recursos não gerenciados.
         ~UnitOfWork()
         {
           // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
           Dispose(false);
         }

        // Código adicionado para implementar corretamente o padrão descartável.
        public void Dispose()
        {
            // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
            Dispose(true);
            // TODO: remover marca de comentário da linha a seguir se o finalizador for substituído acima.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
