﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Service;
using Scheque.Entidade;
using Scheque.Repository;

namespace Scheque.Service
{
    public class ValidaUsuario
    {
        public static void ValidaUsuairo(Usuario user)
        {
            ValidaNome(user.Nome);
            ValidaLogin(user.Login);
        }

        private static void ValidaNome(String nome)
        {
            if (ValidaDados.ExistCaracterEspeciais(nome) || ValidaDados.ContemNumeros(nome) ||
                nome.Trim().Equals(""))
            {
                throw new UsuarioException("Nome de usuário não só pode conter letras.");
            }
        }
        
        private static void ValidaLogin(string login)
        {
            if (ValidaDados.ExistCaracterEspeciais(login))
            {
                throw new UsuarioException("Login do usuário não só pode conter caracteres especiais.");
            }
           
        }
    }
}
