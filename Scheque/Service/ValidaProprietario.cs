﻿using ADVCore.Service;
using Scheque.Entidade;
using Scheque.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheque.Service
{
    class ValidaProprietario
    {
        public static void ValidarProprietario(Proprietario proprietario)
        {
            ValidaNome(proprietario.Nome);
            ValidaDocumento(proprietario.CpfCNPJ);
        }

        private static void ValidaNome(String nome)
        {
            if (ValidaDados.ExistCaracterEspeciais(nome) || ValidaDados.ContemNumeros(nome))
            {
                throw new ProprietarioException("Nome só pode conter letras.");
            }
        }

        private static void ValidaDocumento(String doc)
        {
            if((!ValidaDados.ValidaCNPJ(doc)) && (!ValidaDados.ValidaCpf(doc)))
            {
                throw new ProprietarioException("Número do documento(Cpf/Cnpj) digitado está inválido.");
            }
        }

    }
}
