﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Repository;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Service
{
    public sealed class BancoService : IDisposable, IBancoService
    {
        private bool disposed = false;
        private IBancoRepository _br;

        public BancoService(IBancoRepository br)
        {
            _br = br;
        }

        public Banco Save(Banco banco)
        {
            _br.Save(banco);
            return banco;
        }

        public Banco Update(Banco banco)
        {
            _br.Update(banco);
            return banco;
        }

        public ICollection<Banco> FindAll()
        {
            return _br.AllBancos();
        }

        public Banco FindByCod(string cod)
        {
            return _br.FindByCodBanco(cod);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposed)
                {
                    if (_br != null)
                    {
                        _br.Dispose();
                    }
                }
            }
        }
        ~BancoService()
        {
            Dispose(false);
        }
    }
}
