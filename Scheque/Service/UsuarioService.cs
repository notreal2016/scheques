﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Service;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;
using Scheque.Repository;

namespace Scheque.Service
{
    /// <summary>
    /// Classe encarregada das regras de negócio de um usuário
    /// </summary>
    public sealed class UsuarioService : IUsuarioService, IDisposable
    {
        private bool disposed = false;
        private readonly IUsuarioRepository _repository;
        private readonly IUnitOfWork _unitOfWork;
        public UsuarioService(IUnitOfWork unitOfWork, IUsuarioRepository ur)
        {
            _unitOfWork = unitOfWork;
            _repository = ur;
        }

       
        /// <summary>
        /// Salva os dados de um usuário no banco de dados, verifica a validade dos dados do usuário 
        /// como nome e login.
        /// </summary>
        /// <param name="user">Usuário que terá os dados persistidos no banco de dados</param>
        public void Save(Usuario user)
        {
            try
            {
                ValidaUsuario.ValidaUsuairo(user);
                _repository.Save(user);
            }
            catch (UsuarioException e)
            {
               
                throw new UsuarioException(e.Message);
            }
        }
        /// <summary>
        /// Remove os dados de um usuário do banco de dados a partir do id do usuário
        /// </summary>
        /// <param name="id">Inteiro positivo maior que zero</param>
        public void Remove(Int64 id)
        {
            if (id <= 0)
            {
                throw  new UsuarioException("Id do usuário deve ser positivo e maior que zero");
            }
            _repository.Remove(id);
        }

        public Usuario FindById(Int64 id)
        {
            if (id <= 0)
            {
                throw new UsuarioException("Id do usuário deve ser positivo e maior que zero");
            }
            return _repository.FindById(id);
        }

        public List<Usuario> FindByNome(String nome)
        {
            if (ValidaDados.ExistCaracterEspeciais(nome))
            {
                throw new Exception("Nome do usuário não deve conter caracteres especiais.");
            }

            if (ValidaDados.ContemNumeros(nome))
            {
                throw new Exception("Nome do usuário não deve conter caracteres numeros.");
            }

            return _repository.FindByNome(nome);
        }

        public Usuario LogaUsuario(String login, String senha)
        {
            if (ValidaDados.ExistCaracterEspeciais(login))
            {
                throw new Exception("Login do usuário não deve conter caracteres especiais.");
            }
            return _repository.FindUserbyLoginSenha(login, senha);
        }

        /// <summary>
        /// Verifica se o login repassado já esta cadastrado com outro usuário
        /// </summary>
        /// <param name="login">Login a ser verificado</param>
        /// <param name="id">Id do usuário a ser descartado, como padrão valor 0</param>
        /// <returns>Booleano de confirmação</returns>
        public bool ValidaLogin(String login, long id = 0)
        {

            Usuario user =_repository.FindByLogin(login);
            return user != null && user.Id != id;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_repository != null)
                    {
                        _unitOfWork.Dispose();
                    }
                }
            }
            disposed = true;
        }

        public IEnumerable<Usuario> FindByAll()
        {
            return _repository.FindAll();
        }
    }
}
