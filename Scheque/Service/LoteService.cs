﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Service;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;
using Scheque.Repository;
using Scheque.Uteis;
using DateTime = System.DateTime;

namespace Scheque.Service
{
    /// <summary>
    /// Classe de serviço de lote de cheques
    /// </summary>
    public sealed class LoteService : IDisposable, ILoteService
    {
        
        private readonly ILoteRepository _repositorio;
        private readonly IClienteRepository _clientes;
        private readonly IBancoRepository _banco;
        private readonly IContaRepository _contas;
        private readonly ICalendarioRepository _calendarios;
        private readonly IChequeRepository _cheques;
        private readonly IProprietarioRepoitory _proprietarios;
        private readonly IRepositoryMovimentoDoc _doc;
        private readonly IRepositoryNegativado _rn;
        private bool disposed = false;
        private readonly IUnitOfWork _unitOfWork;

        public LoteService(IUnitOfWork unitOfWork, ILoteRepository lr, IClienteRepository cr, IBancoRepository br, IContaRepository contarep, ICalendarioRepository cad,
            IChequeRepository chrep, IProprietarioRepoitory proRep, IRepositoryMovimentoDoc rDoc, IRepositoryNegativado rn)
        {
            _unitOfWork = unitOfWork;
            _repositorio = lr;
            _clientes = cr;
            _banco = br;
            _contas = contarep;
            _calendarios = cad;
            _cheques = chrep;
            _proprietarios = proRep;
            _doc = rDoc;
            _rn = rn;
        }

        
        /// <summary>
        /// Busca por negatvações pelo Id do Emitente do cheque 
        /// </summary>
        /// <param name="id">Id do Emitente</param>
        /// <returns>Lista de negativações encontradas</returns>
        public List<Negativado> NegativadosByEmitente(long id)
        {
            return _rn.FindNegativadoByEmitente(id);
        }
        /// <summary>
        /// Busca por Negativados pelo CNPJ ou CPF do Emitente
        /// </summary>
        /// <param name="cpfcnpj">Documento CPF ou CNPJ a ser usado como filtro</param>
        /// <returns>Lista de Negativados</returns>
        public List<Negativado> NegativadosByDoc(string cpfcnpj)
        {
            return _rn.FindNegativadoByDoc(cpfcnpj);
        }
        /// <summary>
        /// Busca por negativações de uma conta especifica
        /// </summary>
        /// <param name="idBanco">Id da conta</param>
        /// <param name="agencia">Agencia</param>
        /// <param name="conta">Conta</param>
        /// <returns>Lista de negativações</returns>
        public List<Negativado> NegativadosByDadosConta(string idBanco, string agencia, string conta)
        {
            return _rn.FinNegativadosByDadosConta(idBanco, agencia, conta);
        }

        /// <summary>
        /// Busca por negativações de uma conta especifica pelo ID repassado
        /// </summary>
        /// <param name="id">id da conta</param>
        /// <returns>Retorna da lista</returns>
        public List<Negativado> NegativadoByIdConta(long id)
        {
            return _rn.FindNegativadoByConta(id);
        }
        
        /// <summary>
        /// Método encarregado de validar e salvar os dados de um lote no banco de dados
        /// Não permitido lote com taxas diferentes da cadastradas ao cliente
        /// Não permitido taxa de juros zerada
        /// Não permitido lote sem cliente
        /// Não permitido lote com quantidade de documentos zero ou negativo
        /// </summary>
        /// <param name="lote">Lote cujos dados serão salvos no banco de dados</param>
        public void Salvar(Lote lote)
        {
            ValidaLote.Valida(lote);
            bool novo = (lote.Id == 0);
            _repositorio.Save(lote);
            _unitOfWork.Commit();
            if (novo)
            {
                _doc.IncludeAll(MontaLoteDocumentos(lote.Cheques));
                _unitOfWork.Commit();
            }
        }

        private List<MovimentoDocumento> MontaLoteDocumentos(List<Cheque> loteAllCheuqes)
        {
            List<MovimentoDocumento> movimento = new List<MovimentoDocumento>();
            foreach (Cheque cheque in loteAllCheuqes)
            {
                movimento.Add(new MovimentoCheque()
                {
                    Data = DateTime.Now,
                    IdCheque = cheque.Id,
                    Status = StatusDocumento.DIGITADO
          
                });
            }

            return  movimento;
        }


        /// <summary>
        /// Lista todos os lotes cadastrados no sistema
        /// </summary>
        /// <returns>Lista de lotes</returns>
        public List<Lote> AllLotes()
        {
            return _repositorio.FindAlLotes().ToList();
        }

        /// <summary>
        /// Busca lotes filtrados pelo nome ,ou parte dele, do cliente
        /// </summary>
        /// <param name="nome">Nome ou parte dele</param>
        /// <returns>Lista de lotes</returns>
        public List<Lote> FindLoteByNomeCliente(String nome)
        {
            if (nome.Trim().Equals(""))
            {
                throw  new LoteException("Busca não pode ser realizada se o campo estiver em branco.");
            }
            return _repositorio.FindByCliente(nome).ToList();
        }
        /// <summary>
        /// Busca por lotes de uma determinada data
        /// </summary>
        /// <param name="data">Data da busca do lote</param>
        /// <returns></returns>
        public List<Lote> FindByData(DateTime data)
        {
            if (data > DateTime.Now)
            {
                throw  new LoteException("Data de busca não pode ser superior a data da maquina.");

            }
            return _repositorio.FindByDateLote(data).ToList();
        }
        /// <summary>
        /// Lista todos os clientes cadastrados
        /// </summary>
        /// <returns>Lista todos os clientes da base de dados</returns>
        public List<Cliente> AllClientes()
        {
            return _clientes.FindAllClientes().ToList();
        }
        /// <summary>
        /// Método apresenta uma lista contendo todos os banco cadastrados
        /// </summary>
        /// <returns>Lista de bancos cadastrados</returns>
        public List<Banco> AllBancos()
        {
            return _banco.AllBancos().ToList();
        }
        /// <summary>
        /// Busca por um banco a partir do código do banco febraban
        /// </summary>
        /// <param name="cobBancosText">Código do banco</param>
        /// <returns>Banco localizado</returns>
        public Banco FindBancoByCodBanco(string cobBancosText)
        {
            return _banco.FindByCodBanco(cobBancosText);
        }
        /// <summary>
        /// Busca por um cliente a partir do nome ou parte dele.
        /// </summary>
        /// <param name="nome">Nome ou parde dele</param>
        /// <returns>Lista de clientes localizados</returns>
        public List<Cliente> FindClienteByNome(String nome)
        {
            return _clientes.FindByName(nome).ToList();
        }
        /// <summary>
        /// Busca seletiva de lote
        /// </summary>
        /// <param name="tipo">Tipo de busca</param>
        /// <param name="termo">Termo da busca</param>
        /// <returns>Lista de resultados</returns>
        public Lote[] BuscaSelecionada(TipoBusca tipo, String termo)
        {
            switch (tipo)
            {
                case TipoBusca.CLIENTE:
                    return FindLoteByNomeCliente(termo).ToArray();
                case TipoBusca.DATA:
                    return FindLoteByData(termo.Split('/')).ToArray();
                case TipoBusca.LOTE:
                    return new Lote[] {FindLoteById((Int64.Parse(termo)))};
                default:
                    return null;
            }

        }
        /// <summary>
        /// Busca por um lote a partir do id 
        /// </summary>
        /// <param name="id">Número do lote</param>
        /// <returns>Lote localizado</returns>
        public Lote FindLoteById(Int64 id)
        {
            return _repositorio.FindById(id);
        }

        /// <summary>
        /// Busca a partir de uma data
        /// </summary>
        /// <param name="data">Data a ser pesquisada</param>
        /// <returns>Lista de lotes por data</returns>
        public List<Lote> FindLoteByData(String[] data)
        {
            int dia = int.Parse(data[0]);
            int mes = int.Parse(data[1]);
            int ano = int.Parse(data[2]);
            try
            {
                DateTime date = new DateTime(ano, mes, dia);
                return _repositorio.FindByDateLote(date).ToList();
            }
            catch (LoteException e)
            {
                throw new LoteException("Data informada inválida");
            }
        }

        public Conta FindContaByDados(String IdBanco, String agencia, String conta, string operacao = "", int numeroDigitoCC =6)
        {
            return _contas.FindContaByDados(IdBanco, agencia, conta, operacao, numeroDigitoCC );
        }
        /// <summary>
        /// Busca o proprietário de uma conta a partir do CNPJ ou CPF do mesmo
        /// </summary>
        /// <param name="documento">CPF ou CNPJ do proprietario de uma conta</param>
        /// <returns></returns>
        public Proprietario FindProprietarioByDocumento(String documento)
        {
            return _proprietarios.FindByDocumento(documento);
            
        }

        /// <summary>
        /// Método retorna a quantidade de dias entre a dataInicial repassada e a atual 
        /// levando em consideração se é ou não feriado bem como se deve ou não 
        /// contar os fins de semana
        /// </summary>
        /// <param name="dataInicial"> Data incial do calculo</param>
        /// <param name="dataFinal">Data final do calculo</param>
        /// <param name="contaFimDeSemana">Considera fim de semana</param>
        /// <param name="contaFeriados">Considera feriados</param>
        /// <param name="diasMinimo">Conta tempo mínimo</param>
        /// <returns></returns>
        public int ContaDias(DateTime dataInicial, DateTime dataFinal, bool contaFimDeSemana = true, bool contaFeriados = true, int diasMinimo = 0)
        {
            DateTime dt1 = dataFinal;
            TimeSpan diasTime = dt1 - dataInicial;
            
            if (diasTime.Days < diasMinimo)
            {
                DateTime novoVencimento = dataInicial.AddDays(diasMinimo);
                return ContaDias(dataInicial, novoVencimento, contaFimDeSemana, contaFeriados);
            }

            if (contaFimDeSemana == false)
            {
                if (contaFeriados && IsFeriado(dt1) && !(dt1.DayOfWeek == DayOfWeek.Saturday || dt1.DayOfWeek == DayOfWeek.Sunday))
                {
                    dt1 = dt1.AddDays(1);
                    return ContaDias(dataInicial, dt1, contaFimDeSemana, contaFeriados);
                }
                return diasTime.Days;
            }
            
            switch (dt1.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    dt1 = dt1.AddDays(2);
                    return ContaDias(dataInicial, dt1, contaFimDeSemana, contaFeriados);

                case DayOfWeek.Sunday:
                    dt1 = dt1.AddDays(1);
                    return ContaDias(dataInicial, dt1, contaFimDeSemana, contaFeriados);

                default:
                    if (contaFeriados && IsFeriado(dt1))
                    {
                        dt1 = dt1.AddDays(1);
                        return ContaDias(dataInicial, dt1, contaFimDeSemana, contaFeriados);
                    }
                    diasTime = dt1 - dataInicial;
                    return diasTime.Days;

            }
        }

        /// <summary>
        /// Método verifica se uma data especifica está contido na lista de feriados 
        /// </summary>
        /// <param name="dt1">Data a ser verificada</param>
        /// <returns>Boelano de confirmação</returns>
        public bool IsFeriado(DateTime dt1)
        {
            Calendario c = _calendarios.FindData(dt1.Day, dt1.Month);
            return ( c!= null);
        }

        /// <summary>
        /// Método efetua o calculo de juros simples ou compostos
        /// </summary>
        /// <param name="valor">Valor de prinicipal</param>
        /// <param name="taxa">Taxa aplicada ao mês</param>
        /// <param name="dias">Número de dias para o calculo</param>
        /// <param name="jusrosCompostos">Se o Juros é composto, default = false</param>
        /// <returns>Valor do juros para o período informado.</returns>
        public Decimal CalculaJuros(decimal valor, decimal taxa, int dias, bool jusrosCompostos = false)
        {
            if (jusrosCompostos)
            {
                return Juros.JurosCompostoMesAppDia(valor, taxa, dias);
            }

            return Juros.JurosSimplesMesAppDia(valor, taxa, dias);
        }
        /// <summary>
        /// Persiste os dados de uma conta no sistema de dados 
        /// </summary>
        /// <param name="conta">Conta cujos dados serão persistidos</param>
        public void SalvaConta(Conta conta)
        {
            _contas.Save(conta);
        }
        /// <summary>
        /// Verifica se o cheque repassado já esta cadastrado no banco de dados.
        /// </summary>
        /// <param name="cheque">Chque que ira ser pesquisado</param>
        /// <returns>Resposta se foi localizado</returns>
        public Boolean IsChequeCadastrado(Cheque cheque)
        {
            return IsChequeCadastrado(cheque.ContaCheque.IdBanco, cheque.ContaCheque.Agencia,
                cheque.ContaCheque.ContaCorrente, cheque.ContaCheque.Operacao, cheque.NumeroCheque, cheque.IdLote);
        }
        /// <summary>
        /// Verifica se um cheque especifico já esta cadastrado
        /// </summary>
        /// <param name="idBanco">Id do Banco</param>
        /// <param name="agencia">Agencia da conta</param>
        /// <param name="conta">Conta corrente</param>
        /// <param name="operacao">Operação</param>
        /// <param name="numero">Número do cheque</param>
        /// <param name="idLote">Id do Lote que está o chque</param>
        /// <returns>Booleano de confirmação</returns>
        public Boolean IsChequeCadastrado(string idBanco, string agencia, string conta, string operacao, string numero, long? idLote)
        {
            Cheque ch = _cheques.FindByDados(idBanco, agencia, conta, operacao, numero);
            if (ch != null && ch.IdLote != idLote)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        /// Retorna informaçõe para alimentar um relatório de cheques 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Dictionary<String, object> RelatorioLote(Int64 id)
        {
            Lote l = _repositorio.FindById(id);
            Dictionary<String, object> retorno = new Dictionary<string, object>()
            {
                {"Lote", new List<Lote>() { l }  },
                {"Cheques", l.Cheques }
            };
            return retorno;
        }
        /// <summary>
        /// Busca por todos os cheques de um mesmo emitente que estejam cadastradas no sistema 
        /// </summary>
        /// <param name="id">Id do emitente do cheque</param>
        /// <param name="naodepositados">Paramentro de filtragem se o cheque foi depositado ou não</param>
        /// <returns>Lista de cheques de um determinado emitente</returns>
        public List<Cheque> FindAllChequesByIdEmitente(long id)
        {
            List<Cheque> cheques = new List<Cheque>();
            cheques.AddRange(_cheques.FindChequesByProprietarioNoDep(id));
            return cheques;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
            
        }

        public Cliente FindClienteById(long id) => _clientes.FindById(id);

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _unitOfWork.Dispose();
                }
            }
            disposed = true;
        }

        public Conta FindContaById(long id) => _contas.FindContaById(id);
        public Proprietario FindByDocs(string cnpjpf) => _proprietarios.FindByDocumento(cnpjpf);
        public Cheque FindChequeById(long idCheque) => _cheques.FindById(idCheque);

        public Conta FindContaByIdNoTrack(long idConta)
        {
            return _contas.FindContaByIdNoTrack(idConta);
        }

        public Conta FindContaByDadosNoTrack(string IdBanco, string agencia, string conta, string operacao = "", int numeroDigitoCC = 6)
        {
           return _contas.FindContaByDadosNoTrack(IdBanco, agencia, conta, operacao, numeroDigitoCC);
        }
    }

    public enum TipoBusca
    {
        LOTE = 0, CLIENTE = 1, DATA = 2, NOME = 3
    }
}
