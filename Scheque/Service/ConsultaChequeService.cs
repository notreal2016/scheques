﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Entidade;
using Scheque.EntidadesFind;
using Scheque.Interface;

namespace Scheque.Service
{
    public class ConsultaChequeService : IConsultaChequeService,IDisposable
    {
        private IUnitOfWork _unitOfWork;
        private Cheque _cheque;
        private ConsultaEmitente _emitente;
        private DadosConta _dadosConta;
        private List<MovimentoCheque> _movimento;
        private IChequeRepository _cr;
        private IContaRepository _conteR;
        private IProprietarioRepoitory _pr;
        private IRepositoryNegativado _rn;
        public ConsultaChequeService(IUnitOfWork unitOfWork, string idBanco, string conta, string agencia, string numero , string opercao)
        {
            _unitOfWork = unitOfWork;
            _cr = unitOfWork.ChequeRepository;
            _conteR = unitOfWork.ContaRepository;
            _pr = unitOfWork.ProprietarioRepoitory;
            _rn = unitOfWork.RepositoryNegativado;

            _cheque = _cr.FindByDados(idBanco, agencia, conta, "", numero);
            if (_cheque != null)
            {
                if (_cheque.ContaCheque.Proprietario != null)
                {
                    MontaEmitente();
                }
                _dadosConta = MontaDadosConta(_cheque.ContaCheque);
                _movimento = _unitOfWork.RepositoryMovimentoDoc.FindByMovCh(_cheque.Id);
            }
            else
            {
                throw  new Exception("500|Cheque nao localizado");
            }
                        
        }

        private void MontaEmitente()
        {
            var emitente = _cheque.ContaCheque.Proprietario;
            var contas = _conteR.FindContasByDocEmitente(emitente.CpfCNPJ);
            List<DadosConta> dadosContas  = new List<DadosConta>();
            contas.ForEach(c =>
            {
                dadosContas.Add(MontaDadosConta(c));
            });
            List<String> clientes = _pr.AllClientes(emitente.CpfCNPJ);
            _emitente = new ConsultaEmitente(dadosContas, emitente, clientes);

        }

        private DadosConta MontaDadosConta(Conta conta)
        {
            decimal valor , vDevolvido, vADepositar;
            Int32 quantidade , qDevolvido, qADepositar ;
            DateTime uAdd = new DateTime() , uDevolvido = new DateTime();

            var cheques = _cr.FindAllChequesByIdConta(conta.Id);
            var negativados = _rn.FindNegativadoByConta(conta.Id);

            valor = cheques.Sum(c => c.Valor);
            quantidade = cheques.Count;
            qDevolvido = negativados.Count;
            uAdd = cheques.Count > 0 ? cheques.Max(n => n.Vencimento): new DateTime(01,01,01);
            vDevolvido = negativados.Count >0? negativados.Sum(n => n.Valor):0;
            uDevolvido = negativados.Count >0? negativados.Max(n => n.Data): uDevolvido;
            vADepositar = cheques.Where(c => c.Status == StatusDocumento.DIGITADO).Sum(c1 => c1.Valor);
            qADepositar = cheques.Where(c => c.Status == StatusDocumento.DIGITADO).Count();
            DadosConta dados = new DadosConta(conta,valor, quantidade, vDevolvido, qDevolvido, uAdd, uDevolvido, vADepositar, qADepositar);
            return dados;
        }

        public Cheque Cheque => _cheque;

        public ConsultaEmitente Emitente => _emitente;

        public DadosConta DadosConta => _dadosConta;

        public List<MovimentoCheque> Movimento => _movimento.OrderByDescending(m => m.Data).ToList();

        /// <summary>
        /// Adiciona um movimento ao cheque;
        /// </summary>
        /// <param name="movimento"></param>
        public void AddMovimento(MovimentoCheque movimento)
        {
            try
            {
                if (movimento.Status == _cheque.Status)
                {
                    throw  new Exception("Cheque já está registrado por último com esse status");
                }
                movimento.IdCheque = _cheque.Id;
                _cheque.Status = movimento.Status;
                _unitOfWork.RepositoryMovimentoDoc.Save(movimento);
                _unitOfWork.ChequeRepository.Save(_cheque);
                _movimento = _unitOfWork.RepositoryMovimentoDoc.FindByMovCh(_cheque.Id);
            }
            catch (Exception e)
            {
                throw;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // Para detectar chamadas redundantes

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _unitOfWork.Dispose();
                }

                // TODO: liberar recursos não gerenciados (objetos não gerenciados) e substituir um finalizador abaixo.
                // TODO: definir campos grandes como nulos.

                disposedValue = true;
            }
        }

        // TODO: substituir um finalizador somente se Dispose(bool disposing) acima tiver o código para liberar recursos não gerenciados.
         ~ConsultaChequeService()
         {
           // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
           Dispose(false);
         }

        // Código adicionado para implementar corretamente o padrão descartável.
        public void Dispose()
        {
            // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
            Dispose(true);
            // TODO: remover marca de comentário da linha a seguir se o finalizador for substituído acima.
            // GC.SuppressFinalize(this);
        }
        #endregion


    }
}
