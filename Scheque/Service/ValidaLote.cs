﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Entidade;
using Scheque.Repository;

namespace Scheque.Service
{
    public class ValidaLote
    {
        public static void Valida(Lote lote)
        {
                if (lote.IdCliente == 0)
                {
                    throw new LoteException("Não foi selecionando um cliente para o respectivo lote.");
                }
                
                //ValidaClienteNotNull(cliente);
                ValidaQuantidadeDocumentos(lote.Quantidade);
                ValidaValorLote(lote.ValorLote);
                //ValidaTaxa(cliente, lote.Taxa, lote.Administrativa);
            

        }

        private static void ValidaTaxa(Cliente loteCliente,  decimal loteTaxa, decimal loteAdministrativa)
        {
            if (loteTaxa <= 0)
            {
                throw  new LoteException("Valor da taxa de juros deve ser um número positivo e maior que zero.");
            }

            if (loteAdministrativa < 0)
            {
                throw new LoteException("Valor da taxa administrativa deve ser um número positivo.");
            }

            if (loteCliente.Taxa != 0 && loteCliente.Taxa != loteTaxa)
            {
                throw new LoteException("Valor da taxa de juros aplicada no lote não pode ser diferente da taxa cadastrada para o cliente " + loteCliente.Nome + ".");
            }

            if (loteCliente.Administrativa != 0 && loteCliente.Administrativa != loteAdministrativa)
            {
                throw new LoteException("Valor da taxa administrativa aplicada no lote não poderá ser diferente da cadastrada para o cliente " + loteCliente.Nome + ".");
            }
        }

        private static void ValidaValorLote(decimal loteValorLote)
        {
            if (loteValorLote <= 0)
            {
                throw  new LoteException("O valor do lote tem que ser positivo e maior que zero.");
            }
        }

        private static void ValidaQuantidadeDocumentos(int loteQuantidade)
        {
            if (loteQuantidade <= 0)
            {
                throw new LoteException("A quantidade de documentos do lote deve ser positiva e maior que zero");
            }
            
        }

        private static void ValidaClienteNotNull(Cliente cliente)
        {
            if (cliente == null)
            {
                throw  new LoteException("Cliente de um lote não pode ser em branco ou nulo");
            }
        }

    }
}
