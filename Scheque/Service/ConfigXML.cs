﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;

namespace Scheque.Service
{
    public class ConfigXML
    {
        public static void ModificaXml(String dados)
        {

            //XmlReaderSettings settings = new XmlReaderSettings();
            //settings.Schemas.Add()

            XmlDocument document = new XmlDocument();

            document.Load("setings.xml");

            XPathNavigator navigator = document.CreateNavigator();

            navigator.MoveToChild("Servidor", String.Empty);
            navigator.MoveToChild("Endereco", String.Empty);

            navigator.SetValue(dados);
            navigator.MoveToRoot();
            document.Save("setings.xml");
        }
    }
}
