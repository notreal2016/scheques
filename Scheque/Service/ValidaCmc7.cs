﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Scheque
{
    public class ValidaCmc7
    {
        
        /// <summary>
        /// Metodo Validar Cmc7 campo 1
        /// </summary>
        /// <param name="c1"> String contendo digitos do campo 1</param>
        /// <param name="dvC1"> valor do digito de validacao do campo 1</param>
        /// <returns>retorna true se o campo for valido</returns>
        public static bool ValidaC1(String c1, int dvC1)
        {
            char[] _Txt = c1.ToCharArray();
            int _Soma = 0;
            int _Valor = 0;

            for (int i = 0; i <= 6; i += 2)
            {
                _Valor = (int) char.GetNumericValue(_Txt[i]);
                _Valor *= 2;

                if (_Valor > 9)
                {
                    _Valor = (_Valor % 10) + (_Valor / 10);
                    _Soma += _Valor;
                }
                else
                {
                    _Soma += _Valor;
                }
            }

            for (int i = 1; i <= 6; i += 2)
            {
                _Valor = (int) char.GetNumericValue(_Txt[i]);
                _Soma += _Valor;
            }

            

            int _LastNumb = _Soma % 10;


            int _Div = 10 - _LastNumb;
            _Div = (_Div > 9) ? 0 : _Div;

            return _Div == dvC1;
        }

        /// <summary>
        /// Metodo Validar Cmc7 campo 2
        /// </summary>
        /// <param name="c2">String contendo digitos do campo 2</param>
        /// <param name="dvC2">valor do digito de validacao do campo 2</param>
        /// <returns>retorna true se o campo for valido</returns>
        public static bool ValidaC2(String c2, int dvC2)
        {
            char[] _Txt = c2.ToCharArray();
            int _Soma = 0;
            int _Valor = 0;

            for (int i = 0; i <= 9; i += 2)
            {
                _Valor = (int) char.GetNumericValue(_Txt[i]);
                _Soma += _Valor;
            }

            for (int i = 1; i <= 9; i += 2)
            {
                _Valor = (int) char.GetNumericValue(_Txt[i]);
                _Valor *= 2;

                if (_Valor > 9)
                {
                    _Valor = (_Valor % 10) + (_Valor / 10);
                    _Soma += _Valor;
                }
                else
                {
                    _Soma += _Valor;
                }
            }

            int _LastNumb = _Soma % 10;


            int _Div = 10 - _LastNumb;
            _Div = (_Div > 9) ? 0 : _Div;

            if(_LastNumb > 9)
            {
                _Div = 0;
            }

            // int _Div = (_LastNumb > 9) ? (10 - _LastNumb):0;

            System.Console.WriteLine("Last " + _LastNumb);

            System.Console.WriteLine("Div " + _Div);

            System.Console.WriteLine("C2 " + dvC2);
            return _Div == dvC2;
        }

        /// <summary>
        /// Metodo Validar Cmc7 campo 3
        /// </summary>
        /// <param name="c3">String contendo digitos do campo 3</param>
        /// <param name="dvC3"> valor do digito de validacao do campo 3</param>
        /// <returns>retorna true se o campo for valido</returns>
        public static bool ValidaC3(String c3, int dvC3)
        {
            char[] _Txt = c3.ToCharArray();
            int _Soma = 0;
            int _Valor = 0;

            for (int i = 0; i <= 9; i += 2)
            {
                _Valor = (int) char.GetNumericValue(_Txt[i]);
                _Soma += _Valor;
            }

            for (int i = 1; i <= 9; i += 2)
            {
                _Valor = (int) char.GetNumericValue(_Txt[i]);
                _Valor *= 2;

                if (_Valor > 9)
                {
                    _Valor = (_Valor % 10) + (_Valor / 10);
                    _Soma += _Valor;
                }
                else
                {
                    _Soma += _Valor;
                }
            }

            int _LastNumb = _Soma % 10;
            int _Div = 10 - _LastNumb;

            _Div = (_Div > 9) ? 0 : _Div;
            return _Div == dvC3;
        }

        /// <summary>
        /// Metodo Validar Cmc7 Completo
        /// </summary>
        /// <param name="cmc7">String contendo CMC7</param>
        /// <returns>Retorna true se o cmc7 for valido</returns>
        public static bool ValidaCmc(String cmc7)
        {

            if (VerificaLetras(cmc7.Trim()) || cmc7.Trim().Length !=30) return false;
                int[] digitos = ExtraiDigistosVerificadores(cmc7);
                String C1 = cmc7.Substring(0, 7);
                String C2 = cmc7.Substring(8, 10);
                String C3 = cmc7.Substring(19, 10);
            return ((ValidaC1(C1, digitos[0]) && ValidaC2(C2, digitos[1]) && ValidaC3(C3, digitos[2])));
    
        }
        /// <summary>
        /// Método retorna grupo de 03 dígitos verificadores, C1,C2 e C3
        /// </summary>
        /// <param name="cmc7">Codigo CMC7 que terá os dv extraidos</param>
        /// <returns>Array com dvC1, dvC2 e dvC3 respectivamente</returns>
        public static int[] ExtraiDigistosVerificadores(String cmc7)
        {
            int[] resposta = { (int)char.GetNumericValue(cmc7[18]), (int)char.GetNumericValue(cmc7[7]), (int)char.GetNumericValue(cmc7[29])};
            return resposta;
        }

        /// <summary>
        /// Metodo para extrair cod do banco
        /// </summary>
        /// <param name="CMC7">String contendo mcm7</param>
        /// <returns>String contendo cod do banco</returns>
        public static String ExtraiCodBanco(String CMC7)
        {
            return CMC7.Substring(0, 3);
        }

        /// <summary>
        /// Metodo para extrair cod da agencia
        /// </summary>
        /// <param name="CMC7">String contendo cmc7</param>
        /// <returns>String contendo num do cod da agencia</returns>
        public static String ExtraiCodAgencia(String CMC7)
        {
            return CMC7.Substring(3, 4);
        }

        /// <summary>
        /// Metodo para extrair num da camara de compensacao
        /// </summary>
        /// <param name="CMC7">String contendo cmc7</param>
        /// <returns>String cod da camara de Compensacao</returns>
        public static String ExtraiCamaraCompensacao(String CMC7)
        {
            return CMC7.Substring(8, 3);
        }

        /// <summary>
        /// Metodo para extrair numero do cheque
        /// </summary>
        /// <param name="CMC7">String contendo cmc7</param>
        /// <returns>String contendo numero do cheque</returns>
        public static String ExtraiNumDocumento(String CMC7)
        {
            return CMC7.Substring(11, 6);
        }

        /// <summary>
        /// Metodo para extrair tipo
        /// </summary>
        /// <param name="CMC7"></param>
        /// <returns></returns>
        public static String ExtraiTipo(String CMC7)
        {
            return CMC7.Substring(17, 1);
        }

        /// <summary>
        /// Metodo para extrair numero da conta do cliente a partir do cod cmc7
        /// </summary>
        /// <param name="CMC7">String com o cod cmc7</param>
        /// <returns>retorna String contendo cod do numero da conta do cliente</returns>
        public static String ExtraiCodOperadorConta(String CMC7)
        {
            return CMC7.Substring(19, 3);
        }

        /// <summary>
        /// Metodo para extrair o nummero da conta do cliente
        /// </summary>
        /// <param name="CMC7">String contendo numero do cmc7</param>
        /// <returns>String contento numero da conta do cliente</returns>
        public static String ExtraiNumContaCliente(String CMC7)
        {
            String _Operador = CMC7.Substring(22, 6);
            return _Operador;
        }


        /// <summary>
        /// Metodo para concatenar Cmc7 a partir do C1, C2 e C3
        /// </summary>
        /// <param name="C1">String contendo C1</param>
        /// <param name="C2">String contendo C2</param>
        /// <param name="C3">String contendo C3</param>
        /// <returns>retorna String contendo CMC7</returns>
        public static String MontaCmc7(String C1, String C2, String C3)
        {
            return C1 + C2 + C3;
        }

        /// <summary>
        /// Metodo para remover caracteres especiais
        /// </summary>
        /// <param name="CMC7">String contendo cmc7</param>
        /// <returns>String contendo cmc7 sem os caracteres especias</returns>
        public static String RemoveCaracteresEspeciais(String CMC7)
        {
            String[] caractres = { "<", "-", "_", " ", "!", ">", ".", ",", "{", "}", "[", "]", "\\", "/", "|", "&",
                "(", ")", ":", ";", "#", "@", "$", "%", "*", "=", "+", "^", "~", "´", "`", "?", "¨" };
            foreach (var item in caractres)
            {
                CMC7 = CMC7.Replace(item, "");
            }
            return CMC7;
        }

        /// <summary>
        /// Metodo para verificar se há letras
        /// </summary>
        /// <param name="texto">String contendo cmc7</param>
        /// <returns>Retorna True se nao conter letras, retorna false se conter letras</returns>
        public static bool VerificaLetras(String texto)
        {
            return Regex.IsMatch(texto, ("asdfghjklçqwertyuiopzxcvbnmQWERTYUIOPASDFGHJKLÇZXCVBNM"));
            
        }
    }
}

