﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Service;
using Scheque.Repository;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Service
{
    public sealed class EmpresaService: IDisposable, IEmpresaService
    {
        private readonly IEmpresaRepository _er;
        private bool disposed = false;

        public EmpresaService(IEmpresaRepository er)
        {
            _er = er;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_er != null)
                    {
                        _er.Dispose();
                        
                    }
                }
            }
            disposed = true;
        }

        public Empresa FindById(long id)
        {
            return _er.FindById(id);
        }

        public Empresa Save(Empresa empresa)
        {
            if (ValidaDados.ExistCaracterEspeciais(empresa.Nome.Replace("-", "")))
            {
                throw  new Exception("Nome da empresa não pode conter caracteres especiais.");
            }
            if (ValidaDados.ExistCaracterEspeciais(empresa.Razao.Replace("-", "")))
            {
                throw  new Exception("Razão da empresa não pode conter caracteres especiais.");
            }
            return _er.Save(empresa);
        }
    }
}
