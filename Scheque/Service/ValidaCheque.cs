﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Service;
using Scheque.Entidade;

namespace Scheque.Service
{
    public class ValidaCheque
    {
        public static void ValidaCheques(Cheque cheque)
        {
            if (!ValidaDados.SoNumeros(cheque.NumeroCheque) || cheque.NumeroCheque == null || cheque.NumeroCheque.Trim().Equals(""))
            {
                throw new ChequeException("Número do cheque só pode conter números.");
            }

            if (cheque.Valor <=0)
            {
                throw new ChequeException("Valor do cheque deve ser um número positivo e maior que zero.");
            }

            
            
        }
    }
}
