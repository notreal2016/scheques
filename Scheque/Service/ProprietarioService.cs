﻿using ADVCore.Service;
using Scheque.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Repository;
using Scheque.Context;
using Scheque.Interface;

namespace Scheque.Service
{
    public sealed class ProprietarioService : IDisposable, IProprietarioService
    {

        private readonly IProprietarioRepoitory _repository;
        private Boolean disposed;
        private readonly IUnitOfWork _unitOfWork;

        public ProprietarioService(IUnitOfWork unitOfWork, IProprietarioRepoitory pr)
        {
            _unitOfWork = unitOfWork;
            _repository = pr;
        }

        public Proprietario FindByDocs(String doc)
        {
            if ((!ValidaDados.SoNumeros(doc)))
            {
                throw new ProprietarioException("Campo do documento deve conter apenas números");
            }
            else if ((!ValidaDados.ValidaCNPJ(doc)) && (!ValidaDados.ValidaCpf(doc)))
            {
                throw new ProprietarioException("Documento inválido");
            }
            else
            {
                return _repository.FindByDocumento(doc);
            }
        }
        
        public void Save(Proprietario proprietario)
        {
            try
            {
                ValidaProprietario.ValidarProprietario(proprietario);
                _repository.Save(proprietario);
            }
            catch (ProprietarioException e)
            {
                throw new ProprietarioException(e.Message);
            }
        }

        public void Remove(Int64 id)
        {
            if (id <= 0)
            {
                throw new ProprietarioException("Id do proprietario deve ser positivo e maior que zero");
            }
            _repository.Remove(id);
        }

        public Proprietario FindById(Int64 id)
        {
            if (id <= 0)
            {
                throw new ProprietarioException("Id do proprietario deve ser positivo e maior que zero");
            }
            return _repository.FindById(id);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_repository != null)
                    {
                        _unitOfWork.Dispose();
                    }
                }
            }
            disposed = true;
            
        }
    }
}
