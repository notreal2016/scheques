﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Scheque.Context;
using Scheque.Entidade;

namespace Scheque.Service
{
    class GeraUsuario
    {


        public static void GerarUsuario(String login, String senha, ScContext context)
        {

            String comando = String.Format("CREATE USER '{0}'@'%' IDENTIFIED BY '{1}';" +
                                "GRANT ALL PRIVILEGES ON * . * TO '{0}'@'%'; " +
                                "GRANT GRANT OPTION ON * . * TO '{0}'@'%';" +
                                "FLUSH PRIVILEGES;", login, senha);

            using (MySqlConnection conn = new MySqlConnection(context.ConnectionSeting))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(comando, conn);
                cmd.ExecuteNonQuery();
            }
        }

        public static void DeleteUsuario(String user, ScContext context)
        {
            String SQLComando = String.Format("DROP USER {0}@'%'", user);
            using (MySqlConnection conn = new MySqlConnection(context.ConnectionSeting))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(SQLComando, conn);
                cmd.ExecuteNonQuery();
            }
        }

        public static void AtivaUsuario(String user, ScContext context, bool ativo = true)
        {
            String SQLComando;
            if (ativo)
            {
                SQLComando = String.Format("GRANT ALL PRIVILEGES ON * . * TO '{0}'@'%'; GRANT GRANT OPTION ON * . * TO '{0}'@'%'; FLUSH PRIVILEGES;", user);
            }
            else
            {
                SQLComando = String.Format("REVOKE ALL ON *.* FROM '{0}'@'%'; FLUSH PRIVILEGES;", user);
            }
            using (MySqlConnection conn = new MySqlConnection(context.ConnectionSeting))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(SQLComando, conn);
                cmd.ExecuteNonQuery();
            }
        }
        /// <summary>
        /// Metodo para verificar se um login existe no mysql pelo login
        /// </summary>
        /// <param name="login">String contendo login do usuario</param>
        /// <returns>true caso usuario exista</returns>
        public static bool FindByLogin(String login, String Setings)
        {
            String SQLComando = ("Select user from mysql.user where user ='" + login + "';");
            using (MySqlConnection conn = new MySqlConnection(Setings))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(SQLComando, conn);
                String _valor = Convert.ToString(cmd.ExecuteScalar());
                return ((!_valor.Trim().Equals("")));

            }
        }
        

       

        public static void AlteraDadosUsuario(String user, String newPassword, String newUser, ScContext context)
        {
            String SQLComando = "";
            SQLComando = String.Format("SET PASSWORD FOR '{0}'@'%' = '{1}'; FLUSH PRIVILEGES;", user, newPassword);
            if (!user.Equals(newUser))
            {
                SQLComando += String.Format(" rename user '{0}'@'%' to '{1}'; FLUSH PRIVILEGES; ", user, newUser);
            }
            using (MySqlConnection conn = new MySqlConnection(context.ConnectionSeting))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand(SQLComando, conn);
                cmd.ExecuteNonQuery();
            }
        }
    }
}