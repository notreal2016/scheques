﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ADVCore.Service;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;
using Scheque.Repository;

namespace Scheque.Service
{
    /// <summary>
    /// Classe de serviços de cliente
    /// </summary>
    public sealed class ClienteService : IDisposable, IClienteService
    {
        private readonly IClienteRepository _repository;
        private bool disposed = false;
        private readonly IUnitOfWork _iUnitOfWork;

        /// <summary>
        /// Cria um objeto do tipo classe clienteService
        /// </summary>
        /// <param name="context">Context paramentro</param>
        public ClienteService(IUnitOfWork unitOfWork, IClienteRepository cr)
        {
            _iUnitOfWork = unitOfWork;
            _repository = cr;
        }
        /// <summary>
        /// Salva os dados de um cliente 
        /// </summary>
        /// <param name="cliente">Cliente cujo dados serão salvos</param>
        public void Save(Cliente cliente)
        {
            try
            {
                ValidaCliente(cliente);
                _repository.Save(cliente);
            }
            catch (ClienteException e)
            {
                throw new ClienteException(e.Message);
            }
            
        }

        /// <summary>
        /// Faz a verificação dos dados de um cliente no banco de dados
        /// </summary>
        /// <param name="cliente">dados do cliente a validar</param>
        private void ValidaCliente(Cliente cliente)
        {
            if (cliente.Nome.Trim().Equals(""))
            {
                throw  new ClienteException("Nome do cliente não pode ser em branco ou vazio.");
            }

            if (ValidaDados.ExistCaracterEspeciais(cliente.Nome))
            {
                throw new ClienteException("Nome do cliente não pode conter caracteres especiais.");
            }

            if (ValidaDados.ExistCaracterEspeciais(cliente.Endereco))
            {
                throw  new ClienteException("Endereço do cliente não pode contér caracteres especiais.");
            }

            if (cliente.Taxa < 0)
            {
                throw  new ClienteException("A taxa de juros aplicada ao cliente não pode ser negatívo.");
            }

            if (cliente.Administrativa < 0)
            {
                throw new ClienteException("A taxa admisntrativa ao cliente não pode ser negatíva.");
            }
        }

        /// <summary>
        /// Exclui o cliente no sistema
        /// </summary>
        /// <param name="cliente">Cliente cujo dados serão excluido</param>
        public void Exclir(Cliente cliente)
        {
            _repository.Remove(cliente.Id);
        }

        /// <summary>
        /// Busca por um cliente apartir do id repassado
        /// </summary>
        /// <param name="id">Id do cliente sendo positivo e maior que zero</param>
        /// <returns>Cliente localizado</returns>
        public Cliente FindById(Int64 id)
        {
            if (id <= 0)
            {
                throw  new ClienteException("Id deve ser um número positivo e maior que zero.");
            }

            
            return _repository.FindById(id);
        }


        /// <summary>
        /// Método busca por todos os clientes cadastrados
        /// </summary>
        /// <returns>Retorna uma lista de clientes</returns>
        public List<Cliente> AllClientes()
        {

            return _repository.FindAllClientes().ToList();
        }
        /// <summary>
        /// Método efetua a busca de clientes pelo nome repassado como paramentro
        /// </summary>
        /// <param name="nome">Nome de cliente a ser buscado</param>
        /// <returns>Lista de clientes</returns>
        public List<Cliente> FindByName(String nome)
        {
            if (nome.Trim().Equals(""))
            {
                throw  new ClienteException("O nome do cliente não pode estar em branco ou vazio.");
            }

            if (ValidaDados.ExistCaracterEspeciais(nome.Replace("%", "")))
            {
                throw  new ClienteException("Nome do cliente para busca não pode conter caracteres especiais");
            }

            return _repository.FindByName(nome).ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _repository.Dispose(disposing);
                    
                }
            }
            disposed = true;
        }
        ~ClienteService()
        {
            Dispose(false);
        }
    }
}
