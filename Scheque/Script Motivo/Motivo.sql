use chequesdata;

create table if not exists motivo(
id serial,
motivo int,
descricao varchar(150));

insert into motivo(motivo, descricao) values (11, 'Cheque sem fundo - 1º apresentação');
insert into motivo(motivo, descricao) values (12, 'Cheque sem fundo - 2º apresentação');
insert into motivo(motivo, descricao) values (13, 'Conta encerrada');
insert into motivo(motivo, descricao) values (14, 'Prática espúria');

insert into motivo(motivo, descricao) values (20, 'Cheque sustado ou revogado em virtude de roubo, furto ou extravio de folhas de cheque em branco ');
insert into motivo(motivo, descricao) values (21, 'Cheque sustado ou revogado');
insert into motivo(motivo, descricao) values (22, 'Divergência ou insuficiência de assinatura');
insert into motivo(motivo, descricao) values (23, 'Cheques emitidos por entidades e órgãos da administração pública federal direta e indireta, em desacordo com os requisitos constantes do art. 74, § 2º, do Decreto-lei nº 200, de 25 de fevereiro de 1967 ');
insert into motivo(motivo, descricao) values (24, 'Bloqueio judicial ou determinação do Bacen');
insert into motivo(motivo, descricao) values (25, 'Cancelamento de talonário pelo participante destinatário');
insert into motivo(motivo, descricao) values (27, 'Feriado municipal não previsto');
insert into motivo(motivo, descricao) values (28, 'Cheque sustado ou revogado em virtude de roubo, furto ou extravio');
insert into motivo(motivo, descricao) values (30, 'Furto ou roubo de cheque');
insert into motivo(motivo, descricao) values (70, 'Sustação ou revogação provisória');

insert into motivo(motivo, descricao) values (31, 'Erro formal (sem data de emissão, com o mês grafado numericamente, ausência de assinatura ou não registro do valor por extenso)');
insert into motivo(motivo, descricao) values (33, 'Divergência de endosso');
insert into motivo(motivo, descricao) values (34, 'Cheque apresentado por participante que não o indicado no cruzamento em preto, sem o endosso-mandato');
insert into motivo(motivo, descricao) values (35, 'Cheque fraudado, emitido sem prévio controle ou responsabilidade do participante ("cheque universal"), ou com adulteração da praça acada, ou ainda com rasura no preenchimento ');

insert into motivo(motivo, descricao) values (37, 'Registro inconsistente ');
insert into motivo(motivo, descricao) values (38, 'Assinatura digital ausente ou inválida');
insert into motivo(motivo, descricao) values (39, 'Imagem fora do padrão');
insert into motivo(motivo, descricao) values (40, 'Moeda Inválida');
insert into motivo(motivo, descricao) values (41, 'Cheque apresentado a participante que não o destinatário');
insert into motivo(motivo, descricao) values (43, 'Cheque, devolvido anteriormente pelos motivos 21, 22, 23, 24, 31 e 34, não passível de reapresentação em virtude de persistir o motivo da devolução');
insert into motivo(motivo, descricao) values (44, 'Cheque prescrito');
insert into motivo(motivo, descricao) values (45, 'Cheque emitido por entidade obrigada a realizar movimentação e utilização de recursos financeiros do Tesouro Nacional mediante Ordem Bancária');
insert into motivo(motivo, descricao) values (48, 'Cheque de valor superior a R$100,00 (cem reais), emitido sem a identificação do beneficiário');
insert into motivo(motivo, descricao) values (49, 'Remessa nula, caracterizada pela reapresentação de cheque devolvido pelos motivos 12, 13, 14, 20, 25, 28, 30, 35, 43, 44 e 45');

insert into motivo(motivo, descricao) values (59, 'Informação essencial faltante ou inconsistente não passível de verificação pelo participante remetente e não enquadrada no motivo 31');
insert into motivo(motivo, descricao) values (60, 'Instrumento inadequado para a finalidade');
insert into motivo(motivo, descricao) values (61, 'Papel não compensável');

insert into motivo(motivo, descricao) values (71, 'Inadimplemento contratual da cooperativa de crédito no acordo de compensação');
insert into motivo(motivo, descricao) values (72, 'Contrato de Compensação encerrado');