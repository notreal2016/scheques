﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Scheque.Entidade;

namespace Scheque.Context
{
    public class ScContext : DbContext, IScContext
    {

        private String _server, _user, _password, _dabase, _port;


        private void RecolheDados()
        {
            String[] campos = base.Database.Connection.ConnectionString.Split(';');
            _server = campos[0].Split('=')[1];
            _port = campos[1].Split('=')[1];
            _dabase = campos[2].Split('=')[1];
            _user = campos[3].Split('=')[1];
            _password = campos[4].Split('=')[1];

        }

        public ScContext()
        {
            RecolheDados();

        }

        public string ConnectionSeting => String.Format("server = {0}; port={1}; database={2}; user id={3}; password={4}", _server, _port, _dabase, _user, _password);
        public string Server => _server;
        public string User => _user;
        public string Password => _password;
        public string Dabase => _dabase;
        public string Port => _port;


        public virtual DbSet<Banco> Bancos { get; set; }
        public virtual DbSet<Motivo> Motivos { get; set; }
        public virtual DbSet<Negativado> Negativados { get; set; }
        public virtual DbSet<Empresa> Empresas { get; set; }

        public virtual DbSet<Calendario> Calendarios { get; set; }
        public virtual DbSet<Cheque> Cheques { get; set; }

        public virtual DbSet<Cliente> Clientes { get; set; }
        public virtual DbSet<Conta> Contas { get; set; }
        public virtual DbSet<Emprestimo> Emprestimos { get; set; }
        public virtual DbSet<Lote> Lotes { get; set; }
        public virtual DbSet<Promissoria> Promissorias { get; set; }
        public virtual DbSet<Proprietario> Proprietarios { get; set; }
        public virtual DbSet<Registro> Registros { get; set; }
        public virtual DbSet<Usuario> Usuarios { get; set; }
        public virtual DbSet<MovimentoCliente> Movimentos { get; set; }
        public virtual DbSet<Historico> Historicos { get; set; }
        public virtual DbSet<MovimentoDocumento> MovimentoDocumentos { get; set; }
        public virtual DbSet<MovimentoCheque> MovimentoCheques { get; set; }
        public virtual DbSet<MovimentoDuplicata> MovimentoDuplicatas { get; set; }
        public virtual DbSet<LoteDeposito> LoteDepositos { get; set; }

        public override int SaveChanges()
        {
            //Processo reremoção dos campos orfãos.
            var orphanedResponses = ChangeTracker.Entries().Where(
                e => (e.State == EntityState.Modified || e.State == EntityState.Added) &&
                     e.Entity is Promissoria &&
                     e.Reference("Emprestimo").CurrentValue == null);

            foreach (var orphanedResponse in orphanedResponses)
            {
                Promissorias.Remove(orphanedResponse.Entity as Promissoria);
            }

            return base.SaveChanges();
        }
    }
}
