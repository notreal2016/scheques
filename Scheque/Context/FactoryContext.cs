﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Scheque.Context
{
    public class FactoryContext
    {
        private readonly static ScContext _context = new ScContext();

        static FactoryContext()
        {

        }

        private FactoryContext()
        {

        }

        public static ScContext GetContext() => _context;

      

    }
}
