﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using Scheque.Entidade;

namespace Scheque.Context
{
    public interface IScContext: IDisposable
    {
        DbSet<Banco> Bancos { get; set; }
        DbSet<Calendario> Calendarios { get; set; }
        DbSet<Cheque> Cheques { get; set; }
        DbSet<Cliente> Clientes { get; set; }
        string ConnectionSeting { get; }
        DbSet<Conta> Contas { get; set; }
        string Dabase { get; }
        DbSet<Empresa> Empresas { get; set; }
        DbSet<Emprestimo> Emprestimos { get; set; }
        DbSet<Historico> Historicos { get; set; }
        DbSet<LoteDeposito> LoteDepositos { get; set; }
        DbSet<Lote> Lotes { get; set; }
        DbSet<Motivo> Motivos { get; set; }
        DbSet<MovimentoCheque> MovimentoCheques { get; set; }
        DbSet<MovimentoDocumento> MovimentoDocumentos { get; set; }
        DbSet<MovimentoDuplicata> MovimentoDuplicatas { get; set; }
        DbSet<MovimentoCliente> Movimentos { get; set; }
        DbSet<Negativado> Negativados { get; set; }
        string Password { get; }
        string Port { get; }
        DbSet<Promissoria> Promissorias { get; set; }
        DbSet<Proprietario> Proprietarios { get; set; }
        DbSet<Registro> Registros { get; set; }
        string Server { get; }
        string User { get; }
        DbSet<Usuario> Usuarios { get; set; }

        Database Database { get; }
        //
        // Resumo:
        //     Provides access to features of the context that deal with change tracking of
        //     entities.
        DbChangeTracker ChangeTracker { get; }
        //
        // Resumo:
        //     Provides access to configuration options for the context.
        DbContextConfiguration Configuration { get; }

        //
        // Resumo:
        //     Calls the protected Dispose method.
        void Dispose();
        //
        // Resumo:
        //     Gets a System.Data.Entity.Infrastructure.DbEntityEntry object for the given entity
        //     providing access to information about the entity and the ability to perform actions
        //     on the entity.
        //
        // Parâmetros:
        //   entity:
        //     The entity.
        //
        // Devoluções:
        //     An entry for the entity.
        DbEntityEntry Entry(object entity);
        //
        // Resumo:
        //     Gets a System.Data.Entity.Infrastructure.DbEntityEntry`1 object for the given
        //     entity providing access to information about the entity and the ability to perform
        //     actions on the entity.
        //
        // Parâmetros:
        //   entity:
        //     The entity.
        //
        // Parâmetros de Tipo:
        //   TEntity:
        //     The type of the entity.
        //
        // Devoluções:
        //     An entry for the entity.
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        //
        [EditorBrowsable(EditorBrowsableState.Never)]
        bool Equals(object obj);
        //
        [EditorBrowsable(EditorBrowsableState.Never)]
        int GetHashCode();
        //
        [EditorBrowsable(EditorBrowsableState.Never)]
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        Type GetType();
        //
        // Resumo:
        //     Validates tracked entities and returns a Collection of System.Data.Entity.Validation.DbEntityValidationResult
        //     containing validation results.
        //
        // Devoluções:
        //     Collection of validation results for invalid entities. The collection is never
        //     null and must not contain null values or results for valid entities.
        //
        // Comentários:
        //     1. This method calls DetectChanges() to determine states of the tracked entities
        //     unless DbContextConfiguration.AutoDetectChangesEnabled is set to false. 2. By
        //     default only Added on Modified entities are validated. The user is able to change
        //     this behavior by overriding ShouldValidateEntity method.
        [SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
        IEnumerable<DbEntityValidationResult> GetValidationErrors();
        //
        // Resumo:
        //     Saves all changes made in this context to the underlying database.
        //
        // Devoluções:
        //     The number of state entries written to the underlying database. This can include
        //     state entries for entities and/or relationships. Relationship state entries are
        //     created for many-to-many relationships and relationships where there is no foreign
        //     key property included in the entity class (often referred to as independent associations).
        //
        // Exceções:
        //   T:System.Data.Entity.Infrastructure.DbUpdateException:
        //     An error occurred sending updates to the database.
        //
        //   T:System.Data.Entity.Infrastructure.DbUpdateConcurrencyException:
        //     A database command did not affect the expected number of rows. This usually indicates
        //     an optimistic concurrency violation; that is, a row has been changed in the database
        //     since it was queried.
        //
        //   T:System.Data.Entity.Validation.DbEntityValidationException:
        //     The save was aborted because validation of entity property values failed.
        //
        //   T:System.NotSupportedException:
        //     An attempt was made to use unsupported behavior such as executing multiple asynchronous
        //     commands concurrently on the same context instance.
        //
        //   T:System.ObjectDisposedException:
        //     The context or connection have been disposed.
        //
        //   T:System.InvalidOperationException:
        //     Some error occurred attempting to process entities in the context either before
        //     or after sending commands to the database.
        int SaveChanges();
        //
        // Resumo:
        //     Asynchronously saves all changes made in this context to the underlying database.
        //
        // Parâmetros:
        //   cancellationToken:
        //     A System.Threading.CancellationToken to observe while waiting for the task to
        //     complete.
        //
        // Devoluções:
        //     A task that represents the asynchronous save operation. The task result contains
        //     the number of state entries written to the underlying database. This can include
        //     state entries for entities and/or relationships. Relationship state entries are
        //     created for many-to-many relationships and relationships where there is no foreign
        //     key property included in the entity class (often referred to as independent associations).
        //
        // Exceções:
        //   T:System.InvalidOperationException:
        //     Thrown if the context has been disposed.
        //
        // Comentários:
        //     Multiple active operations on the same context instance are not supported. Use
        //     'await' to ensure that any asynchronous operations have completed before calling
        //     another method on this context.
        [SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "cancellationToken")]
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
        //
        // Resumo:
        //     Asynchronously saves all changes made in this context to the underlying database.
        //
        // Devoluções:
        //     A task that represents the asynchronous save operation. The task result contains
        //     the number of state entries written to the underlying database. This can include
        //     state entries for entities and/or relationships. Relationship state entries are
        //     created for many-to-many relationships and relationships where there is no foreign
        //     key property included in the entity class (often referred to as independent associations).
        //
        // Exceções:
        //   T:System.Data.Entity.Infrastructure.DbUpdateException:
        //     An error occurred sending updates to the database.
        //
        //   T:System.Data.Entity.Infrastructure.DbUpdateConcurrencyException:
        //     A database command did not affect the expected number of rows. This usually indicates
        //     an optimistic concurrency violation; that is, a row has been changed in the database
        //     since it was queried.
        //
        //   T:System.Data.Entity.Validation.DbEntityValidationException:
        //     The save was aborted because validation of entity property values failed.
        //
        //   T:System.NotSupportedException:
        //     An attempt was made to use unsupported behavior such as executing multiple asynchronous
        //     commands concurrently on the same context instance.
        //
        //   T:System.ObjectDisposedException:
        //     The context or connection have been disposed.
        //
        //   T:System.InvalidOperationException:
        //     Some error occurred attempting to process entities in the context either before
        //     or after sending commands to the database.
        //
        // Comentários:
        //     Multiple active operations on the same context instance are not supported. Use
        //     'await' to ensure that any asynchronous operations have completed before calling
        //     another method on this context.
        Task<int> SaveChangesAsync();
        //
        // Resumo:
        //     Returns a non-generic System.Data.Entity.DbSet instance for access to entities
        //     of the given type in the context and the underlying store.
        //
        // Parâmetros:
        //   entityType:
        //     The type of entity for which a set should be returned.
        //
        // Devoluções:
        //     A set for the given entity type.
        //
        // Comentários:
        //     Note that Entity Framework requires that this method return the same instance
        //     each time that it is called for a given context instance and entity type. Also,
        //     the generic System.Data.Entity.DbSet`1 returned by the System.Data.Entity.DbContext.Set(System.Type)
        //     method must wrap the same underlying query and set of entities. These invariants
        //     must be maintained if this method is overridden for anything other than creating
        //     test doubles for unit testing. See the System.Data.Entity.DbSet class for more
        //     details.
        [SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Set")]
        DbSet Set(Type entityType);
        //
        // Resumo:
        //     Returns a System.Data.Entity.DbSet`1 instance for access to entities of the given
        //     type in the context and the underlying store.
        //
        // Parâmetros de Tipo:
        //   TEntity:
        //     The type entity for which a set should be returned.
        //
        // Devoluções:
        //     A set for the given entity type.
        //
        // Comentários:
        //     Note that Entity Framework requires that this method return the same instance
        //     each time that it is called for a given context instance and entity type. Also,
        //     the non-generic System.Data.Entity.DbSet returned by the System.Data.Entity.DbContext.Set(System.Type)
        //     method must wrap the same underlying query and set of entities. These invariants
        //     must be maintained if this method is overridden for anything other than creating
        //     test doubles for unit testing. See the System.Data.Entity.DbSet`1 class for more
        //     details.
        [SuppressMessage("Microsoft.Naming", "CA1716:IdentifiersShouldNotMatchKeywords", MessageId = "Set")]
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        //
        [EditorBrowsable(EditorBrowsableState.Never)]
        string ToString();
    }
}