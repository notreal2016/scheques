﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Service;

namespace Scheque.Interface
{
    public interface IUnitOfWork : IDisposable
    {
        IBancoRepository BancoRepository { get;}
        ICalendarioRepository CalendarioRepository { get;}
        IChequeRepository ChequeRepository { get;}
        IClienteRepository ClienteRepository { get;}
        IContaRepository ContaRepository { get;}
        IEmpresaRepository EmpresaRepository { get;}
        IEmprestimoRepository EmprestimoRepository { get;}
        ILoteRepository LoteRepository { get; }
        IPromissoriaRepository PromissoriaRepository { get;}
        IProprietarioRepoitory ProprietarioRepoitory { get;}
        IRepositoryMovimentoDoc RepositoryMovimentoDoc { get;}
        IRepositoryNegativado RepositoryNegativado { get;}
        IUsuarioRepository UsuarioRepository { get;}
        IUsuarioService UsuarioService { get;}
        IBancoService BancoService { get; }
        ILoteService LoteService { get;}
        IEmpresaService EmpresaService { get;}
        IEmprestimoService  EmprestimoService { get;}
        IProprietarioService ProprietarioService { get;}
        IClienteService ClienteService { get;}

        void RegistraAlterado(IPersistenceBase entidade, IUnitOfWorkRepository respository);
        void RegistraNovo(IPersistenceBase entidade, IUnitOfWorkRepository respository);
        void RegistraRemovido(IPersistenceBase entidade, IUnitOfWorkRepository respository);
        void Commit();
        
    }
}
