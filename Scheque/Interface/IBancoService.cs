﻿using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Service
{
    public interface IBancoService
    {
        Banco Save(Banco banco);
        Banco Update(Banco banco);
        ICollection<Banco> FindAll();
        Banco FindByCod(string cod);
        void Dispose(bool disposing);
    }
}