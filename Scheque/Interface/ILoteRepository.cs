﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Interface
{
    public interface ILoteRepository : IUnitOfWorkRepository
    {
        void Save(Lote lote);

        /// <summary>
        /// Remove um lote e seus cheques do banco de dados
        /// </summary>
        /// <param name="idLote">Id do lote a ser removido</param>
        void Remove(Int64 idLote);

        IEnumerable<Lote> FindAlLotes();

        /// <summary>
        /// Busca por um ou mais lotes cadastrado a um cliente cujo nome ou parte dele foi repassado como paramentro
        /// </summary>
        /// <param name="nomeCliente">Nome ou parte do nome do cliente que será usado para a pesquisa.</param>
        /// <returns>Coleção de Lotes do cliente localizado</returns>
        IEnumerable<Lote> FindByCliente(String nomeCliente);

        /// <summary>
        /// Método efetua a busca por todos os lotes cadastrado em uma determinada data.
        /// </summary>
        /// <param name="data">Data usada pelo filtro</param>
        /// <returns>Lista de lotes em ordem alfabetica de nome filtrados pela data repassada. </returns>
        IEnumerable<Lote> FindByDateLote(DateTime data);

        /// <summary>
        /// Busca por um lote a partir do id repassado
        /// </summary>
        /// <param name="idLote">Id do lote a ser buscado</param>
        /// <returns>Lote do id repassado</returns>
        Lote FindById(Int64 idLote);

        void Dispose(bool disposing);
    }
}