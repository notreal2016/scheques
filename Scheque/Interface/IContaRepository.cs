﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Interface
{
    public interface IContaRepository : IUnitOfWorkRepository
    {
        /// <summary>
        /// Método exibe uma lista de todas as contas cadastradas.
        /// </summary>
        /// <returns>Lista de contas cadastradas</returns>
        List<Conta> AllContas();

        /// <summary>
        /// Método retorna uma conta a partir do id da conta cadastrada
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Conta FindContaById(Int64 id);

        /// <summary>
        /// Método busca uma conta cadastrada a partir dos dados da conta
        /// </summary>
        /// <param name="idbanco">Id do banco </param>
        /// <param name="agencia">Número da agência</param>
        /// <param name="conta">Número da conta</param>
        /// <returns>Conta localizada</returns>
        Conta FindContaByDados(String idbanco, String agencia, String conta, string operacao, int numeroDigitosCC = 6);

        /// <summary>
        /// Método busca todas as contas de um emitente a partir do Id do emitente
        /// </summary>
        /// <param name="id">Id do emitente</param>
        /// <returns>Lista de contas</returns>
        List<Conta> FindContaByIdProprietario(Int64 id);

        /// <summary>
        /// Método retorna todas as contas vinculadas ao documento do cpf ou cnpj do emitente
        /// </summary>
        /// <param name="documento">CPF ou CNPJ do emitente</param>
        /// <returns>Lista de contas cadastradas</returns>
        List<Conta> FindContasByDocEmitente(String documento);

        /// <summary>
        /// Método salva os dados de uma conta no banco de dados
        /// </summary>
        /// <param name="conta">Conta que terá os dados persistidos</param>
        void Save(Conta conta);

        /// <summary>
        /// Remove uma conta da base de dados a partir do Id da conta repassado
        /// </summary>
        /// <param name="_id">Id da conta</param>
        void Remove(Int64 _id);

        void Dispose(bool disposing);
        Conta FindContaByIdNoTrack(long idConta);
        Conta FindContaByDadosNoTrack(string idBanco, string agencia, string conta, string operacao, int numeroDigitosCC = 6);
    }
}