﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Service
{
    public interface ILoteService
    {
        /// <summary>
        /// Busca por negatvações pelo Id do Emitente do cheque 
        /// </summary>
        /// <param name="id">Id do Emitente</param>
        /// <returns>Lista de negativações encontradas</returns>
        List<Negativado> NegativadosByEmitente(long id);

        /// <summary>
        /// Busca por Negativados pelo CNPJ ou CPF do Emitente
        /// </summary>
        /// <param name="cpfcnpj">Documento CPF ou CNPJ a ser usado como filtro</param>
        /// <returns>Lista de Negativados</returns>
        List<Negativado> NegativadosByDoc(string cpfcnpj);

        /// <summary>
        /// Busca por negativações de uma conta especifica
        /// </summary>
        /// <param name="idBanco">Id da conta</param>
        /// <param name="agencia">Agencia</param>
        /// <param name="conta">Conta</param>
        /// <returns>Lista de negativações</returns>
        List<Negativado> NegativadosByDadosConta(string idBanco, string agencia, string conta);

        /// <summary>
        /// Busca por negativações de uma conta especifica pelo ID repassado
        /// </summary>
        /// <param name="id">id da conta</param>
        /// <returns>Retorna da lista</returns>
        List<Negativado> NegativadoByIdConta(long id);

        /// <summary>
        /// Método encarregado de validar e salvar os dados de um lote no banco de dados
        /// Não permitido lote com taxas diferentes da cadastradas ao cliente
        /// Não permitido taxa de juros zerada
        /// Não permitido lote sem cliente
        /// Não permitido lote com quantidade de documentos zero ou negativo
        /// </summary>
        /// <param name="lote">Lote cujos dados serão salvos no banco de dados</param>
        void Salvar(Lote lote);
        Cheque FindChequeById(long idCheque);

        /// <summary>
        /// Lista todos os lotes cadastrados no sistema
        /// </summary>
        /// <returns>Lista de lotes</returns>
        List<Lote> AllLotes();

        /// <summary>
        /// Busca lotes filtrados pelo nome ,ou parte dele, do cliente
        /// </summary>
        /// <param name="nome">Nome ou parte dele</param>
        /// <returns>Lista de lotes</returns>
        List<Lote> FindLoteByNomeCliente(String nome);

        /// <summary>
        /// Busca por lotes de uma determinada data
        /// </summary>
        /// <param name="data">Data da busca do lote</param>
        /// <returns></returns>
        List<Lote> FindByData(DateTime data);

        /// <summary>
        /// Lista todos os clientes cadastrados
        /// </summary>
        /// <returns>Lista todos os clientes da base de dados</returns>
        List<Cliente> AllClientes();

        /// <summary>
        /// Método apresenta uma lista contendo todos os banco cadastrados
        /// </summary>
        /// <returns>Lista de bancos cadastrados</returns>
        List<Banco> AllBancos();

        /// <summary>
        /// Busca por um banco a partir do código do banco febraban
        /// </summary>
        /// <param name="cobBancosText">Código do banco</param>
        /// <returns>Banco localizado</returns>
        Banco FindBancoByCodBanco(string cobBancosText);

        /// <summary>
        /// Busca por um cliente a partir do nome ou parte dele.
        /// </summary>
        /// <param name="nome">Nome ou parde dele</param>
        /// <returns>Lista de clientes localizados</returns>
        List<Cliente> FindClienteByNome(String nome);

        /// <summary>
        /// Busca seletiva de lote
        /// </summary>
        /// <param name="tipo">Tipo de busca</param>
        /// <param name="termo">Termo da busca</param>
        /// <returns>Lista de resultados</returns>
        Lote[] BuscaSelecionada(TipoBusca tipo, String termo);

        /// <summary>
        /// Busca por um lote a partir do id 
        /// </summary>
        /// <param name="id">Número do lote</param>
        /// <returns>Lote localizado</returns>
        Lote FindLoteById(Int64 id);

        /// <summary>
        /// Busca a partir de uma data
        /// </summary>
        /// <param name="data">Data a ser pesquisada</param>
        /// <returns>Lista de lotes por data</returns>
        List<Lote> FindLoteByData(String[] data);

        Conta FindContaByDados(String IdBanco, String agencia, String conta, string operacao, int numeroDigitosCC = 6);

        /// <summary>
        /// Busca o proprietário de uma conta a partir do CNPJ ou CPF do mesmo
        /// </summary>
        /// <param name="documento">CPF ou CNPJ do proprietario de uma conta</param>
        /// <returns></returns>
        Proprietario FindProprietarioByDocumento(String documento);

        /// <summary>
        /// Método retorna a quantidade de dias entre a dataInicial repassada e a atual 
        /// levando em consideração se é ou não feriado bem como se deve ou não 
        /// contar os fins de semana
        /// </summary>
        /// <param name="dataInicial"> Data incial do calculo</param>
        /// <param name="dataFinal">Data final do calculo</param>
        /// <param name="contaFimDeSemana">Considera fim de semana</param>
        /// <param name="contaFeriados">Considera feriados</param>
        /// <param name="diasMinimo">Conta tempo mínimo</param>
        /// <returns></returns>
        int ContaDias(DateTime dataInicial, DateTime dataFinal, bool contaFimDeSemana = true, bool contaFeriados = true, int diasMinimo = 0);

        /// <summary>
        /// Método verifica se uma data especifica está contido na lista de feriados 
        /// </summary>
        /// <param name="dt1">Data a ser verificada</param>
        /// <returns>Boelano de confirmação</returns>
        bool IsFeriado(DateTime dt1);

        /// <summary>
        /// Método efetua o calculo de juros simples ou compostos
        /// </summary>
        /// <param name="valor">Valor de prinicipal</param>
        /// <param name="taxa">Taxa aplicada ao mês</param>
        /// <param name="dias">Número de dias para o calculo</param>
        /// <param name="jusrosCompostos">Se o Juros é composto, default = false</param>
        /// <returns>Valor do juros para o período informado.</returns>
        Decimal CalculaJuros(decimal valor, decimal taxa, int dias, bool jusrosCompostos = false);

        /// <summary>
        /// Persiste os dados de uma conta no sistema de dados 
        /// </summary>
        /// <param name="conta">Conta cujos dados serão persistidos</param>
        void SalvaConta(Conta conta);

        /// <summary>
        /// Verifica se o cheque repassado já esta cadastrado no banco de dados.
        /// </summary>
        /// <param name="cheque">Chque que ira ser pesquisado</param>
        /// <returns>Resposta se foi localizado</returns>
        Boolean IsChequeCadastrado(Cheque cheque);

        /// <summary>
        /// Verifica se um cheque especifico já esta cadastrado
        /// </summary>
        /// <param name="idBanco">Id do Banco</param>
        /// <param name="agencia">Agencia da conta</param>
        /// <param name="conta">Conta corrente</param>
        /// <param name="operacao">Operação</param>
        /// <param name="numero">Número do cheque</param>
        /// <param name="idLote">Id do Lote que está o chque</param>
        /// <returns>Booleano de confirmação</returns>
        Boolean IsChequeCadastrado(string idBanco, string agencia, string conta, string operacao, string numero, long? idLote);

        /// <summary>
        /// Retorna informaçõe para alimentar um relatório de cheques 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Dictionary<String, object> RelatorioLote(Int64 id);

        /// <summary>
        /// Busca por todos os cheques de um mesmo emitente que estejam cadastradas no sistema 
        /// </summary>
        /// <param name="id">Id do emitente do cheque</param>
        /// <param name="naodepositados">Paramentro de filtragem se o cheque foi depositado ou não</param>
        /// <returns>Lista de cheques de um determinado emitente</returns>
        List<Cheque> FindAllChequesByIdEmitente(long id);

        void Dispose(bool disposing);
        /// <summary>
        /// Busca por um cliente que tenha o id referenciado
        /// </summary>
        /// <param name="id">Id do cliente</param>
        /// <returns>Cliente localizado</returns>
        Cliente FindClienteById(long id);
        Conta FindContaById(long id);
        Proprietario FindByDocs(string v);
        Conta FindContaByIdNoTrack(long idConta);
        Conta FindContaByDadosNoTrack(String IdBanco, String agencia, String conta, string operacao = "", int numeroDigitosCC = 6 );
    }
}