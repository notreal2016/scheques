﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Interface
{
    public interface IPromissoriaRepository: IUnitOfWorkRepository
    {
        /// <summary>
        /// Persiste os dados de uma promissoria no banco de dados
        /// </summary>
        /// <param name="promissoria">Promissória a ser persistida</param>
        void Save(Promissoria promissoria);

        /// <summary>
        /// Remove uma promissória do banco de dados apartir do id repassado
        /// </summary>
        /// <param name="id">Id da promissória a ser removida.</param>
        void Remove(Int64 id);

        /// <summary>
        /// Busca por uma promissória a partir do Id
        /// </summary>
        /// <param name="id">ID da promissoria</param>
        /// <returns></returns>
        Promissoria FindById(Int64 id);

        /// <summary>
        /// Método retorna uma lista de promissorias vencidas de um cliente apartir do ID repassado
        /// </summary>
        /// <param name="idCliente">id do cliente que procurará as promissórias vencidas</param>
        /// <returns></returns>
        List<Promissoria> FindVencidasByIdCliente(Int64 idCliente);

        /// <summary>
        /// Busca todas as promissorias vencidas 
        /// </summary>
        /// <returns></returns>
        List<Promissoria> FindAllVencidas();

        /// <summary>
        /// Busca por promissórias a vencer do cliente do id 
        /// </summary>
        /// <param name="idCliente">Id do cliente que será feito a busca</param>
        /// <returns>Lista de promissorias a vencer de um cliente</returns>
        List<Promissoria> FindAVecerByIdCliente(Int64 idCliente);

        /// <summary>
        /// Busca por todas as pormissoras a vencer
        /// </summary>
        /// <returns></returns>
        List<Promissoria> FindAllAVecer();

        /// <summary>
        /// Busca por promissorias do dia do cliente do id
        /// </summary>
        /// <param name="idCliente">Id do cliente a ser repassada</param>
        /// <returns></returns>
        List<Promissoria> FindDoDiaByIdCliente(Int64 idCliente);

        /// <summary>
        /// Busca por todas as promissórias vencendos no dia atual
        /// </summary>
        /// <returns></returns>
        List<Promissoria> FindAllDoDia();

        /// <summary>
        /// Bsuca por todas as promissórias do cliende do id
        /// </summary>
        /// <param name="idCliente">Id do cliente</param>
        /// <returns></returns>
        List<Promissoria> FindAllByIdCliente(Int64 idCliente);

        /// <summary>
        /// Busca por todas as promissórias de um cliente ainda não quitadas
        /// </summary>
        /// <param name="idCliente">Id do cliente</param>
        /// <returns></returns>
        List<Promissoria> FindAllByIdClienteAbertas(Int64 idCliente);

        /// <summary>
        /// Busca por todas as promissórias não quitadas
        /// </summary>
        /// <returns></returns>
        List<Promissoria> FindAllAbertas();

        /// <summary>
        /// Busca todas as promissorias cadastradas
        /// </summary>
        /// <returns></returns>
        List<Promissoria> FindAll();

        void Dispose(bool disposing);
    }
}