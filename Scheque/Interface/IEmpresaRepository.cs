﻿using Scheque.Entidade;

namespace Scheque.Interface
{
    public interface IEmpresaRepository: IUnitOfWorkRepository
    {
        /// <summary>
        /// Persiste os dados de uma emrpesa no banco de dados
        /// </summary>
        /// <param name="empresa">Empresa cujos dados serão persistidos</param>
        /// <returns>Empresa após salva</returns>
        Empresa Save(Empresa empresa);

        /// <summary>
        /// Busca por uma empresa pelo ID repassado
        /// </summary>
        /// <param name="id">Id da empresa</param>
        /// <returns>Empresa localizada ou null</returns>
        Empresa FindById(long id);

        void Dispose(bool disposing);
        void Dispose();
    }
}