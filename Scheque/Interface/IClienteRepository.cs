﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Entidade;

namespace Scheque.Interface
{
    public interface IClienteRepository : IUnitOfWorkRepository
    {
        /// <summary>
        /// Grava os dados de um cliente no banco de dados 
        /// </summary>
        /// <param name="cliente">Cliente cujo dados seram presistidos no banco de dados</param>
        void Save(Cliente cliente);

        /// <summary>
        /// Remove um cliente do banco de dados a partir do id do cliente repassado
        /// </summary>
        /// <param name="idCliente">Id do cliente a ser remvido da base de dados</param>
        void Remove(Int64 idCliente);

        /// <summary>
        /// Busca por todos os clientes cadastrado no banco de dados
        /// </summary>
        /// <returns>Retorna uma coleção de clientes em ordem alfabetica de nome</returns>
        IEnumerable<Cliente> FindAllClientes();

        IEnumerable<Cliente> FindByName(string nome);

        /// <summary>
        /// Busca um cliente no banco de dados a partir do Id repassado como paramentro
        /// </summary>
        /// <param name="id">Id do cliente a ser buscado</param>
        /// <returns>Cliente localizado</returns>
        Cliente FindById(Int64 id);

        //Cliente Refresh(Cliente cliente);
        //void RefreshAll();
        void Dispose(bool disposing);
    }
}
