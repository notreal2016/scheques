﻿using Scheque.Entidade;

namespace Scheque.Service
{
    public interface IEmpresaService
    {
        void Dispose(bool disposing);
        Empresa FindById(long id);
        Empresa Save(Empresa empresa);
    }
}