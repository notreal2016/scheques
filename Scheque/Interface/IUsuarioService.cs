﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Service
{
    public interface IUsuarioService 
    {
        /// <summary>
        /// Salva os dados de um usuário no banco de dados, verifica a validade dos dados do usuário 
        /// como nome e login.
        /// </summary>
        /// <param name="user">Usuário que terá os dados persistidos no banco de dados</param>
        void Save(Usuario user);

        /// <summary>
        /// Remove os dados de um usuário do banco de dados a partir do id do usuário
        /// </summary>
        /// <param name="id">Inteiro positivo maior que zero</param>
        void Remove(Int64 id);

        Usuario FindById(Int64 id);
        List<Usuario> FindByNome(String nome);
        Usuario LogaUsuario(String login, String senha);
        IEnumerable<Usuario> FindByAll();
        bool ValidaLogin(String login, long id = 0);
    }
}