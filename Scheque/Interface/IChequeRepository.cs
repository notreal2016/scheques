﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Interface
{
    public interface IChequeRepository : IUnitOfWorkRepository
    {
        void Save(Cheque cheque);
        void Remove(Int64 id);
        Cheque FindById(Int64 id);

        /// <summary>
        /// Busca por um cheque no banco de dados pelos paramentros repassados
        /// </summary>
        /// <param name="idBanco">Id do banco</param>
        /// <param name="agencia">Agencia</param>
        /// <param name="conta">Conta corrente</param>
        /// <param name="operacao">Operação</param>
        /// <param name="NCheque">Número do chque</param>
        /// <returns>Retorna o cheque localizado ou null para não localizado</returns>
        Cheque FindByDados(String idBanco, String agencia, String conta, String  operacao,  String NCheque);

        List<Cheque> FindAllChequesByIdConta(long idConta);
        List<Cheque> FindAll();

        /// <summary>
        /// Busca todos os cheques em condição de deposito do dia
        /// </summary>
        /// <returns>Lista de cheques</returns>
        List<Cheque> FindChequesDoDia();

        /// <summary>
        /// Busca por todos os cheques com condição de deposito
        /// </summary>
        /// <param name="inicio">Data inicial da busca</param>
        /// <param name="fim">Data final da busca</param>
        /// <returns>Lista de cheques a depositar</returns>
        List<Cheque> FindAllChequesADepositar(DateTime inicio, DateTime fim);

        /// <summary>
        /// Busca por todos os cheques a depositar do edmitente do id 
        /// </summary>
        /// <param name="idProprietario">Id do proprietários</param>
        /// <returns>Lista de cheques</returns>
        List<Cheque> FindChequesByProprietarioNoDep(Int64 idProprietario);

        void Dispose(bool disposing);
    }
}