﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Interface
{
    public interface IUsuarioRepository :  IUnitOfWorkRepository
    {
        /// <summary>
        /// Salva os dados de um usuário na base de dados
        /// </summary>
        /// <param name="usuario">Usuário a ter os dados salvos na base de dados</param>
        void Save(Usuario usuario);

        /// <summary>
        /// Remove um usuário do banco de dados
        /// </summary>
        /// <param name="idUsuario">Id do usuário</param>
        void Remove(Int64 idUsuario);

        /// <summary>
        /// Busca usuário por meio de login e sennha
        /// </summary>
        /// <param name="login"></param>
        /// <param name="senha"></param>
        /// <returns></returns>
        Usuario FindUserbyLoginSenha(String login, String senha);

        /// <summary>
        /// Busca usuário pelo id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Usuario FindById(Int64 id);

        /// <summary>
        /// Retorna uma lista de usuários a partir de parte do nome do mesmo
        /// </summary>
        /// <param name="nome">Nome usuário</param>
        /// <returns>Lista de usuarios</returns>
        List<Usuario> FindByNome(String nome);
        /// <summary>
        /// Busca por usuário que contém o login repassado
        /// </summary>
        /// <param name="login">Login a ser usado para busca</param>
        /// <returns>Usuário cadastrado</returns>
        Usuario FindByLogin(string login);
        /// <summary>
        /// Lista todos os usuários cadastrados
        /// </summary>
        /// <returns></returns>
        IEnumerable<Usuario> FindAll();

        
    }
}