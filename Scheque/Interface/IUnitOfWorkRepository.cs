﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace Scheque.Interface
{
    public interface IUnitOfWorkRepository : IDisposable
    {
        void InsertPersiste(IPersistenceBase entidade);
        void DeletePersiste(IPersistenceBase entidade);
        void UpdatePersiste(IPersistenceBase entidade);
        
    }
}
