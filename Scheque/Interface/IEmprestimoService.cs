﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Service
{
    public interface IEmprestimoService
    {
        /// <summary>
        /// Lista todos os clientes cadastrado em ordem alfabetica
        /// </summary>
        /// <returns></returns>
        List<Cliente> AllClientes();

        /// <summary>
        /// Salva os dados no banco de dados
        /// </summary>
        /// <param name="emprestimo"></param>
        void Save(Emprestimo emprestimo);

        /// <summary>
        /// Cria tabla de parcelas a partir do emprestimo repassado. Tabela pode ser do tipo Price ou SAC, sendo price como padrão
        /// </summary>
        /// <param name="emp"> Emprestimo</param>
        /// <param name="sac">Tabela SAC = padão false</param>
        /// <returns></returns>
        List<AbstracDocumento> CriaPromissorias(Emprestimo emp, bool sac = false, bool diaFixo = true);

        /// <summary>
        /// Cria a lista de cheques por meio da tabela SAC ou PRICE
        /// </summary>
        /// <param name="emp">Emprestimo que gerará as parcelas</param>
        /// <param name="sac">Define se aplica a tabela SAC, como padrão false</param>
        /// <param name="diaFixo">Se será criado com dia fixo ou intervalo de dias</param>
        /// <returns></returns>
        List<AbstracDocumento> CriaCheque(Emprestimo emp, bool sac = false, bool diaFixo = true);

        /// <summary>
        /// Cria uma promissor no processo de antecipação
        /// </summary>
        /// <param name="valor">Valor da parcela</param>
        /// <returns>Promissora criada</returns>
        Promissoria CriaPromissoriaAntecipacao(decimal valor);

        /// <summary>
        /// Atualizar os valores de uma parcela de um emprestimo do tipo antecipado.
        /// </summary>
        /// <param name="pro">Promissoria a ser atualizada</param>
        /// <param name="vencimento">Vencimento</param>
        /// <returns></returns>
        Promissoria AtualizaPromissoraAntecipacao(Promissoria pro, DateTime vencimento);

        /// <summary>
        /// Retorna um emprestimo por meio do id repassado.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Emprestimo FindById(Int64 id);

        /// <summary>
        /// Busca por clientes que contenha o nome repassado no nome
        /// </summary>
        /// <param name="nome">Termo de busca</param>
        /// <returns>Lista de clientes</returns>
        List<Cliente> FindClienteByNome(string nome);

        /// <summary>
        /// Busca por um banco apartir do cod dele repassado como paramentro
        /// </summary>
        /// <param name="codBanco">Codigo do banco</param>
        /// <returns>Banco localizado</returns>
        Banco FindBancoByCodBanco(string codBanco);

        /// <summary>
        /// Busca por uma conta baseado nos dados repassados como parametro
        /// </summary>
        /// <param name="idBanco">Id do Banco</param>
        /// <param name="agcencia">Agencia</param>
        /// <param name="conta">Conta</param>
        /// <returns>Conta localizado ou null quando não localizado</returns>
        Conta FindContaByDados(string idBanco, string agcencia, string conta, string operacao, int NumeroDigitosCC=6);

        /// <summary>
        /// Metodo busca por um emitente/proprietario apartir do documento repassado
        /// </summary>
        /// <param name="doc">Documento para busca</param>
        /// <returns>Emitente/Proprietário da do documento</returns>
        Proprietario FindProprietarioByDocumentos(String doc);

        /// <summary>
        /// Método busca por todos os bancos cadastrados no bando de dados
        /// </summary>
        /// <returns>Lista de bancos</returns>
        List<Banco> FindAllBancos();

        /// <summary>
        /// Salva os dados de uma conta no banco de dados
        /// </summary>
        /// <param name="conta">Dados da conta</param>
        void SaveConta(Conta conta);

        /// <summary>
        /// Localiza todo os emprestimos de um cliente pelo nome repassado
        /// </summary>
        /// <param name="termo">Nome do cliente que será buscado emprestimos</param>
        /// <returns>Lista de emprestimos</returns>
        List<Emprestimo> FindEmpByNomeCliente(string termo);

        /// <summary>
        /// Remove o emprestimo do banco de dados
        /// </summary>
        /// <param name="emprestimo">Empresimo a ser removido</param>
        void Remove(Emprestimo emprestimo);

        /// <summary>
        /// Montas todas as promissorias de um emprestimo
        /// </summary>
        /// <param name="id">Id do emprestimo</param>
        /// <returns></returns>
        Dictionary<String, object> DadosPromissorias(Int64 id);
    }
}