﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Entidade;

namespace Scheque.Interface
{
    public interface IBancoRepository : IUnitOfWorkRepository
    {
        /// <summary>
        /// Retorna uma lista com todos os bancos cadastrado
        /// </summary>
        /// <returns>Icolection de Bancos</returns>
        IList<Banco> AllBancos();

        /// <summary>
        /// Retorna o banco a partir do código do banco repassado
        /// </summary>
        /// <param name="codBanco">Codigo do banco</param>
        /// <returns>Banco cadastrado</returns>
        Banco FindByCodBanco(String codBanco);

        /// <summary>
        /// Salva os dados de um banco no banco de dados
        /// </summary>
        /// <param name="banco">Banco que terá os dados cadastrados</param>
        void Save(Banco banco);

        /// <summary>
        /// Atualiza os dados de um banco no banco de dados 
        /// </summary>
        /// <param name="banco">Banco cujos dados serão atualizados</param>
        void Update(Banco banco);
        

        /// <summary>
        /// Remove os dados de um banco no banco de dados
        /// </summary>
        /// <param name="codBanco">Código do banco que será removido do banco de dados.</param>
        void Remove(String codBanco);
        
        
    }
}
