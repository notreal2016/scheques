﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Interface
{
    public interface ICalendarioRepository :IUnitOfWorkRepository
    {
        /// <summary>
        /// Método persiste os dados de uma data no calendário no banco de dados.
        /// </summary>
        /// <param name="calendario">Data no calendário</param>
        void Save(Calendario calendario);

        /// <summary>
        /// Método retorna todas as datas do calendário
        /// </summary>
        /// <returns>Lista de datas de calendario</returns>
        IEnumerable<Calendario> AllCalendarios();

        /// <summary>
        /// Busca por uma data no banco de calendário a partir dos paramentros repassados.
        /// </summary>
        /// <param name="dia">Dia do calendário</param>
        /// <param name="mes">Mês do calendário</param>
        /// <returns>Data localizada no calendário</returns>
        Calendario FindData(int dia, int mes);

        /// <summary>
        /// Remove uma data calendario da base de dados a partir do id repassado
        /// </summary>
        /// <param name="id">Id da data calendário a ser removido da base.</param>
        void Remove(Int64 id);

        /// <summary>
        /// Busca por datas que estejam entre o mês e ano repassado
        /// </summary>
        /// <param name="mes">Mês do filtro</param>
        /// <param name="ano">Ano do Filtro</param>
        /// <returns>Lista de Calendarios</returns>
        List<Calendario> CalendarioMes(string mes, string ano);

        /// <summary>
        /// Busca por datas de um determinado ano
        /// </summary>
        /// <param name="ano">Ano para o filtro</param>
        /// <returns>Lista de calendarios</returns>
        List<Calendario> CalendarioAno(string ano);

        /// <summary>
        /// Busca por uma data especifica , dia, mês e ano
        /// </summary>
        /// <param name="dia">Dia do filtro</param>
        /// <param name="mes">Mês do filtro</param>
        /// <param name="ano">Ano do filtro</param>
        /// <returns>Data especifica ou null</returns>
        Calendario CalendarioDia(string dia, string mes , string ano);

        Calendario FindById(long id);
        void Dispose(bool disposing);
    }
}