﻿using System.Collections.Generic;
using Scheque.Entidade;
using Scheque.EntidadesFind;

namespace Scheque.Service
{
    public interface IConsultaChequeService
    {
        Cheque Cheque { get; }
        ConsultaEmitente Emitente { get; }
        DadosConta DadosConta { get; }
        List<MovimentoCheque> Movimento { get; }

        /// <summary>
        /// Adiciona um movimento ao cheque;
        /// </summary>
        /// <param name="movimento"></param>
        void AddMovimento(MovimentoCheque movimento);
    }
}