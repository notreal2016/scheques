﻿using System;
using Scheque.Entidade;

namespace Scheque.Service
{
    public interface IProprietarioService
    {
        Proprietario FindByDocs(String doc);
        void Save(Proprietario proprietario);
        void Remove(Int64 id);
        Proprietario FindById(Int64 id);
        void Dispose(bool disposing);
    }
}