﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Interface
{
    public interface IRepositoryMovimentoDoc: IUnitOfWorkRepository
    {
        /// <summary>
        /// Metodo adiciona a base de dados vários documentos de uma unica vez.
        /// </summary>
        /// <param name="documentos"></param>
        void IncludeAll(List<MovimentoDocumento> documentos);

        /// <summary>
        /// Método encarregado de persistir os dados de um movimento de documento no banco de dados
        /// </summary>
        /// <param name="movimento">Movimento que será persistido</param>
        void Save(MovimentoDocumento movimento);

        List<MovimentoCheque> FindByMovCh(long idCheque);

        /// <summary>
        /// Método remove um movimento do banco de dados apartir do id repassado
        /// </summary>
        /// <param name="id">id do movimento a ser removido.</param>
        void Remove(Int64 id);

        /// <summary>
        /// Busca por um movimento apartir do ID repassado
        /// </summary>
        /// <param name="id">Id do momento a ser buscado</param>
        /// <returns>Retorna um MovimentoDocumento pelo Id</returns>
        MovimentoDocumento FindById(Int64 id);

        /// <summary>
        /// Busca por todos os movimentos de um documento repassado pelo seu id
        /// </summary>
        /// <param name="idCheque">id do documeto que será pesquisado</param>
        /// <returns>Lista de movimentos do documento repassado</returns>
        List<MovimentoDocumento> FindByMovDoc(Int64 idCheque);

        /// <summary>
        /// Busca por movimentos de um documento a partir do id e intervalo de datas repassado
        /// </summary>
        /// <param name="idCheque">Id do cheque</param>
        /// <param name="inicio">Data inicial</param>
        /// <param name="fim">Data final</param>
        /// <returns>Lista de movimento</returns>
        List<MovimentoDocumento> FindByMovDocByIntevalo(Int64 idCheque, DateTime inicio, DateTime fim);

        void Dispose(bool disposing);
    }
}