﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Service
{
    public interface IClienteService
    {
        /// <summary>
        /// Salva os dados de um cliente 
        /// </summary>
        /// <param name="cliente">Cliente cujo dados serão salvos</param>
        void Save(Cliente cliente);

        /// <summary>
        /// Exclui o cliente no sistema
        /// </summary>
        /// <param name="cliente">Cliente cujo dados serão excluido</param>
        void Exclir(Cliente cliente);

        /// <summary>
        /// Busca por um cliente apartir do id repassado
        /// </summary>
        /// <param name="id">Id do cliente sendo positivo e maior que zero</param>
        /// <returns>Cliente localizado</returns>
        Cliente FindById(Int64 id);

        //Cliente Refresh(Cliente c);

        /// <summary>
        /// Método busca por todos os clientes cadastrados
        /// </summary>
        /// <returns>Retorna uma lista de clientes</returns>
        List<Cliente> AllClientes();

        /// <summary>
        /// Método efetua a busca de clientes pelo nome repassado como paramentro
        /// </summary>
        /// <param name="nome">Nome de cliente a ser buscado</param>
        /// <returns>Lista de clientes</returns>
        List<Cliente> FindByName(String nome);

        void Dispose(bool disposing);
    }
}