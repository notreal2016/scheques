﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Interface
{
    public interface IEmprestimoRepository: IUnitOfWorkRepository
    {
        /// <summary>
        /// Persiste os dados de um emprestimo no banco de dados
        /// </summary>
        /// <param name="emprestimo">Emprestimo cujo dados serão persistidos</param>
        void Save(Emprestimo emprestimo);

        /// <summary>
        /// Remove um emprestimo a partir do Id repassado
        /// </summary>
        /// <param name="id">Id do emprestimo</param>
        void Remove(Int64 id);

        /// <summary>
        /// Busca por um emprestimo a partir do id repassado
        /// </summary>
        /// <param name="id">Id cujo emprestimo será buscado</param>
        /// <returns>Emprestimo localizado</returns>
        Emprestimo FindById(Int64 id);

        /// <summary>
        /// Busca lista os emprestimos de um cliente podendo ser todos ou só os não quitados
        /// </summary>
        /// <param name="nomeCliente">Nome ou parte do nome do cliente</param>
        /// <param name="naoQuitados">Se deseja só o não quitados, como default false</param>
        /// <returns>Lista de emprestimos</returns>
        List<Emprestimo> FindByIdCliente(Int64 idCliente, bool naoQuitados = false);

        /// <summary>
        /// Busca por emprestimos de um determinado cliente.
        /// </summary>
        /// <param name="nome">Nome do cliente ou parte dele para busca</param>
        /// <returns>Lista de clientes</returns>
        List<Emprestimo> FindByNomeCliente(String nome);

        /// <summary>
        /// Lista todos os emprestimos aplicados em um intervalo de data 
        /// </summary>
        /// <param name="inicio">Data inicial</param>
        /// <param name="fim">Data final</param>
        /// <returns>Lista de emprestimos</returns>
        List<Emprestimo> FindByDateEmprestimo(DateTime inicio, DateTime fim);

        /// <summary>
        /// Exibe todos os emprestimos do banco de dados
        /// </summary>
        /// <returns></returns>
        List<Emprestimo> AllEmprestimos();

        void Dispose(bool disposing);
    }
}