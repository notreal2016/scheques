﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Interface
{
    public interface IProprietarioRepoitory: IUnitOfWorkRepository
    {
        void Save(Proprietario proprietario);
        Proprietario FindByDocumento(String documento);
        Proprietario FindById(Int64 id);
        void Remove(Int64 id);
        void Dispose(bool disposing);
        List<String> AllClientes(string CNPJCPF);
    }
}