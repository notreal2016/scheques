﻿using System;
using System.Collections.Generic;
using Scheque.Entidade;

namespace Scheque.Interface
{
    public interface IRepositoryNegativado : IUnitOfWorkRepository
    {
        /// <summary>
        /// Persiste os dados de uma negativação no banco de dados 
        /// </summary>
        /// <param name="negativado">Item a ser negativado</param>
        /// <returns>negativação após a persistencia</returns>
        Negativado Save(Negativado negativado);

        /// <summary>
        /// Remove uma negativação do banco de dados
        /// </summary>
        /// <param name="id">Id da negativação a ser removida</param>
        void Remove(long id);

        /// <summary>
        /// Busca por uma negativação pelo ID repassado
        /// </summary>
        /// <param name="id">Id da negativação</param>
        /// <returns>Negativação localizada ou null</returns>
        Negativado FindById(long id);

        /// <summary>
        /// Busca por todas as negativas de um emitente pelo id repassado
        /// </summary>
        /// <param name="id">Id do Emitente</param>
        /// <returns>Lista de Negativações</returns>
        List<Negativado> FindNegativadoByEmitente(long id);

        /// <summary>
        /// Busca por negativação de uma conta pelo Id da Conta
        /// </summary>
        /// <param name="id">Id da conta</param>
        /// <returns>Lista de negatvações</returns>
        List<Negativado> FindNegativadoByConta(long id);

        /// <summary>
        /// Busca por negativações de clientes pelo CPF ou CNPJ
        /// </summary>
        /// <param name="doc">CPF ou CNPJ </param>
        /// <returns>Lista de negativações</returns>
        List<Negativado> FindNegativadoByDoc(String doc);

        /// <summary>
        /// Busca negativações de uma conta pelos dados da conta 
        /// </summary>
        /// <param name="idBanco">Id do banco da conta </param>
        /// <param name="agencia">Agencia da conta </param>
        /// <param name="conta">Conta </param>
        /// <returns>Lista de negativações</returns>
        List<Negativado> FinNegativadosByDadosConta (String idBanco, String agencia, String conta);

        void Dispose(bool disposing);
    }
}