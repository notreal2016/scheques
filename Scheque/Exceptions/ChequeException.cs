﻿using System;
using System.Runtime.Serialization;

namespace Scheque.Service
{
    [Serializable]
    internal class ChequeException : Exception
    {
        public ChequeException()
        {
        }

        public ChequeException(string message) : base(message)
        {
        }

        public ChequeException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ChequeException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}