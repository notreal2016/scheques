﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Scheque.Repository
{
    [Serializable]
    internal class ProprietarioException : Exception
    {
        public ProprietarioException()
        {
        }

        public ProprietarioException(string message) : base(message)
        {
        }

        public ProprietarioException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ProprietarioException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
