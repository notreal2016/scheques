﻿using System;
using System.Runtime.Serialization;

namespace Scheque.Repository
{
    [Serializable]
    internal class BancoException : Exception
    {
        public BancoException()
        {
        }

        public BancoException(string message) : base(message)
        {
        }

        public BancoException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BancoException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}