﻿using System;
using System.Runtime.Serialization;

namespace Scheque.Repository
{
    [Serializable]
    internal class LoteException : Exception
    {
        public LoteException()
        {
        }

        public LoteException(string message) : base(message)
        {
        }

        public LoteException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected LoteException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}