﻿using System;
using System.Runtime.Serialization;

namespace Scheque.Repository
{
    [Serializable]
    internal class UsuarioException : Exception
    {
        public UsuarioException()
        {
        }

        public UsuarioException(string message) : base(message)
        {
        }

        public UsuarioException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected UsuarioException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}