﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Service
{
    /// <summary>
    /// Classe de persistencia de negativações
    /// </summary>
    public class RepositoryNegativado : IDisposable, IRepositoryNegativado
    {
        private IScContext _context;
        private Boolean disposed = false;
        private readonly IUnitOfWork _unitOfWork;

        ~RepositoryNegativado()
        {
            Dispose(false);
        }

        public RepositoryNegativado(IScContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }
        /// <summary>
        /// Persiste os dados de uma negativação no banco de dados 
        /// </summary>
        /// <param name="negativado">Item a ser negativado</param>
        /// <returns>negativação após a persistencia</returns>
        public Negativado Save(Negativado negativado)
        {
            if (negativado.Id == 0)
            {
                _unitOfWork.RegistraNovo(negativado, this);
            }
            else
            {
                _unitOfWork.RegistraAlterado(negativado, this);
            }

            return negativado;
            
        }
        /// <summary>
        /// Remove uma negativação do banco de dados
        /// </summary>
        /// <param name="id">Id da negativação a ser removida</param>
        public void Remove(long id)
        {
            Negativado negativado = FindById(id);
            _unitOfWork.RegistraRemovido(negativado, this);
            
        }

        /// <summary>
        /// Busca por uma negativação pelo ID repassado
        /// </summary>
        /// <param name="id">Id da negativação</param>
        /// <returns>Negativação localizada ou null</returns>
        public Negativado FindById(long id)
        {
            var result = _context.Negativados.Include(n => n.Conta).Where(n1 => n1.Id == id);
            return result.Count() > 0 ? result.First() : null; //_context.Negativados.Find(id);
        }
        /// <summary>
        /// Busca por todas as negativas de um emitente pelo id repassado
        /// </summary>
        /// <param name="id">Id do Emitente</param>
        /// <returns>Lista de Negativações</returns>
        public List<Negativado> FindNegativadoByEmitente(long id)
        {
            var result = _context.Negativados.Include(n => n.Conta).Where(n1 => n1.Id == id).ToList();
            return result;
        }
        /// <summary>
        /// Busca por negativação de uma conta pelo Id da Conta
        /// </summary>
        /// <param name="id">Id da conta</param>
        /// <returns>Lista de negatvações</returns>
        public List<Negativado> FindNegativadoByConta(long id)
        {
            return _context.Negativados.Include(n => n.Conta).Where(n1 => n1.Conta.Id == id).ToList();
            //return _context.Negativados.Where( n => n.Conta.Id == id).ToList();
        }
        /// <summary>
        /// Busca por negativações de clientes pelo CPF ou CNPJ
        /// </summary>
        /// <param name="doc">CPF ou CNPJ </param>
        /// <returns>Lista de negativações</returns>
        public List<Negativado> FindNegativadoByDoc(String doc)
        {
            return _context.Negativados.Include(n => n.Conta).Where(n1 => n1.Conta.Proprietario.CpfCNPJ.Equals(doc))
                .ToList();
            //return _context.Negativados.Where( n => n.Conta.Proprietario.CpfCNPJ.Equals(doc)).ToList();
        }
        /// <summary>
        /// Busca negativações de uma conta pelos dados da conta 
        /// </summary>
        /// <param name="idBanco">Id do banco da conta </param>
        /// <param name="agencia">Agencia da conta </param>
        /// <param name="conta">Conta </param>
        /// <returns>Lista de negativações</returns>
        public List<Negativado> FinNegativadosByDadosConta (String idBanco, String agencia, String conta)
        {
            return _context.Negativados.Include(n1 => n1.Conta).Where(n =>
                n.Conta.IdBanco.Equals(idBanco) && n.Conta.Agencia.Equals(agencia) &&
                n.Conta.ContaCorrente.Equals(conta)).ToList();
            
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                        _context = null;
                    }
                }
            }
            disposed = true;
        }

        public void InsertPersiste(IPersistenceBase entidade)
        {
            _context.Negativados.Add((Negativado) entidade);
            _context.SaveChanges();
        }

        public void DeletePersiste(IPersistenceBase entidade)
        {
            _context.Negativados.Remove((Negativado) entidade);
            _context.SaveChanges();
        }

        public void UpdatePersiste(IPersistenceBase entidade)
        {
            using(IScContext c = new ScContext())
            {
                Negativado neg = (Negativado)entidade;
                c.Negativados.Attach(neg);
                c.Entry(neg).State = EntityState.Modified;
                c.SaveChanges();
            }
            
        }
    }
}