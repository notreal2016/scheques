﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Repository
{
    /// <summary>
    /// Classe encarregada de persistir os dados de emprestimo no banco de dados
    /// </summary>
    public class EmprestimoRepository: IDisposable, IEmprestimoRepository
    {
        private IScContext _context;
        private bool disposed = false;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Cria um objeto de persistencia de dados de emprestimo
        /// </summary>
        /// <param name="context">Conexão dos dados</param>
        public EmprestimoRepository(IScContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Persiste os dados de um emprestimo no banco de dados
        /// </summary>
        /// <param name="emprestimo">Emprestimo cujo dados serão persistidos</param>
        public void Save(Emprestimo emprestimo)
        {
            if (emprestimo.Id == 0)
            {
                _unitOfWork.RegistraNovo(emprestimo, this);
            }
            else
            {
                _unitOfWork.RegistraAlterado(emprestimo, this);
            }
        }
        /// <summary>
        /// Remove um emprestimo a partir do Id repassado
        /// </summary>
        /// <param name="id">Id do emprestimo</param>
        public void Remove(Int64 id)
        {
            Emprestimo em = FindById(id);
            //_context.Emprestimos.Remove(em);
            _unitOfWork.RegistraRemovido(em, this);

        }
        /// <summary>
        /// Busca por um emprestimo a partir do id repassado
        /// </summary>
        /// <param name="id">Id cujo emprestimo será buscado</param>
        /// <returns>Emprestimo localizado</returns>
        public Emprestimo FindById(Int64 id)
        {
            var result = _context.Emprestimos.Include(e => e.Cliente).Include(e1 => e1.Documentos)
                .Where(e2 => e2.Id == id);
            return result.Count() > 0? result.First(): null;
        }

        /// <summary>
        /// Busca lista os emprestimos de um cliente podendo ser todos ou só os não quitados
        /// </summary>
        /// <param name="nomeCliente">Nome ou parte do nome do cliente</param>
        /// <param name="naoQuitados">Se deseja só o não quitados, como default false</param>
        /// <returns>Lista de emprestimos</returns>
        public List<Emprestimo> FindByIdCliente(Int64 idCliente, bool naoQuitados = false)
        {
            //String SQL = "";
            List<Emprestimo> emprestimos = null;
            if (naoQuitados)
            {
                //SQL = String.Format("Select * from emprestimo left join cliente on cliente.id = emprestimo.idCliente where cliente.Id ={0} and quitado = false;", idCliente);
                emprestimos =  _context.Emprestimos.Include(e => e.Cliente).Include(e1 => e1.Documentos)
                    .Where(e2 => e2.Quitado == false && e2.IdCliente == idCliente).ToList();
            }
            else
            {
                emprestimos = _context.Emprestimos.Include(e => e.Cliente).Include(e1 => e1.Documentos)
                    .Where(e2 => e2.IdCliente == idCliente).ToList();
                //SQL = String.Format("Select * from emprestimo left join cliente on cliente.id = emprestimo.idCliente where cliente.id = {0};", idCliente);
            }

            return emprestimos; //_context.Emprestimos.SqlQuery(SQL).ToList();
        }

        /// <summary>
        /// Busca por emprestimos de um determinado cliente.
        /// </summary>
        /// <param name="nome">Nome do cliente ou parte dele para busca</param>
        /// <returns>Lista de clientes</returns>
        public List<Emprestimo> FindByNomeCliente(String nome)
        {
            //String SQL = String.Format("select * from emprestimo left join cliente on cliente.id = emprestimo.IdCliente where cliente.nome like '%{0}%';", nome);
            var result = _context.Emprestimos.Include(e => e.Cliente).Include(e1 => e1.Documentos)
                .Where(e2 => e2.Cliente.Nome.Contains(nome)).ToList();
            return result; //_context.Emprestimos.SqlQuery(SQL).ToList();
        }
        /// <summary>
        /// Lista todos os emprestimos aplicados em um intervalo de data 
        /// </summary>
        /// <param name="inicio">Data inicial</param>
        /// <param name="fim">Data final</param>
        /// <returns>Lista de emprestimos</returns>
        public List<Emprestimo> FindByDateEmprestimo(DateTime inicio, DateTime fim)
        {
            /*String SQL =
                String.Format(
                    "Select * from emprestimo where datacontrato between '{0:yyyy-MM-dd} 00:00:00' and '{1:yyyy-MM-dd} 23:59:59';",inicio, fim);

            return _context.Emprestimos.SqlQuery(SQL).ToList();*/
            var result = _context.Emprestimos.Include(e => e.Cliente).Include(e1 => e1.Documentos)
                .Where(e2 => e2.DataContrato >= inicio && e2.DataContrato <= fim).ToList();
            return result;
        }

        /// <summary>
        /// Exibe todos os emprestimos do banco de dados
        /// </summary>
        /// <returns></returns>
        public List<Emprestimo> AllEmprestimos()
        {
            return _context.Emprestimos.Include(e => e.Cliente).Include(e1 => e1.Documentos).ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                        _context = null;
                    }
                }
            }
            disposed = true;
        }

        ~EmprestimoRepository(){
            Dispose(false);
        }

        public void InsertPersiste(IPersistenceBase entidade)
        {
            Emprestimo emprestimo = (Emprestimo)entidade;
            _context.Emprestimos.Add(emprestimo);
            _context.SaveChanges();
        }

        public void DeletePersiste(IPersistenceBase entidade)
        {
            Emprestimo emprestimo = (Emprestimo)entidade;
            _context.Emprestimos.Remove(emprestimo);
            _context.SaveChanges();

        }

        public void UpdatePersiste(IPersistenceBase entidade)
        {
            Emprestimo emprestimo = (Emprestimo) entidade;
            _context.Emprestimos.Attach(emprestimo);
            _context.Entry(emprestimo).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
