﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Repository
{
    /// <summary>
    /// Classe encarregada da persistencia dos dados de um lote no banco de dados além das pesquisa de dados
    /// </summary>
    public sealed class LoteRepository : IDisposable, ILoteRepository
    {
        private IScContext _context;
        private bool disposed = false;
        private readonly IUnitOfWork _unitOfWork;
        /// <summary>
        /// Controe um objeto do tipo LotesContext encaregado da persistencia dos dados de um lote no banco de dados
        /// </summary>
        /// <param name="context">Objeto de contexto de conexão com o banco de dados</param>
        public LoteRepository(IScContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }

        public void Save(Lote lote)
        {
            if (lote.Id == 0)
            {
                _unitOfWork.RegistraNovo(lote, this);
            }
            else
            {
                _unitOfWork.RegistraAlterado(lote, this);
            }
            
        }
        /// <summary>
        /// Remove um lote e seus cheques do banco de dados
        /// </summary>
        /// <param name="idLote">Id do lote a ser removido</param>
        public void Remove(Int64 idLote)
        {
            Lote lote = _context.Lotes.Find(idLote);
            _unitOfWork.RegistraRemovido(lote, this);
          
        }

        public IEnumerable<Lote> FindAlLotes()
        {
            var result = _context.Lotes.Include(l => l.Cliente).Include(l1 => l1.Cheques).OrderBy(l2 => l2.Id).ToList();
            return result; 
        }
        /// <summary>
        /// Busca por um ou mais lotes cadastrado a um cliente cujo nome ou parte dele foi repassado como paramentro
        /// </summary>
        /// <param name="nomeCliente">Nome ou parte do nome do cliente que será usado para a pesquisa.</param>
        /// <returns>Coleção de Lotes do cliente localizado</returns>
        public IEnumerable<Lote> FindByCliente(String nomeCliente)
        {
            
            var result = _context.Lotes.Include(l => l.Cliente)
                .Include(l1 => l1.Cheques)
                .Where(l2 => l2.Cliente.Nome.ToLower().Contains(nomeCliente.ToLower())).OrderBy(l3 => l3.Cliente.Nome).ToList();
            return result;
        }
        /// <summary>
        /// Método efetua a busca por todos os lotes cadastrado em uma determinada data.
        /// </summary>
        /// <param name="data">Data usada pelo filtro</param>
        /// <returns>Lista de lotes em ordem alfabetica de nome filtrados pela data repassada. </returns>
        public IEnumerable<Lote> FindByDateLote(DateTime data)
        {
            var result = _context.Lotes.Include(l => l.Cliente).Include(l1 => l1.Cheques).ToList()
                .Where(l2 => l2.DataLote >= data && l2.DataLote <= data).OrderBy(l3 =>l3.Cliente.Nome).ToList();
            return result;
        }

        /// <summary>
        /// Busca por um lote a partir do id repassado
        /// </summary>
        /// <param name="idLote">Id do lote a ser buscado</param>
        /// <returns>Lote do id repassado</returns>
        public Lote FindById(Int64 idLote)
        {
            var result = _context.Lotes
                .Include(l => l.Cliente)
                .Include(l1 => l1.Cheques)
                .Include("Cheques.ContaCheque.Banco")
                .Include("Cheques.ContaCheque.Proprietario")
                .Include("Cheques.ContaCheque")
                .ToList().Where(l2 => l2.Id == idLote).ToList();
            return result.Count > 0? result[0] : null;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                        _context = null;
                    }
                }
            }
            disposed = true;
        }

        public void InsertPersiste(IPersistenceBase entidade)
        {
            try
            {
                using(IScContext context = new ScContext())
                {
                    Lote lote = VinculaReferencias((Lote)entidade);
                    /*lote.Cheques.ForEach(c => {
                        c.ContaCheque = _context.Contas.Find(c.IdConta);
                    });*/
                    context.Lotes.Add(lote);
                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {

                throw new Exception(e.Message);
            }
        }

        private Lote VinculaReferencias(Lote lote)
        {
            var idCliente = lote.IdCliente;
            lote.Cliente = null;
            lote.IdCliente = idCliente;
            lote.Cheques.ForEach(c =>
            {
                if (c.ContaCheque != null)
                {
                    var idConta = c.ContaCheque.Id;
                    c.ContaCheque = null;
                    c.IdConta = idConta;
                }
                //_context.Entry(c).State = EntityState.Modified;
            });
            
            return lote;
        }
        public void DeletePersiste(IPersistenceBase entidade)
        {
            Lote lote = (Lote)entidade;
            _context.Lotes.Remove(lote);
            _context.SaveChanges();
        }

        public void UpdatePersiste(IPersistenceBase entidade)
        {
            using(IScContext context = new ScContext())
            {
                Lote lote = (Lote)entidade;
                lote.Cheques.ForEach(c => {
                    var idconta = c.IdConta;
                    c.ContaCheque = null;
                    c.IdConta = idconta;
                    if (c.Id == 0)
                    {
                        _context.Entry(c).State = EntityState.Added;
                    }
                });
                //_context.ChangeTracker.DetectChanges();
                _context.Entry(lote.Cliente).State = EntityState.Unchanged;
                _context.Entry(lote).State = EntityState.Modified;
                _context.SaveChanges();
            }
                
        }
    }
}
