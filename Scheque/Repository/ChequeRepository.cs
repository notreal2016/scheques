﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Repository
{
    public class ChequeRepository : IDisposable, IChequeRepository
    {
        private IScContext _context;
        private Boolean disposed = false;
        private readonly IUnitOfWork _unitOfWork;

        public ChequeRepository(IScContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }

        public void Save(Cheque cheque)
        {
     
            if (cheque.Id == 0)
            {
                _unitOfWork.RegistraNovo(cheque, this);               
            }
            else
            {
                _unitOfWork.RegistraAlterado(cheque, this);               
            }
        }
        
        public void Remove(Int64 id)
        {
            Cheque ch = FindById(id);
            _unitOfWork.RegistraRemovido(ch, this);

        }

        public Cheque FindById(Int64 id)
        {
            var result =  _context.Cheques.Include("ContaCheque.Banco")
                .Include("ContaCheque.Proprietario")
                .Include("ContaCheque")
                .Where(c3 => c3.Id == id).ToList();

            return result.Count() > 0 ? result[0] : null;
        }

        /// <summary>
        /// Busca por um cheque no banco de dados pelos paramentros repassados
        /// </summary>
        /// <param name="idBanco">Id do banco</param>
        /// <param name="agencia">Agencia</param>
        /// <param name="conta">Conta corrente</param>
        /// <param name="operacao">Operação</param>
        /// <param name="NCheque">Número do chque</param>
        /// <returns>Retorna o cheque localizado ou null para não localizado</returns>
        public Cheque FindByDados(String idBanco, String agencia, String conta, String  operacao ,  String NCheque)
        {
            String SQL = String.Format("select documento.* from documento left join conta on documento.IdConta = conta.Id where conta.IdBanco = '{0}' and " +
                                       "conta.ContaCorrente = '{1}' and conta.Agencia = '{2}'  and documento.NumeroCheque = '{4}'",
            idBanco, conta, agencia, operacao, NCheque);
            //var result = _context.Cheques.SqlQuery(SQL);
            var result = _context.Cheques.Include(c => c.ContaCheque.Banco)
                .Include(c1 => c1.ContaCheque.Proprietario)
                .Include(c2 => c2.ContaCheque).Where(c3 =>
                    c3.ContaCheque.ContaCorrente == conta && c3.ContaCheque.IdBanco == idBanco &&
                    c3.ContaCheque.Agencia == agencia  &&
                    c3.NumeroCheque == NCheque);
            return (result.Count() == 0) ? null : result.First();
        }

        public List<Cheque> FindAll()
        {
            return _context.Cheques.Include(c => c.ContaCheque.Banco)
                .Include(c1 => c1.ContaCheque.Proprietario)
                .Include(c2 => c2.ContaCheque).ToList();
        }
        /// <summary>
        /// Busca todos os cheques em condição de deposito do dia
        /// </summary>
        /// <returns>Lista de cheques</returns>
        public List<Cheque> FindChequesDoDia()
        {

            return FindAllChequesADepositar(DateTime.Now, DateTime.Now);
        }
        
        /// <summary>
        /// Busca por todos os cheques com condição de deposito
        /// </summary>
        /// <param name="inicio">Data inicial da busca</param>
        /// <param name="fim">Data final da busca</param>
        /// <returns>Lista de cheques a depositar</returns>
        public List<Cheque> FindAllChequesADepositar(DateTime inicio, DateTime fim)
        {
            
            var result = _context.Cheques.Include(c => c.ContaCheque.Banco)
                .Include(c1 => c1.ContaCheque.Proprietario)
                .Include(c2 => c2.ContaCheque).Where(c3 => c3.Vencimento >= inicio && c3.Vencimento <= fim && c3.Status == StatusDocumento.DIGITADO).ToList();
            return result; 
        }

        /// <summary>
        /// Busca por todos os cheques a depositar do edmitente do id 
        /// </summary>
        /// <param name="idProprietario">Id do proprietários</param>
        /// <returns>Lista de cheques</returns>
        public List<Cheque> FindChequesByProprietarioNoDep(Int64 idProprietario)
        {
            
            var result = _context.Cheques.Include(c => c.ContaCheque.Banco)
                .Include(c1 => c1.ContaCheque.Proprietario)
                .Include(c2 => c2.ContaCheque).Where(c3 => c3.ContaCheque.IdProprietario == idProprietario && c3.Status == StatusDocumento.DIGITADO).ToList();
            return
                result; 
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public List<Cheque> FindAllChequesByIdConta(long idConta)
        {
            var result = _context.Cheques.Include(c => c.ContaCheque.Banco)
                .Include(c1 => c1.ContaCheque.Proprietario)
                .Include(c2 => c2.ContaCheque).Where(c3 => c3.IdConta == idConta).ToList();
            return result; 
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                  _context.Dispose();
                }
            }
            disposed = true;
        }

        ~ChequeRepository()
        {
            Dispose(false);
        }

        public void InsertPersiste(IPersistenceBase entidade)
        {
            _context.Cheques.Add((Cheque) entidade);
            _context.SaveChanges();

        }

        public void DeletePersiste(IPersistenceBase entidade)
        {
            
            _context.Cheques.Remove((Cheque) entidade);
            _context.SaveChanges();
        }

        public void UpdatePersiste(IPersistenceBase entidade)
        {
            using (IScContext c = new ScContext())
            {
                Cheque cheque = (Cheque)entidade;
                c.Cheques.Attach(cheque);
                c.Entry(cheque).State = EntityState.Modified;
                c.SaveChanges();
            }
            
        }
    }
}
