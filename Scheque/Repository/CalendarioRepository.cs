﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Repository
{
    /// <summary>
    /// Classe encarregada de tratar os dados de uma data no calendário
    /// </summary>
    public sealed class CalendarioRepository : IDisposable, ICalendarioRepository
    {
        private IScContext _context;
        private Boolean disposed = true;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Cria um objeto calendario
        /// </summary>
        /// <param name="context">Classe de persistencia</param>
        public CalendarioRepository(IScContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }
        /// <summary>
        /// Método persiste os dados de uma data no calendário no banco de dados.
        /// </summary>
        /// <param name="calendario">Data no calendário</param>
        public void Save(Calendario calendario)
        {
            if (calendario.Id == 0)
            {
                _unitOfWork.RegistraNovo(calendario, this);
            }
            else
            {
                _unitOfWork.RegistraAlterado(calendario, this);
            }
            
        }

        /// <summary>
        /// Método retorna todas as datas do calendário
        /// </summary>
        /// <returns>Lista de datas de calendario</returns>
        public IEnumerable<Calendario> AllCalendarios()
        {
            return
                _context.Calendarios.OrderBy(c =>
                    c.DataFeriado); //_context.Calendarios.SqlQuery("Select * from calendario order by DataFeriado;");
        }

        /// <summary>
        /// Busca por uma data no banco de calendário a partir dos paramentros repassados.
        /// </summary>
        /// <param name="dia">Dia do calendário</param>
        /// <param name="mes">Mês do calendário</param>
        /// <returns>Data localizada no calendário</returns>
        public Calendario FindData(int dia, int mes)
        {
            var result = _context.Calendarios.SqlQuery
            ("Select * from calendario where month(DataFeriado) = "+mes+" and day(DataFeriado) = "+dia+";");
            if (result.Count() > 0)
            {
                return result.First();
            }
            return null;
        }
        /// <summary>
        /// Remove uma data calendario da base de dados a partir do id repassado
        /// </summary>
        /// <param name="id">Id da data calendário a ser removido da base.</param>
        public void Remove(Int64 id)
        {
            Calendario data = _context.Calendarios.Find(id);
            /*_context.Calendarios.Remove(data);
            _context.SaveChanges();*/
            _unitOfWork.RegistraRemovido(data, this);
        }
        /// <summary>
        /// Busca por datas que estejam entre o mês e ano repassado
        /// </summary>
        /// <param name="mes">Mês do filtro</param>
        /// <param name="ano">Ano do Filtro</param>
        /// <returns>Lista de Calendarios</returns>
        public List<Calendario> CalendarioMes(string mes, string ano)
        {
            int a = Int32.Parse(ano), m = int.Parse(mes) ;
            return _context.Calendarios.Where(c => c.DataFeriado.Month == m && c.DataFeriado.Year == a).ToList();
        }
        /// <summary>
        /// Busca por datas de um determinado ano
        /// </summary>
        /// <param name="ano">Ano para o filtro</param>
        /// <returns>Lista de calendarios</returns>
        public List<Calendario> CalendarioAno(string ano)
        {
            return _context.Calendarios.Where(c => c.Ano == ano).ToList();
        }
        /// <summary>
        /// Busca por uma data especifica , dia, mês e ano
        /// </summary>
        /// <param name="dia">Dia do filtro</param>
        /// <param name="mes">Mês do filtro</param>
        /// <param name="ano">Ano do filtro</param>
        /// <returns>Data especifica ou null</returns>
        public Calendario CalendarioDia(string dia, string mes , string ano)
        {
           return _context.Calendarios.Where(c => c.Dia == dia && c.Mes == mes && c.Ano == ano).Single();
        }

        public Calendario FindById(long id)
        {
            return _context.Calendarios.Find(id);
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                        _context = null;
                    }
                }
            }
            disposed = true;
        }

        ~CalendarioRepository()
        {
            Dispose(false);
        }

        public void InsertPersiste(IPersistenceBase entidade)
        {
            _context.Calendarios.Add((Calendario)entidade);
            _context.SaveChanges();
        }

        public void DeletePersiste(IPersistenceBase entidade)
        {
            _context.Calendarios.Remove((Calendario) entidade);
            _context.SaveChanges();
        }

        public void UpdatePersiste(IPersistenceBase entidade)
        {
            using(IScContext c = new ScContext())
            {
                Calendario calendario = (Calendario)entidade;
                c.Calendarios.Attach(calendario);
                c.Entry(calendario).State = EntityState.Modified;
                c.SaveChanges();
            }
            
        }
    }
}
