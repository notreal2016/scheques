﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Repository
{
    public sealed class ProprietarioRepoitory : IDisposable, IProprietarioRepoitory
    {
        private IScContext _context;
        private bool disposed = false;
        private readonly IUnitOfWork _unitOfWork;

        ~ProprietarioRepoitory()
        {
            Dispose(false);
        }

        public ProprietarioRepoitory(IScContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }

        public void Save(Proprietario proprietario)
        {
            if (proprietario.Id == 0)
            {
                _unitOfWork.RegistraNovo(proprietario, this);
            }
            else
            {
                _unitOfWork.RegistraAlterado(proprietario, this);
            }
        }

        public Proprietario FindByDocumento(String documento)
        {
            var result = _context.Proprietarios.Where(p => p.CpfCNPJ.Equals(documento));
            return result.Count() > 0 ? result.First() : null;
            
        }

        public Proprietario FindById(Int64 id)
        {
            return _context.Proprietarios.Find(id);
        }

        public void Remove(Int64 id)
        {
            Proprietario p = FindById(id);
            _unitOfWork.RegistraRemovido(p, this);
        }

        public List<String> AllClientes(string CNPJCPF)
        {
            List<String> clientes = new List<string>();
            var allc = _context.Lotes.Where(L =>
                L.Cheques.Where(c => c.ContaCheque.Proprietario.CpfCNPJ.Equals(CNPJCPF)).Count() > 0).ToList();
            allc.ForEach(c => {
                if (!clientes.Contains(c.Cliente.Nome))
                {
                    clientes.Add(c.Cliente.Nome);
                }
            });
            return clientes;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                        _context = null;
                    }
                }
            }
            disposed = true;
        }

        public void InsertPersiste(IPersistenceBase entidade)
        {
            _context.Proprietarios.Add((Proprietario) entidade);
            _context.SaveChanges();
        }

        public void DeletePersiste(IPersistenceBase entidade)
        {
            _context.Proprietarios.Remove((Proprietario) entidade);
            _context.SaveChanges();
        }

        public void UpdatePersiste(IPersistenceBase entidade)
        {
            using(IScContext c = new ScContext())
            {
                Proprietario pro = (Proprietario)entidade;
                c.Proprietarios.Attach(pro);
                c.Entry(pro).State = EntityState.Modified;
                c.SaveChanges();
            }
            
        }
    }
}
