﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32.SafeHandles;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Repository
{
    

    /// <summary>
    /// Classe encarregada de guardar dados de um banco.
    /// </summary>
    public sealed class BancoRepository:  IBancoRepository
    {
        private IScContext _context;
        private readonly IUnitOfWork _unitOfWork;
        
        public BancoRepository(IScContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }
        /// <summary>
        /// Retorna uma lista com todos os bancos cadastrado
        /// </summary>
        /// <returns>Icolection de Bancos</returns>
        public IList<Banco> AllBancos()
        {
            return _context.Bancos.ToList();
        }

        /// <summary>
        /// Retorna o banco a partir do código do banco repassado
        /// </summary>
        /// <param name="codBanco">Codigo do banco</param>
        /// <returns>Banco cadastrado</returns>
        public Banco FindByCodBanco(String codBanco)
        {
            return _context.Bancos.Find(codBanco);
        }
        /// <summary>
        /// Salva os dados de um banco no banco de dados
        /// </summary>
        /// <param name="banco">Banco que terá os dados cadastrados</param>
        public void Save(Banco banco)
        {
            
            _unitOfWork.RegistraNovo(banco, this);
        }
        /// <summary>
        /// Atualiza os dados de um banco no banco de dados 
        /// </summary>
        /// <param name="banco">Banco cujos dados serão atualizados</param>
        public void Update(Banco banco)
        {
            _unitOfWork.RegistraAlterado(banco, this);
        }
        /// <summary>
        /// Remove os dados de um banco no banco de dados
        /// </summary>
        /// <param name="codBanco">Código do banco que será removido do banco de dados.</param>
        public void Remove(String codBanco)
        {
            Banco banco = FindByCodBanco(codBanco);
            _unitOfWork.RegistraRemovido(banco, this);
        }

        
        public void InsertPersiste(IPersistenceBase entidade)
        {
            _context.Bancos.Add((Banco) entidade);
            _context.SaveChanges();
        }

        public void DeletePersiste(IPersistenceBase entidade)
        {
            _context.Bancos.Remove((Banco) entidade);
            _context.SaveChanges();
        }

        public void UpdatePersiste(IPersistenceBase entidade)
        {
           _context = new ScContext();
            Banco banco = (Banco) entidade;
            
           // _context.Bancos.Attach(banco);
            _context.Entry(banco).State = EntityState.Modified;
            

            //_context.Bancos.Attach(banco);
            //_context.Entry(banco).State = EntityState.Modified;
            _context.SaveChanges();
        }

        #region IDisposable Support
        private bool disposedValue = false; // Para detectar chamadas redundantes

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                // TODO: liberar recursos não gerenciados (objetos não gerenciados) e substituir um finalizador abaixo.
                // TODO: definir campos grandes como nulos.

                disposedValue = true;
            }
        }

        // TODO: substituir um finalizador somente se Dispose(bool disposing) acima tiver o código para liberar recursos não gerenciados.
        ~BancoRepository()
         {
           // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
           Dispose(false);
         }

        // Código adicionado para implementar corretamente o padrão descartável.
        public void Dispose()
        {
            // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
            Dispose(true);
            // TODO: remover marca de comentário da linha a seguir se o finalizador for substituído acima.
            // GC.SuppressFinalize(this);
        }
        #endregion



    }

}
