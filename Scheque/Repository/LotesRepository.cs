﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;

namespace Scheque.Repository
{
    /// <summary>
    /// Classe encarregada da persistencia dos dados de um lote no banco de dados além das pesquisa de dados
    /// </summary>
    public class LotesRepository
    {
        private SCContext _context;

        /// <summary>
        /// Controe um objeto do tipo LotesContext encaregado da persistencia dos dados de um lote no banco de dados
        /// </summary>
        /// <param name="context">Objeto de contexto de conexão com o banco de dados</param>
        public LotesRepository(SCContext context)
        {
            _context = context;
        }

        public void Save(Lote lote)
        {
            
            try
            {
                if (lote.Id == 0)
                {
                    _context.Lotes.Add(lote);
                }
                else
                {
                    _context.Entry(lote).State = EntityState.Modified;
                }
                _context.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                String retorno = "CONTATE O SUPORTE E REPASSE O ERRO ABAIXO: \n\r";
                foreach (var eve in e.EntityValidationErrors)
                {
                    retorno += "\n\r"+ String.Format("Tipo de entidade \"{0}\" no status \"{1}\" tem os seguintes erros de validação:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    foreach (var ve in eve.ValidationErrors)
                    {
                        retorno += "\n\r" + String.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw new LoteException(retorno);
            }
        }
        /// <summary>
        /// Remove um lote e seus cheques do banco de dados
        /// </summary>
        /// <param name="idLote">Id do lote a ser removido</param>
        public void Remove(Int64 idLote)
        {
            Lote lote = _context.Lotes.Find(idLote);
            try
            {
                _context.Lotes.Remove(lote);
                _context.SaveChanges();
            }
            catch (LoteException e)
            {
                throw new LoteException(e.Message);
            }
        }

        public IEnumerable<Lote> FindAlLotes()
        {
            return _context.Lotes.OrderBy(lote => lote.Id);
        }
        /// <summary>
        /// Busca por um ou mais lotes cadastrado a um cliente cujo nome ou parte dele foi repassado como paramentro
        /// </summary>
        /// <param name="nomeCliente">Nome ou parte do nome do cliente que será usado para a pesquisa.</param>
        /// <returns>Coleção de Lotes do cliente localizado</returns>
        public IEnumerable<Lote> FindByCliente(String nomeCliente)
        {
            return _context.Lotes.SqlQuery("Select * from Lote left join Cliente on lote.idcliente = Cliente.id where Cliente.Nome like '%" + nomeCliente + "%';")
                .OrderBy(lote => lote.DataLote);

        }
        /// <summary>
        /// Método efetua a busca por todos os lotes cadastrado em uma determinada data.
        /// </summary>
        /// <param name="data">Data usada pelo filtro</param>
        /// <returns>Lista de lotes em ordem alfabetica de nome filtrados pela data repassada. </returns>
        public IEnumerable<Lote> FindByDateLote(DateTime data)
        {
            String SQL = String.Format("Select * from Lote where DataLote between '{0:yyyy-MM-dd} 00:00:00' and '{0:yyyy-MM-dd} 23:59:59';", data);
            return _context.Lotes.SqlQuery(SQL)
                .OrderBy(lote => lote.Cliente.Nome);
        }

        /// <summary>
        /// Busca por um lote a partir do id repassado
        /// </summary>
        /// <param name="idLote">Id do lote a ser buscado</param>
        /// <returns>Lote do id repassado</returns>
        public Lote FindById(Int64 idLote)
        {
            return _context.Lotes.Find(idLote);
        }
    }
}
