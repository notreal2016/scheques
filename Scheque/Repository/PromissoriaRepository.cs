﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Repository
{
    /// <summary>
    /// Classe encarregada da peristencia dos dados de uma promissora no banco de dados.
    /// </summary>
    public class PromissoriaRepository : IDisposable, IPromissoriaRepository
    {
        private bool disposed = false;
        private ScContext _context;
        private readonly IUnitOfWork _unitOfWork;

        ~PromissoriaRepository()
        {
            Dispose(false);
        }

        public PromissoriaRepository(ScContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }
        /// <summary>
        /// Persiste os dados de uma promissoria no banco de dados
        /// </summary>
        /// <param name="promissoria">Promissória a ser persistida</param>
        public void Save(Promissoria promissoria)
        {
            if (promissoria.Id == 0)
            {
                _unitOfWork.RegistraNovo(promissoria, this);
            }
            else
            {
                _unitOfWork.RegistraAlterado(promissoria, this);
            }
            /*if (promissoria.Id == 0)
            {
                _context.Promissorias.Add(promissoria);
            }
            else
            {
                _context.Entry(promissoria).State = EntityState.Modified;
            }

            _context.SaveChanges();*/

        }

        /// <summary>
        /// Remove uma promissória do banco de dados apartir do id repassado
        /// </summary>
        /// <param name="id">Id da promissória a ser removida.</param>
        public void Remove(Int64 id)
        {
            Promissoria p = FindById(id);
            //_context.Promissorias.Remove(p);
            _unitOfWork.RegistraRemovido(p, this);
        }
        /// <summary>
        /// Busca por uma promissória a partir do Id
        /// </summary>
        /// <param name="id">ID da promissoria</param>
        /// <returns></returns>
        public Promissoria FindById(Int64 id)
        {
            var result = _context.Promissorias.Include(p => p.Emprestimo).Where(p1 => p1.Id == id);
            return result.Count() > 0? result.First(): null;
        }
        /// <summary>
        /// Método retorna uma lista de promissorias vencidas de um cliente apartir do ID repassado
        /// </summary>
        /// <param name="idCliente">id do cliente que procurará as promissórias vencidas</param>
        /// <returns></returns>
        public List<Promissoria> FindVencidasByIdCliente(Int64 idCliente)
        {
            /*String SQL = String.Format("Select * from documento left join emprestimo on documento.IdEmprestimo = emprestimo.id where emprestimo.idCliente = {0} and documento.vencimento < '{1:yyyy-MM-dd} 23:59:59' and status = {2:D};", idCliente, DateTime.Now, StatusDocumento.DIGITADO);
            return _context.Promissorias.SqlQuery(SQL).ToList();*/
            var result = _context.Promissorias.Include(p => p.Emprestimo)
                .Where(p1 => p1.Emprestimo.Cliente.Id == idCliente).ToList();
            return result;
        }
        /// <summary>
        /// Busca todas as promissorias vencidas 
        /// </summary>
        /// <returns></returns>
        public List<Promissoria> FindAllVencidas()
        {
            /*String SQL = String.Format("Select * from documento left join emprestimo on documento.IdEmprestimo = emprestimo.id where  documento.vencimento < '{0:yyyy-MM-dd} 23:59:59' and status = {1:D};" ,DateTime.Now, StatusDocumento.DIGITADO);
            return _context.Promissorias.SqlQuery(SQL).ToList();*/
            var result = _context.Promissorias.Include(p => p.Emprestimo).Where(p1 =>
                p1.Vencimento <= DateTime.Now && p1.Status == StatusDocumento.DIGITADO).ToList();
            return result;
        }
        /// <summary>
        /// Busca por promissórias a vencer do cliente do id 
        /// </summary>
        /// <param name="idCliente">Id do cliente que será feito a busca</param>
        /// <returns>Lista de promissorias a vencer de um cliente</returns>
        public List<Promissoria> FindAVecerByIdCliente(Int64 idCliente)
        {
            /*String SQL = String.Format("Select * from documento left join emprestimo on documento.IdEmprestimo = emprestimo.id where emprestimo.idCliente = {0} and documento.vencimento >= '{1:yyyy-MM-dd} 00:00:00' and status = {2:D};", idCliente, DateTime.Now, StatusDocumento.DIGITADO);
            return _context.Promissorias.SqlQuery(SQL).ToList();*/
            var result = _context.Promissorias.Include(p => p.Emprestimo)
                .Where(p1 => p1.Emprestimo.IdCliente == idCliente).ToList();
            return result;
        }
        /// <summary>
        /// Busca por todas as pormissoras a vencer
        /// </summary>
        /// <returns></returns>
        public List<Promissoria> FindAllAVecer()
        {
            /*String SQL = String.Format("Select * from documento left join emprestimo on documento.IdEmprestimo = emprestimo.id where documento.vencimento >= '{0:yyyy-MM-dd} 00:00:00' and status = {1:D};", DateTime.Now, StatusDocumento.DIGITADO);
            return _context.Promissorias.SqlQuery(SQL).ToList();*/
            var result = _context.Promissorias.Include(p => p.Emprestimo).Where(p1 =>
                p1.Vencimento > DateTime.Now && p1.Status == StatusDocumento.DIGITADO).ToList();
            return result;
        }

        /// <summary>
        /// Busca por promissorias do dia do cliente do id
        /// </summary>
        /// <param name="idCliente">Id do cliente a ser repassada</param>
        /// <returns></returns>
        public List<Promissoria> FindDoDiaByIdCliente(Int64 idCliente)
        {
            /*String SQL = String.Format("Select * from documento left join emprestimo on documento.IdEmprestimo = emprestimo.id where emprestimo.idCliente = {0} and documento.vencimento '{1:yyyy-MM-dd} 00:00:00' and '{1:yyyy-MM-dd} 23:59:59' and status = {2:D};", idCliente, DateTime.Now, StatusDocumento.DIGITADO);
            return _context.Promissorias.SqlQuery(SQL).ToList();*/
            var result = _context.Promissorias.Include(p => p.Emprestimo).Where(p1 => p1.Emprestimo.IdCliente == idCliente && p1.Vencimento.ToShortDateString().Equals(DateTime.Now.ToShortDateString()) && p1.Status == StatusDocumento.DIGITADO).ToList();
            return result;
        }
        /// <summary>
        /// Busca por todas as promissórias vencendos no dia atual
        /// </summary>
        /// <returns></returns>
        public List<Promissoria> FindAllDoDia()
        {
            /*String SQL = String.Format("Select * from documento left join emprestimo on documento.IdEmprestimo = emprestimo.id where  documento.vencimento between '{0:yyyy-MM-dd} 00:00:00' and '{0:yyyy-MM-dd} 23:59:59' and status = {1:D};",DateTime.Now, StatusDocumento.DIGITADO);
            return _context.Promissorias.SqlQuery(SQL).ToList();*/
            var result = _context.Promissorias.Include(p => p.Emprestimo).Where(p1 => p1.Vencimento.ToShortDateString().Equals(DateTime.Now.ToShortDateString()) && p1.Status == StatusDocumento.DIGITADO).ToList();
            return result;
        }
        /// <summary>
        /// Bsuca por todas as promissórias do cliende do id
        /// </summary>
        /// <param name="idCliente">Id do cliente</param>
        /// <returns></returns>
        public List<Promissoria> FindAllByIdCliente(Int64 idCliente)
        {
            /*String SQL = String.Format("Select * from documento left join emprestimo on documento.IdEmprestimo = emprestimo.id where emprestimo.idCliente = {0};", idCliente);
            return _context.Promissorias.SqlQuery(SQL).ToList();*/
            var result = _context.Promissorias.Include(p => p.Emprestimo).Where(p1 => p1.Emprestimo.IdCliente == idCliente ).ToList();
            return result;
        }

        /// <summary>
        /// Busca por todas as promissórias de um cliente ainda não quitadas
        /// </summary>
        /// <param name="idCliente">Id do cliente</param>
        /// <returns></returns>
        public List<Promissoria> FindAllByIdClienteAbertas(Int64 idCliente)
        {
            /* String SQL = String.Format("Select * from documento left join emprestimo on documento.IdEmprestimo = emprestimo.id where emprestimo.idCliente = {0} and status = {1:D};", idCliente, StatusDocumento.DIGITADO);
             return _context.Promissorias.SqlQuery(SQL).ToList();*/
            var result = _context.Promissorias.Include(p => p.Emprestimo).Where(p1 => p1.Emprestimo.IdCliente == idCliente && p1.Status == StatusDocumento.DIGITADO).ToList();
            return result;
        }

        /// <summary>
        /// Busca por todas as promissórias não quitadas
        /// </summary>
        /// <returns></returns>
        public List<Promissoria> FindAllAbertas()
        {
            /*String SQL = String.Format("Select * from documento left join emprestimo on documento.IdEmprestimo = emprestimo.id where status = {0:D};",  StatusDocumento.DIGITADO);
            return _context.Promissorias.SqlQuery(SQL).ToList();*/
            var result = _context.Promissorias.Include(p => p.Emprestimo).Where(p1 => p1.Status == StatusDocumento.DIGITADO).ToList();
            return result;
        }

        /// <summary>
        /// Busca todas as promissorias cadastradas
        /// </summary>
        /// <returns></returns>
        public List<Promissoria> FindAll()
        {
            var result = _context.Promissorias.Include(p => p.Emprestimo).OrderBy(p2 => p2.Emissao).ToList();
            return result;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                        _context = null;
                    }
                }
            }
            disposed = true;
        }

        public void InsertPersiste(IPersistenceBase entidade)
        {
            _context.Promissorias.Add((Promissoria) entidade);
            _context.SaveChanges();
        }

        public void DeletePersiste(IPersistenceBase entidade)
        {
            _context.Promissorias.Remove((Promissoria) entidade);
            _context.SaveChanges();
        }

        public void UpdatePersiste(IPersistenceBase entidade)
        {
            Promissoria promissoria = (Promissoria) entidade;
            _context.Promissorias.Attach(promissoria);
            _context.Entry(promissoria).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
