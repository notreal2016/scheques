﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Repository
{
    /// <summary>
    /// Classe encarregada dos serviços de persistenciad e dados de uma empresa.
    /// </summary>
    public sealed class EmpresaRepository : IDisposable, IEmpresaRepository
    {
        private IScContext _context;
        private Boolean dispose = false;
        private readonly IUnitOfWork _unitOfWork;

        public EmpresaRepository(IScContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }
        /// <summary>
        /// Persiste os dados de uma emrpesa no banco de dados
        /// </summary>
        /// <param name="empresa">Empresa cujos dados serão persistidos</param>
        /// <returns>Empresa após salva</returns>
        public Empresa Save(Empresa empresa)
        {
            if (empresa.Id == 0)
            {
                _unitOfWork.RegistraNovo(empresa, this);
            }
            else
            {
                _unitOfWork.RegistraAlterado(empresa, this);
            }

            return empresa;
            
        }
        /// <summary>
        /// Busca por uma empresa pelo ID repassado
        /// </summary>
        /// <param name="id">Id da empresa</param>
        /// <returns>Empresa localizada ou null</returns>
        public Empresa FindById(long id)
        {
            return _context.Empresas.Find(id);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.dispose)
            {
                if (disposing)
                {
                    if(_context != null)
                    {
                        _context.Dispose();
                        _context = null;
                    }
                }
            }
            dispose = true;
        }

        ~EmpresaRepository()
        {
            Dispose(false);
        }

        public void InsertPersiste(IPersistenceBase entidade)
        {
            _context.Empresas.Add((Empresa) entidade);
            _context.SaveChanges();
        }

        public void DeletePersiste(IPersistenceBase entidade)
        {
            _context.Empresas.Remove((Empresa) entidade);
            _context.SaveChanges();
        }

        public void UpdatePersiste(IPersistenceBase entidade)
        {
            using(IScContext c = new ScContext())
            {
                Empresa empresa = (Empresa)entidade;
                c.Empresas.Attach(empresa);
                c.Entry(empresa).State = EntityState.Modified;
                c.SaveChanges();
            }
            
        }
    }
}
