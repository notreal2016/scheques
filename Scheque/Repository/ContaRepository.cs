﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Repository
{
    /// <summary>
    /// Classe encarregada de tratar os dados de uma conta pertencente ao emitente de um cheque
    /// </summary>
    public sealed class ContaRepository: IDisposable, IContaRepository
    {
        private IScContext _context;
        private bool disposed = false;
        private readonly IUnitOfWork _unitOfWork;

        public ContaRepository(IScContext context , IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Método exibe uma lista de todas as contas cadastradas.
        /// </summary>
        /// <returns>Lista de contas cadastradas</returns>
        public List<Conta> AllContas()
        {
            return _context.Contas.Include(c => c.Banco).Include(c1 => c1.Proprietario).ToList();
        }

        /// <summary>
        /// Método retorna uma conta a partir do id da conta cadastrada
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Conta FindContaById(Int64 id)
        {
            var result =  _context.Contas.Include(c => c.Banco).Include(c1 => c1.Proprietario).Where(c2 => c2.Id == id).ToList();
            return result.Count > 0 ? result[0] : null; 
        }
        /// <summary>
        /// Método busca uma conta cadastrada a partir dos dados da conta
        /// </summary>
        /// <param name="idbanco">Id do banco </param>
        /// <param name="agencia">Número da agência</param>
        /// <param name="conta">Número da conta</param>
        /// <returns>Conta localizada</returns>
        public Conta FindContaByDados(String idbanco, String agencia, String conta, string operacao = "", int numeroDigitosCC = 6)
        {
            var resultado = _context.Contas.Include(c => c.Banco)
                .Include(c1 => c1.Proprietario)
                .Where(c => c.IdBanco == idbanco && c.Agencia == agencia && c.ContaCorrente == conta);
            return ValidaConta(idbanco, agencia, conta, numeroDigitosCC, resultado);

        }

        private static Conta ValidaConta(string idbanco, string agencia, string conta, int numeroDigitosCC, IQueryable<Conta> resultado)
        {
            Conta cc = (resultado.Count() != 0) ? resultado.First() : null;
            if (cc != null)
            {
                Conta nconta = new Conta()
                {
                    Agencia = agencia,
                    ContaCorrente = conta,
                    IdBanco = idbanco
                };

                if (cc.ComparaConta(nconta, numeroDigitosCC))
                {
                    return cc;
                }
            }
            return null;
        }

        /// <summary>
        /// Método busca todas as contas de um emitente a partir do Id do emitente
        /// </summary>
        /// <param name="id">Id do emitente</param>
        /// <returns>Lista de contas</returns>
        public List<Conta> FindContaByIdProprietario(Int64 id)
        {

            return _context.Contas.Include(c => c.Banco)
                .Include(c1 => c1.Proprietario)
                .Where(c2 => c2.IdProprietario ==id).ToList();
        }

        /// <summary>
        /// Método retorna todas as contas vinculadas ao documento do cpf ou cnpj do emitente
        /// </summary>
        /// <param name="documento">CPF ou CNPJ do emitente</param>
        /// <returns>Lista de contas cadastradas</returns>
        public List<Conta> FindContasByDocEmitente(String documento)
        {
            return _context.Contas.Include(c => c.Banco)
                .Include(c1 => c1.Proprietario)
                .Where(c2 => c2.Proprietario.CpfCNPJ ==documento).ToList();
        }

        /// <summary>
        /// Método salva os dados de uma conta no banco de dados
        /// </summary>
        /// <param name="conta">Conta que terá os dados persistidos</param>
        public void Save(Conta conta)
        {
            if (conta.Id == 0)
            {
                _unitOfWork.RegistraNovo(conta, this);
            }
            else
            {
                _unitOfWork.RegistraAlterado(conta, this);
            }
        }

        /// <summary>
        /// Remove uma conta da base de dados a partir do Id da conta repassado
        /// </summary>
        /// <param name="_id">Id da conta</param>
        public void Remove(Int64 _id)
        {
            Conta conta = FindContaById(_id);
            _unitOfWork.RegistraRemovido(conta, this);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                        _context = null;
                    }
                }
            }
            disposed = true;
        }

        ~ContaRepository()
        {
            Dispose(false);
        }

        public void InsertPersiste(IPersistenceBase entidade)
        {
            Conta conta = (Conta) entidade;
            _context.Entry(conta).State = EntityState.Added;
            _context.Contas.Add((Conta) entidade);
            _context.SaveChanges();
        }

        public void DeletePersiste(IPersistenceBase entidade)
        {
            Conta conta = (Conta)entidade;
            _context.Entry(conta).State = EntityState.Deleted;
            _context.Contas.Remove((Conta) entidade);
            _context.SaveChanges();
        }

        public void UpdatePersiste(IPersistenceBase entidade)
        {
            using(IScContext c = new ScContext())
            {
                Conta conta = (Conta)entidade;
                //_context.Contas.Attach(conta);
                //_context.Entry(conta.Banco).State = EntityState.Unchanged;
                
                //_context.Entry(conta.Proprietario).State = EntityState.Modified;
                _context.Entry(conta).State = EntityState.Modified;
                _context.SaveChanges();
            }
                
        }

        public Conta FindContaByIdNoTrack(long idConta)
        {
            var result = _context.Contas.AsNoTracking().Where(c => c.Id == idConta).ToList<Conta>();
            return result.Count > 0 ? result[0] : null;
        }

        public Conta FindContaByDadosNoTrack(string idBanco, string agencia, string conta, string operacao, int numeroDigitosCC = 6)
        {
            var resultado = _context.Contas.AsNoTracking().Include(c => c.Banco).Include(c1 => c1.Proprietario)
                .Where(c => c.IdBanco == idBanco && c.Agencia == agencia && c.ContaCorrente == conta);
            return ValidaConta(idBanco, agencia, conta, numeroDigitosCC, resultado);
        }
    }
}
