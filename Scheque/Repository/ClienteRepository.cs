﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Repository
{
    

    /// <summary>
    /// Classe encarregada do processo de persistencia e busca no banco de dados de dados de clientes
    /// </summary>
    public sealed class ClienteRepository : IDisposable, IClienteRepository
    {
        private IScContext _context;
        private bool disposed = false;
        private readonly IUnitOfWork _unitOfWork;

        /// <summary>
        /// Controe a classe de persisencia de cliente
        /// </summary>
        /// <param name="context">Contexto de conexão com o banco de dados</param>
        public ClienteRepository(IScContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Grava os dados de um cliente no banco de dados 
        /// </summary>
        /// <param name="cliente">Cliente cujo dados seram presistidos no banco de dados</param>
        public void Save(Cliente cliente)
        {
            if (cliente.Id == 0)
            {
                _unitOfWork.RegistraNovo(cliente, this);
            }
            else
            {
                _unitOfWork.RegistraAlterado(cliente, this);
            }
           
        }

        /// <summary>
        /// Remove um cliente do banco de dados a partir do id do cliente repassado
        /// </summary>
        /// <param name="idCliente">Id do cliente a ser remvido da base de dados</param>
        public void Remove(Int64 idCliente)
        {
            Cliente cliente = _context.Clientes.Find(idCliente);
            _unitOfWork.RegistraRemovido(cliente, this);
           
        }

        /// <summary>
        /// Busca por todos os clientes cadastrado no banco de dados
        /// </summary>
        /// <returns>Retorna uma coleção de clientes em ordem alfabetica de nome</returns>
        public IEnumerable<Cliente> FindAllClientes()
        {
           
            return _context.Clientes.OrderBy(cliente => cliente.Nome);
        }


        public IEnumerable<Cliente> FindByName(string nome)
        {
            //RefreshAll();
            return _context.Clientes.SqlQuery("Select * from Cliente where nome like '%" + nome + "%';");
        }
        /// <summary>
        /// Busca um cliente no banco de dados a partir do Id repassado como paramentro
        /// </summary>
        /// <param name="id">Id do cliente a ser buscado</param>
        /// <returns>Cliente localizado</returns>
        public Cliente FindById(Int64 id)
        {
            return _context.Clientes.Find(id); //Refresh(_context.Clientes.Find(id));
        }

       
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                        _context = null;
                    }
                }
            }
            disposed = true;
        }

        ~ClienteRepository()
        {
            Dispose(false);
        }

        public void InsertPersiste(IPersistenceBase entidade)
        {
            _context.Clientes.Add((Cliente) entidade);
            _context.SaveChanges();
        }

        public void DeletePersiste(IPersistenceBase entidade)
        {
            _context.Clientes.Remove((Cliente) entidade);
            _context.SaveChanges();
        }

        public void UpdatePersiste(IPersistenceBase entidade)
        {
            try
            {
                using(IScContext c = new ScContext())
                {
                    Cliente cliente = c.Clientes.Attach((Cliente)entidade);
                    c.Entry(cliente).State = EntityState.Modified;
                    c.SaveChanges();
                }
                
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
            
        }
    }
}
