﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;

namespace Scheque.Repository
{
    public class ChequeRepositorycs
    {
        private SCContext _context;

        public ChequeRepositorycs(SCContext context)
        {
            _context = context; 
        }

        public void Save(Cheque cheque)
        {
     
            if (cheque.Id == 0)
            {
                _context.Cheques.Add(cheque);
            }
            else
            {
                _context.Entry(cheque).State = EntityState.Modified;
            }
            _context.SaveChanges();
            
        }
        
        public void Remove(Int64 id)
        {
            Cheque ch = FindById(id);
            _context.Cheques.Remove(ch);
            _context.SaveChanges();

        }

        public Cheque FindById(Int64 id)
        {
            return _context.Cheques.Find(id);
        }

        /// <summary>
        /// Busca por um cheque no banco de dados pelos paramentros repassados
        /// </summary>
        /// <param name="idBanco">Id do banco</param>
        /// <param name="agencia">Agencia</param>
        /// <param name="conta">Conta corrente</param>
        /// <param name="operacao">Operação</param>
        /// <param name="NCheque">Número do chque</param>
        /// <returns>Retorna o cheque localizado ou null para não localizado</returns>
        public Cheque FindByDados(String idBanco, String agencia, String conta, String  operacao,  String NCheque)
        {
            String SQL = String.Format("select documento.* from documento left join conta on documento.IdConta = conta.Id where conta.IdBanco = '{0}' and " +
                                       "conta.ContaCorrente = '{1}' and conta.Agencia = '{2}' and conta.Operacao = '{3}' and documento.NumeroCheque = '{4}'",
            idBanco, conta, agencia, operacao, NCheque);
            var result = _context.Cheques.SqlQuery(SQL);
            return (result.Count() == 0) ? null : result.First();
        }


    }
}
