﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;
using Scheque.Service;

namespace Scheque.Repository
{
    public sealed class UsuarioRepository :  IUsuarioRepository
    {
        private readonly IScContext _context;
        private readonly IUnitOfWork _unitOfWork;

        public UsuarioRepository(IScContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }


        /// <summary>
        /// Salva os dados de um usuário na base de dados
        /// </summary>
        /// <param name="usuario">Usuário a ter os dados salvos na base de dados</param>
        public void Save(Usuario usuario)
        {
            if (usuario.Id == 0)
            {
                _unitOfWork.RegistraNovo(usuario, this);
            }
            else
            {
                _unitOfWork.RegistraAlterado(usuario, this);
            }
         
        }
        /// <summary>
        /// Remove um usuário do banco de dados
        /// </summary>
        /// <param name="idUsuario">Id do usuário</param>
        public void Remove(Int64 idUsuario)
        {
            Usuario usuario = _context.Usuarios.Find(idUsuario);
            _unitOfWork.RegistraRemovido(usuario, this);
        }

        /// <summary>
        /// Busca usuário por meio de login e sennha
        /// </summary>
        /// <param name="login"></param>
        /// <param name="senha"></param>
        /// <returns></returns>
        public Usuario FindUserbyLoginSenha(String login, String senha)
        {
            Usuario user = null;
            try
            {
                var result = _context.Usuarios.Include(u => u.Empresa)
                    .Where(u1 => u1.Login.Equals(login) && u1.Senha.Equals(senha));
                user = (!result.Any()) ? null : result.First();
            }
            catch (UsuarioException e)
            {
                throw new UsuarioException(e.Message);
            }
            return user;
        }
        /// <summary>
        /// Busca usuário pelo id 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Usuario FindById(Int64 id)
        {
            var result = _context.Usuarios.Include(u => u.Empresa).Where(u1 => u1.Id == id).ToList();
            return (!result.Any()) ? null : result[0];
        }
        /// <summary>
        /// Retorna uma lista de usuários a partir de parte do nome do mesmo
        /// </summary>
        /// <param name="nome">Nome usuário</param>
        /// <returns>Lista de usuarios</returns>
        public List<Usuario> FindByNome(String nome)
        {
            var result = _context.Usuarios.Include(u => u.Empresa).Where(u1 => u1.Nome.Contains(nome)).ToList();
            return result;
        }
        /// <summary>
        /// Busca por usuário cadastrado que contém o login repassado
        /// </summary>
        /// <param name="login">Login de busca</param>
        /// <returns>Usuário localizado</returns>
        public Usuario FindByLogin(string login)
        {
            try
            {
                var result = _context.Usuarios.Include(u1 => u1.Empresa).Single(u => u.Login == login);
                return result;
            }
            catch (Exception e)
            {
                return null;
            }
            
        }

        
        public IEnumerable<Usuario> FindAll()
        {
            return _context.Usuarios.Include(u => u.Empresa).ToList();
        }

        public void InsertPersiste(IPersistenceBase entidade)
        {
            _context.Usuarios.Add((Usuario) entidade);
            _context.SaveChanges();
        }

        public void DeletePersiste(IPersistenceBase entidade)
        {
            _context.Usuarios.Remove((Usuario) entidade);
            _context.SaveChanges();
        }

        public void UpdatePersiste(IPersistenceBase entidade)
        {
            using (IScContext c = new ScContext())
            {
                Usuario user = (Usuario)entidade;
                var idEmpresa = user.IdEmpresa;
                user.Empresa = null;
                user.IdEmpresa = idEmpresa;
                try
                {
                    _context.Entry(user).State = EntityState.Modified;
                    _context.SaveChanges();
                }
                catch (Exception)
                {
                    c.Entry(user).State = EntityState.Modified;
                    c.SaveChanges();
                }
                
            }
            
        }

        #region IDisposable Support
        private bool disposedValue = false; // Para detectar chamadas redundantes

        void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                // TODO: liberar recursos não gerenciados (objetos não gerenciados) e substituir um finalizador abaixo.
                // TODO: definir campos grandes como nulos.

                disposedValue = true;
            }
        }

        // TODO: substituir um finalizador somente se Dispose(bool disposing) acima tiver o código para liberar recursos não gerenciados.
         ~UsuarioRepository()
         {
           // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
           Dispose(false);
         }

        // Código adicionado para implementar corretamente o padrão descartável.
        public void Dispose()
        {
            // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
            Dispose(true);
            // TODO: remover marca de comentário da linha a seguir se o finalizador for substituído acima.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
