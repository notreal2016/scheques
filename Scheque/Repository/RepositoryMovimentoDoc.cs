﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Repository
{
    /// <summary>
    /// Classe encarregada a efetuar a persistencia de dados de um movimento referente a um documento do sistema
    /// </summary>
    public class RepositoryMovimentoDoc : IDisposable, IRepositoryMovimentoDoc
    {
        private IScContext _context;
        private bool disposed = false;
        private readonly IUnitOfWork _unitOfWork;

        ~RepositoryMovimentoDoc()
        {
            Dispose(false);
        }

        public RepositoryMovimentoDoc(IScContext context, IUnitOfWork unitOfWork)
        {
            _context = context;
            _unitOfWork = unitOfWork;
        }

        /// <summary>
        /// Metodo adiciona a base de dados vários documentos de uma unica vez.
        /// </summary>
        /// <param name="documentos"></param>
        public void IncludeAll(List<MovimentoDocumento> documentos)
        {
            /*_context.MovimentoDocumentos.AddRange(documentos);
            _context.SaveChanges();*/
            foreach (var doc in documentos)    
            {
                _unitOfWork.RegistraNovo(doc, this);
            }
        }

        /// <summary>
        /// Método encarregado de persistir os dados de um movimento de documento no banco de dados
        /// </summary>
        /// <param name="movimento">Movimento que será persistido</param>
        public void Save(MovimentoDocumento movimento)
        {
            if (movimento.Id == 0)
            {
                _unitOfWork.RegistraNovo(movimento, this);
            }
            else
            {
                _unitOfWork.RegistraAlterado(movimento, this);
            }
            /*
            if (movimento.Id == 0)
            {
                _context.MovimentoDocumentos.Add(movimento);
            }
            else
            {
                _context.Entry(movimento).State = EntityState.Modified;
            }
            _context.SaveChanges();
            */

        }
        /// <summary>
        /// Método remove um movimento do banco de dados apartir do id repassado
        /// </summary>
        /// <param name="id">id do movimento a ser removido.</param>
        public void Remove(Int64 id)
        {
            MovimentoDocumento doc = _context.MovimentoDocumentos.Find(id);
            /*_context.MovimentoDocumentos.Remove(doc);
            _context.SaveChanges();*/
            _unitOfWork.RegistraRemovido(doc, this);
        }

        /// <summary>
        /// Busca por um movimento apartir do ID repassado
        /// </summary>
        /// <param name="id">Id do momento a ser buscado</param>
        /// <returns>Retorna um MovimentoDocumento pelo Id</returns>
        public MovimentoDocumento FindById(Int64 id)
        {
            return _context.MovimentoDocumentos.Find(id);
        }

        /// <summary>
        /// Busca por todos os movimentos de um documento repassado pelo seu id
        /// </summary>
        /// <param name="idCheque">id do documeto que será pesquisado</param>
        /// <returns>Lista de movimentos do documento repassado</returns>
        public List<MovimentoDocumento> FindByMovDoc(Int64 idCheque)
        {
            String SQL = String.Format("Select * from MovimentoDocumento where iddocumento = {0}", idCheque);
            return _context.MovimentoDocumentos.SqlQuery(SQL).ToList();
        }

        public List<MovimentoCheque> FindByMovCh(long idCheque)
        {
            var reslt = _context.MovimentoCheques.Include(m => m.Motivo).Where(m1 => m1.IdCheque == idCheque).ToList();
            //return _context.MovimentoCheques.Where(m => m.IdCheque == idCheque).ToList();
            return reslt;
        }
        /// <summary>
        /// Busca por movimentos de um documento a partir do id e intervalo de datas repassado
        /// </summary>
        /// <param name="idCheque">Id do cheque</param>
        /// <param name="inicio">Data inicial</param>
        /// <param name="fim">Data final</param>
        /// <returns>Lista de movimento</returns>
        public List<MovimentoDocumento> FindByMovDocByIntevalo(Int64 idCheque, DateTime inicio, DateTime fim)
        {
            String SQL = String.Format("Select * from MovimentoDocumento where iddocumento = {0} and data between '{1:yyyy-MM-dd} 00:00:00' and '{2:yyyy-MM-dd} 23:59:59'", idCheque, inicio, fim);
            return _context.MovimentoDocumentos.SqlQuery(SQL).ToList();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                        _context = null;
                    }
                }
            }
            disposed = true;
        }

        public void InsertPersiste(IPersistenceBase entidade)
        {
            MovimentoDocumento mov = (MovimentoDocumento) entidade;
            _context.Entry(mov).State = EntityState.Added;
            _context.MovimentoDocumentos.Add(mov);
            _context.SaveChanges();

        }

        public void DeletePersiste(IPersistenceBase entidade)
        {
            _context.MovimentoDocumentos.Remove((MovimentoDocumento) entidade);
            _context.SaveChanges();
        }

        public void UpdatePersiste(IPersistenceBase entidade)
        {
            using(IScContext c = new ScContext())
            {
                MovimentoDocumento mov = (MovimentoDocumento)entidade;
                c.MovimentoDocumentos.Attach(mov);
                c.Entry(mov).State = EntityState.Modified;
                c.SaveChanges();
            }
            
        }
    }
}
