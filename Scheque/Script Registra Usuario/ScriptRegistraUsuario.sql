use chequesdata;
/* Tabela Auditoria */
drop trigger if exists InsereDocumento;
drop trigger if exists InsereLote;
drop trigger if exists InsereCliente;
drop trigger if exists InsereCalendario;
drop trigger if exists AtualizaMovimentoDocumento;
drop trigger if exists AtualizaMovimentoCliente;
drop trigger if exists AtualizaLoteDeposito;
drop trigger if exists AtualizaHistorico;
drop trigger if exists AtualizaEmprestimo;
drop trigger if exists AtualizaCalendario;
drop trigger if exists AtualizaUsuario;
drop trigger if exists AtualizaCliente;
drop trigger if exists AtualizaLote;
drop trigger if exists AtualizaDocumento;
drop trigger if exists DeletaUsuario;
drop trigger if exists DeletaMovimentoDocumento;
drop trigger if exists DeletaMovimentoCliente;
drop trigger if exists DeletaLoteDeposito;
drop trigger if exists DeletaHistorico;
drop trigger if exists DeletaEmprestimo;
drop trigger if exists DeletaCalendario;
drop trigger if exists DeletaCliente;
drop trigger if exists DeletaLote;
drop trigger if exists DeletaDocumento;
drop trigger if exists InsereUsuario;
drop trigger if exists InsereMovimentoDocumento;
drop trigger if exists InsereMovimentoCliente;
drop trigger if exists InsereHistorico;
drop trigger if exists InsereLoteDeposito;
drop trigger if exists InsereEmprestimo;
drop trigger if exists RegistraUser;


create table if not exists Auditoria(
id int not null auto_increment,
Tabela varchar(100) not null,
idExterno int,
Usuario varchar(100) not null,
historico varchar(50),
DataHora Timestamp,
primary key(id));


/*  ----INSERCOES----  */
use chequesdata;
/*  Gatilho Inserir em Documento*/
delimiter $
create trigger InsereDocumento after insert on Documento
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Documento', new.id, @usuario, 'Inserir', @sysdate);

end$
delimiter;



/* Gatilho Inserir em Lote */
delimiter $
create trigger InsereLote after insert on Lote
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Lote', new.id, @usuario, 'Inserir', @sysdate);

end$
delimiter;



/* Gatilho Inserir em Cliente */
delimiter $
create trigger InsereCliente after insert on Cliente
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Cliente', new.id, @usuario, 'Inserir', @sysdate);

end$
delimiter;



/* Gatilho Inserir em Calendario */
delimiter $
create trigger InsereCalendario after insert on Calendario
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Calendario', new.id, @usuario, 'Inserir', @sysdate);

end$
delimiter;



/* Gatilho Inserir em Emprestimo */
delimiter $
create trigger InsereEmprestimo after insert on Emprestimo
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Emprestimo', new.id, @usuario, 'Inserir', @sysdate);

end$
delimiter;



/* Gatilho Inserir em Historico */
delimiter $
create trigger InsereHistorico after insert on Historico
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Historico', new.id, @usuario, 'Inserir', @sysdate);

end$
delimiter;



/* Gatilho Inserir em LoteDeposito */
delimiter $
create trigger InsereLoteDeposito after insert on LoteDeposito
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('LoteDeposito', new.id, @usuario, 'Inserir', @sysdate);

end$
delimiter;



/* Gatilho Inserir em MovimentoCliente */
delimiter $
create trigger InsereMovimentoCliente after insert on MovimentoCliente
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('MovimentoCliente', new.id, @usuario, 'Inserir', @sysdate);

end$
delimiter;



/* Gatilho Inserir em MovimentoDocumento */
delimiter $
create trigger InsereMovimentoDocumento after insert on MovimentoDocumento
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('MovimentoDocumento', new.id, @usuario, 'Inserir', @sysdate);

end$
delimiter;



/* Gatilho Inserir em Usuario */
delimiter $
create trigger InsereUsuario after insert on Usuario
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Usuario', new.id, @usuario, 'Inserir', @sysdate);

end$
delimiter;


/*  ---DELETES---  */

/* Gatilho Deleta em Documento */
delimiter $
create trigger DeletaDocumento after delete on Documento
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Documento', old.id, @usuario, 'Delete', @sysdate);

end$
delimiter;



/* Gatilho Deleta em Lote */
delimiter $
create trigger DeletaLote after delete on Lote
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Lote', old.id, @usuario, 'Delete', @sysdate);

end$
delimiter;



/* Gatilho Deleta em Cliente */
delimiter $
create trigger DeletaCliente after delete on Cliente
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Cliente', old.id, @usuario, 'Delete', @sysdate);

end$
delimiter;



/* Gatilho Deleta em Calendario */
delimiter $
create trigger DeletaCalendario after delete on Calendario
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Calendario', old.id, @usuario, 'Delete', @sysdate);

end$
delimiter;



/* Gatilho Deleta em Emprestimo */
delimiter $
create trigger DeletaEmprestimo after delete on Emprestimo
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Emprestimo', old.id, @usuario, 'Delete', @sysdate);

end$
delimiter;



/* Gatilho Deleta em Historico */
delimiter $
create trigger DeletaHistorico after delete on Historico
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Historico', old.id, @usuario, 'Delete', @sysdate);

end$
delimiter;



/* Gatilho Deleta em LoteDeposito */
delimiter $
create trigger DeletaLoteDeposito after delete on LoteDeposito
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('LoteDeposito', old.id, @usuario, 'Delete', @sysdate);

end$
delimiter;



/* Gatilho Deleta em MovimentoCliente */
delimiter $
create trigger DeletaMovimentoCliente after delete on MovimentoCliente
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('MovimentoCliente', old.id, @usuario, 'Delete', @sysdate);

end$
delimiter;



/* Gatilho Deleta em MovimentoDocumento */
delimiter $
create trigger DeletaMovimentoDocumento after delete on MovimentoDocumento
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('MovimentoDocumento', old.id, @usuario, 'Delete', @sysdate);

end$
delimiter;



/* Gatilho Deleta em Usuario */
delimiter $
create trigger DeletaUsuario after delete on Usuario
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Usuario', old.id, @usuario, 'Delete', @sysdate);

end$
delimiter;


/* ---Atualiza--- */

/* Gatilho Atualiza em Documento */
delimiter $
create trigger AtualizaDocumento after update on Documento
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Documento', old.id, @usuario, 'Atualiza', @sysdate);

end$
delimiter;



/* Gatilho Atualiza em Lote */
delimiter $
create trigger AtualizaLote after update on Lote
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Lote', old.id, @usuario, 'Atualiza', @sysdate);

end$
delimiter;



/* Gatilho Atualiza em Cliente */
delimiter $
create trigger AtualizaCliente after update on Cliente
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Cliente', old.id, @usuario, 'Atualiza', @sysdate);

end$
delimiter;



/* Gatilho Atualiza em Usuario */
delimiter $
create trigger AtualizaUsuario after update on Usuario
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Usuario', old.id, @usuario, 'Atualiza', @sysdate);

end$
delimiter;



/* Gatilho Atualiza em Calendario */
delimiter $
create trigger AtualizaCalendario after update on Calendario
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Calendario', old.id, @usuario, 'Atualiza', @sysdate);

end$
delimiter;



/* Gatilho Atualiza em Emprestimo */
delimiter $
create trigger AtualizaEmprestimo after update on Emprestimo
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Emprestimo', old.id, @usuario, 'Atualiza', @sysdate);

end$
delimiter;



/* Gatilho Atualiza em Historico */
delimiter $
create trigger AtualizaHistorico after update on Historico
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('Historico', old.id, @usuario, 'Atualiza', @sysdate);

end$
delimiter;



/* Gatilho Atualiza em LoteDeposito */
delimiter $
create trigger AtualizaLoteDeposito after update on LoteDeposito
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('LoteDeposito', old.id, @usuario, 'Atualiza', @sysdate);

end$
delimiter;



/* Gatilho Atualiza em MovimentoCliente */
delimiter $
create trigger AtualizaMovimentoCliente after update on MovimentoCliente
for each row

begin

SET @usuario = null;
set @sysdate = null;

select user
into @usuario
from mysql.user
WHERE user = (SELECT SUBSTR(USER(),1,INSTR(USER(),'@')-1));

select now()
into @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('MovimentoCliente', old.id, @usuario, 'Atualiza', @sysdate);

end$
delimiter;



/*Gatilho atualiza movimento documetno */
delimiter $
create trigger AtualizaMovimentoDocumento after update on MovimentoDocumento
for each row

begin

SET @usuario = null;
set @sysdate = null;

SELECT 
    user
INTO @usuario FROM
    mysql.user
WHERE
    user = (SELECT 
            SUBSTR(USER(),
                    1,
                    INSTR(USER(), '@') - 1)
        );

SELECT NOW() INTO @sysdate;

insert into Auditoria(Tabela, idExterno, Usuario, historico, DataHora) 
values ('MovimentoDocumento', old.id, @usuario, 'Atualiza', @sysdate);

end$
