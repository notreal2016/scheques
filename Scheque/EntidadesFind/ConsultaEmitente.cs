﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Entidade;

namespace Scheque.EntidadesFind
{
    /// <summary>
    /// Classe encarregada de trazer todos os dados de um emitente do sistema.
    /// </summary>
    public class ConsultaEmitente
    {
        private List<DadosConta> _contas ;
        private Proprietario _emitente;
        private List<String> _clientes ;
        private decimal _totalTransitado;
        private decimal _totalADepositar;
        private decimal _totalDevolvido;
        private int _qADepositar;
        private int _qDevolvido;
        private Dictionary<String, decimal> _bancos = new Dictionary<string, decimal>();


        public ConsultaEmitente(List<DadosConta> contas, Proprietario emitente, List<string> clientes)
        {
            _contas = contas;
            _emitente = emitente;
            _clientes = clientes;
            Totalizacao();
        }

        private void Totalizacao()
        {
            _contas.ForEach(c =>
            {
                _totalDevolvido += c.VDevolvidos;
                _totalTransitado += c.Valor;
                _totalADepositar += c.TotalADepositar;
                _qADepositar += c.QADepositar;
                _qDevolvido += c.QDevolvidos;
                if (_bancos.Keys.Contains(c.Conta.IdBanco))
                {
                    _bancos[c.Conta.IdBanco] += c.Valor;
                }
                else
                {
                    _bancos.Add(c.Conta.IdBanco, c.Valor);
                }
                
            });
        }

        public int QADepositar => _qADepositar;
        public int QDevolvido => _qDevolvido;
        public List<DadosConta> Contas => _contas;
        public Proprietario Emitente => _emitente;
        public List<string> Clientes => _clientes;
        public decimal TotalTransitado => _totalTransitado;
        public decimal TotalDevolvido => _totalDevolvido;
        public Dictionary<string, decimal> Bancos => _bancos;

        public decimal TotalADepositar => _totalADepositar;
    }
}
