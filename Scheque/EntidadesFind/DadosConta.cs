﻿using Scheque.Entidade;
using System;

namespace Scheque
{
    public class DadosConta
    {
        private Conta _conta;
        private decimal _valor;
        private int _quantidade;
        private decimal _vDevolvidos;
        private int _qDevolvidos;
        private decimal _totalADepositar;
        private DateTime _ultimaAdicao;
        private DateTime _ultimaDevolicao;
        private int _qADepositar;

        public DadosConta(Conta conta , decimal valor, int quantidade, decimal vDevolvido, int qDevolvido, DateTime uAdd, DateTime uDevolvido, decimal totalADepositar, int qADepositar)
        {
            _conta = conta;
            _valor = valor;
            _quantidade = quantidade;
            _vDevolvidos = vDevolvido;
            _qDevolvidos = qDevolvido;
            _ultimaAdicao = uAdd;
            _ultimaDevolicao = uDevolvido;
            _totalADepositar = totalADepositar;
            _qADepositar = qADepositar;
        }

        public Conta Conta => _conta;
        public decimal Valor => _valor;
        public int Quantidade => _quantidade;
        public decimal VDevolvidos => _vDevolvidos;
        public int QDevolvidos => _qDevolvidos;
        public DateTime UltimaAdicao => _ultimaAdicao;
        public DateTime UltimaDevolicao => _ultimaDevolicao;
        public decimal TotalADepositar => _totalADepositar;
        public int QADepositar => _qADepositar;
    }
}