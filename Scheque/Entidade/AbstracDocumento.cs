﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Interface;
using Scheque.Uteis;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe abstrata para arquivar dados de um documento.
    /// </summary>
    [Table("Documento")]
    public abstract class AbstracDocumento : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private decimal _valor;
        private DateTime _vencimento;
        private StatusDocumento _status;

        public AbstracDocumento()
        {
            Status = StatusDocumento.DIGITADO;
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public decimal Valor
        {
            get { return Math.Round( _valor,2); }
            set {  _valor = Math.Round(value,2); }
        }

        public DateTime Vencimento
        {
            get { return _vencimento; }
            set { _vencimento = value; }
        }

        public int Dias { get; set; }

        public DateTime NovoVencimento { get; set; }

        public String Feriado { get; set; }

        [NotMapped]
        public String DiaDaSemana
        {
            get { return Util.DiaDaSemana(Vencimento);}
            private set { }
        }

        [NotMapped]
        public String DiaDaSemanaNovoVencimento
        {
            get { return (NovoVencimento == null) ? null : Util.DiaDaSemana(NovoVencimento);}
            private set { }
        }
        [NotMapped]
        public int DiasAdicionais
        {
            get
            {
                if (NovoVencimento != null)
                {
                    TimeSpan diasTime = NovoVencimento - Vencimento;
                    return diasTime.Days;
                }

                return 0;
            }
            private set { }
        }

        
        public long? IdEmprestimo
        {
            get;
            set;
        }
        [ForeignKey("IdEmprestimo")]
        public virtual Emprestimo Emprestimo
        {
            get;
            set;
        }

        public StatusDocumento Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string toEstenso => Util.toExtenso((decimal)Valor);

        public override string ToString()
        {
            String retorno = String.Format(" {0:dd/MM/yyyy} - Parcela : {1:C} - Amortização: {2:C} - Juros: {3:C} ",
                _vencimento, Valor, ((Promissoria) this).Valor - ((Promissoria) this).JurosPromissoria,
                ((Promissoria) this).JurosPromissoria);

            return (_vencimento.Year == 0001)?String.Format("Pacela única sem vencimento prévio no valor de R$ {0:C}.", _valor): retorno;
        }
    }
}
