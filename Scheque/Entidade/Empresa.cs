﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Interface;

namespace Scheque.Entidade
{
    [Table("Empresa")]
    public class Empresa : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private long _id;
        private string _nome;
        private string _razao;
        private Endereco _endereco;
        private string _cnpj;

        public Empresa()
        {
            _endereco = new Endereco();
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }
        [Required, MinLength(10)]
        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }
        [Required, MinLength(10)]
        public string Razao
        {
            get { return _razao; }
            set { _razao = value; }
        }

        public Endereco Endereco
        {
            get { return _endereco; }
            set { _endereco = value; }
        }
        [Required, MaxLength(19), MinLength(14)]
        public string Cnpj
        {
            get { return _cnpj; }
            set { _cnpj = value; }
        }
    }
}
