﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Entidade;
using Scheque.Interface;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe encarregada de cuidar dos dados de um lote de cheque para desconto
    /// </summary>
    [Table("Lote")]
    public class Lote : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private Int64 _idCliente;
        private int _quantidade;
        private decimal _valorLote;
        private decimal _taxa;
        private decimal _administrativa;
        private decimal _valorDescontado;
        private DateTime _dataLote;
        private List<Cheque> _cheques = new List<Cheque>();
        private long _idAntigo;

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }



        public long IdCliente
        {
            get { return _idCliente; }
            set { _idCliente = value; }
        }

        public int Quantidade
        {
            get { return _quantidade; }
            set { _quantidade = value; }
        }

        public decimal ValorLote
        {
            get { return _valorLote; }
            set { _valorLote = value; }
        }

        public decimal Taxa
        {
            get { return _taxa; }
            set { _taxa = value; }
        }

        public decimal Administrativa
        {
            get { return _administrativa; }
            set { _administrativa = value; }
        }

        public decimal ValorDescontado
        {
            get { return _valorDescontado; }
            set { _valorDescontado = value; }
        }

        public DateTime DataLote
        {
            get { return _dataLote; }
            set { _dataLote = value; }
        }
        /// <summary>
        /// Valor total de juros cobrados no lote do cheque descontados.
        /// </summary>
        [NotMapped]
        public decimal TotalJuros
        {
            get
            {
                decimal totalJuros = 0;
                foreach (var cheque in Cheques)
                {
                    totalJuros += cheque.JurosCheque;
                }

                return Math.Round(totalJuros,2);
            }
            private set { }
        }
        /// <summary>
        /// Lista de chques pertencentes ao lote.
        /// </summary>
        /// <returns></returns>
        public virtual List<Cheque> Cheques
        {
            get { return _cheques; }
            set { _cheques = value; }
        }

        [NotMapped]
        public List<Cheque> AllCheques
        {
            get { return _cheques; }
            private set { }
        }

        [ForeignKey("IdCliente")]
        public virtual Cliente Cliente { get; set; }

        public void AddCheque(Cheque cheque, int index = -1)
        {
            if(index >= 0 && index <= (_cheques.Count +1)){
                _cheques[index] = cheque;
            }
            else
            {
                bool encontrado = false;
                for (int i = 0; i < _cheques.Count; i++)
                {
                    if (_cheques[i].Equals(cheque))
                    {
                        _cheques[i]=cheque;
                        return;
                    }
                }
                if (!encontrado)
                {
                    _cheques.Add(cheque);
                }
            }

        }

        public void RemoveCheque(Cheque cheque)
        {
            _cheques.Remove(cheque);
        }

        public void RemoveAllCheuqes()
        {
            _cheques = new List<Cheque>();
        }

        [NotMapped]
        public decimal TotalCheques
        {
            get
            {
                decimal total = 0;
                foreach (var cheque in _cheques)
                {
                    total += cheque.Valor;
                }

                return Math.Round(total,2);
            }
            private set { }
        }
        [NotMapped]
        public String NomeCliente
        {
            get { return Cliente !=null? Cliente.Nome.ToUpper():""; }
            private set { }

        }

        public long IdAntigo { get => _idAntigo; set => _idAntigo = value; }

        public override string ToString()
        {
            return "Lote: " + Id + " - " + Cliente.Nome.ToUpper() + " " + (string.Format("{0:C}", ValorLote));
        }
    }
}
