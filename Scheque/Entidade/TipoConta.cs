﻿namespace Scheque.Entidade
{
    public enum TipoConta
    {
        DINHEIRO, BANCO, DESPESA, CHEQUE, PROMISSORIA
    }
}