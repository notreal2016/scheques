﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Interface;

namespace Scheque.Entidade
{
    [Table("Registro")]
    public class Registro : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Int64 _id;
        public String _cnpjCpf;
        public String _nome;
        public String _endereco;
        public int _numero;
        public String _bairro;
        public String _telefone;
        public String _chave;
        public String _serial;

        public string CnpjCpf
        {
            get { return _cnpjCpf; }
            set { _cnpjCpf = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        public string Endereco
        {
            get { return _endereco; }
            set { _endereco = value; }
        }

        public int Numero
        {
            get { return _numero; }
            set { _numero = value; }
        }

        public string Bairro
        {
            get { return _bairro; }
            set { _bairro = value; }
        }

        public string Telefone
        {
            get { return _telefone; }
            set { _telefone = value; }
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Chave
        {
            get { return _chave; }
            set { _chave = value; }
        }

        public string Serial
        {
            get { return _serial; }
            set { _serial = value; }
        }
    }
}
