﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Interface;

namespace Scheque.Entidade
{
    public class MovimentoDuplicata: MovimentoDocumento
    {
        private Int64? _idEmprestimo;

        public long? IdEmprestimo
        {
            get { return _idEmprestimo; }
            set { _idEmprestimo = value; }
        }
    }
}
