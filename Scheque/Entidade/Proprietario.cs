﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using Scheque.Interface;

namespace Scheque.Entidade
{
    [Table("Proprietario")]
    public class Proprietario : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private String _cpfcnpj;
        private String _nome;

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }
        [Index("IX_Cpf", 2, IsUnique = true), MaxLength(18)]
        public string CpfCNPJ
        {
            get { return _cpfcnpj; }
            set { _cpfcnpj = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }
    }
}