﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Interface;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe encarregada dos dados de movimento relacionando a um documento
    /// </summary>
    [Table("MovimentoDocumento")]
    public class MovimentoDocumento : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private StatusDocumento _status;
        private DateTime _data;
        private Int64? _idMovimentoCliente;

        public MovimentoDocumento()
        {

        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public StatusDocumento Status
        {
            get { return _status; }
            set { _status = value; }
        }


        public long? IdMovimentoCliente
        {
            get { return _idMovimentoCliente; }
            set { _idMovimentoCliente = value; }
        }

        public DateTime Data
        {
            get { return _data; }
            set { _data = value; }
        }





    }
}
