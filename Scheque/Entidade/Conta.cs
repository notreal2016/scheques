﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Scheque.Interface;

namespace Scheque.Entidade
{
    [Table("Conta")]
    [Serializable, JsonObject]
    public class Conta : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private Int64? _idProprietario;
        private String _idBanco;
        private String _agencia;
        private String _contaCorrente;
        private String _operacao;
        private Boolean _daEmpresa = false;
        private string _compensassao;
        

        public Conta()
        {
            _operacao = "000";
        }


        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public long? IdProprietario
        {
            get { return _idProprietario; }
            set { _idProprietario = value; }
        }

        public string IdBanco
        {
            get { return _idBanco; }
            set { _idBanco = value; }
        }

        public string Agencia
        {
            get { return _agencia; }
            set { _agencia = value; }
        }

        public string ContaCorrente
        {
            get { return _contaCorrente; }
            set { _contaCorrente = value; }
        }

        public string Operacao
        {
            get { return _operacao; }
            set { _operacao = value; }
        }

        [ForeignKey("IdProprietario")]
        public virtual Proprietario Proprietario { get; set; }

        [ForeignKey("IdBanco")]
        public virtual Banco Banco { get; set; }
        public bool DaEmpresa
        {
            get
            {
                return _daEmpresa;
            }
            set
            {
                _daEmpresa = value;
            }
        }

        public string Compensassao 
            { 
            get => _compensassao; 
            set => _compensassao = value; 
            }
        
        public bool ComparaConta(Conta c, int ndConta = 6 )
        {
            //Condição para bancos cujas as contas não tem 6 dígitos e sim 5 caso do Itaú
            string thisCC ="", newCC ="";
            for (int i = ndConta -1; i >=0 ; i--)
            {
                thisCC += this.ContaCorrente[i];
                newCC += c.ContaCorrente[i];
            }
            if (c.Agencia.Equals(this.Agencia) && thisCC.Equals(newCC)  && this.IdBanco.Equals(c.IdBanco))
            {
                return true;
            }
            return false;
        }

        
    }
}