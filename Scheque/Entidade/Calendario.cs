﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Interface;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe encarregada de arquivar dados de um feriado no calendário
    /// </summary>
    [Table("Calendario")]
    public class Calendario : IPersistenceBase
    {


        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private DateTime _dataFeriado;
        private bool _repetirAoAno;
        private String _nomeFeriado;

        public Calendario()
        {
            _repetirAoAno = true;
        }

        public string NomeFeriado
        {
            get { return _nomeFeriado; }
            set { _nomeFeriado = value; }
        }

        public DateTime DataFeriado
        {
            get { return _dataFeriado; }
            set { _dataFeriado = value; }
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public bool RepetirAoAno
        {
            get { return _repetirAoAno; }
            set { _repetirAoAno = value; }
        }

        [NotMapped]
        public string Dia => String.Format("{0:dd}", DataFeriado);

        [NotMapped]
        public string Ano => string.Format("{0:yyyy}", DataFeriado);

        [NotMapped]
        public string Mes => string.Format("{0:MM}", DataFeriado);

    }
}
