﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Interface;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe encarregada em armazenar os dados de uma negativação ocorrida no sistema
    /// </summary>
    [Table("Negativado")]
    public class Negativado : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private Int64 _idConta,  _idMotivo ;
        private Int64? _idDocumento;
        private DateTime _data;
        private decimal _valor;

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public long IdConta
        {
            get { return _idConta; }
            set { _idConta = value; }
        }

        public long? IdDocumento
        {
            get { return _idDocumento; }
            set { _idDocumento = value; }
        }

        public long IdMotivo
        {
            get { return _idMotivo; }
            set { _idMotivo = value; }
        }

        public DateTime Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public decimal Valor
        {
            get => _valor;
            set => _valor = value;
        }

        [ForeignKey("IdConta")]
        public virtual Conta Conta { get; set;}
    }

    
}
