﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Interface;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe que contém dados de uma conta que recebeu dados de restrição.
    /// </summary>
    [Table("Restrito")]
    public class Restrito : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private Conta _conta;

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public Conta Conta
        {
            get { return _conta; }
            set { _conta = value; }
        }
    }
}
