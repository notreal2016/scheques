﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Scheque.Interface;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe encarregada de arquivar dados de um emprestimo realizado
    /// </summary>
    [Table("Emprestimo")]
    public class Emprestimo : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private Int64 _idCliente;
        private DateTime _dataContrato;
        private decimal _valor;
        private int _numeroParcelas;
        private int _intervalo;
        private decimal _taxaJuros;
        private decimal _taxaAdministrativa;
        private List<AbstracDocumento> _documentos = new List<AbstracDocumento>();
        private bool _quitado;
        private bool _adiantamento;
        private bool _sac;
        private bool _fixo;
        private TipoEmprestimo _tipo;
        private long _idAntigo;

        public Emprestimo()
        {
            _quitado = false;
            _adiantamento = false;
            _sac = false;
            _fixo = false;
        }

        
        public TipoEmprestimo Tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }

        public bool Sac
        {
            get { return _sac; }
            set { _sac = value; }
        }

        public bool Fixo
        {
            get { return _fixo; }
            set { _fixo = value; }
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public long IdCliente
        {
            get { return _idCliente; }
            set { _idCliente = value; }
        }
        [ForeignKey("IdCliente")]
        public virtual Cliente Cliente { get; set; }
        public DateTime DataContrato
        {
            get { return _dataContrato; }
            set { _dataContrato = value; }
        }

        public decimal Valor
        {
            get { return _valor; }
            set { _valor = value; }
        }

        public int NumeroParcelas
        {
            get { return _numeroParcelas; }
            set { _numeroParcelas = value; }
        }

        public int Intervalo
        {
            get { return _intervalo; }
            set { _intervalo = value; }
        }

        public decimal TaxaJuros
        {
            get { return _taxaJuros; }
            set { _taxaJuros = value; }
        }

        public decimal TaxaAdministrativa
        {
            get { return _taxaAdministrativa; }
            set { _taxaAdministrativa = value; }
        }

        public virtual List<AbstracDocumento> Documentos
        {
            get { return _documentos; }
            set { _documentos = value; }
        }

        public bool Quitado
        {
            get { return _quitado; }
            set { _quitado = value; }
        }

        public void AddDocumento(AbstracDocumento documento)
        {
            _documentos.Add(documento);
        }

        public void RemDocumento(AbstracDocumento doc)
        {
            _documentos.Remove(doc);
        }

        public void RemAllDocumentos()
        {
            var list = _documentos.ToList();
            foreach (var abstracDocumento in list)
            {
                RemDocumento(abstracDocumento);
            }

        }

        public bool Adiantamento
        {
            get { return _adiantamento; }
            set { _adiantamento = value; }
        }

        public long IdAntigo { get => _idAntigo; set => _idAntigo = value; }

        public override string ToString()
        {
            return String.Format("Valor : {0:C} - {1}", Valor, Cliente.Nome);
        }
    }
}
