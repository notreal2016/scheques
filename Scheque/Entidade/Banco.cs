﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Scheque.Interface;

namespace Scheque.Entidade
{
    [Table("Banco")]
    public class Banco : IPersistenceBase
    {
        [Key]
        private String _id;
        private String _nome;
        private String _sigla;
        private int _numeroDigitosConta = 6;

        
        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        public string Sigla
        {
            get { return _sigla; }
            set { _sigla = value; }
        }

        public int NumeroDigitosConta { get => _numeroDigitosConta; set => _numeroDigitosConta = value; }

        public override string ToString()
        {
            return this.Id + " - " + this.Nome;
        }
    }

}