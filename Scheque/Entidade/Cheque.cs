﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Scheque.Interface;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe encarregada de guardar dados de um cheque
    /// </summary>
    public class Cheque : AbstracDocumento
    {
        private Int64 _idConta;
        private String _numeroCheque;
        private decimal _jurosCheque;
        
        private Int64? _idLote;

        public long IdConta
        {
            get { return _idConta; }
            set { _idConta = value; }
        }

        public string NumeroCheque
        {
            get { return _numeroCheque; }
            set { _numeroCheque = value; }
        }

        
        public long? IdLoteDeposito
        {
            get;
            set;
        }

        public decimal JurosCheque
        {
            get { return Math.Round(_jurosCheque, 2); }
            set { _jurosCheque = Math.Round(value,2); }
        }

       
        [ForeignKey("IdConta")]
        public virtual Conta ContaCheque
        {
            get;
            set;
        }
        
        [NotMapped]
        public decimal ValorDescontado
        {
            get
            {
                return base.Valor - JurosCheque;}
            private set {}
        }
        
        public long? IdLote
        {
            get { return _idLote; }
            set { _idLote = value; }
        }

        public override string ToString()
        {
            
            return ((ContaCheque == null)?"Dados da conta do cheque não informado":"BANCO: " +  ContaCheque.Banco.Id +
                   " AG: " + ContaCheque.Agencia +
                   " CC: " + ContaCheque.ContaCorrente + " Nº: " + this.NumeroCheque) +
                   " VENC.: " + this.Vencimento.ToShortDateString() + " " + (String.Format("{0:C}", Valor)) ;
        }

        [NotMapped]
        public String Banco
        {
            get { return ContaCheque.IdBanco;}
            private set { }
        }

        [NotMapped]
        public String Agencia
        {
            get { return ContaCheque.Agencia; }
            private set { }
        }

        [NotMapped]
        public String CC
        {
            get { return ContaCheque.ContaCorrente; }
            private set { }
        }

        [NotMapped]
        public String Emitente
        {
            get { return ContaCheque.Proprietario!= null ? ContaCheque.Proprietario.Nome.ToUpper():"EMITENTE NÃO CADASTRADO"; }
            private set { }
        }

        [NotMapped]
        public String DocEmitente
        {
            get { return ContaCheque.Proprietario != null ? ContaCheque.Proprietario.CpfCNPJ: "EMITENTE NÃO CADASTRADO"; }
            private set { }
        }
        [ForeignKey("IdLote")]
        public virtual Lote Lote
        {
            get;
            set;
        }
        [ForeignKey("IdLoteDeposito")]
        public virtual LoteDeposito LoteDeposito
        {
            get;
            set;
        }

        
        public override bool Equals(object obj)
        {
           
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            Cheque c = (Cheque) obj;
            if (c.ContaCheque != null && this.ContaCheque != null && c.ContaCheque.Equals(this.ContaCheque) && this.NumeroCheque == c.NumeroCheque)
            {
                return true;
            }

            return base.Equals(obj);
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}