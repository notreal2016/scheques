﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Interface;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe encarregada de arquivar dados de um usuário.
    /// </summary>
    [Table("Usuario")]
    [Serializable]
    public class Usuario : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private String _nome;
        private String _login;
        private String _senha;
        private bool _ativo;
        private long? _idEmpresa;
        private Boolean _primeiro = true;
        private TipoUsuario _tipo = TipoUsuario.SIMPLES;

        public TipoUsuario Tipo
        {
            get => _tipo;
            set => _tipo = value;
        }

        public Usuario()
        {
            _ativo = true;
        }

        public bool Primeiro
        {
            get { return _primeiro; }
            set { _primeiro = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Login
        {
            get { return _login; }
            set { _login = value; }
        }

        public string Senha
        {
            get { return _senha; }
            set { _senha = value; }
        }

        public bool Ativo
        {
            get { return _ativo; }
            set { _ativo = value; }
        }

        public override string ToString()
        {
            return Nome.ToUpper();
        }

        
        public long? IdEmpresa
        {
            get { return _idEmpresa; }
            set { _idEmpresa = value; }
        }
        [ForeignKey("IdEmpresa")]
        public virtual Empresa Empresa { get; set; }
    }
}
