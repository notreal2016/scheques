﻿using System.Runtime.Remoting.Messaging;

namespace Scheque.Entidade
{
    public enum StatusDocumento
    {
        DIGITADO =0 , SEPARADO= 1, DEVOLVIDO=2, USADO=3, ENTREGUE=4, DEPOSITADO=5, QUITADO=6, QUITADOPARCIAL=7, TODOS=8
            
    }
}