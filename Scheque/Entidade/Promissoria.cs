﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Uteis;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe encarregada de guardar os dados de uma promissória.
    /// </summary>
    public class Promissoria: AbstracDocumento
    {
        
        private decimal _jurosPromissoria;
        

        public decimal JurosPromissoria
        {
            get { return _jurosPromissoria; }
            set { _jurosPromissoria = value; }
        }

        [NotMapped]
        public String NomeCliente => base.Emprestimo.Cliente.Nome;

        [NotMapped]
        public String DocumentoCliente => base.Emprestimo.Cliente.Cpfcnpj;

        [NotMapped]
        public DateTime Emissao => base.Emprestimo.DataContrato;
        
    }
}
