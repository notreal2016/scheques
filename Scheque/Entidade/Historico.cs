﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Scheque.Interface;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe encarregada das informações de um histórico de transações.
    /// </summary>
    [Table("Historico")]
    public class Historico : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private TipoHistorico _tipo;
        private String _descricao;
        private TipoConta _tipoConta;
        private TipoConta _tipoReverso;
        private Boolean _exclusivo;

        /// <summary>
        /// Cria um hitórico de uma transação
        /// </summary>
        /// <param name="tipo">Tipo de histórico podendo ser crédito ou débito</param>
        /// <param name="descricao">Descrição do histórico</param>
        /// <param name="tipoConta">Tipo de conta a qual fará a transação</param>
        /// <param name="tipoReverso">Tipo de conta reversa a transação</param>
        /// <param name="exclusivo">Se a transação é exclusiva para promissoria</param>
        public Historico(TipoHistorico tipo, string descricao, TipoConta tipoConta, TipoConta tipoReverso, bool exclusivo)
        {
            _tipo = tipo;
            _descricao = descricao;
            _tipoConta = tipoConta;
            _tipoReverso = tipoReverso;
            _exclusivo = exclusivo;
        }
        /// <summary>
        /// Cria um histórico em branco
        /// </summary>
        public Historico()
        {
        }

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public TipoHistorico Tipo
        {
            get { return _tipo; }
            set { _tipo = value; }
        }

        public string Descricao
        {
            get { return _descricao; }
            set { _descricao = value; }
        }

        public TipoConta TipoConta
        {
            get { return _tipoConta; }
            set { _tipoConta = value; }
        }

        public TipoConta TipoReverso
        {
            get { return _tipoReverso; }
            set { _tipoReverso = value; }
        }

        public bool Exclusivo
        {
            get { return _exclusivo; }
            set { _exclusivo = value; }
        }
    }
}