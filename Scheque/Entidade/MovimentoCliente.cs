﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Interface;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe encarregada de guardar dados de movimento realizado por um cliente
    /// </summary>
    [Table("MovimentoCliente")]
    public class MovimentoCliente : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private Int64 _idCliente;
        private Int64 _idHitorico;
        private decimal _valor;
        private DateTime _data;
        private String _descricao;
        private Int64 _idUser;
        /// <summary>
        /// Cria um objeto movimento em branco
        /// </summary>
        public MovimentoCliente()
        {
        }
        /// <summary>
        /// Cria um objeto tipo movimento de cliente
        /// </summary>
        /// <param name="idCliente">id do cliente</param>
        /// <param name="idHitorico">id do histórico</param>
        /// <param name="valor">Valor do movimento</param>
        /// <param name="data">Data do movimento</param>
        /// <param name="descricao">Descrição do movimento</param>
        /// <param name="idUser">Id do usuário</param>
        public MovimentoCliente(long idCliente, long idHitorico, decimal valor, DateTime data, string descricao, long idUser)
        {
            _idCliente = idCliente;
            _idHitorico = idHitorico;
            _valor = valor;
            _data = data;
            _descricao = descricao;
            _idUser = idUser;
        }

        
        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public long IdCliente
        {
            get { return _idCliente; }
            set { _idCliente = value; }
        }
            
        public long IdHitorico
        {
            get { return _idHitorico; }
            set { _idHitorico = value; }
        }

        public decimal Valor
        {
            get { return _valor; }
            set { _valor = value; }
        }

        public DateTime Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public string Descricao
        {
            get { return _descricao; }
            set { _descricao = value; }
        }

        public long IdUser
        {
            get { return _idUser; }
            set { _idUser = value; }
        }

        [ForeignKey("IdCliente")]
        public virtual Cliente Cliente { get; set; }
        [ForeignKey("IdHitorico")]
        public virtual Historico Historico { get; set; }
    }
}
