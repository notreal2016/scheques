﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Scheque.Interface;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe encarregada de agregar dados de cheques separado para deposito.
    /// </summary>
    [Table("LoteDeposito")]
    public class LoteDeposito : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private DateTime _data;
        private Int64 _idContaBanco;
        private Int64 _quantidadeDocumento;
        private decimal _valorLote;
        private Boolean _realizado = false;
        private Int64 _idUsuario;
        private List<Cheque> _cheques;

        public long Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public DateTime Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public long IdContaBanco
        {
            get { return _idContaBanco; }
            set { _idContaBanco = value; }
        }

        public long QuantidadeDocumento
        {
            get { return _quantidadeDocumento; }
            set { _quantidadeDocumento = value; }
        }

        public decimal ValorLote
        {
            get { return _valorLote; }
            set { _valorLote = value; }
        }

        public bool Realizado
        {
            get { return _realizado; }
            set { _realizado = value; }
        }

        public long IdUsuario
        {
            get { return _idUsuario; }
            set { _idUsuario = value; }
        }

        public virtual List<Cheque> Cheques
        {
            get { return _cheques; }
            set { _cheques = value; }
        }
    }
}
