﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Server;
using Scheque.Interface;

namespace Scheque.Entidade
{
    [Table("Cliente")]
    public class Cliente : IPersistenceBase
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        private Int64 _id;
        private String _nome;
        private String _CPFCNPJ;
        private String _endereco;
        private int _numero;
        private String _bairro;
        private String _cidada;
        private String _uf;
        private String _telefone;
        private decimal _taxa;
        private decimal _administrativa;
        private bool _acumulado;
        private int _diasMinimo;
        private long? _idEmpresa;
        public Cliente()
        {
            _acumulado = false;
      

        }

        public long? IdEmpresa
        {
            get { return _idEmpresa; }
            set { _idEmpresa = value; }
        }

        [ForeignKey("IdEmpresa")]
        public virtual Empresa Empresa { get; set; }
        public Int64 Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Nome
        {
            get { return _nome; }
            set { _nome = value; }
        }

        public string Cpfcnpj
        {
            get { return _CPFCNPJ; }
            set { _CPFCNPJ = value; }
        }

        public string Endereco
        {
            get { return _endereco; }
            set { _endereco = value; }
        }

        public int Numero
        {
            get { return _numero; }
            set { _numero = value; }
        }

        public string Bairro
        {
            get { return _bairro; }
            set { _bairro = value; }
        }

        public string Cidada
        {
            get { return _cidada; }
            set { _cidada = value; }
        }

        public string Uf
        {
            get { return _uf; }
            set { _uf = value; }
        }

        public string Telefone
        {
            get { return _telefone; }
            set { _telefone = value; }
        }

        public decimal Taxa
        {
            get { return _taxa; }
            set { _taxa = value; }
        }

        public decimal Administrativa
        {
            get { return _administrativa; }
            set { _administrativa = value; }
        }

        public override string ToString()
        {
            return Nome.ToUpper() + " - " + Id;
        }

        public bool Acumulado
        {
            get { return _acumulado; }
            set { _acumulado = value; }
        }

        public int DiasMinimo
        {
            get
            {
                return _diasMinimo;
            }

            set
            {
                _diasMinimo = value;
            }
        }

    }
}
