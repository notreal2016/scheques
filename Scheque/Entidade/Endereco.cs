﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Scheque.Entidade
{
    [ComplexType]
    public class Endereco 
    {
        private string _logradouro;
        private string _bairro;
        private string _cidade;
        private string _uf;
        private string _cep;

        public Endereco()
        {
        }

        public string Logradouro
        {
            get { return _logradouro; }
            set { _logradouro = value; }
        }

        public string Bairro
        {
            get { return _bairro; }
            set { _bairro = value; }
        }

        public string Cidade
        {
            get { return _cidade; }
            set { _cidade = value; }
        }
        [MaxLength(2), MinLength(2)]
        public string Uf
        {
            get { return _uf; }
            set { _uf = value; }
        }
        [MaxLength(9), MinLength(9)]
        public string Cep
        {
            get { return _cep; }
            set { _cep = value; }
        }
    }
}