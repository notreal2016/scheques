﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Interface;

namespace Scheque.Entidade
{
    /// <summary>
    /// Classe encarregada de manter os dados dos motivos de devolução segundo a febraban
    /// </summary>
    [Table("Motivo")]
    public class Motivo : IPersistenceBase
    {
        [Key]
        private int _id;
        private String _descircao;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Descircao
        {
            get { return _descircao; }
            set { _descircao = value; }
        }
    }
}
