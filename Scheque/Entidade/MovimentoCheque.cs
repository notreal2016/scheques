﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Scheque.Interface;

namespace Scheque.Entidade
{
    public class MovimentoCheque : MovimentoDocumento,  IPersistenceBase
    {
        private Int64? _idCheque;
        private Int64? _idLoteSeparacao;
        private Int32? _idMotivoCheque;

        public long? IdCheque
        {
            get { return _idCheque; }
            set { _idCheque = value; }
        }

        public long? IdLoteSeparacao
        {
            get { return _idLoteSeparacao; }
            set { _idLoteSeparacao = value; }
        }

        public int? IdMotivoCheque
        {
            get => _idMotivoCheque;
            set => _idMotivoCheque = value;
        }

        [ForeignKey("IdMotivoCheque")]
        public virtual Motivo Motivo
        {
            get; set;
        }
    }
}
