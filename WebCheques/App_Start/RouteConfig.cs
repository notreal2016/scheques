﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace WebCheques
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            
            routes.MapRoute(
                name: "VaidaLogin",
                url: "Usuario/ValidaLogin/{login}/{id}",
                defaults: new { controller = "Usuario", action = "ValidaLogin", login = UrlParameter.Optional , id = UrlParameter.Optional}
            );

          

            routes.MapRoute(
                name: "ConsultaCheque",
                url: "Consulta/ConsultaCheque/{cmc7}/{idBanco}/{agencia}/{conta}/{operacao}/{numero}",
                defaults: new
                {
                    controller = "Consulta", action = "ConsultaCheque",
                    cmc7 = UrlParameter.Optional,
                    idBanco = UrlParameter.Optional, agencia = UrlParameter.Optional,
                    conta = UrlParameter.Optional, opercao = UrlParameter.Optional, numero = UrlParameter.Optional
                }
            );

            routes.MapRoute(
                name: "GravaConta",
                url: "Cheque/GravaConta/{informacao}",
                defaults: new { controller = "Cheque", action = "GravaConta", informacao = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ListaLotes",
                url: "Cheque/ListaLotes/{termo}/{tipo}",
                defaults: new { controller = "Cheque", action = "ListaLotes", termo = UrlParameter.Optional, tipo = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "BuscaEmitente",
                url: "Cheque/BuscaEmitente/{cnpj}",
                defaults: new { controller = "Cheque", action = "BuscaEmitente", cnpj = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ChequeLista",
                url: "Consulta/chequelista/{idConta}/{status}",
                defaults: new { controller = "Consulta", action = "chequelista", idconta = UrlParameter.Optional, status = UrlParameter.Optional }
            );

            routes.MapRoute(
              name: "CalculaJuros",
              url: "Cheque/CalculaJuros/{dtLote}/{dtVencimento}/{taxa}/{valor}/{minimo}/{composto}",
              defaults: new { controller = "Cheque", action = "CalculaJuros", dtLote = UrlParameter.Optional,
              dtVencimento = UrlParameter.Optional, taxa = UrlParameter.Optional, valor = UrlParameter.Optional,
              minimo = UrlParameter.Optional, composto = UrlParameter.Optional});

            
            routes.MapRoute(
              name: "LerDadosChque",
              url: "Cheque/LerDadosCheque/{banco}/{agencia}/{conta}/{cheque}/{operacao}",
              defaults: new
              {
                  controller = "Cheque",
                  action = "LerDadosCheque",
                  banco = UrlParameter.Optional,
                  agencia = UrlParameter.Optional,
                  conta = UrlParameter.Optional,
                  chque = UrlParameter.Optional,
                  operacao = UrlParameter.Optional
              });

            routes.MapRoute(
                name: "LerCMC",
                url: "Cheque/LerCMC/{cmc}",
                defaults: new { controller = "Cheque", action = "LerCMC", cmc = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "FindClienteByNome",
                url: "Cliente/FindClienteByNome/{termo}",
                defaults: new { controller = "Cliente", action = "FindClienteByNome", termo= UrlParameter.Optional}
            );
            routes.MapRoute(
                name: "DadosCheque",
                url: "Cheque/DadosCheque/{index}",
                defaults: new { controller = "Cheque", action = "DadosCheque", index = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "VaidaCPF",
                url: "Cheque/ValidaCPF/{CPF}",
                defaults: new { controller = "Cheque", action = "ValidaCPF", CPF = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "DadosFeriado",
                url: "Calendario/DadosFeriado/{id}/{data}",
                defaults: new { controller = "Calendario", action = "DadosFeriado", id = UrlParameter.Optional, data = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "ListaCliente",
                url: "Cliente/ListaCliente/{termo}",
                defaults: new { controller = "Cliente", action = "ListaCliente", termo = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "ListaUsuario",
                url: "Usuario/ListaUsuario/{termo}",
                defaults: new { controller = "Usuario", action = "ListaUsuario", termo = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "DadosCalendario",
                url: "Calendario/DadosCalendario/{mes}/{ano}",
                defaults: new { controller = "Calendario", action = "DadosCalendario", mes = UrlParameter.Optional, ano = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "Calendario",
                url: "Calendario/Calendario/{mes}/{ano}",
                defaults: new { controller = "Calendario", action = "Calendario", mes = UrlParameter.Optional, ano = UrlParameter.Optional}
            );
            routes.MapRoute(
                name: "VaidaCNPJ",
                url: "Cheque/ValidaCNPJ/{CNPJ}",
                defaults: new { controller = "Cheque", action = "ValidaCNPJ", CNPJ = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "DadosBanco",
                url: "Banco/DadosBanco/{id}",
                defaults: new { controller = "Banco", action = "DadosBanco", id = UrlParameter.Optional}
            );

            routes.MapRoute(
                name: "Banco",
                url: "Banco/banco/{cod}",
                defaults: new { controller = "banco", action = "banco", cod = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "ExcluiFeriado",
                url: "Calendario/ExcluiFeriado/{id}/{mes}/{ano}",
                defaults: new { controller = "Calendario", action = "ExcluiFeriado", id = UrlParameter.Optional, mes = UrlParameter.Optional, ano = UrlParameter.Optional}
            );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
