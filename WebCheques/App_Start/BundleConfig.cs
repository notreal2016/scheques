﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace WebCheques.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/endScript").Include(
                "~/js/admin.js",
                "~/plugins/jquery-validation/jquery.validate.js",
                "~/plugins/jquery-steps/jquery.steps.js",
                "~/js/pages/forms/form-wizard.js",
                "~/plugins/bootstrap-select/js/bootstrap-select.js",
                "~/plugins/dropzone/dropzone.js",
                "~/plugins/jquery-spinner/js/jquery.spinner.js",
                "~/scripts/jquery.unobtrusive-ajax.min.js",
                "~/js/pages/ui/tooltips-popovers.js"
                ));
            bundles.Add(new ScriptBundle("~/bundles/jQuery").Include(
                "~/scripts/jquery-1.10.2.min.js",
                "~/Scripts/jquery.validate.min.js", 
                "~/Scripts/jquery.validade.unobtrusive.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
                "~/plugins/jquery/jquery.min.js",
                "~/plugins/bootstrap/js/bootstrap.js",
                
                "~/plugins/jquery-slimscroll/jquery.slimscroll.js",
                "~/plugins/node-waves/waves.js",
                "~/plugins/jquery-countto/jquery.countTo.js",
                "~/plugins/raphael/raphael.min.js",
                "~/plugins/morrisjs/morris.js",
                "~/plugins/bootstrap-notify/bootstrap-notify.js",
                "~/plugins/jquery-sparkline/jquery.sparkline.js",
                "~/js/pages/ui/tooltips-popovers.js",
                "~/js/pages/ui/dialogs.js",
                "~/plugins/sweetalert/sweetalert.min.js",
                "~/js/demo.js",
                "~/plugins/sweetalert/sweetalert.min.js",
                "~/js/pages/ui/dialogs.js",
                "~/plugins/bootstrap-select/js/bootstrap-select.js",
                
                "~/scripts/jquery.mask.js",
                "~/plugins/multi-select/js/jquery.multi-select.js",
                
                "~/scripts/jquery.mask.js",
                "~/scripts/Util.js"
                ));
            
            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/plugins/sweetalert/sweetalert.css",
                "~/plugins/bootstrap/css/bootstrap.css",
                "~/plugins/node-waves/waves.css",
                "~/plugins/animate-css/animate.css",
                "~/plugins/morrisjs/morris.css",
                "~/css/style.css",
                "~/css/themes/all-themes.css",
                "~/plugins/sweetalert/sweetalert.css",
                "~/plugins/multi-select/css/multi-select.css",
                "~/plugins/bootstrap-select/css/bootstrap-select.css",
                "~/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css",
                "~/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css",
                "~/plugins/multi-select/css/multi-select.css",
                "~/plugins/bootstrap-select/css/bootstrap-select.css" 
                ));

            bundles.Add(new StyleBundle("~/Content/dataTable").Include(
                        "~/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css",
                        "~/plugins/multi-select/css/multi-select.css"
                ));
            
            bundles.Add(new ScriptBundle("~/bundles/dataTable").Include(
                "~/plugins/jquery-datatable/jquery.dataTables.js",
                "~/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js",
                "~/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js",
                "~/plugins/jquery-datatable/extensions/export/buttons.flash.min.js",
                "~/plugins/jquery-datatable/extensions/export/jszip.min.js",
                "~/plugins/jquery-datatable/extensions/export/pdfmake.min.js",
                "~/plugins/jquery-datatable/extensions/export/vfs_fonts.js",
                "~/plugins/jquery-datatable/extensions/export/buttons.html5.min.js",
                "~/plugins/jquery-datatable/extensions/export/buttons.print.min.js",
                "~/js/pages/tables/jquery-datatable.js"
                ));
            
            }


    }
}