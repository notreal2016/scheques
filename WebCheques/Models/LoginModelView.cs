﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using  System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace WebCheques.Models
{
    public class LoginModelView
    {
        [HiddenInput]
        public string UrlRetorno { get; set; }
        [Required(ErrorMessage = "Login deve ser informado.")]
        public string Login { get; set; }
        [Required(ErrorMessage = "Senha deve ser informado.")]
        [DataType(DataType.Password)]
        public string Senha { get; set; }
        
    }
}