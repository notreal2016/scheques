﻿//mask
var maskBehavior = function(val) 
{
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},options = {onKeyPress: function(val, e, field, options) {
    field.mask(maskBehavior.apply({}, arguments), options);
}};

$('.phone').mask(maskBehavior, options);

var maskMoeda = function(val) 
{
    return "999999,00";
},options = {onKeyPress: function(val, e, field, options) {
        
    field.mask(maskMoeda.apply({}, arguments), options);
}};

$('.moeda').mask(maskMoeda, options);

var maskData = function(val) 
{
    return "99/99/9999";
},options = {onKeyPress: function(val, e, field, options) {
        
    field.mask(maskData.apply({}, arguments), options);
}};

$('.dataMask').mask(maskData, options);


var maskCep =function(val) {
        return val.replace(/\D/g, '').length === 8 ? '00000-000' : '00000-009';
    },
    options = {onKeyPress: function(val, e, field, options) {
        field.mask(maskCep.apply({}, arguments), options);
    }};
$('.CEP').mask(maskCep, options);

var maskCNPJ = function(val){
    return val.replace(/\D/g, '').length === 14 ? '00.000.000/0000-00' : '00.000.000/0000-09';
    },
    options = {onKeyPress: function(val, e, field, options) {
        field.mask(maskCNPJ.apply({}, arguments), options);
    }};
$('.CNPJ').mask(maskCNPJ, options);

var maskCPF = function(val){
    return val.replace(/\D/g, '').length === 11 ? '000.000.000-00' : '000.000.000-009';
    },
    options = {onKeyPress: function(val, e, field, options) {
        field.mask(maskCPF.apply({}, arguments), options);
    }};
$('.CPF').mask(maskCPF, options);

var maskCPFCNPJ = function(val){
        return val.replace(/\D/g, '').length === 14 ? '00.000.000/0000-00':'000.000.000-00999'  ;
    },
    options = {onKeyPress: function(val, e, field, options) {
        field.mask(maskCPFCNPJ.apply({}, arguments), options);
    }};
$('.CPFCNPJ').mask(maskCPFCNPJ, options);

$('.dataMask').mask('00/00/0000');
$('.dinheiro').mask('000.000.000.000.000,00', { reverse: true });
$('.agencia').mask('0000', { reverse: true });
$('.operacao').mask('000', { reverse: true });
$('.conta').mask('000000', { reverse: true });
$('.cheque').mask('000000', { reverse: true });

var latitude;
var longitude;

//Método encarregado de validar CNPJ ou CPF, precisa ser editado a URL caso mude de projeto
function validaData(data) {
    var ano = (data.length === 10) ? data.substr(8) : data.substr(6) ,
        mes = data.substr(3, 2),
        dia = data.substr(0, 2),
        M30 = ['04', '06', '09', '11'],
        v_mes = /(0[1-9])|(1[0-2])/.test(mes),
        //v_ano = /(19[1-9]\d)|(20\d\d)|2100/.test(ano),
        rexpr = new RegExp(mes),
        fev29 = ano % 4 ? 28 : 29;
    
    if (v_mes ) {
        if (mes === '02') return (dia >= 1 && dia <= fev29);
        else if (rexpr.test(M30)) return /((0[1-9])|([1-2]\d)|30)/.test(dia);
        else return /((0[1-9])|([1-2]\d)|3[0-1])/.test(dia);
    }
    return false;
}
function validaCPFCNPJ(cpfcnpj, rotulo) {
    var cnpj = document.getElementById(cpfcnpj).value;
    var label = document.getElementById(rotulo);
    var t = false;
    var url="";
    if (cnpj.replace(/\D/g, '').length === 11) {
        url = "/cheque/ValidaCPF?CPF=" + cnpj;
            
    }else if (cnpj.replace(/\D/g, '').length === 14) {
        url = "/cheque/ValidaCNPJ?CNPJ=" + cnpj;
    } else {
        label.style.display = "block";
        label.innerText = "CPF ou CNPJ invalido!";
        return false;
    }
    
    $.ajax({
        type: "GET",
        url: url,
        data: t,
        contentType: "application/json;",
        dataType: "json",
        success: function(t) {
                
            if (t === false) {
                label.style.display = "block";
                label.innerText = cnpj.replace(/\D/g, '').length === 11 ? "CPF invalido!" : "CNPJ invalido!";
                return false;       
            } else {
                label.style.display = "none";
                label.innerText = "";
                return true;    
            }
        },
        failure: function(t) {
            window.exibeMensagem("500-The Request was hit by an Error. Error Code: JEX-028\n\n" + t);
            return false;
        }
    });

}
//Geoposição
function getLocation(el1, el2)
{
    latitude = document.getElementById(el1);
    longitude = document.getElementById(el2);

    if (navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(showPosition);
    }

}
function showPosition(position) {
        
    latitude.value = position.coords.latitude.toString();
    longitude.value = position.coords.longitude;
    document.getElementById("divCoordenadas").style.display = "block";
}