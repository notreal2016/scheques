﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Interface;
using Scheque.Repository;
using Scheque.Service;

namespace WebCheques.Controllers
{
    public class BancoController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IBancoService _bancoService;

        public BancoController(IUnitOfWork unitOfWork, IBancoService service)
        {

            _unitOfWork = unitOfWork;
            _bancoService = service;
        }

        public ActionResult Banco(string cod = "-1")
        {
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            List<Banco> bancos = _bancoService.FindAll().ToList();
            ViewBag.Id = cod;
            ViewBag.Mensagem = Session["Mensagem"] != null ? Session["Mensagem"] : "|";
            Session.Remove("Mensagem");
            return View(bancos);
        }
        public ActionResult SalvaBanco(Banco banco, FormCollection dados)
        {
            try
            {
                Scheque.Entidade.Banco b = _bancoService.FindByCod(banco.Id);
                if (b == null)
                {
                    _bancoService.Save(banco);
                }
                else
                {
                    _bancoService.Update(banco);
                }
                    _unitOfWork.Commit();
                    Session["Mensagem"] = "100-Dados do banco salvo com sucesso!";
                    return RedirectToAction("Banco", "Banco", new { cod = banco.Id });
                
            }

            catch (Exception e)

            {
                Session["Mensagem"] = "500-Erro ao salvar os dados do banco.";
                return RedirectToAction("Banco", "Banco", new { cod = banco.Id });
            }
        }
        public PartialViewResult DadosBanco(string id = "0")
        {
            Banco banco = new Banco();
            if (!id.Equals("0"))
            {
                
                banco = _bancoService.FindByCod(id);
            }
            return PartialView(banco);
        }


    }
}