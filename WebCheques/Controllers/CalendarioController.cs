﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scheque.Entidade;
using Scheque.Interface;

namespace WebCheques.Controllers
{
    [Authorize]
    public class CalendarioController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ICalendarioRepository _repository;

        public CalendarioController(IUnitOfWork unitOfWork, ICalendarioRepository cr)
        {
            _unitOfWork = unitOfWork;
            _repository = cr;
        }

        public PartialViewResult DadosFeriado(long id = 0, string data = "")
        {
            Scheque.Entidade.Calendario feriado = new Calendario();
            if (id > 0)
            {
                    feriado = _repository.FindById(id);
            }
            else
            {
                feriado.NomeFeriado = "";
            }
            ViewBag.Data = data;
            return PartialView(feriado);
        }

        public ActionResult Calendario(string mes = "", string ano = "")
        {
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Mes = mes;
            ViewBag.Ano = ano;
            ViewBag.Mensagem = Session["Mensagem"] != null ? Session["Mensagem"] : "|";
            Session.Remove("Mensagem");
            return View();
        }

        public ActionResult ExcluiFeriado(long id, int mes, int ano)
        {   
            try
            {
                _repository.Remove(id);
                _unitOfWork.Commit();
                Session["Mensagem"] = "100|Feriado removido com sucesso!";
                return RedirectToAction("Calendario", "Calendario", new { mes = mes, ano = ano });
            }
            catch (Exception e)
            {
                Session["Mensagem"] = "500|Erro ao remover o feriado -" + e.Message;
                return RedirectToAction("Calendario", "Calendario", new { mes = mes, ano = ano });
            }

        }

        public ActionResult SalvaFeriado(FormCollection dados)
        {
            Calendario feriado = new Calendario();
            feriado.DataFeriado = DateTime.Parse(dados["feriado"]);
            feriado.NomeFeriado = dados["NomeFeriado"];
            if (dados["Id"] != null)
            {
                feriado.Id = long.Parse(dados["Id"]);
            }
            try
            {
                
                _repository.Save(feriado);
                _unitOfWork.Commit();
                Session["Mensagem"] = "100-Feriado salvo com sucesso!";

                
                return Redirect(string.Format("Calendario?mes={0}&ano={1}", feriado.Mes, feriado.Ano));
            }

            catch (Exception e)

            {
                Session["Mensagem"] = "500-Feriado nao foi salvo!";
                return Redirect(string.Format("Calendario?mes={0}&ano={1}", feriado.Mes, feriado.Ano));
            }

        }
        public PartialViewResult DadosCalendario(string mes = "", string ano = "")
        {
            if (mes.Equals(""))
            {
                mes = string.Format("{0:MM}", DateTime.Now);
            }

            if (ano.Equals(""))
            {
                ano = string.Format("{0:yyyy}", DateTime.Now);
            }

         
            List<Calendario> lista = _repository.CalendarioMes(mes, ano);
            ViewBag.Mes = mes;
            ViewBag.Ano = ano;
            return PartialView(lista);
        }
    }
}