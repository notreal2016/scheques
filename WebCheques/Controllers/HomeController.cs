﻿using Scheque.Entidade;
using System;

using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Scheque.Context;
using Scheque.Interface;
using Scheque.Repository;
using Scheque.Service;
using WebCheques.Models;

namespace WebCheques.Controllers
{
    
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;

        public HomeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login(string ReturnUrl)
        {
            string mensagem = Session["Mensagem"] != null ? Session["Mensagem"].ToString() : "|";
            ViewBag.Mensagem = mensagem;
            ViewBag.IdOp = 1;
            Session.Remove("Mensagem");
            var usuario = new LoginModelView()
            {
                UrlRetorno = ReturnUrl
            };
            return View(usuario);
        }
        [HttpPost]
        public ActionResult Logar(FormCollection login)
        {
            
            Usuario usuario = new Usuario();
            IUsuarioService us = _unitOfWork.UsuarioService;
            try
            {
                usuario = us.LogaUsuario(login["Login"], login["Senha"]);
                if (usuario == null)
                {
                    Session["Mensagem"] = "402-Login ou senha invalidos.";
                    return RedirectToAction("login", "Home");
                }

                if (!usuario.Ativo)
                {
                    Session["Mensagem"] = "403-Usuario com acesso inativo.";
                    return RedirectToAction("login", "Home");
                }

                Session["User"] = usuario;
                var identity = new ClaimsIdentity(new []
                {
                    new Claim(ClaimTypes.Name, usuario.Nome),
                    new Claim("Login", usuario.Login), 

                }, "ApplicationCookie");
                Request.GetOwinContext().Authentication.SignIn(identity);
                if (usuario.Primeiro)
                {
                    Session["Mensagem"] = "502-Primeiro acesso do usuario, sua senha padrao 'senha1' deve ser trocada.";
                    return RedirectToAction("perfil", "Usuario" , new {id = usuario.Id});
                }

                if (!String.IsNullOrWhiteSpace(login["UrlRetorno"]) || Url.IsLocalUrl(login["UrlRetorno"]))
                {
                    return Redirect(login["UrlRetorno"]);
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
                
            }
            catch (Exception e)
            {
                Session["Mensagem"] = "550-Falha de conexao com o servidor.";
                return RedirectToAction("login", "Home");
            }
        }

        

        
    }
}