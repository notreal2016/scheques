﻿using Scheque.Entidade;
using System;
using System.Web.Mvc;
using ADVCore.Service;
using Scheque.Service;
using Scheque.Interface;
using Scheque.Uteis;

namespace WebCheques.Controllers
{
    [Authorize]
    public class EmpresaController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEmpresaService _service;

        public EmpresaController(IUnitOfWork unitOfWork, IEmpresaService es)
        {
            _unitOfWork = unitOfWork;
            _service = es;
        }

        public ActionResult Index(long id = -1)
        {
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Empresa empresa = new Empresa();
            if (id > 0)
            {
                empresa = _service.FindById(id);
            }
            ViewBag.IdEmpresa = id;
            ViewBag.Mensagem = Session["Mensagem"] != null ? Session["Mensagem"] : "|";
            Session.Remove("Mensagem");
            ViewData["ListaUF"] = new SelectList(Util.Ufs());
            return View(empresa);
        }

        public ActionResult SalvaEmpresa(Empresa empresa, FormCollection dados)
        {
            
            try
            {
                if (!ValidaDados.ValidaCNPJ(empresa.Cnpj))
                {
                    Session["Mensagem"] = "500-CNPJ invalido!!";
                    return RedirectToAction("Index", "Empresa", new { id = empresa.Id });
                }
                _service.Save(empresa);
                _unitOfWork.Commit();
                Session["Mensagem"] = "100-Dados da empresa salvos com sucesso!";
            }
            catch (Exception e)
            {
                Session["Mensagem"] = string.Format("500-Erro ao salvar os dados - {0} - {1}", e.HResult, e.Message);
            }
            return RedirectToAction("index", "empresa", new { id = empresa.Id });
        }
    }
}