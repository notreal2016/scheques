﻿using Scheque.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ADVCore.Service;
using Newtonsoft.Json.Linq;
using Scheque.Service;
using Scheque.Interface;
using Scheque.Uteis;

namespace WebCheques.Controllers
{
    [Authorize]
    public class ChequeController : Controller
    {
        //private static Lote _lote ;
        private static int _indexCheque = -1;
        private string _mensagem ;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoteService _loteService;
        
        public ChequeController(IUnitOfWork unitOfWork, ILoteService ls)
        {
            _unitOfWork = unitOfWork;
            _loteService = ls;
        }

        /// <summary>
        /// View da capa de lote 
        /// </summary>
        /// <param name="id">Id do lote como padrão 0</param>
        /// <returns></returns>
        public PartialViewResult CapaLote(long id = 0)
        {
            Lote lote = Session["Lote"] as Lote;
            if (id > 0 && lote.Id != id)
            {
                lote = _loteService.FindLoteById(id);
            }else if (id == -1)
            {
                lote = new Lote();
            }

            ViewBag.Clientes = new SelectList(_loteService.AllClientes(), "Id", "Nome", lote.IdCliente);
            if (_mensagem != null && !_mensagem.Equals(""))
            {
                ViewBag.Mensagem(_mensagem);
                _mensagem = null;
            }
            Session["Lote"] = lote;
            return PartialView(lote);
        }


        /// <summary>
        /// Cria um cheque pelos dados repassados
        /// </summary>
        /// <param name="dadosCheque">JSonObjeto do cheque</param>
        /// <param name="cheque">Cheque que deve ter os dados atualiazdos</param>
        /// <returns>Cheque apos sua montagem</returns>
        private Cheque CriaCheque(FormCollection dadosCheque)
        {
            Lote lote = Session["Lote"] as Lote;
            long idCheque = dadosCheque["IdCheque"] != null? long.Parse(dadosCheque["IdCheque"]):0;
            Cliente cliente = lote.Cliente; //_loteService.AllClientes().Single(c => c.Id == _lote.IdCliente);
            Cheque cheque = new Cheque();
            if (idCheque > 0)
            {
                cheque = _loteService.FindChequeById(idCheque);
            }
            cheque.ContaCheque =  GravaConta(dadosCheque, cheque.ContaCheque != null? cheque.ContaCheque : new Conta());
            cheque.IdConta = cheque.ContaCheque.Id;
            cheque.Vencimento = DateTime.Parse(dadosCheque["Vencimento"].ToString());
            cheque.Dias = _loteService.ContaDias(lote.DataLote, cheque.Vencimento, true, true, cliente.DiasMinimo);
            cheque.NovoVencimento = lote.DataLote.AddDays(cheque.Dias);
            cheque.Status = StatusDocumento.DIGITADO;
            cheque.Valor = decimal.Parse(dadosCheque["Valor"].ToString());
            cheque.JurosCheque = _loteService.CalculaJuros(cheque.Valor, lote.Taxa, cheque.Dias, cliente.Acumulado);
            cheque.IdLote = lote.Id;
            cheque.NumeroCheque = dadosCheque["Numero"].ToString();
            return cheque;
        }

        /// <summary>
        /// Ajusta dados de um cheque na lista de cheques no lote atual
        /// </summary>
        /// <param name="cheque">Cheque que deve ser tratado</param>
        /// <param name="index">Posição dele na lista</param>
        private void AjustaCheque(Cheque cheque, int index)
        {
            Lote lote = Session["Lote"] as Lote;
            if (index < lote.AllCheques.Count )
            {
                Cheque c = lote.AllCheques[index];
                lote.AddCheque(cheque, index);
                index++;
                AjustaCheque(c, index);
            }
            else
            {
                lote.AddCheque(cheque);
            }
            Session["Lote"] = lote;
        }

        /// <summary>
        /// Adiciona um cheque no lote
        /// </summary>
        /// <param name="dadosCheque"></param>
        /// <returns></returns>
        public ActionResult AddCheque(FormCollection dadosCheque)
        {
           
            AjustaCheque(
                CriaCheque(dadosCheque), 
                _indexCheque < 0? 0 : _indexCheque );
            return RedirectToAction("DadosCheque", "Cheque");
        }

        
        public RedirectResult SubimitLote(FormCollection dados = null)
        {
            Lote lote = Session["Lote"] as Lote;
            var quantidade = int.Parse(dados["Quantidade"]);
            var valorLote = decimal.Parse(dados["ValorLote"]);
            long idlote = dados["id"] != null ? long.Parse(dados["id"]) : 0;
            DateTime vencimento = new DateTime();

            /*if ((_lote == null || _lote.Id == 0 || _lote.Cheques.Count == 0) && idlote == 0 )
            {
                _lote = new Lote();
            }
            else if (_lote.Cheques.Count == 0 && idlote > 0)
            {
                _lote = _loteService.FindLoteById(idlote);
            }*/
            

            if (quantidade <= 0)
            {
                _mensagem = "500|Quantidade deve ser maior que zero.";
                return Redirect("../../Cheque/AssistenteLote/-1");
            }
            if (valorLote <= 0)
            {
                _mensagem = "500|Valor do lote deve ser maior que zero.";
                return Redirect("../../Cheque/AssistenteLote/-1");
            }
            try
            {
                vencimento = DateTime.Parse(dados["DataLote"]);
            }
            catch (Exception)
            {
                _mensagem = "500|Data do lote inválido.";
                return Redirect("../../Cheque/AssistenteLote/-1");
            }
            long loteId = long.Parse(dados["Id"]);
            lote.IdCliente = long.Parse(dados["IdCliente"]);
            lote.Cliente = _loteService.FindClienteById(lote.IdCliente);
            lote.Quantidade = quantidade;
            lote.DataLote = vencimento;
            lote.Taxa = decimal.Parse(string.Format("{0:F2}", dados["Taxa"]));
            lote.Administrativa = decimal.Parse(string.Format("{0:F2}", dados["Administrativa"]));
            lote.ValorLote = valorLote;
            lote.Id = loteId <0 ? 0 : loteId;
            Session["Lote"] = lote;

            return Redirect("../../cheque/dadoscheque/0");
        }

        public JsonResult BuscaEmitente(string cnpj)
        {
            Lote lote = Session["Lote"] as Lote;
            if (lote.Cheques.Count > 0)
            {
                IList<Cheque> cheque = lote.Cheques.Where(c => c.ContaCheque.Proprietario !=null && c.ContaCheque.Proprietario.CpfCNPJ == cnpj).ToList();
                if (cheque.Count > 0)
                {
                    return Json(cheque[0].ContaCheque.Proprietario.Nome, JsonRequestBehavior.AllowGet);
                }
            }
            Proprietario emitente = _loteService.FindProprietarioByDocumento(cnpj);
            if (emitente != null)
            {
                return Json(emitente.Nome, JsonRequestBehavior.AllowGet);
            }
            return Json("", JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult DadosCheque(int index =-1)
        {
            Lote lote = Session["Lote"] as Lote;
            List<SelectListItem> _bancos = new List<SelectListItem>();
            Cheque cheque = new Cheque();
            Cliente cliente = lote.Cliente;//_loteService.AllClientes().Single(c => c.Id == lote.IdCliente);
            if (lote != null & lote.AllCheques.Count > 0 && lote.AllCheques.Count >= index && index >= 0)
            {
                cheque = lote.Cheques[index];
                lote.RemoveCheque(cheque);
            }
            _indexCheque = index;
            
            _loteService.AllBancos().ForEach(c =>
            {
                _bancos.Add(new SelectListItem()
                {
                    Value = c.Id,
                    Text = c.Id + "-" + c.Nome
                });
            });
            if (lote != null)
            {
                JObject dadosLote = new JObject();
                dadosLote.Add("Total", lote.ValorLote.ToString("C2"));
                int percentual = lote.Quantidade > 0 ? (lote.AllCheques.Count * 100) / lote.Quantidade : 0;
                dadosLote.Add("Percentual", percentual.ToString());
                dadosLote.Add("Data", lote.DataLote.ToShortDateString());
                dadosLote.Add("IdLote", lote.Id);
                dadosLote.Add("Taxa", lote.Taxa);
                dadosLote.Add("Administrativa", lote.Administrativa);
                dadosLote.Add("Dias", cliente != null ? cliente.DiasMinimo : 0);
                dadosLote.Add("Acumulado", cliente != null ? cliente.Acumulado : true);
                dadosLote.Add("Concluido", lote.Cheques.Count == lote.Quantidade);
                if (lote.Cheques.Count == lote.Quantidade)
                {
                    var dif = lote.TotalCheques - lote.ValorLote;
                    if (dif > 0)
                    {
                        dadosLote.Add("Cod", 500);
                        dadosLote.Add("Mesagem", String.Format("500-Lote com diferença. Total de cheques é maior que o lote em {0:C2}.", dif));

                    }
                    else if (dif < 0)
                    {
                        dadosLote.Add("Cod", 500);
                        dadosLote.Add("Mesagem", String.Format("500-Lote com diferença. Total de cheques é menor que o lote em {0:C2}.", dif * -1));

                    }
                    else
                    {
                        dadosLote.Add("Cod", 100);
                    }
                }
                ViewBag.DadosLote = dadosLote;
            }
            ViewBag.Bancos = _bancos;
            ViewBag.Cheque = cheque;
            ViewData["Cheques"] = lote != null ? lote.Cheques : new List<Cheque>(); ;
            return PartialView();
        }
        
        public PartialViewResult LoteFinal()
        {
            Lote lote = Session["Lote"] as Lote;
            try
            {
                lote.ValorDescontado = lote.ValorLote - (lote.TotalJuros + lote.Administrativa);
                _loteService.Salvar(lote);
                //lote = _loteService.FindLoteById(lote.Id);
                Session["Lote"] = lote;
                return PartialView(lote);
            }
            catch (Exception e)
            {
                ViewBag.Mensagem = "510|Erro ao gravar o lote.";
                lote = _loteService.FindLoteById(lote.Id);
                Session["Lote"] = lote;
                return PartialView(lote);
            }
        }
        
        /// <summary>
        /// Processo que calcula juros de um cheque
        /// </summary>
        /// <param name="dtLote">Data do lote</param>
        /// <param name="dtVencimento">Data do Vencimento</param>
        /// <param name="valor">Valor do Cheque</param>
        /// <param name="taxa">Taxa de Juros ao mês</param>
        /// <param name="composto">Usa juros composto por padrão é true</param>
        /// <returns>JSon com Dias , Juros e Valor Descontado do cheque</returns>
        
        public JsonResult CalculaJuros(string dtLote, string dtVencimento, string taxa, string valor, int minimo =0 , bool composto = true)
        {
            try
            {
                DateTime dtLot = DateTime.Parse(dtLote), dtVenc = DateTime.Parse(dtVencimento);
                decimal vCheque = decimal.Parse(valor), vTaxa = decimal.Parse(taxa);
                var dias = _loteService.ContaDias(dtLot, dtVenc, true, true, minimo);
                var juros = _loteService.CalculaJuros(vCheque, vTaxa, dias, composto);
                DateTime nVencimento = dtLot.AddDays(dias);
                List<Object> calculo = new List<object>();
                calculo.Add(new
                {
                    Dias = dias.ToString(),
                    VPago = (vCheque - juros).ToString("C2"),
                    VJuros = juros.ToString("C2")
                });
                return Json(calculo, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult FindDadosCliente(long id)
        {
            Cliente c = _loteService.FindClienteById(id);
            List<Object> dados = new List<object>();
            dados.Add(new
            {
                Taxa = c.Taxa.ToString("N2"),
                Acumulado = c.Acumulado,
                Adm = c.Administrativa.ToString("N2")
            });
            return Json(dados, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Metodo avalia CMC e destrincha os dados de um código CMC
        /// </summary>
        /// <param name="CMC">Código CMC a ser analisado</param>
        /// <returns>JSon com os dados.</returns>
        public JsonResult LerCMC(String CMC)
        {
            CMC = Util.ExtraiNumeros(CMC);
            if (!Scheque.ValidaCmc7.ValidaCmc(CMC))
            {
                List<object> result1 = new List<object>();
                result1.Add(new {
                    COD = 500,
                    MENSAGEM = "Código CMC está inválido"
                });
                return Json(result1, JsonRequestBehavior.AllowGet);
            }
            string banco = Scheque.ValidaCmc7.ExtraiCodBanco(CMC);
            string agencia = Scheque.ValidaCmc7.ExtraiCodAgencia(CMC);
            string conta = Scheque.ValidaCmc7.ExtraiNumContaCliente(CMC);
            string cheque = Scheque.ValidaCmc7.ExtraiNumDocumento(CMC);
            string operacao = Scheque.ValidaCmc7.ExtraiCodOperadorConta(CMC);            
            return LerDadosCheque(banco, agencia, conta, cheque, operacao);
        }

        private Conta GravaConta(FormCollection dados, Conta conta)
        {   
            //Para o caso de um novo cheque
            if (conta.Id == 0)
            {
                //Tratamento para o caso de contas com menos de 6 dígitos
                Banco bc = _loteService.FindBancoByCodBanco(dados["DropBanco"].ToString());
                conta = _loteService.FindContaByDados(dados["DropBanco"].ToString(), dados["Agencia"].ToString(), dados["Conta"].ToString(), dados["Operacao"].ToString(), bc.NumeroDigitosConta);
            }
            //Caso não encontrado uma conta previamente cadastrada
            if (conta == null) 
            {
                conta = new Conta();
                conta.IdBanco = dados["DropBanco"].ToString();
                conta.Agencia = dados["Agencia"].ToString();
                conta.ContaCorrente = dados["Conta"].ToString();
                conta.Operacao = dados["Operacao"].ToString();
                conta.DaEmpresa = false;
            }
            //Preparando o caso do emitente da conta
            var cpfcnpj = dados["CPFCNPJ"].ToString();
            if (conta.Proprietario != null && conta.Proprietario.CpfCNPJ.Equals(cpfcnpj))
            {
                conta.Proprietario.Nome = dados["Emitente"].ToString();
            }
            else
            {
                Proprietario p ;
                {
                    p = _loteService.FindByDocs(dados["CPFCNPJ"].ToString());
                    if (p != null)
                    {
                        conta.Proprietario = p;
                    }
                    else
                    {
                        conta.Proprietario = new Proprietario { CpfCNPJ = dados["CPFCNPJ"].ToString(), Nome = dados["Emitente"].ToString() };
                    }

                    conta.IdProprietario = conta.Proprietario.Id;
                }
            }
            _loteService.SalvaConta(conta);
            _unitOfWork.Commit();
            return conta;
        }

        public JsonResult LerDadosCheque(string banco, string agencia, string conta, string cheque, string operacao)
        {
            Lote lote = Session["Lote"] as Lote;
                int cod = 100;
                decimal total = 0;
                int qnEmitente = 0;
                int qnConta = 0;
                long idCC = 0;
                string doc = "";
                string nomeEmitente = "";

                if (_loteService.IsChequeCadastrado(banco, agencia, conta, operacao, cheque, lote.Id))
                {
                    List<object> result1 = new List<object>();
                    result1.Add(new
                    {
                        COD = 501,
                        MENSAGEM = "Cheque já cadastrado em outro lote."
                    });
                    return Json(result1, JsonRequestBehavior.AllowGet);
                }
                if (lote.Cheques.Where(c => c.Banco.Equals(banco) &&
               c.ContaCheque.ContaCorrente.Equals(conta)  &&
               c.NumeroCheque.Equals(cheque)).Count() > 0)
                {
                    List<Object> result1 = new List<object>();
                    result1.Add(new
                    {
                        COD = 502,
                        MENSAGEM = "Cheque já cadastrado neste lote."
                    });
                    return Json(result1, JsonRequestBehavior.AllowGet);
                }
                var bc = _loteService.FindBancoByCodBanco(banco);
                Conta cc = _loteService.FindContaByDados(banco, agencia, conta, "", bc.NumeroDigitosConta);
                if (cc != null)
                {
                    qnConta = _loteService.NegativadoByIdConta(cc.Id).Count;
                    cod = 101;
                    idCC = cc.Id;
                    doc = cc.Proprietario != null ? cc.Proprietario.CpfCNPJ : "";
                    nomeEmitente = cc.Proprietario != null ? cc.Proprietario.Nome : "";
                    var emitente = cc.Proprietario != null ? _loteService.FindProprietarioByDocumento(doc) : null;
                    if (emitente != null)
                    {
                        _loteService.FindAllChequesByIdEmitente(emitente.Id).ForEach(ch => {
                            total += ch.Valor;
                        });
                    }
                    if (emitente != null) //Inadiplencia por emitente
                    {
                        qnEmitente = _loteService.NegativadosByEmitente(emitente.Id).Count;
                    }
                    else
                    {
                        qnEmitente = _loteService.NegativadosByDoc(doc).Count;
                    }

                }
                else
                {
                    qnConta = _loteService.NegativadosByDadosConta(banco, agencia, conta).Count;

                }
                //Informações de cheques já repassados ainda não compensados

                if (lote != null && lote.Cheques.Count > 0)
                {
                    lote.Cheques.Where(c => c.ContaCheque.IdBanco.Equals(banco) &&
                    c.ContaCheque.Agencia.Equals(agencia) && c.ContaCheque.ContaCorrente.Equals(conta)).ToList().ForEach(ch =>
                    {
                        total += ch.Valor;
                    });
                }

                List<Object> resultado = new List<object>();
                    resultado.Add(new
                {
                    COD = cod,
                    Banco = banco,
                    Agencia = agencia,
                    Conta = conta,
                    Operacao = operacao,
                    Cheque = cheque,
                    Total = total,
                    NegativoConta = qnConta,
                    NegativoEmitente = qnEmitente,
                    IdConta = idCC,
                    NomeEmitente = nomeEmitente.ToUpper(),
                    Doc = doc

                });
                return Json(resultado, JsonRequestBehavior.AllowGet);
            
        }

        public PartialViewResult AssitenteLote()
        {
           return PartialView();
        }

        public PartialViewResult ListaLotes(string termo, TipoBusca tipo)
        {
               List<JObject> lista = new List<JObject>();

                try
                {
                    switch (tipo)
                    {
                        case TipoBusca.NOME:
                            lista.AddRange(PreencheListaLote(_loteService.FindLoteByNomeCliente(termo)));
                            break;
                        case TipoBusca.DATA:
                            lista.AddRange(PreencheListaLote(_loteService.FindLoteByData(termo.Split('/'))));
                            break;
                        case TipoBusca.LOTE:
                            lista.AddRange(PreencheListaLote(new List<Lote>() { _loteService.FindLoteById(long.Parse(termo)) }));
                            break;
                    }
                    ViewData["Dados"] = lista;
                    return PartialView();
                }

                catch (Exception e)

                {
                    ViewData["Dados"] = lista;
                    return PartialView();
                }
            
        }
        /// <summary>
        /// Cria uma lista de objetos Jason com os dados de um lote como Id, Nome do cliente, Data e Valor do lote
        /// </summary>
        /// <param name="list">Lista com os dados do lote</param>
        /// <returns>Lista de JObjetos</returns>
        private List<JObject> PreencheListaLote(List<Lote> list)
        {
            List<JObject> result = new List<JObject>();
            list.ForEach(l => 
            {
                JObject o = new JObject();
                o.Add("Id", l.Id);
                o.Add("Nome", l.Cliente.Nome);
                o.Add("Data", l.DataLote.ToShortDateString());
                o.Add("Valor", l.ValorLote);
                result.Add(o);
            });
            return result;
        }

        public PartialViewResult EditaLote(long id = 0)
        {
            Lote lote = Session["Lote"] as Lote;
            {
                if (id > 0)
                {
                    lote = _loteService.FindLoteById(id);
                }
                Session["Lote"] = lote;
                return PartialView(lote);
            }
            
        }

        public ActionResult Lote(long id = 0)
        {
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Lote lote = Session["Lote"] != null ? Session["Lote"] as Lote : new Lote();
            if (id > 0)
            {
              lote = _loteService.FindLoteById(id);
            }
            
            return View(lote);
        }
        public ActionResult ValidaCNPJ(string CNPJ)
        {
            return Json(ValidaDados.ValidaCNPJ(CNPJ), JsonRequestBehavior.AllowGet);
        }
        
        public ActionResult ValidaCPF(string CPF)
        {
            return Json(ValidaDados.ValidaCpf(CPF), JsonRequestBehavior.AllowGet);
        }

        public ActionResult NovoLote()
        {
            Session["Lote"] = new Lote();
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EncerraLote()
        {
            if (Session["Lote"] == null)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }
            Session.Remove("Lote");
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            Request.GetOwinContext().Authentication.SignOut("ApplicationCookie");
            return RedirectToAction("Index", "Home");
        }

        public ActionResult PrintLote(long id = 0)
        {
            var ls = _unitOfWork.LoteService;
            Lote lote = new Lote();
            if (id > 0)
            {
                lote = ls.FindLoteById(id);
            }
            return View(lote);
        }

        public ActionResult FindClienteByNome(string termo)
        {
            var ls = _unitOfWork.LoteService;
            return Json(ls.FindClienteByNome(termo), JsonRequestBehavior.AllowGet);
        }
    }
}