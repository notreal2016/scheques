﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Scheque.Entidade;
using Scheque.Interface;
using Scheque.Service;
using Scheque.Uteis;

namespace WebCheques.Controllers
{
    [Authorize]
    public class ClienteController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IClienteService _service;

        public ClienteController(IUnitOfWork unitOfWork, IClienteService cs)
        {
            _unitOfWork = unitOfWork;
            _service = cs;
        }
        public ActionResult Cliente(long id = -1)
        {
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            ViewBag.Id = id;
            if (Session["Mensagem"] != null)
            {
                ViewBag.Mensagem = Session["Mensagem"];
                Session.Remove("Mensagem");
            }

            return View();
        }
        public PartialViewResult DadosCliente(long id = 0)
        {
            
            Cliente cliente = new Cliente();
            if (id > 0)
            {
                cliente = _service.FindById(id);
            }
            ViewData["ListaUF"] = new SelectList(Util.Ufs());
            return PartialView(cliente);
        }
        public ActionResult SalvaCliente(Cliente cliente, FormCollection dados)
        {
            
            try
            {
                cliente.Acumulado = dados["Acumulado1"].Equals("on");
                _service.Save(cliente);
                _unitOfWork.Commit();
                Session["Mensagem"] = "100-Dados do cliente salvos com sucessso.";
            }
            catch (Exception e)
            {
                Session["Mensagem"] = "500- " + e.HResult + " : " + e.Message;
                return RedirectToAction("Cliente", "Cliente", new { id = cliente.Id == 0 ? -1 : cliente.Id });
            }
            return RedirectToAction("Cliente", "Cliente", new { id = cliente.Id });
        }

        public PartialViewResult ListaCliente(string termo = "")
        {
            List<Cliente> lista = new List<Cliente>();
            if (termo != "")
            {
                var sc = _unitOfWork.ClienteService;
                if (termo.ToLower().Equals("todos"))
                {
                    lista.AddRange(sc.AllClientes());
                }
                else
                {
                    lista.AddRange(sc.FindByName(termo));
                }
                return PartialView(lista);
            }
            else
            {
                ViewBag.Mensagem = "401-Não foi possivel efetuar uma busca.";
                return PartialView(lista);
            }
        }

    }
}