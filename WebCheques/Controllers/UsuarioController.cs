﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ADVCore.Service;
using Scheque.Entidade;
using Scheque.Interface;
using Scheque.Service;

namespace WebCheques.Controllers
{
    [Authorize]
    public class UsuarioController : Controller
    {
        private readonly IUnitOfWork _uiOfWork;
        private readonly IUsuarioService _service;

        public UsuarioController(IUnitOfWork uiOfWork, IUsuarioService us)
        {
            _uiOfWork = uiOfWork;
            _service = us;
        }

        public ActionResult Perfil(long id)
        {
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Usuario user = id > 0 ? _service.FindById(id) : new Usuario();
            ViewBag.Mensagem = Session["Mensagem"] != null ? Session["Mensagem"] : "|";
            Session.Remove("Mensagem");
            return View(user);
        }
        public ActionResult AteraraSenha(FormCollection dados)
        {
            

            Usuario user = _service.FindById(long.Parse(dados["txtId"]));
            String antiga = dados["OldPassword"], nova = dados["NewPassword"], confirma = dados["NewPasswordConfirm"];
            if (!user.Senha.Equals(antiga))
            {
                Session["Mensagem"] = "501-Senha antiga nao confere!";
                return RedirectToAction("Perfil", "Usuario", new { id = user.Id });
            }
            if (!nova.Equals(confirma))
            {
                Session["Mensagem"] = "501-Senhas nao conferem!";
                return RedirectToAction("Perfil", "Usuario", new { id = user.Id });
            }
            if (nova.Contains("*") || nova.Contains("%") ||
                nova.Contains("/") || nova.Contains("\\"))
            {
                Session["Mensagem"] = "501-Senhas contem caracteres invalidos!";
                return RedirectToAction("Perfil", "Usuario", new { id = user.Id });
            }
            if (!(ValidaDados.ExistCaracterEspeciais(nova) &&
                ValidaDados.ContemNumeros(nova)))
            {
                Session["Mensagem"] = "501-Senhas devem conter numeros e caracteres especias!";
                return RedirectToAction("Perfil", "Usuario", new { id = user.Id });
            }

            user.Senha = dados["NewPassword"];
            user.Primeiro = false;
            try
            {
                _service.Save(user);
                _uiOfWork.Commit();
                Session["Mensagem"] = "100-Senha alterada com sucesso!";
                return RedirectToAction("Perfil", "Usuario", new { id = user.Id });
            }
            catch (Exception e)
            {
                Session["Mensagem"] = string.Format("501- Erro contate o suporte :{0}-{1} ", e.HResult, e.Message);
                return RedirectToAction("Perfil", "Usuario", new { id = user.Id });
            }
        }
        public ActionResult ValidaLogin(string login, long id)
        {
            return Json( _service.ValidaLogin(login, id), JsonRequestBehavior.AllowGet);
        }

        public ActionResult SalvaPerfil(Usuario user, FormCollection dados)
        {
            try
            {
                
                Usuario user_session = Session["User"] as Usuario;
                if (user_session == null)
                {
                    return RedirectToAction("Login", "Home");
                }
                user.IdEmpresa = user_session.IdEmpresa;
                user.Senha = dados["TxtSenha"];
                user.Ativo = bool.Parse(dados["ativo"]);
                _service.Save(user);
                _uiOfWork.Commit();
                Usuario user1 = Session["User"] as Usuario;
                if (user1.Id == user.Id)
                {
                    Session["User"] = user;
                }
                Session["Mensagem"] = "100-Dados salvos com sucesso!";
                return RedirectToAction("Perfil", "Usuario", new { id = user.Id });
            }
            catch (Exception e)
            {
                Session["Mensagem"] = "500-Erro ao salvar dados " + e.Message;
                return RedirectToAction("Perfil", "Usuario", new { id = user.Id });
            }
        }

        public ActionResult Usuario(long id = -1)
        {
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            Usuario user = new Usuario();
            ViewBag.Id = id;
            if (Session["Mensagem"] != null)
            {
                ViewBag.Mensagem = Session["Mensagem"];
                Session.Remove("Mensagem");
            }
            return View();
        }

        public PartialViewResult DadosUsuario(long id = 0)
        {
            if (id > 0)
            {
                return PartialView(_service.FindById(id));
            }
            return PartialView(new Usuario());
        }

        public PartialViewResult ListaUsuario(string termo)
        {
            List<Usuario> lista = new List<Usuario>();
            if (termo != "")
            {
                
                if (termo.ToLower().Equals("todos"))
                {
                    lista.AddRange(_service.FindByAll());
                }
                else
                {
                    lista.AddRange(_service.FindByNome(termo));
                }
            }
            return PartialView(lista);
        }

        public ActionResult SalvaUsuario(Usuario user, FormCollection dados)
        {
            try
            {
                Usuario userlog = Session["user"] as Usuario;
                var idEmpresa = dados["IdEmpresa"] != null  && !dados["IdEmpresa"].Equals("")? long.Parse(dados["IdEmpresa"]) : 0;

                user.Ativo = dados["Ativo"].Equals("on");
                user.IdEmpresa = idEmpresa >0? idEmpresa: userlog.IdEmpresa;
                if (user.Id == 0)
                {
                    user.Senha = "senha1";
                }
                else
                {
                    user.Senha = _service.FindById(user.Id).Senha;
                }
                _service.Save(user);
                _uiOfWork.Commit();
                Session["Mensagem"] = "100-Dados salvos com sucesso! A senha criada por padrao e 'senha1', devera ser trocada no primeiro acesso.";
                return RedirectToAction("Usuario", "Usuario", new { id = user.Id });
            }
            catch (Exception e)
            {
                Session["Mensagem"] = "500-Dados nao foram salvos.";
                return RedirectToAction("Usuario", "Usuario", new { id = user.Id != 0 ? user.Id : -1 });
            }
        }
    }
}