﻿using Scheque.Entidade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ADVCore.Service;
using Newtonsoft.Json.Linq;
using Scheque.Context;
using Scheque.Service;
using Scheque.Interface;
using Scheque.Uteis;
using System.Web.Mvc;
using Scheque;

namespace WebCheques.Controllers
{
    [Authorize]
    public class ConsultaController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private IConsultaChequeService _ccs;
        public ConsultaController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            
        }

        public PartialViewResult ConsultaCheque(string cmc7 = "",string idBanco = "", string agencia = "", string conta = "", string operacao = "", string numero = "")
        {
            string mensagem = "|";
            if (!cmc7.Trim().Equals(""))
            {
                cmc7 = Util.ExtraiNumeros(cmc7);
            }
            if ((idBanco.Equals("") && cmc7.Equals(""))|| !ValidaCmc7.ValidaCmc(cmc7))
            {
                mensagem = "500|Dados do cheque invalido.";
                
            }
            else
            {
                
                idBanco = ValidaCmc7.ExtraiCodBanco(cmc7);
                agencia = ValidaCmc7.ExtraiCodAgencia(cmc7);
                conta = ValidaCmc7.ExtraiNumContaCliente(cmc7);
                operacao = ValidaCmc7.ExtraiCodOperadorConta(cmc7);
                numero = ValidaCmc7.ExtraiNumDocumento(cmc7);
                try
                {
                    _ccs = new ConsultaChequeService(_unitOfWork, idBanco, conta, agencia, numero, operacao);
                    ViewData["CCS"] = _ccs;
                }
                catch (Exception e)
                {
                    mensagem = e.Message;
                    ViewBag.Mensagem = mensagem;
                    return PartialView();
                }
            }
            ViewBag.Mensagem = mensagem;
            return PartialView();
        }

        public ActionResult Index()
        {
            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "Home");
            }
            return View();
        }

        public ActionResult ChequesLista(long idConta, StatusDocumento status = StatusDocumento.TODOS)
        {
            IChequeRepository cr = _unitOfWork.ChequeRepository;
            IList<Cheque> cheques = new List<Cheque>();
            switch (status)
            {
                case StatusDocumento.DIGITADO:
                    cheques = cr.FindAllChequesByIdConta(idConta).Where(c => c.Status == status).ToList();
                    break;
                case StatusDocumento.SEPARADO:
                    break;
                case StatusDocumento.DEVOLVIDO:
                    break;
                case StatusDocumento.USADO:
                    break;
                case StatusDocumento.ENTREGUE:
                    break;
                case StatusDocumento.DEPOSITADO:
                    break;
                case StatusDocumento.QUITADO:
                    break;
                case StatusDocumento.QUITADOPARCIAL:
                    break;
                case StatusDocumento.TODOS:
                    cheques = cr.FindAllChequesByIdConta(idConta);
                    break;
                default:
                    break;
            }
            return View(cheques);
        }

    }
}