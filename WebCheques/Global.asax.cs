﻿using System.Data.Entity;
using System.Security.Claims;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Http;
using System.Web.Optimization;
using Scheque.Context;
#pragma warning disable CS0105 // The using directive for 'System.Web.Routing' appeared previously in this namespace
using System.Web.Routing;
#pragma warning restore CS0105 // The using directive for 'System.Web.Routing' appeared previously in this namespace
using Scheque.Interface;
using Scheque.Repository;
using Scheque.Service;
using SimpleInjector;
using SimpleInjector.Diagnostics;
using SimpleInjector.Integration.Web;
using SimpleInjector.Integration.Web.Mvc;
using SimpleInjector.Integration.WebApi;
using WebCheques.App_Start;


namespace WebCheques
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            //WebApiConfig.Register(GlobalConfiguration.Configuration);
            
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.UniqueClaimTypeIdentifier = ClaimTypes.Name;
            var container = new Container();
            
            container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

            container.Register<IScContext, ScContext>(Lifestyle.Scoped);
            container.Register<IBancoService, BancoService>(Lifestyle.Scoped);
            container.Register<IBancoRepository, BancoRepository>(Lifestyle.Scoped);
            container.Register<IUsuarioService, UsuarioService>(Lifestyle.Scoped);
            container.Register<IUsuarioRepository, UsuarioRepository>(Lifestyle.Scoped);
            container.Register<IClienteService, ClienteService>(Lifestyle.Scoped);
            container.Register<IClienteRepository, ClienteRepository>(Lifestyle.Scoped);
            container.Register<ICalendarioRepository, CalendarioRepository>(Lifestyle.Scoped);
            container.Register<IEmpresaRepository, EmpresaRepository>(Lifestyle.Scoped);
            container.Register<IEmpresaService, EmpresaService>(Lifestyle.Scoped);
            container.Register<IProprietarioService, ProprietarioService>(Lifestyle.Scoped);
            container.Register<IProprietarioRepoitory, ProprietarioRepoitory>(Lifestyle.Scoped);
            container.Register<ILoteService, LoteService>(Lifestyle.Scoped);
            container.Register<ILoteRepository, LoteRepository>(Lifestyle.Scoped);
            container.Register<IRepositoryMovimentoDoc, RepositoryMovimentoDoc>(Lifestyle.Scoped);
            container.Register<IRepositoryNegativado, RepositoryNegativado>(Lifestyle.Scoped);
            container.Register<IContaRepository, ContaRepository>(Lifestyle.Scoped);
            container.Register<IUnitOfWork, UnitOfWork>(Lifestyle.Scoped);
            container.Register<IChequeRepository, ChequeRepository>(Lifestyle.Scoped);
            
            //container.Register<IUsuarioService, UsuarioService>(Lifestyle.Scoped);
            //container.Register<IUsuarioRepository, UsuarioRepository>(Lifestyle.Scoped);

            container.Verify();
            DependencyResolver.SetResolver(
                new SimpleInjectorDependencyResolver(container));

            
        
        }
    }
}
