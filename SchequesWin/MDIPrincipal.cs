﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MaterialSkin.Controls;
using Scheque.Context;
using Scheque.Entidade;
using SchequesWin.Forms;

namespace SchequesWin
{
    public partial class MDIPrincipal : Form
    {
        private int childFormNumber = 0;
        private SCContext _context;
        private FormLogin _login;

        public FormLogin Login
        {
            get { return _login; }
            set { _login = value; }
        }

        public MDIPrincipal()
        {
            InitializeComponent();
            _context = FactoryContext.GetContext();
        }

        private void ShowNewForm(object sender, EventArgs e)
        {
            Form childForm = new FormModelo();
            childForm.MdiParent = this;
            childForm.Text = "Window " + childFormNumber++;
            childForm.Show();
        }

        private void OpenFile(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = openFileDialog.FileName;
            }
        }

        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                string FileName = saveFileDialog.FileName;
            }
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void CutToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void CopyToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void PasteToolStripMenuItem_Click(object sender, EventArgs e)
        {
        }

        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        private void CascadeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.Cascade);
        }

        private void TileVerticalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileVertical);
        }

        private void TileHorizontalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.TileHorizontal);
        }

        private void ArrangeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LayoutMdi(MdiLayout.ArrangeIcons);
        }

        private void CloseAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            foreach (Form childForm in MdiChildren)
            {
                childForm.Close();
            }
        }

        private void loteChequeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form childForm = new FormLoteCheques(this);
            childForm.Show();
            childForm.WindowState = FormWindowState.Normal;
            childForm.WindowState = FormWindowState.Maximized;
        }

        private void usuarioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form childForm = new FormUsuario(this);
            childForm.Show();
            childForm.WindowState = FormWindowState.Normal;
            childForm.WindowState = FormWindowState.Maximized;
        }

        public SCContext Context
        {
            get { return _context; }
            set { _context = value; }
        }

        public Usuario User { get; set; }

        public void MontaLogout()
        {
            this.lbUser.Text = User.Login;
        }

        private void MDIPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Login.Dispose();
        }

        private void picLogout_Click(object sender, EventArgs e)
        {
            Login.Visible = true;
            Login.Focus();
            Login.LimpaCampos();
            Dispose();
        }

        private void clienteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form childForm = new FormCliente(this);
            childForm.Show();
            childForm.WindowState = FormWindowState.Normal;
            childForm.WindowState = FormWindowState.Maximized;
        }

        private void emprestimoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form childForm = new FormEmprestimo(this);
            childForm.Show();
            childForm.WindowState = FormWindowState.Normal;
            childForm.WindowState = FormWindowState.Maximized;
        }


        private void proprietárioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form childForm = new FormBuscaEmitente(this);
            childForm.Show();
            childForm.WindowState = FormWindowState.Normal;
            childForm.WindowState = FormWindowState.Maximized;
        }
    }
}
