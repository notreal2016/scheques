﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ADVCore.Service;
using MaterialSkin.Controls;
using Scheque;
using Scheque.Entidade;
using Scheque.Service;

namespace SchequesWin.Forms
{
    public partial class FormBuscaChequeAssist : Form
    {
        private ListBox _lista;
        private LoteService _service;

        /// <summary>
        /// Formulário auxiliar de busca de cheque dentro de um listBox
        /// </summary>
        /// <param name="lista">ListBox que contem os cheques</param>
        /// <param name="bancos">Combobox com os bancos <param>
        public FormBuscaChequeAssist(ListBox lista, ComboBox bancos, LoteService servico)
        {
            InitializeComponent();
            _lista = lista;
            this.cobBancos.DataSource = bancos.DataSource;
            _service = servico;
            this.KeyPreview = true;
            this.cobBancos.SelectedItem = null;
        }

        private void btFinaliza_Click(object sender, EventArgs e)
        {
            ValidaCampos();
            LocalizaCheque();
        }

        private void LocalizaCheque()
        {
            bool l = false;
            foreach (Cheque item in this._lista.Items)
            {
                if (item.ContaCheque.Banco == (Banco) cobBancos.SelectedItem &&
                    item.ContaCheque.Agencia.Equals(txtAgencia.Text) &&
                    item.ContaCheque.ContaCorrente.Equals(txtCC.Text) &&
                    item.NumeroCheque.Equals(txtNCh.Text))
                {
                    _lista.SelectedItem = item;
                    l = true;
                    break;
                    
                }
            }

            if (l)
            {
                Dispose(true);
                return;
            }
            MessageBox.Show("Não foi localizado nehum cheque com os dados acima com este lote", "Localizador de cheque",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ValidaCampos()
        {
            if (txtC1.Text.Trim().Equals("") && 
                (cobBancos.SelectedItem == null || 
                 txtOpercacao.Text.Trim().Equals("")|| 
                 txtAgencia.Text.Trim().Equals("")||
                 txtCC.Text.Trim().Equals("") || 
                 txtNCh.Text.Trim().Equals("")))
            {

                MessageBox.Show(
                    "Campos nescessário para a busca não foram digitados.\n\rVerifirque os campos e repita a busca.",
                    "Localiza cheque", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtC1.Focus();
                return;
            }

          
        }

        private void txtC1_Leave(object sender, EventArgs e)
        {
            String cmc7 = ValidaCmc7.RemoveCaracteresEspeciais(txtC1.Text);
            if (cmc7.Length == 30 && ValidaCmc7.ValidaCmc(cmc7))
            {
                ExtrairDados(cmc7);
                
                
            }
            else if (cmc7.Length == 30 && !ValidaCmc7.ValidaCmc(cmc7))
            {
                MessageBox.Show("Codigo CMC7 inválido", "Localiza Cheque", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                return;
            }
            else if (cmc7.Length != 30 && cmc7.Length != 0)
            {
                MessageBox.Show("Erro de leitura, CMC7 está inválido pela leitura ou digitação", "Cadastro de lote de cheques", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                txtC1.Text = "";
                txtC1.Focus();
            }
        }

        /// <summary>
        /// Método extrai do CMC7 repassado os dados para preencher os campos no formulário
        /// </summary>
        /// <param name="cmc7">Código CMC7</param>
        private void ExtrairDados(string cmc7)
        {
            Banco banco = _service.FindBancoByCodBanco(ValidaCmc7.ExtraiCodBanco(cmc7));
            if (banco == null)
            {
                MessageBox.Show("Dados do cheque estão inválidos, tente refazer a leitura ou digite manualmente.",
                    "Cadastro de lote", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            cobBancos.SelectedItem = banco;
            txtCC.Text = ValidaCmc7.ExtraiNumContaCliente(cmc7);
            txtAgencia.Text = ValidaCmc7.ExtraiCodAgencia(cmc7);
            txtOpercacao.Text = ValidaCmc7.ExtraiCodOperadorConta(cmc7);
            txtNCh.Text = ValidaCmc7.ExtraiNumDocumento(cmc7);
        }

        private void btCancela_Click(object sender, EventArgs e)
        {
            this.Dispose(true);
        }

        private void cobBancos_Leave(object sender, EventArgs e)
        {
            Banco banco = null;
            if (!cobBancos.Text.Trim().Equals("") && ValidaDados.SoNumeros(cobBancos.Text) && cobBancos.Text.Length == 3)
            {
                banco = _service.FindBancoByCodBanco(cobBancos.Text);
                cobBancos.SelectedItem = banco;
            }

            if (cobBancos.SelectedItem == null)
            {
                MessageBox.Show("Código de banco informado é inválido.", "Cód de banco.", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                cobBancos.Focus();
            }
        }

        private void FormBuscaChequeAssist_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);
                    break;
            }
            

        }

        private void txtAgencia_Layout(object sender, LayoutEventArgs e)
        {
            txtAgencia.Text = Scheque.Uteis.Util.PreencheMascara(txtAgencia.Text, txtAgencia.Mask.Length, '0');
        }

        private void txtOpercacao_Layout(object sender, LayoutEventArgs e)
        {
            txtOpercacao.Text = Scheque.Uteis.Util.PreencheMascara(txtOpercacao.Text, txtOpercacao.Mask.Length, '0');
        }

        private void txtCC_Layout(object sender, LayoutEventArgs e)
        {
            txtCC.Text = Scheque.Uteis.Util.PreencheMascara(txtCC.Text, txtCC.Mask.Length, '0');
        }

        private void txtNCh_Leave(object sender, EventArgs e)
        {
            txtNCh.Text = Scheque.Uteis.Util.PreencheMascara(txtNCh.Text, txtNCh.Mask.Length, '0');
        }
    }
}
