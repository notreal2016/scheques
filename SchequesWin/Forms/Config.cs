﻿using Scheque.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using Scheque.Service;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ADVCore.Service;

namespace SchequesWin.Forms
{
    public partial class Config : Form
    {
        private SCContext _context;

        public Config()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(txtIP.Text == null)
            {
                MessageBox.Show("Ip não pode ser nulo.", "Configuração", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }else
            {
                ConfigXML.ModificaXml(txtIP.Text);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}
