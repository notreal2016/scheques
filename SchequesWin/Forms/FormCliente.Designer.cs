﻿namespace SchequesWin.Forms
{
    partial class FormCliente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCliente));
            this.barraBusca = new System.Windows.Forms.ToolStrip();
            this.btExcluir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.cobResultado1 = new System.Windows.Forms.ToolStripComboBox();
            this.btEditar = new System.Windows.Forms.ToolStripButton();
            this.btImprimirLote = new System.Windows.Forms.ToolStripButton();
            this.btDesfazer = new System.Windows.Forms.ToolStripButton();
            this.barraInicial1 = new System.Windows.Forms.ToolStrip();
            this.btNovo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.labTermo = new System.Windows.Forms.ToolStripLabel();
            this.txtTermo = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cobBusca = new System.Windows.Forms.ToolStripComboBox();
            this.btBusca = new System.Windows.Forms.ToolStripButton();
            this.barraBusc = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.cobResulta = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.barraInicial = new System.Windows.Forms.ToolStrip();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel4 = new System.Windows.Forms.ToolStripLabel();
            this.txtTer = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel5 = new System.Windows.Forms.ToolStripLabel();
            this.CobTipo = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtEndereco = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCidade = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtUf = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtJuros = new NumericBox.NumericBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAdmin = new NumericBox.NumericBox();
            this.chkAcumulado = new System.Windows.Forms.CheckBox();
            this.chkPraso = new System.Windows.Forms.CheckBox();
            this.btFinaliza = new System.Windows.Forms.Button();
            this.btCancela = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.labId = new System.Windows.Forms.Label();
            this.txtBairro = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCPF = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNumero = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.panelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcone)).BeginInit();
            this.panelTop.SuspendLayout();
            this.panelBarra.SuspendLayout();
            this.panelContainer.SuspendLayout();
            this.barraBusca.SuspendLayout();
            this.barraInicial1.SuspendLayout();
            this.barraBusc.SuspendLayout();
            this.barraInicial.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelLeft
            // 
            this.panelLeft.Size = new System.Drawing.Size(75, 440);
            // 
            // picIcone
            // 
            this.picIcone.Image = null;
            this.picIcone.Size = new System.Drawing.Size(42, 42);
            // 
            // panelTop
            // 
            this.panelTop.Size = new System.Drawing.Size(840, 95);
            // 
            // panelBarra
            // 
            this.panelBarra.Controls.Add(this.barraBusc);
            this.panelBarra.Controls.Add(this.barraInicial);
            this.panelBarra.Size = new System.Drawing.Size(840, 62);
            // 
            // panelContainer
            // 
            this.panelContainer.Controls.Add(this.txtNumero);
            this.panelContainer.Controls.Add(this.label11);
            this.panelContainer.Controls.Add(this.txtCPF);
            this.panelContainer.Controls.Add(this.label10);
            this.panelContainer.Controls.Add(this.txtBairro);
            this.panelContainer.Controls.Add(this.label9);
            this.panelContainer.Controls.Add(this.labId);
            this.panelContainer.Controls.Add(this.label8);
            this.panelContainer.Controls.Add(this.btFinaliza);
            this.panelContainer.Controls.Add(this.btCancela);
            this.panelContainer.Controls.Add(this.chkPraso);
            this.panelContainer.Controls.Add(this.chkAcumulado);
            this.panelContainer.Controls.Add(this.label7);
            this.panelContainer.Controls.Add(this.txtAdmin);
            this.panelContainer.Controls.Add(this.label6);
            this.panelContainer.Controls.Add(this.txtJuros);
            this.panelContainer.Controls.Add(this.txtTelefone);
            this.panelContainer.Controls.Add(this.label5);
            this.panelContainer.Controls.Add(this.txtUf);
            this.panelContainer.Controls.Add(this.label4);
            this.panelContainer.Controls.Add(this.txtCidade);
            this.panelContainer.Controls.Add(this.label3);
            this.panelContainer.Controls.Add(this.txtEndereco);
            this.panelContainer.Controls.Add(this.label2);
            this.panelContainer.Controls.Add(this.txtNome);
            this.panelContainer.Controls.Add(this.label1);
            this.panelContainer.Size = new System.Drawing.Size(840, 345);
            this.panelContainer.Visible = false;
            // 
            // barraBusca
            // 
            this.barraBusca.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.barraBusca.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.barraBusca.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btExcluir,
            this.toolStripSeparator3,
            this.toolStripLabel2,
            this.cobResultado1,
            this.btEditar,
            this.btImprimirLote,
            this.btDesfazer});
            this.barraBusca.Location = new System.Drawing.Point(0, 31);
            this.barraBusca.Name = "barraBusca";
            this.barraBusca.Size = new System.Drawing.Size(831, 31);
            this.barraBusca.TabIndex = 5;
            this.barraBusca.Text = "toolStrip2";
            this.barraBusca.Visible = false;
            // 
            // btExcluir
            // 
            this.btExcluir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btExcluir.Image")));
            this.btExcluir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btExcluir.Name = "btExcluir";
            this.btExcluir.Size = new System.Drawing.Size(28, 28);
            this.btExcluir.Text = "Exclui o lote";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(59, 28);
            this.toolStripLabel2.Text = "Resultado";
            // 
            // cobResultado1
            // 
            this.cobResultado1.Name = "cobResultado1";
            this.cobResultado1.Size = new System.Drawing.Size(400, 31);
            // 
            // btEditar
            // 
            this.btEditar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btEditar.Image = ((System.Drawing.Image)(resources.GetObject("btEditar.Image")));
            this.btEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btEditar.Name = "btEditar";
            this.btEditar.Size = new System.Drawing.Size(28, 28);
            this.btEditar.Text = "Edita o lote";
            // 
            // btImprimirLote
            // 
            this.btImprimirLote.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btImprimirLote.Image = ((System.Drawing.Image)(resources.GetObject("btImprimirLote.Image")));
            this.btImprimirLote.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btImprimirLote.Name = "btImprimirLote";
            this.btImprimirLote.Size = new System.Drawing.Size(28, 28);
            this.btImprimirLote.Text = "Imprimir o lote";
            // 
            // btDesfazer
            // 
            this.btDesfazer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btDesfazer.Image = ((System.Drawing.Image)(resources.GetObject("btDesfazer.Image")));
            this.btDesfazer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btDesfazer.Name = "btDesfazer";
            this.btDesfazer.Size = new System.Drawing.Size(28, 28);
            this.btDesfazer.Text = "Desfaz o filtro";
            // 
            // barraInicial1
            // 
            this.barraInicial1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.barraInicial1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btNovo,
            this.toolStripSeparator1,
            this.labTermo,
            this.txtTermo,
            this.toolStripLabel1,
            this.cobBusca,
            this.btBusca});
            this.barraInicial1.Location = new System.Drawing.Point(0, 0);
            this.barraInicial1.Name = "barraInicial1";
            this.barraInicial1.Size = new System.Drawing.Size(831, 31);
            this.barraInicial1.TabIndex = 4;
            this.barraInicial1.Text = "toolStrip1";
            // 
            // btNovo
            // 
            this.btNovo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btNovo.Image = ((System.Drawing.Image)(resources.GetObject("btNovo.Image")));
            this.btNovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btNovo.Name = "btNovo";
            this.btNovo.Size = new System.Drawing.Size(28, 28);
            this.btNovo.Text = "Novo lote";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // labTermo
            // 
            this.labTermo.Name = "labTermo";
            this.labTermo.Size = new System.Drawing.Size(91, 28);
            this.labTermo.Text = "Termo de busca";
            // 
            // txtTermo
            // 
            this.txtTermo.Name = "txtTermo";
            this.txtTermo.Size = new System.Drawing.Size(200, 31);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(84, 28);
            this.toolStripLabel1.Text = "Tipo de busca:";
            // 
            // cobBusca
            // 
            this.cobBusca.Name = "cobBusca";
            this.cobBusca.Size = new System.Drawing.Size(121, 31);
            // 
            // btBusca
            // 
            this.btBusca.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btBusca.Image = ((System.Drawing.Image)(resources.GetObject("btBusca.Image")));
            this.btBusca.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btBusca.Name = "btBusca";
            this.btBusca.Size = new System.Drawing.Size(28, 28);
            this.btBusca.Text = "Buscar";
            // 
            // barraBusc
            // 
            this.barraBusc.Font = new System.Drawing.Font("Arial Unicode MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barraBusc.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.barraBusc.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripSeparator2,
            this.toolStripLabel3,
            this.cobResulta,
            this.toolStripButton2,
            this.toolStripButton4});
            this.barraBusc.Location = new System.Drawing.Point(0, 31);
            this.barraBusc.Name = "barraBusc";
            this.barraBusc.Size = new System.Drawing.Size(837, 31);
            this.barraBusc.TabIndex = 5;
            this.barraBusc.Text = "toolStrip2";
            this.barraBusc.Visible = false;
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(28, 28);
            this.toolStripButton1.Text = "Exclui o lote";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(66, 28);
            this.toolStripLabel3.Text = "Resultado";
            // 
            // cobResulta
            // 
            this.cobResulta.Name = "cobResulta";
            this.cobResulta.Size = new System.Drawing.Size(400, 31);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(28, 28);
            this.toolStripButton2.Text = "Edita o lote";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(28, 28);
            this.toolStripButton4.Text = "Desfaz o filtro";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // barraInicial
            // 
            this.barraInicial.Font = new System.Drawing.Font("Arial Unicode MS", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.barraInicial.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.barraInicial.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton5,
            this.toolStripSeparator4,
            this.toolStripLabel4,
            this.txtTer,
            this.toolStripLabel5,
            this.CobTipo,
            this.toolStripButton6});
            this.barraInicial.Location = new System.Drawing.Point(0, 0);
            this.barraInicial.Name = "barraInicial";
            this.barraInicial.Size = new System.Drawing.Size(840, 31);
            this.barraInicial.TabIndex = 4;
            this.barraInicial.Text = "toolStrip1";
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(28, 28);
            this.toolStripButton5.Text = "Novo lote";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripLabel4
            // 
            this.toolStripLabel4.Name = "toolStripLabel4";
            this.toolStripLabel4.Size = new System.Drawing.Size(102, 28);
            this.toolStripLabel4.Text = "Termo de busca";
            // 
            // txtTer
            // 
            this.txtTer.Name = "txtTer";
            this.txtTer.Size = new System.Drawing.Size(200, 31);
            // 
            // toolStripLabel5
            // 
            this.toolStripLabel5.Name = "toolStripLabel5";
            this.toolStripLabel5.Size = new System.Drawing.Size(94, 28);
            this.toolStripLabel5.Text = "Tipo de busca:";
            // 
            // CobTipo
            // 
            this.CobTipo.Name = "CobTipo";
            this.CobTipo.Size = new System.Drawing.Size(121, 31);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(28, 28);
            this.toolStripButton6.Text = "Buscar";
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(57, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome:";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(14, 51);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(451, 29);
            this.txtNome.TabIndex = 1;
            // 
            // txtEndereco
            // 
            this.txtEndereco.Location = new System.Drawing.Point(14, 101);
            this.txtEndereco.Name = "txtEndereco";
            this.txtEndereco.Size = new System.Drawing.Size(361, 29);
            this.txtEndereco.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(151, 21);
            this.label2.TabIndex = 2;
            this.label2.Text = "Endereço completo:";
            // 
            // txtCidade
            // 
            this.txtCidade.Location = new System.Drawing.Point(14, 151);
            this.txtCidade.Name = "txtCidade";
            this.txtCidade.Size = new System.Drawing.Size(200, 29);
            this.txtCidade.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 21);
            this.label3.TabIndex = 4;
            this.label3.Text = "Cidade:";
            // 
            // txtUf
            // 
            this.txtUf.Location = new System.Drawing.Point(220, 151);
            this.txtUf.Name = "txtUf";
            this.txtUf.Size = new System.Drawing.Size(36, 29);
            this.txtUf.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(220, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(30, 21);
            this.label4.TabIndex = 6;
            this.label4.Text = "Uf:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(265, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 21);
            this.label5.TabIndex = 8;
            this.label5.Text = "Telefone:";
            // 
            // txtTelefone
            // 
            this.txtTelefone.Location = new System.Drawing.Point(268, 152);
            this.txtTelefone.Mask = "(099) 0000-0000";
            this.txtTelefone.Name = "txtTelefone";
            this.txtTelefone.Size = new System.Drawing.Size(197, 29);
            this.txtTelefone.TabIndex = 9;
            // 
            // txtJuros
            // 
            this.txtJuros.BackColor = System.Drawing.SystemColors.Window;
            this.txtJuros.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtJuros.Location = new System.Drawing.Point(182, 184);
            this.txtJuros.Name = "txtJuros";
            this.txtJuros.NegativeNumberColor = System.Drawing.Color.Red;
            this.txtJuros.Size = new System.Drawing.Size(59, 29);
            this.txtJuros.TabIndex = 10;
            this.txtJuros.Text = "0,00";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(14, 188);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 21);
            this.label6.TabIndex = 11;
            this.label6.Text = "Taxa de Juros a.m.:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(265, 188);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(184, 21);
            this.label7.TabIndex = 13;
            this.label7.Text = "Taxa Adiministrativa R$: ";
            // 
            // txtAdmin
            // 
            this.txtAdmin.BackColor = System.Drawing.SystemColors.Window;
            this.txtAdmin.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAdmin.Location = new System.Drawing.Point(471, 184);
            this.txtAdmin.Name = "txtAdmin";
            this.txtAdmin.NegativeNumberColor = System.Drawing.Color.Red;
            this.txtAdmin.Size = new System.Drawing.Size(72, 29);
            this.txtAdmin.TabIndex = 12;
            this.txtAdmin.Text = "0,00";
            // 
            // chkAcumulado
            // 
            this.chkAcumulado.AutoSize = true;
            this.chkAcumulado.Location = new System.Drawing.Point(14, 233);
            this.chkAcumulado.Name = "chkAcumulado";
            this.chkAcumulado.Size = new System.Drawing.Size(152, 25);
            this.chkAcumulado.TabIndex = 14;
            this.chkAcumulado.Text = "Juros Acumulado";
            this.chkAcumulado.UseVisualStyleBackColor = true;
            // 
            // chkPraso
            // 
            this.chkPraso.AutoSize = true;
            this.chkPraso.Location = new System.Drawing.Point(14, 261);
            this.chkPraso.Name = "chkPraso";
            this.chkPraso.Size = new System.Drawing.Size(203, 25);
            this.chkPraso.TabIndex = 15;
            this.chkPraso.Text = "Ativa vencimento mínimo";
            this.chkPraso.UseVisualStyleBackColor = true;
            // 
            // btFinaliza
            // 
            this.btFinaliza.Image = global::SchequesWin.Properties.Resources.Lixo_24;
            this.btFinaliza.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btFinaliza.Location = new System.Drawing.Point(665, 293);
            this.btFinaliza.Name = "btFinaliza";
            this.btFinaliza.Size = new System.Drawing.Size(46, 37);
            this.btFinaliza.TabIndex = 41;
            this.btFinaliza.TabStop = false;
            this.btFinaliza.UseVisualStyleBackColor = true;
            this.btFinaliza.Click += new System.EventHandler(this.btFinaliza_Click);
            // 
            // btCancela
            // 
            this.btCancela.Image = global::SchequesWin.Properties.Resources.Excluir_24;
            this.btCancela.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btCancela.Location = new System.Drawing.Point(4, 293);
            this.btCancela.Name = "btCancela";
            this.btCancela.Size = new System.Drawing.Size(46, 37);
            this.btCancela.TabIndex = 40;
            this.btCancela.TabStop = false;
            this.btCancela.UseVisualStyleBackColor = true;
            this.btCancela.Click += new System.EventHandler(this.btCancela_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(14, 3);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(28, 21);
            this.label8.TabIndex = 42;
            this.label8.Text = "Id:";
            // 
            // labId
            // 
            this.labId.AutoSize = true;
            this.labId.Location = new System.Drawing.Point(48, 3);
            this.labId.Name = "labId";
            this.labId.Size = new System.Drawing.Size(0, 21);
            this.labId.TabIndex = 43;
            // 
            // txtBairro
            // 
            this.txtBairro.Location = new System.Drawing.Point(421, 101);
            this.txtBairro.Name = "txtBairro";
            this.txtBairro.Size = new System.Drawing.Size(290, 29);
            this.txtBairro.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(421, 80);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 21);
            this.label9.TabIndex = 44;
            this.label9.Text = "Bairro:";
            // 
            // txtCPF
            // 
            this.txtCPF.Location = new System.Drawing.Point(474, 51);
            this.txtCPF.Name = "txtCPF";
            this.txtCPF.Size = new System.Drawing.Size(237, 29);
            this.txtCPF.TabIndex = 2;
            this.txtCPF.Leave += new System.EventHandler(this.textCPF_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(471, 30);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(116, 21);
            this.label10.TabIndex = 46;
            this.label10.Text = "CPF ou CNPJ:";
            // 
            // txtNumero
            // 
            this.txtNumero.Location = new System.Drawing.Point(379, 101);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.Size = new System.Drawing.Size(36, 29);
            this.txtNumero.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(379, 80);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(32, 21);
            this.label11.TabIndex = 47;
            this.label11.Text = "Nº.";
            // 
            // FormCliente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(915, 440);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormCliente";
            this.Text = "Cadastro de cliente";
            this.panelLeft.ResumeLayout(false);
            this.panelLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcone)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.panelBarra.ResumeLayout(false);
            this.panelBarra.PerformLayout();
            this.panelContainer.ResumeLayout(false);
            this.panelContainer.PerformLayout();
            this.barraBusca.ResumeLayout(false);
            this.barraBusca.PerformLayout();
            this.barraInicial1.ResumeLayout(false);
            this.barraInicial1.PerformLayout();
            this.barraBusc.ResumeLayout(false);
            this.barraBusc.PerformLayout();
            this.barraInicial.ResumeLayout(false);
            this.barraInicial.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip barraBusca;
        private System.Windows.Forms.ToolStripButton btExcluir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox cobResultado1;
        private System.Windows.Forms.ToolStripButton btEditar;
        private System.Windows.Forms.ToolStripButton btImprimirLote;
        private System.Windows.Forms.ToolStripButton btDesfazer;
        private System.Windows.Forms.ToolStrip barraInicial1;
        private System.Windows.Forms.ToolStripButton btNovo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel labTermo;
        private System.Windows.Forms.ToolStripTextBox txtTermo;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox cobBusca;
        private System.Windows.Forms.ToolStripButton btBusca;
        private System.Windows.Forms.ToolStrip barraBusc;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripComboBox cobResulta;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStrip barraInicial;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripLabel toolStripLabel4;
        private System.Windows.Forms.ToolStripTextBox txtTer;
        private System.Windows.Forms.ToolStripLabel toolStripLabel5;
        private System.Windows.Forms.ToolStripComboBox CobTipo;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.MaskedTextBox txtTelefone;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtUf;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCidade;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtEndereco;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkPraso;
        private System.Windows.Forms.CheckBox chkAcumulado;
        private System.Windows.Forms.Label label7;
        private NumericBox.NumericBox txtAdmin;
        private System.Windows.Forms.Label label6;
        private NumericBox.NumericBox txtJuros;
        private System.Windows.Forms.Button btFinaliza;
        private System.Windows.Forms.Button btCancela;
        private System.Windows.Forms.Label labId;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCPF;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtBairro;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNumero;
        private System.Windows.Forms.Label label11;
    }
}