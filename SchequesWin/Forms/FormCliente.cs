﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Scheque.Entidade;
using Scheque.Service;

namespace SchequesWin.Forms
{
    public partial class FormCliente : FormModelo
    {
        
        private ComboBox _origem;
        private Cliente _cliente;

        private ClienteService _service;
        
        public FormCliente(MDIPrincipal mDIPrincipal, ComboBox combo = null)
        {
            this.MdiParent = mDIPrincipal;
            Inicializa();
            _origem = combo;
            if (_origem != null && _origem.SelectedItem != null)
            {
                _cliente = (Cliente) _origem.SelectedItem;
                PreencheCampos();
                barraInicial.Enabled = false;
                barraBusca.Enabled = false;
                panelContainer.Visible = true;
            }else if (_origem != null)
            {
                LimpaCampos();
                barraInicial.Enabled = false;
                barraBusca.Enabled = false;
                panelContainer.Visible = true;
            }
        }
        
        private void Inicializa()
        {
            InitializeComponent();
            CobTipo.Items.Add(TipoBusca.NOME);
            _service = new ClienteService(((MDIPrincipal) this.MdiParent).Context);

        }
        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            this.panelContainer.Visible = true;
            LimpaCampos();
            txtNome.Focus();
            barraBusc.Enabled = false;
            barraInicial.Enabled = false;
        }

        private void LimpaCampos()
        {
            txtNome.Text = "";
            txtEndereco.Text = "";
            txtCidade.Text = "";
            txtTelefone.Text = "";
            txtJuros.Text = "0";
            txtAdmin.Text = "0";
            txtUf.Text = "";
            chkAcumulado.Checked = false;
            chkPraso.Checked = false;
            labId.Text = "";
            txtCPF.Text = "";
            _cliente = null;
        }

        private void btFinaliza_Click(object sender, EventArgs e)
        {
            try
            {
                CriaCliente();
                barraInicial.Enabled = true;
                barraBusc.Enabled = true;
                panelContainer.Visible = false;
                _service.Save(_cliente);
                MessageBox.Show("Cliente cadastrado com sucesso!", Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                if (_origem != null)
                {
                    _origem.DataSource = _service.AllClientes();
                    _origem.SelectedItem = _cliente;
                    _origem.Focus();
                    Dispose(true);
                }
                LimpaCampos();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            

        }

        private void CriaCliente()
        {
            if (_cliente == null)
            {
                _cliente = new Cliente();
            }

            _cliente.Nome = txtNome.Text;
            _cliente.Endereco = txtEndereco.Text;
            _cliente.Bairro = txtBairro.Text;
            _cliente.Cidada = txtCidade.Text;
            _cliente.Uf = txtUf.Text;
            _cliente.Administrativa = double.Parse(txtAdmin.Text);
            _cliente.Cpfcnpj = txtCPF.Text;
            _cliente.Taxa = double.Parse(txtJuros.Text);
            _cliente.Acumulado = chkAcumulado.Checked;
            //_cliente.AplicaTempoMinimo = chkPraso.Checked;
            _cliente.Telefone = txtTelefone.Text;
            _cliente.Numero = (txtNumero.Text.Trim().Equals(""))?0:int.Parse(txtNumero.Text);


        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            if (txtTer.Text.Trim().Equals(""))
            {
                MessageBox.Show("Não foi repassado dados para a busca.", Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                txtTer.Focus();
                return;
            }

            cobResulta.ComboBox.DataSource = _service.FindByName(txtTer.Text);
            barraBusc.Visible = true;
            barraBusc.Focus();

        }

        private void textCPF_Leave(object sender, EventArgs e)
        {
            if (!txtCPF.Text.Trim().Equals(""))
            {

                try
                {
                    txtCPF.Text = Scheque.Uteis.Util.AplicaMascara(txtCPF.Text);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtCPF.Focus();
                }
            }
        }

        private void btCancela_Click(object sender, EventArgs e)
        {
            var reposta = MessageBox.Show("Deseja realmente cancelar o processo de cadastro de um cliente?", Text,
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (reposta == DialogResult.Yes)
            {
                LimpaCampos();
                panelContainer.Visible = false;
                barraBusc.Enabled = true;
                barraInicial.Enabled = true;
                if (_origem != null)
                {
                    Dispose(true);
                }
            }
            
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (cobResulta.SelectedItem == null)
            {
                MessageBox.Show("Para edição deve ser selecionado um cliente na lista de resultado.", Text,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                cobResulta.Focus();
            }

            _cliente = (Cliente) cobResulta.SelectedItem;
            _cliente = _service.Refresh(_cliente);
            PreencheCampos();
            panelContainer.Visible = true;
            barraBusc.Enabled = false;
            barraInicial.Enabled = false;
        }

        private void PreencheCampos()
        {
            labId.Text = _cliente.Id.ToString();
            txtNome.Text = _cliente.Nome;
            txtEndereco.Text = _cliente.Endereco ;
            txtBairro.Text = _cliente.Bairro ;
            txtCidade.Text = _cliente.Cidada ;
            txtUf.Text = _cliente.Uf ;
            txtAdmin.Text = String.Format("{0:F}", _cliente.Administrativa); 
            txtCPF.Text = _cliente.Cpfcnpj ;
            txtJuros.Text = String.Format("{0:F}", _cliente.Taxa );
            chkAcumulado.Checked = _cliente.Acumulado ;
           // chkPraso.Checked = _cliente.AplicaTempoMinimo ;
            txtTelefone.Text = _cliente.Telefone ;
            txtNumero.Text = _cliente.Numero.ToString();

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            if (cobResulta.SelectedItem == null)
            {
                MessageBox.Show("Para excluir um cliente deve ser selecionado no campo de resultado.", Text,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                cobResulta.Focus();
                return;
                
            }

            _cliente = (Cliente) cobResulta.SelectedItem;
            PreencheCampos();
            panelContainer.Visible = true;
            panelContainer.Enabled = false;

            var resultado = MessageBox.Show(String.Format("Deseja excluir o cliente {0} do sistema?", _cliente.Nome),
                Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (resultado == DialogResult.Yes)
            {
                try
                {
                    _service.Exclir(_cliente);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
                finally
                {
                    LimpaCampos();
                    panelContainer.Visible = false;
                    panelContainer.Enabled = true;
                    
                }

            }
            else
            {
                LimpaCampos();
                panelContainer.Visible = false;
                panelContainer.Enabled = true;
            }

        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            barraBusca.Visible = false;
            barraBusc.Visible = false;
            txtTermo.Focus();
        }
    }
}
