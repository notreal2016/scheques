﻿using System;
using System.Windows.Forms;
using System.Xml.Linq;
using ADVCore.Service;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Service;

namespace SchequesWin.Forms
{
    public partial class FormLogin : Form
    {
        private SCContext _context;
        private Usuario _user;
        public FormLogin()
        {
            InitializeComponent();
        }

        private void btLogar_Click(object sender, EventArgs e)
        {
            try
            {
                VerificaDados();
                String server = ColetandoEndereco();
                _context = FactoryContext.GetContext("laerton", "123456", server);
                UsuarioService service = new UsuarioService(_context);
                _user = service.LogaUsuario(txtLogin.Text, txtSenha.Text);
                if (_user == null || _user.Id == 0)
                {
                    MessageBox.Show("Usuário ou senha inválidos!", Text, MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                    return;
                }

                if (_user.Ativo)
                {
                    MessageBox.Show("Usuário está bloqueado para uso do sistema, contate outro operador para libera-lo.", Text, MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                    return;
                }

                FactoryContext.CloseContext();
                _context = FactoryContext.GetContext(_user.Login, _user.Senha, server);
                MDIPrincipal principal = new MDIPrincipal();
                principal.Context = _context;
                principal.User = _user;
                principal.Login = this;
                principal.MontaLogout();
                principal.Show();
                this.Visible = false;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtLogin.Focus();
                return;
            }
            

        }

        private void VerificaDados()
        {
            if (txtLogin.Text.Trim().Equals(""))
            {
                throw new Exception("O campo login deve ser preenchido.");
            }

            if (ValidaDados.ExistCaracterEspeciais(txtLogin.Text))
            {
                throw  new Exception("O login do usuário não pode conter caracteres especiais.");
            }

            if (txtSenha.Text.Trim().Equals(""))
            {
                throw  new Exception("A senha do usuário deve ser repassada.");
            }
        }

        public void LimpaCampos()
        {
            txtLogin.Text = "";
            txtSenha.Text = "";
            txtLogin.Focus();
        }

        private void btCancel_Click(object sender, EventArgs e)
        {
            _context = FactoryContext.GetContext();
            MDIPrincipal principal = new MDIPrincipal();
            principal.Context = _context;
            principal.User = new Usuario();
            principal.Login = this;
            principal.MontaLogout();
            principal.Show();
            this.Visible = false;

        }

        private String ColetandoEndereco()
        {
            String retorno = "";
            XElement xml = XElement.Load("setings.xml");
            retorno = xml.Value;
            return retorno;
        }
    }
}
