﻿using System.Windows.Forms;
using MaterialSkin.Controls;

namespace SchequesWin.Forms
{
    partial class FormBuscaChequeAssist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBuscaChequeAssist));
            this.txtNCh = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new MaterialSkin.Controls.MaterialLabel();
            this.txtCC = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new MaterialSkin.Controls.MaterialLabel();
            this.txtOpercacao = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new MaterialSkin.Controls.MaterialLabel();
            this.cobBancos = new System.Windows.Forms.ComboBox();
            this.txtAgencia = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new MaterialSkin.Controls.MaterialLabel();
            this.label15 = new MaterialSkin.Controls.MaterialLabel();
            this.txtC1 = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.label17 = new MaterialSkin.Controls.MaterialLabel();
            this.label1 = new MaterialSkin.Controls.MaterialLabel();
            this.label2 = new MaterialSkin.Controls.MaterialLabel();
            this.btFinaliza = new System.Windows.Forms.Button();
            this.btCancela = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNCh
            // 
            this.txtNCh.Location = new System.Drawing.Point(520, 175);
            this.txtNCh.Mask = "000000";
            this.txtNCh.Name = "txtNCh";
            this.txtNCh.Size = new System.Drawing.Size(89, 26);
            this.txtNCh.TabIndex = 34;
            this.txtNCh.Leave += new System.EventHandler(this.txtNCh_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Depth = 0;
            this.label11.Font = new System.Drawing.Font("Roboto", 11F);
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label11.Location = new System.Drawing.Point(429, 175);
            this.label11.MouseState = MaterialSkin.MouseState.HOVER;
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(86, 19);
            this.label11.TabIndex = 37;
            this.label11.Text = "Nº Cheque:";
            // 
            // txtCC
            // 
            this.txtCC.Location = new System.Drawing.Point(325, 175);
            this.txtCC.Mask = "000000";
            this.txtCC.Name = "txtCC";
            this.txtCC.Size = new System.Drawing.Size(98, 26);
            this.txtCC.TabIndex = 33;
            this.txtCC.Layout += new System.Windows.Forms.LayoutEventHandler(this.txtCC_Layout);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Depth = 0;
            this.label10.Font = new System.Drawing.Font("Roboto", 11F);
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label10.Location = new System.Drawing.Point(285, 178);
            this.label10.MouseState = MaterialSkin.MouseState.HOVER;
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 19);
            this.label10.TabIndex = 36;
            this.label10.Text = "C/C";
            // 
            // txtOpercacao
            // 
            this.txtOpercacao.Location = new System.Drawing.Point(224, 174);
            this.txtOpercacao.Mask = "000";
            this.txtOpercacao.Name = "txtOpercacao";
            this.txtOpercacao.Size = new System.Drawing.Size(59, 26);
            this.txtOpercacao.TabIndex = 32;
            this.txtOpercacao.Layout += new System.Windows.Forms.LayoutEventHandler(this.txtOpercacao_Layout);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Depth = 0;
            this.label9.Font = new System.Drawing.Font("Roboto", 11F);
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label9.Location = new System.Drawing.Point(144, 177);
            this.label9.MouseState = MaterialSkin.MouseState.HOVER;
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 19);
            this.label9.TabIndex = 35;
            this.label9.Text = "Operação:";
            // 
            // cobBancos
            // 
            this.cobBancos.FormattingEnabled = true;
            this.cobBancos.Location = new System.Drawing.Point(69, 139);
            this.cobBancos.Name = "cobBancos";
            this.cobBancos.Size = new System.Drawing.Size(540, 28);
            this.cobBancos.TabIndex = 30;
            this.cobBancos.Leave += new System.EventHandler(this.cobBancos_Leave);
            // 
            // txtAgencia
            // 
            this.txtAgencia.Location = new System.Drawing.Point(82, 175);
            this.txtAgencia.Mask = "0000";
            this.txtAgencia.Name = "txtAgencia";
            this.txtAgencia.Size = new System.Drawing.Size(59, 26);
            this.txtAgencia.TabIndex = 31;
            this.txtAgencia.Layout += new System.Windows.Forms.LayoutEventHandler(this.txtAgencia_Layout);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Depth = 0;
            this.label14.Font = new System.Drawing.Font("Roboto", 11F);
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label14.Location = new System.Drawing.Point(14, 178);
            this.label14.MouseState = MaterialSkin.MouseState.HOVER;
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(67, 19);
            this.label14.TabIndex = 28;
            this.label14.Text = "Agência:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Depth = 0;
            this.label15.Font = new System.Drawing.Font("Roboto", 11F);
            this.label15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label15.Location = new System.Drawing.Point(14, 142);
            this.label15.MouseState = MaterialSkin.MouseState.HOVER;
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(55, 19);
            this.label15.TabIndex = 27;
            this.label15.Text = "Banco:";
            // 
            // txtC1
            // 
            this.txtC1.Depth = 0;
            this.txtC1.Hint = "";
            this.txtC1.Location = new System.Drawing.Point(128, 66);
            this.txtC1.MouseState = MaterialSkin.MouseState.HOVER;
            this.txtC1.Name = "txtC1";
            this.txtC1.PasswordChar = '\0';
            this.txtC1.SelectedText = "";
            this.txtC1.SelectionLength = 0;
            this.txtC1.SelectionStart = 0;
            this.txtC1.Size = new System.Drawing.Size(481, 23);
            this.txtC1.TabIndex = 29;
            this.txtC1.UseSystemPasswordChar = false;
            this.txtC1.Leave += new System.EventHandler(this.txtC1_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Depth = 0;
            this.label17.Font = new System.Drawing.Font("Roboto", 11F);
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label17.Location = new System.Drawing.Point(14, 67);
            this.label17.MouseState = MaterialSkin.MouseState.HOVER;
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(93, 19);
            this.label17.TabIndex = 26;
            this.label17.Text = "Barra CMC7:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Depth = 0;
            this.label1.Font = new System.Drawing.Font("Roboto", 11F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label1.Location = new System.Drawing.Point(97, 30);
            this.label1.MouseState = MaterialSkin.MouseState.HOVER;
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(439, 19);
            this.label1.TabIndex = 38;
            this.label1.Text = "Para efetuar a busca digite o código de barras CMC7 do cheque:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Depth = 0;
            this.label2.Font = new System.Drawing.Font("Roboto", 11F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label2.Location = new System.Drawing.Point(205, 104);
            this.label2.MouseState = MaterialSkin.MouseState.HOVER;
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(223, 19);
            this.label2.TabIndex = 39;
            this.label2.Text = "Ou preencha os campos abaixo:";
            // 
            // btFinaliza
            // 
            this.btFinaliza.AutoSize = true;
            this.btFinaliza.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btFinaliza.Image = global::SchequesWin.Properties.Resources.Lixo_24;
            this.btFinaliza.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btFinaliza.Location = new System.Drawing.Point(573, 218);
            this.btFinaliza.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btFinaliza.Name = "btFinaliza";
            this.btFinaliza.Size = new System.Drawing.Size(30, 30);
            this.btFinaliza.TabIndex = 41;
            this.btFinaliza.TabStop = false;
            this.btFinaliza.UseVisualStyleBackColor = true;
            this.btFinaliza.Click += new System.EventHandler(this.btFinaliza_Click);
            // 
            // btCancela
            // 
            this.btCancela.AutoSize = true;
            this.btCancela.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.btCancela.Image = global::SchequesWin.Properties.Resources.Excluir_24;
            this.btCancela.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btCancela.Location = new System.Drawing.Point(18, 218);
            this.btCancela.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.btCancela.Name = "btCancela";
            this.btCancela.Size = new System.Drawing.Size(30, 30);
            this.btCancela.TabIndex = 40;
            this.btCancela.TabStop = false;
            this.btCancela.UseVisualStyleBackColor = true;
            this.btCancela.Click += new System.EventHandler(this.btCancela_Click);
            // 
            // FormBuscaChequeAssist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(642, 268);
            this.Controls.Add(this.btFinaliza);
            this.Controls.Add(this.btCancela);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNCh);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtCC);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtOpercacao);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.cobBancos);
            this.Controls.Add(this.txtAgencia);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.txtC1);
            this.Controls.Add(this.label17);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormBuscaChequeAssist";
            this.Text = "Localizardor de cheque";
            this.TopMost = true;
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormBuscaChequeAssist_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MaskedTextBox txtNCh;
        private MaterialLabel label11; //MaterialLabel label11;
        private System.Windows.Forms.MaskedTextBox txtCC;
        private MaterialLabel label10;//MaterialLabel label10;
        private System.Windows.Forms.MaskedTextBox txtOpercacao;
        private MaterialLabel label9;//MaterialLabel label9;
        private System.Windows.Forms.ComboBox cobBancos;
        private System.Windows.Forms.MaskedTextBox txtAgencia;
        private MaterialLabel label14;
        private MaterialLabel label15;
        private MaterialSingleLineTextField txtC1;
        private MaterialLabel label17;
        private MaterialLabel label1;
        private MaterialLabel label2;
        private System.Windows.Forms.Button btFinaliza;
        private System.Windows.Forms.Button btCancela;
    }
}