﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Scheque.Entidade;
using Scheque.Service;
using Scheque.Repository;
using System.Windows.Forms;
using Scheque;

namespace SchequesWin.Forms
{
    public partial class FormBuscaEmitente : FormModelo
    {
        private ProprietarioService _service;
        private Proprietario _proprietario;


        public FormBuscaEmitente(MDIPrincipal principal)
        {
            this.MdiParent = principal;
            Inicializa();
        }

        private void Inicializa()
        {
            InitializeComponent();
            _service = new ProprietarioService(((MDIPrincipal)MdiParent).Context);
            //cobBusca.Items.Add(TipoBusca.NOME);
        }

        private void labTitulo_Click(object sender, EventArgs e)
        {

        }

        private void cobBusca_Click(object sender, EventArgs e)
        {

        }

        private void txtTermo_Click(object sender, EventArgs e)
        {

        }

        private void btBusca_Click(object sender, EventArgs e)
        {
            
        }

        private void txtNome_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtLogin_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void PreencheCampos(Proprietario _proprietario)
        {
            labId.Text = _proprietario.Id.ToString();
            txtNome.Text = _proprietario.Nome;
            txtDoc.Text = _proprietario.CpfCNPJ;
        }

        private void LimpaCampos()
        {
            labId.Text = "";
            txtDoc.Text = "";
            txtNome.Text = "";
            _proprietario = new Proprietario();
        }

        private void btBusca_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (cobResultado.Text.Trim().Equals(""))
                {
                    MessageBox.Show("Campo de busca vazio.", "Cadastro de Proprietario", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cobResultado.Focus();
                    return;
                }

                String _doc = ValidaCmc7.RemoveCaracteresEspeciais(cobResultado.Text);

                _doc = Scheque.Uteis.Util.AplicaMascara(_doc);

                Proprietario proprietario = _service.FindByDocs(_doc);

                if (proprietario == null) MessageBox.Show("Proprietario não localizado!", "Cadastro de Proprietario", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    AtivaCampos(true);
                    PreencheCampos(proprietario);
                    this.panelContext.Visible = true;
                    barraInicial.Enabled = false;
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Cadastro de Proprietario", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtNome.Focus();
            }
        }

        private void AtivaCampos(bool b)
        {
            txtNome.Enabled = b;
            txtDoc.Enabled = b;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            this.panelContext.Visible = true;
            txtNome.Focus();
            LimpaCampos();
            barraInicial.Enabled = false;
            AtivaCampos(true);
        }

        private void panelContainer_Paint(object sender, PaintEventArgs e)
        {

        }

        private void CriaProprietario()
        {
            if(_proprietario == null)
            {
                _proprietario = new Proprietario();
            }
            _proprietario.CpfCNPJ = Scheque.Uteis.Util.AplicaMascara(txtDoc.Text);
            _proprietario.Nome = txtNome.Text;
            
        }

        private void txtLogin_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void btFinaliza_Click(object sender, EventArgs e)
        {
            CriaProprietario();

            try
            {
                if(_service.FindByDocs(_proprietario.CpfCNPJ) == null)
                {
                    _service.Save(_proprietario);
                    MessageBox.Show("Proprietário cadastrado com sucesso", "Cadastro de Proprietário", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    this.panelContext.Visible = false;
                    LimpaCampos();
                    barraInicial.Enabled = true;
                }else
                {
                    _proprietario.Id = Int64.Parse(labId.Text);
                    _service.Save(_proprietario);
                }
                
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Cadastro de Proprietario", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtNome.Focus();
            }
        }

        private void btCancela_Click(object sender, EventArgs e)
        {
            this.panelContext.Visible = false;
            barraInicial.Enabled = true;
        }

        private void btEditar_Click(object sender, EventArgs e)
        {
            try
            {
                if (cobResultado.Text.Trim().Equals(""))
                {
                    MessageBox.Show("Deve ser selecionado um proprietário para poder edita-lo");
                    cobResultado.Focus();
                    return;
                }

                String _doc = ValidaCmc7.RemoveCaracteresEspeciais(cobResultado.Text);

                _doc = Scheque.Uteis.Util.AplicaMascara(_doc);

                Proprietario proprietario = _service.FindByDocs(_doc);

                if (proprietario == null) MessageBox.Show("Proprietario não localizado!", "Cadastro de Proprietario", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    AtivaCampos(false);
                    PreencheCampos(proprietario);
                    this.panelContext.Visible = true;
                    barraInicial.Enabled = false;
                }
            }catch(Exception exception)
            {
                MessageBox.Show(exception.Message, "Cadastro de Proprietario", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtNome.Focus();
            }
           
            
        }

        private void btImprimirLote_Click(object sender, EventArgs e)
        {

        }

        private void btDesfazer_Click(object sender, EventArgs e)
        {
            cobResultado.Text = "";
        }
    }
}


