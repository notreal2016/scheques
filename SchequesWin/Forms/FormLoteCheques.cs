﻿using System;

using System.Windows.Forms;
using ADVCore.Service;

using Scheque.Entidade;
using Scheque.Service;
using  Microsoft.VisualBasic;
using Scheque;
using Exception = System.Exception;

namespace SchequesWin.Forms
{
    public partial class FormLoteCheques : FormModelo
    {
        private LoteService _service;
        private Lote _lote;
        private FormCliente _frmCliente;
        private Cheque _chequeAtual;
        private Conta _conta;
        private int _dias = 0;
        private DateTime _novoVencimento;
        private double _juros;

        public FormLoteCheques(MDIPrincipal principal)
        {
            InitializeComponent();
            this.MdiParent = principal;
            InicializaForm();

        }
        
        private void InicializaForm()
        {
            _service = new LoteService(((MDIPrincipal)MdiParent).Context);
            tabLote.Visible = false;
            tabSelect.Visible = false;
            barraBusca.Visible = false;
            AjustaCobClientes();
            AjustaCobBancos();
            CriaCombBusca();
            if (_lote == null) { _lote = new Lote();}

            this.KeyPreview = true;


        }

        private void AjustaCobBancos()
        {
            cobBancos.DataSource = _service.AllBancos();
            cobBancos.SelectedItem = null;
        }

        private void AjustaCobClientes()
        {
            cobClientes.DataSource = _service.AllClientes();
            cobClientes.SelectedItem = null;
        }

        private void FormLoteCheques_Resize(object sender, EventArgs e)
        {
            tabLote.Width = this.Width -5;
            tabLote.Height = this.Height -5;
        }

        private void btNovo_Click(object sender, EventArgs e)
        {
            NovoLote();
            
        }

        private void NovoLote()
        {
            LimpaCampos();
            tabLote.Visible = true;
            tabSelect.Visible = true;
            tbCheques.Enabled = false;
            barraInicial.Enabled = false;
            tbLote.Enabled = true;
            tbCheques.Enabled = false;
            tabLote.SelectedTab = tbLote;
            lbCheques.Items.Clear();
            labNLote.Text = "";
            LabelLote.Text = "";
            labNome.Text = "";
            labInforma.Text = "";
            txtValor.Text = "0";
            cobBancos.SelectedItem = null;
            txtQunatidade.Text = "0";
            txtJuros.Text = "0";
            txtAdmin.Text = "0";
            labDesconto.Text = "R$ ";
            labCET.Text = "R$ ";
            barraBusca.Visible = false;
            cobClientes.SelectedItem = null;
        }

        private void cobBancos_Leave(object sender, EventArgs e)
        {
            Banco banco = null;
            if (!cobBancos.Text.Trim().Equals("") && ValidaDados.SoNumeros(cobBancos.Text) && cobBancos.Text.Length == 3)
            {
                banco = _service.FindBancoByCodBanco(cobBancos.Text);
                cobBancos.SelectedItem = banco;
            }
            
        }

        private void btFiltrar_Click(object sender, EventArgs e)
        {
            var termo = Interaction.InputBox("Informe o termo que deseja fazer a filtragem dos dados do cliente:",
                "Filtro de cliente", "%", 100, 200);
            cobClientes.DataSource = _service.FindClienteByNome(termo);
            if (cobClientes.Items.Count == 0)
            {
                var resposta = MessageBox.Show("O resultado do filtro foi de zero registros, deseja aplicar um novo filtro?",
                    "Filtro de clientes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resposta == DialogResult.Yes)
                {
                    btFiltrar_Click(sender, e);
                }
                else
                {
                    AjustaCobClientes();
                }
            }

        }

        private void cobClientes_Leave(object sender, EventArgs e)
        {
            if (cobClientes.SelectedItem != null)
            {
                Cliente cliente = (Cliente) cobClientes.SelectedItem;
                txtJuros.Text = cliente.Taxa.ToString();
                txtAdmin.Text = cliente.Administrativa.ToString();

            }
        }

        private void CriaCombBusca()
        {
            cobBusca.Items.Add(TipoBusca.CLIENTE);
            cobBusca.Items.Add(TipoBusca.DATA);
            cobBusca.Items.Add(TipoBusca.LOTE);
        }

        private void btBusca_Click(object sender, EventArgs e)
        {
            if (txtTermo.Text.Trim().Equals(""))
            {
                MessageBox.Show("Não foi digitado nenhuma informação para busca.", "Erro na busca",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTermo.Focus();
                return;
            }

            if (cobBusca.SelectedItem == null)
            {
                MessageBox.Show("Não foi selecionado nenhum dipo de busca.");
                return;
            }

            
            AjustaBusca();
            cobResultado.Items.AddRange(_service.BuscaSelecionada((TipoBusca)cobBusca.SelectedItem, txtTermo.Text));
        }

        private void AjustaBusca()
        {
            cobResultado.Items.Clear();
            cobResultado.ComboBox.SelectedItem = null;
            cobResultado.SelectedItem = null;
            barraBusca.Visible = true;
            barraBusca.Enabled = true;
        }

        private void btCheques_Click(object sender, EventArgs e)
        {
            ComecaLote();
        }

        private void ComecaLote()
        {
            _lote.Cliente = (Cliente) cobClientes.SelectedItem;
            _lote.Administrativa = double.Parse(txtAdmin.Text);
            _lote.Taxa = double.Parse(txtJuros.Text);
            _lote.DataLote = txtData.Value;
            _lote.ValorLote = double.Parse(txtValor.Text);
            _lote.Quantidade = int.Parse(txtQunatidade.Text);

            try
            {
                ValidaLote.Valida(_lote);
                tbCheques.Enabled = true;
                tabLote.SelectedTab = tbCheques;
                tbLote.Enabled = false;
                labNome.Text = _lote.Cliente.Nome;
                if (_lote.Id != 0)
                {
                    labNLote.Text = "Nº Lote: " + _lote.Id;
                }

                MontaInformaLote();
                MontaLabInformaCheque();
                txtC1.Focus();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Lote de cheques", MessageBoxButtons.OK, MessageBoxIcon.Error);
                cobClientes.Focus();
            }
        }

        private void MontaInformaLote()
        {
            labInforma.Text = "Valor do Lote R$ " + txtValor.Text + "\n" +
                              "Quantidade: " + txtQunatidade.Text + "\n" +
                              "Documento " + ((lbCheques.Items.Count != int.Parse(txtQunatidade.Text)) 
                                  ? (lbCheques.Items.Count + 1) : (lbCheques.Items.Count)) + " de " 
                              + txtQunatidade.Text +"." ;
           
        }

        private void MontaLabInformaCheque()
        {
            labInformeCh.Text = "";
            labVInforma.Text = "";
        }

        public Lote Lote
        {
            get { return _lote; }
            set { _lote = value; }
        }

        private void BtAdicionarCliente_Click(object sender, EventArgs e)
        {
            _frmCliente = new FormCliente((MDIPrincipal)MdiParent, cobClientes);
            _frmCliente.Show();
            _frmCliente.WindowState = FormWindowState.Normal;
            _frmCliente.WindowState = FormWindowState.Maximized;

        }

        private void btDesfazer_Click(object sender, EventArgs e)
        {
            barraBusca.Visible = false;
            barraBusca.Enabled = false;
        }

        private void btImprimirLote_Click(object sender, EventArgs e)
        {
            if (cobResultado.ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Para imprimir um lote, deve ser selecionado um lote no resultado.", "Cadastro de Lote",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            
            Montarelatorio(((Lote)cobResultado.ComboBox.SelectedItem));

        }
        
        private void btAdd_Click(object sender, EventArgs e)
        {
            if (lbCheques.Items.Count < int.Parse(txtQunatidade.Text))
            {
                Salvar();
                
            }
            else
            {
                tbLote.Enabled = true;
                tbCheques.Enabled = false;
                tabLote.SelectedTab = tbLote;
                btEdit.Enabled = true;
            }
            
        }
        
        private void Salvar()
        {
            if (cobBancos.SelectedItem == null)
            {
                MessageBox.Show("Código de banco informado é inválido.", "Cód de banco.", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                cobBancos.Focus();
            }

            if (txtDocumento.Text.Trim().Equals(""))
            {
                MessageBox.Show("CPF ou CNPJ do emitente deve ser informado.", "Cadastro de lote de cheque",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtDocumento.Focus();
                return;
            }

            DateTime vencimento;
            try
            {
                vencimento = DateTime.Parse(dtVencimento.Text);
            }
            catch (Exception exception)
            {
                MessageBox.Show("Data de vencimento inválida.", "Cadastro de lote", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                dtVencimento.Focus();
                return;
            }

            CriaCheque(vencimento);

            try
            {
                BuscaCheque(_chequeAtual);
                ValidaCheque.ValidaCheques(_chequeAtual);
                lbCheques.Items.Add(_chequeAtual);
                MontaInformaLote();
                if (VerificaFimDeLote())
                {
                    DesativaBarraBusca();
                    Montarelatorio(_lote);
                    LimpaCampos();
                    return;
                }
                LimpaCampos();
                _chequeAtual = new Cheque();
                txtC1.Focus();

            }
            catch (Exception loteException)
            {
                MessageBox.Show(loteException.Message, "Cadastro de lote de cheque", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                lbCheques.Items.Remove(_chequeAtual);
            }
        }

        /// <summary>
        /// Verifica se o cheque em questão esta cadastrado já neste lote ou em outro lote
        /// </summary>
        /// <param name="cheque">Cheque a ser verificado</param>
        private void BuscaCheque(Cheque cheque)
        {
             foreach (Cheque item in this.lbCheques.Items)
            {
                if (item.ContaCheque.Banco == cheque.ContaCheque.Banco &&
                    item.ContaCheque.Agencia.Equals(cheque.ContaCheque.Agencia) &&
                    item.ContaCheque.ContaCorrente.Equals(cheque.ContaCheque.ContaCorrente) &&
                    item.NumeroCheque.Equals(cheque.NumeroCheque) &&
                    item.ContaCheque.Operacao.Equals(cheque.ContaCheque.Operacao))
                {
                    throw new Exception("Cheque já cadastrado neste lote!");
                }
            }

            if (_service.IsChequeCadastrado(cheque))
            {
                throw new Exception("Cheque já cadastrado em outro lote!");
            }

        }
        /// <summary>
        /// Cria um objeto do tipo cheque
        /// </summary>
        /// <param name="vencimento">Vencimento para o cheque que será criado</param>
        private void CriaCheque(DateTime vencimento)
        {
            

            if (_chequeAtual == null)
            {
                _chequeAtual = new Cheque();
            }

            _chequeAtual.NumeroCheque = txtNCh.Text;
            _chequeAtual.Valor = double.Parse(txtVcheque.Text);
            _chequeAtual.Vencimento = vencimento;
            _chequeAtual.Dias = _dias;
            _chequeAtual.JurosCheque = _juros;
            _chequeAtual.Status = StatusDocumento.DIGITADO;


            if (!_novoVencimento.Equals(vencimento))
            {
                _chequeAtual.NovoVencimento = _novoVencimento;
            }

            try
            {
                if (_conta == null)
                {
                    CriaConta();
                }
                _chequeAtual.ContaCheque = _conta;
                
            }
            catch (Exception loteException)
            {
                MessageBox.Show(loteException.Message, "Cadastro de lote de cheque", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                
            }
        }

        /// <summary>
        /// Método verifica informações para fechamento de lote
        /// </summary>
        private bool VerificaFimDeLote()
        {
            double vLote = double.Parse(txtValor.Text);
            if (int.Parse(txtQunatidade.Text) == lbCheques.Items.Count)
            {
                _lote.RemoveAllCheuqes();
                foreach (var chequesItem in lbCheques.Items)
                {
                    _lote.AddCheque((Cheque)chequesItem);
                }

                double vTCheques = Math.Round(_lote.TotalCheques,2);

                if ( vLote != vTCheques)
                {
                    throw new Exception("Valor total do lote está divergente com a soma dos cheques.\n\r" + 
                                        "Valor da diferença " + ((vLote > vTCheques)? "a menor de :" : "a maior de :") + 
                                        (String.Format("{0:C}", (vLote - vTCheques))));
                }

                _lote.ValorDescontado = Math.Round( _lote.ValorLote - _lote.TotalJuros,2);
                _service.Salvar(_lote);
                MessageBox.Show("Lote fechado!\n\r Total do lote:" + (String.Format("{0:C}", _lote.ValorLote)) +
                                "\n\r" +
                                "Valor total juros: " + (String.Format("{0:C}", _lote.TotalJuros)) + "\n\r" +
                                "Valor total com descontos: " + (String.Format("{0:C}", _lote.ValorDescontado)) +
                                "\n\r" +
                                "Quantidade de cheques: " + _lote.Cheques.Count, "Fechamento de lote", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;
            }

            return false;

        }
        /// <summary>
        /// Método cria um objeto Conta com os dados recolhidos do formulário
        /// </summary>
        private void CriaConta()
        {
          
            Proprietario prop = _service.FindProprietarioByDocumento(txtDocumento.Text);
                if (prop == null)
                {
                    prop = new Proprietario()
                    {
                        CpfCNPJ = txtDocumento.Text,
                        Nome = txtEmitente.Text
                    };
                }

                _conta = new Conta()
                {
                    Banco = (Banco)cobBancos.SelectedItem,
                    IdBanco = ((Banco)cobBancos.SelectedItem).Id,
                    Agencia = txtAgencia.Text,
                    ContaCorrente = txtCC.Text,
                    Proprietario = prop,
                    Operacao = txtOpercacao.Text
                };

                try
                {
                    ValidaConta.ValidarConta(_conta);
                    _service.SalvaConta(_conta);
                    _chequeAtual.ContaCheque = _conta;
                }
                catch (ContaException exception)
                {
                    throw new ContaException(exception.Message);
                }
            
        }
        /// <summary>
        /// Método limpa todos os campos da memória do formulário.
        /// </summary>
        private void LimpaCampos()
        {
            txtC1.Focus();
            labVInforma.Text = "";
            labInformeCh.Text = "";
            txtC1.Text = "";
            cobBancos.SelectedItem = null;
            txtAgencia.Text = "";
            txtOpercacao.Text = "";
            txtCC.Text = "";
            txtNCh.Text = "";
            txtDocumento.Text = "";
            txtEmitente.Text = "";
            dtVencimento.Text = "";
            labInformeCh.Text = "";
            _conta = null;
            _chequeAtual = null;
            _dias = 0;
            txtVcheque.Text = "0";
            btEdit.Enabled = true;
        }

        private void textBox10_Leave(object sender, EventArgs e)
        {
            txtCC.Text = Scheque.Uteis.Util.PreencheMascara(txtCC.Text, txtCC.Mask.Length, '0');
            ValidaECriaConta();
        }
        /// <summary>
        /// Método busca se já existe a conta na base, se não valida e cria uma nova conta
        /// Caso a conta já exista ele lança os dados do emitente no formulário.
        /// </summary>
        private void ValidaECriaConta()
        {
            if   (cobBancos.SelectedItem == null) return;
            
            String idBanco = ((Banco) cobBancos.SelectedItem).Id;
            _conta = _service.FindContaByDados(idBanco, txtAgencia.Text, txtCC.Text);
            if (_conta != null)
            {
                if (!txtDocumento.Text.Trim().Equals("") && txtDocumento.Text != _conta.Proprietario.CpfCNPJ)
                {
                    var resposta = MessageBox.Show(
                        "Os dados da conta diverge do que está cadastrado no sistema. \n Deseja manter os dados já cadastrado?",
                        "Cadastro de Lote de Cheques", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                    if (resposta == DialogResult.No)
                    {
                        return;
                    }
                }
                txtDocumento.Text = _conta.Proprietario.CpfCNPJ;
                txtEmitente.Text = _conta.Proprietario.Nome.ToUpper();
            }
        }

        private void txtC1_Leave(object sender, EventArgs e)
        {
            String cmc7 = ValidaCmc7.RemoveCaracteresEspeciais(txtC1.Text);
            if ((cmc7.Length == 30 && !ValidaCmc7.ValidaCmc(cmc7))||(cmc7.Length != 30 && cmc7.Trim().Length !=0))
            {
                MessageBox.Show("Codigo CMC7 inválido", "Cadastro de lote de cheques", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                txtC1.Text = "";
                txtC1.Focus();
                return;
            }else if (cmc7.Trim().Length != 0)
            {
                ExtrairDados(cmc7);
                txtVcheque.Focus();
                ValidaECriaConta();   
            }
            

        }
        /// <summary>
        /// Método extrai do CMC7 repassado os dados para preencher os campos no formulário
        /// </summary>
        /// <param name="cmc7">Código CMC7</param>
        private void ExtrairDados(string cmc7)
        {
            Banco banco = _service.FindBancoByCodBanco(ValidaCmc7.ExtraiCodBanco(cmc7));
            if (banco == null)
            {
                MessageBox.Show("Dados do cheque estão inválidos, tente refazer a leitura ou digite manualmente.",
                    "Cadastro de lote", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            cobBancos.SelectedItem = banco;
            txtCC.Text = ValidaCmc7.ExtraiNumContaCliente(cmc7);
            txtAgencia.Text = ValidaCmc7.ExtraiCodAgencia(cmc7);
            txtOpercacao.Text = ValidaCmc7.ExtraiCodOperadorConta(cmc7);
            txtNCh.Text = ValidaCmc7.ExtraiNumDocumento(cmc7);
        }
        

        private void dtVencimento_Leave(object sender, EventArgs e)
        {
            if (dtVencimento.Text.Length == 6)
            {
                dtVencimento.Text = dtVencimento.Text + DateTime.Now.Year;
            }
            double valor = double.Parse(txtVcheque.Text);
            DateTime vencimento;
            try
            {
                vencimento = DateTime.Parse(dtVencimento.Text);
                if (vencimento < txtData.Value)
                {
                    throw new Exception();
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show("Data de vencimento inválida", "Cadastro de lote", MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                dtVencimento.Focus();
                return;
            }
            if (valor != 0)
            {
                double taxa = double.Parse(txtJuros.Text);
                _dias = _service.ContaDias(txtData.Value, vencimento);
                int diasAdd = _dias - _service.ContaDias(txtData.Value, vencimento, false, false);
                String diaSemana = Scheque.Uteis.Util.DiaDaSemana(vencimento);
                String novoDia = Scheque.Uteis.Util.DiaDaSemana(vencimento.AddDays(diasAdd));
                _novoVencimento = (diasAdd > 0) ? vencimento.AddDays(diasAdd) : vencimento;
                bool feriado = (diasAdd > 2) ? true : false;
                _juros = _service.CalculaJuros(valor, taxa, _dias, ((Cliente) cobClientes.SelectedItem).Acumulado);

                labInformeCh.Text =
                    "Juros:" + "\r\n" +
                    "Valor descontado:" +"\r\n" +
                    "Número de dias :" + "\r\n" +
                    "Dia da semana do vencimento: " + "\r\n" +
                    ((!novoDia.Equals(diaSemana)) ? ("Novo dia do vencimento: ") + "\r\n" +
                                                    "Novo vencimento : " + "\r\n" : "") +
                    "Número de dias adicionados: " + "\r\n" +
                    "Foi adicionado dias de feriado: ";

                labVInforma.Text =
                    (String.Format("{0:C}", _juros)) + "\r\n" +
                    (String.Format("{0:C}", double.Parse(txtVcheque.Text) - _juros)) + "\r\n" +
                    _dias + "\r\n" +
                    diaSemana + "\r\n" +
                    ((!novoDia.Equals(diaSemana)) ? (novoDia) + "\r\n" +
                                                    _novoVencimento.ToShortDateString() + "\r\n" : "") +
                    diasAdd + "\r\n" +
                    ((feriado) ? "sim" : "não");

                if (!txtDocumento.Text.Trim().Equals("") && !txtEmitente.Text.Trim().Equals(""))
                {
                    btAdd.Focus();
                }else if (txtDocumento.Text.Trim().Equals("") && !txtEmitente.Text.Trim().Equals("")){
                    txtEmitente.Focus();
                }
            }
        }

        private void txtDocumento_Leave(object sender, EventArgs e)
        {
            if (txtDocumento.Text.Trim().Equals("") && cobBancos.SelectedItem == null)
            {
                return;
            }
            if (!ValidaDados.SoNumeros(txtDocumento.Text))
            {
                MessageBox.Show("Campo de documento só pode contér números", "Cadastro de lote", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                txtDocumento.Focus();
            }
            
            String texto = txtDocumento.Text.Replace(".", "").Replace("-", "").Replace("/", ""); 
            
            if (!(texto.Length == 14 || texto.Length == 11))
            {
                MessageBox.Show("O campo documento deve ter 11 ou 14 dígitos", "Cadastro de lote", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                txtDocumento.Focus();
            }

            if (texto.Length == 11 && !ValidaDados.ValidaCpf(texto))
            {
                MessageBox.Show("CPF do emitente não é válido. ", "Cadastro de lote", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                txtDocumento.Focus();
            }

            if (texto.Length == 14 && !ValidaDados.ValidaCNPJ(texto))
            {
                MessageBox.Show("CNPJ do emitente não é válido. ", "Cadastro de lote", MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                txtDocumento.Focus();
            }

            try
            {
                txtDocumento.Text = Scheque.Uteis.Util.AplicaMascara(texto);
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                txtDocumento.Focus();
            }

        }

        private void txtAgencia_Leave(object sender, EventArgs e)
        {
            txtAgencia.Text = Scheque.Uteis.Util.PreencheMascara(txtAgencia.Text, txtAgencia.Mask.Length, '0');
        }

        private void txtOpercacao_Leave(object sender, EventArgs e)
        {
            txtOpercacao.Text = Scheque.Uteis.Util.PreencheMascara(txtOpercacao.Text, txtOpercacao.Mask.Length, '0');
        }

        private void txtNCh_Leave(object sender, EventArgs e)
        {
            txtNCh.Text = Scheque.Uteis.Util.PreencheMascara(txtNCh.Text, txtNCh.Mask.Length, '0');
        }

     

        private void FormLoteCheques_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
               /* case Keys.Enter:
                    this.SelectNextControl(this.ActiveControl, !e.Shift, true, true, true);
                    break;*/
                case Keys.F12:
                    if (tbCheques.Enabled == true)
                    {
                        if (lbCheques.Items.Count < int.Parse(txtQunatidade.Text))
                        {
                            Salvar();
                            btEdit.Enabled = true;
                        }
                        else
                        {
                            tbLote.Enabled = true;
                            tbCheques.Enabled = false;
                            tabLote.SelectedTab = tbLote;
                            btEdit.Enabled = true;
                        }
                    }
                    break;
                case Keys.F8:
                    if (tbLote.Enabled == true)
                    {
                        ComecaLote();
                    }
                    break;
                case Keys.F5:
                    if (tbCheques.Enabled == true)
                    {
                        LimpaCampos();
                    }
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.lbCheques.Items.Count == 0)
            {
                MessageBox.Show("Não há cheques disgitados para serem localizados", "Cadastro de lote de cheques",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
                
            }
            Form busca = new FormBuscaChequeAssist(this.lbCheques, this.cobBancos, _service);
            busca.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tbLote.Enabled = true;
            tbCheques.Enabled = false;
            tabLote.SelectedTab = tbLote;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            LimpaCampos();
        }

        private void btEditar_Click(object sender, EventArgs e)
        {
            if (cobResultado.ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Não foi selecionando nenhum item no resultado da pesquisa.",
                    "Cadastro de lote de cheques.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            PreencherCampos((Lote) cobResultado.ComboBox.SelectedItem);
            cobClientes.Focus();
            barraInicial.Enabled = false;
            barraBusca.Enabled = false;

        }

        private void PreencherCampos(Lote lote)
        {
            labNLote.Text = String.Format("Lote Nº: {0}", lote.Id.ToString());
            LabelLote.Text = labNLote.Text;
            cobClientes.SelectedItem = lote.Cliente;
            labNome.Text = lote.Cliente.Nome;
            txtQunatidade.Text = lote.Quantidade.ToString();
            txtValor.Text = string.Format("{0:F2}", lote.ValorLote);
            txtJuros.Text = String.Format("{0:F2}", lote.Taxa);
            txtAdmin.Text = String.Format("{0:F2}", lote.Administrativa);
            labDesconto.Text = String.Format("{0:C2}", lote.ValorDescontado);
            labCET.Text = String.Format("{0:C2}", lote.TotalJuros);
            lbCheques.Items.Clear();
            foreach (var loteCheque in lote.Cheques)
            {
                lbCheques.Items.Add(loteCheque);
            }
            tabLote.Visible = true;
            tabSelect.Visible = true;
            tbLote.Enabled = true;
            tbCheques.Enabled = false;
            tabLote.SelectedTab = tbLote;
            _lote = lote;
            _chequeAtual = new Cheque();
            _conta = new Conta();
            _dias = 0;
            _juros = 0;
            _novoVencimento = new DateTime();
            btEdit.Enabled = true;
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            btCancela_Click(sender, e);
        }

        private void btEdit_Click(object sender, EventArgs e)
        {
            if (lbCheques.SelectedItem == null)
            {
                MessageBox.Show("Nenhum cheque foi selecionado para edição.", "Cadastro de lote", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                return;
            }

            _chequeAtual = (Cheque) lbCheques.SelectedItem;
            PreencherCamposCheque(_chequeAtual);
            lbCheques.Items.Remove(_chequeAtual);
            cobBancos.Focus();
            btEdit.Enabled = false;
            MontaInformaLote();
        }

        private void PreencherCamposCheque(Cheque chequeAtual)
        {
            cobBancos.SelectedItem = chequeAtual.ContaCheque.Banco;
            txtAgencia.Text = chequeAtual.ContaCheque.Agencia;
            txtOpercacao.Text = chequeAtual.ContaCheque.Operacao;
            txtCC.Text = chequeAtual.ContaCheque.ContaCorrente;
            txtNCh.Text = chequeAtual.NumeroCheque;
            txtVcheque.Text = String.Format("{0:F2}", chequeAtual.Valor);
            dtVencimento.Text = chequeAtual.Vencimento.ToShortDateString();
            txtDocumento.Text = chequeAtual.ContaCheque.Proprietario.CpfCNPJ;
            txtEmitente.Text = chequeAtual.ContaCheque.Proprietario.Nome;
            _conta = chequeAtual.ContaCheque;
            _dias = chequeAtual.Dias;
            
        }

        private void btImprimir_Click(object sender, EventArgs e)
        {
            if (_lote == null || _lote.Id == 0)
            {
                MessageBox.Show("Não e possível imprimir um lote que ainda não foi finalizado", "Cadastro de lote",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Montarelatorio(_lote);
        }

        private void Montarelatorio( Lote lote)
        {
            Form report = new FormReport("SchequesWin.Relatorios.RtpLote.rdlc", true, _service.RelatorioLote(lote.Id), null,
                true);
            report.Text = String.Format("Lote {0} de {1}.", lote.Id, lote.Cliente.Nome);
            report.MdiParent = this.MdiParent;
            report.Show();
            report.WindowState = FormWindowState.Normal;
            report.WindowState = FormWindowState.Maximized;
        }

        private void btFinaliza_Click(object sender, EventArgs e)
        {
            if (VerificaFimDeLote())
            {
                DesativaBarraBusca();
                Montarelatorio(_lote);
                LimpaCampos();
            }
        }

        private void DesativaBarraBusca()
        {
            tabLote.Visible = false;
            tabSelect.Visible = false;
            barraInicial.Enabled = true;
            barraBusca.Enabled = true;
            barraBusca.Visible = false;
            
        }

        private void btCancela_Click(object sender, EventArgs e)
        {
            var resposta = MessageBox.Show("Deseja cancelar o process do lote atual?", Text, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            if (resposta == DialogResult.Yes)
            {
                DesativaBarraBusca();
            }
        }
    }

    
    
}
