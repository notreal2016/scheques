﻿namespace SchequesWin.Forms
{
    partial class FormBuscaEmitente
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.barraInicial = new System.Windows.Forms.ToolStrip();
            this.btNovo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cobResultado = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.btBusca = new System.Windows.Forms.ToolStripButton();
            this.panelContext = new System.Windows.Forms.Panel();
            this.btFinaliza = new System.Windows.Forms.Button();
            this.btCancela = new System.Windows.Forms.Button();
            this.labId = new System.Windows.Forms.Label();
            this.txtDoc = new System.Windows.Forms.TextBox();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcone)).BeginInit();
            this.panelTop.SuspendLayout();
            this.panelBarra.SuspendLayout();
            this.panelContainer.SuspendLayout();
            this.barraInicial.SuspendLayout();
            this.panelContext.SuspendLayout();
            this.SuspendLayout();
            // 
            // picIcone
            // 
            this.picIcone.Image = global::SchequesWin.Properties.Resources.Cliente2;
            this.picIcone.Size = new System.Drawing.Size(42, 42);
            // 
            // panelBarra
            // 
            this.panelBarra.Controls.Add(this.barraInicial);
            // 
            // labTitulo
            // 
            this.labTitulo.Location = new System.Drawing.Point(20, 8);
            this.labTitulo.Size = new System.Drawing.Size(182, 22);
            this.labTitulo.Text = "Busca de Emitente";
            this.labTitulo.Click += new System.EventHandler(this.labTitulo_Click);
            // 
            // panelContainer
            // 
            this.panelContainer.Controls.Add(this.panelContext);
            this.panelContainer.Paint += new System.Windows.Forms.PaintEventHandler(this.panelContainer_Paint);
            // 
            // barraInicial
            // 
            this.barraInicial.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btNovo,
            this.toolStripSeparator1,
            this.toolStripLabel1,
            this.cobResultado,
            this.toolStripSeparator2,
            this.btBusca});
            this.barraInicial.Location = new System.Drawing.Point(0, 0);
            this.barraInicial.Name = "barraInicial";
            this.barraInicial.Size = new System.Drawing.Size(864, 25);
            this.barraInicial.TabIndex = 3;
            this.barraInicial.Text = "toolStrip1";
            // 
            // btNovo
            // 
            this.btNovo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btNovo.Image = global::SchequesWin.Properties.Resources.Novo_24;
            this.btNovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btNovo.Name = "btNovo";
            this.btNovo.Size = new System.Drawing.Size(23, 22);
            this.btNovo.Text = "Novo Proprietario";
            this.btNovo.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(176, 22);
            this.toolStripLabel1.Text = "Digite o documento para busca:";
            // 
            // cobResultado
            // 
            this.cobResultado.Name = "cobResultado";
            this.cobResultado.Size = new System.Drawing.Size(100, 25);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // btBusca
            // 
            this.btBusca.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btBusca.Image = global::SchequesWin.Properties.Resources.Lupa_30;
            this.btBusca.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btBusca.Name = "btBusca";
            this.btBusca.Size = new System.Drawing.Size(23, 22);
            this.btBusca.Text = "Buscar";
            this.btBusca.Click += new System.EventHandler(this.btBusca_Click_1);
            // 
            // panelContext
            // 
            this.panelContext.Controls.Add(this.btFinaliza);
            this.panelContext.Controls.Add(this.btCancela);
            this.panelContext.Controls.Add(this.labId);
            this.panelContext.Controls.Add(this.txtDoc);
            this.panelContext.Controls.Add(this.txtNome);
            this.panelContext.Controls.Add(this.label4);
            this.panelContext.Controls.Add(this.label2);
            this.panelContext.Controls.Add(this.label1);
            this.panelContext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelContext.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelContext.Location = new System.Drawing.Point(0, 0);
            this.panelContext.Margin = new System.Windows.Forms.Padding(4);
            this.panelContext.Name = "panelContext";
            this.panelContext.Size = new System.Drawing.Size(864, 356);
            this.panelContext.TabIndex = 5;
            this.panelContext.Visible = false;
            // 
            // btFinaliza
            // 
            this.btFinaliza.Image = global::SchequesWin.Properties.Resources.Lixo_24;
            this.btFinaliza.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btFinaliza.Location = new System.Drawing.Point(551, 174);
            this.btFinaliza.Name = "btFinaliza";
            this.btFinaliza.Size = new System.Drawing.Size(46, 37);
            this.btFinaliza.TabIndex = 39;
            this.btFinaliza.TabStop = false;
            this.btFinaliza.UseVisualStyleBackColor = true;
            this.btFinaliza.Click += new System.EventHandler(this.btFinaliza_Click);
            // 
            // btCancela
            // 
            this.btCancela.Image = global::SchequesWin.Properties.Resources.Excluir_24;
            this.btCancela.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btCancela.Location = new System.Drawing.Point(17, 174);
            this.btCancela.Name = "btCancela";
            this.btCancela.Size = new System.Drawing.Size(46, 37);
            this.btCancela.TabIndex = 38;
            this.btCancela.TabStop = false;
            this.btCancela.UseVisualStyleBackColor = true;
            this.btCancela.Click += new System.EventHandler(this.btCancela_Click);
            // 
            // labId
            // 
            this.labId.AutoSize = true;
            this.labId.Location = new System.Drawing.Point(76, 38);
            this.labId.Name = "labId";
            this.labId.Size = new System.Drawing.Size(0, 20);
            this.labId.TabIndex = 8;
            // 
            // txtDoc
            // 
            this.txtDoc.Location = new System.Drawing.Point(94, 99);
            this.txtDoc.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtDoc.Name = "txtDoc";
            this.txtDoc.Size = new System.Drawing.Size(225, 26);
            this.txtDoc.TabIndex = 6;
            this.txtDoc.TextChanged += new System.EventHandler(this.txtLogin_TextChanged_1);
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(94, 66);
            this.txtNome.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(503, 26);
            this.txtNome.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Id:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cpf/Cnpj:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome:";
            // 
            // FormBuscaEmitente
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 451);
            this.Name = "FormBuscaEmitente";
            this.Text = "FormBuscaEmitente";
            this.panelLeft.ResumeLayout(false);
            this.panelLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcone)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.panelBarra.ResumeLayout(false);
            this.panelBarra.PerformLayout();
            this.panelContainer.ResumeLayout(false);
            this.barraInicial.ResumeLayout(false);
            this.barraInicial.PerformLayout();
            this.panelContext.ResumeLayout(false);
            this.panelContext.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolStrip barraInicial;
        private System.Windows.Forms.ToolStripButton btBusca;
        private System.Windows.Forms.ToolStripButton btNovo;
        private System.Windows.Forms.Panel panelContext;
        private System.Windows.Forms.Button btFinaliza;
        private System.Windows.Forms.Button btCancela;
        private System.Windows.Forms.Label labId;
        private System.Windows.Forms.TextBox txtDoc;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripTextBox cobResultado;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}