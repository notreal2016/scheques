﻿using MaterialSkin.Controls;

namespace SchequesWin.Forms
{
    partial class FormLoteCheques
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLoteCheques));
            this.barraBusca = new System.Windows.Forms.ToolStrip();
            this.btExcluir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.cobResultado = new System.Windows.Forms.ToolStripComboBox();
            this.btEditar = new System.Windows.Forms.ToolStripButton();
            this.btImprimirLote = new System.Windows.Forms.ToolStripButton();
            this.btDesfazer = new System.Windows.Forms.ToolStripButton();
            this.barraInicial = new System.Windows.Forms.ToolStrip();
            this.btNovo = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.labTermo = new System.Windows.Forms.ToolStripLabel();
            this.txtTermo = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cobBusca = new System.Windows.Forms.ToolStripComboBox();
            this.btBusca = new System.Windows.Forms.ToolStripButton();
            this.panelAbas = new System.Windows.Forms.Panel();
            this.tabSelect = new MaterialSkin.Controls.MaterialTabSelector();
            this.tabLote = new MaterialSkin.Controls.MaterialTabControl();
            this.tbLote = new System.Windows.Forms.TabPage();
            this.PanelLote = new System.Windows.Forms.Panel();
            this.btImprimir = new System.Windows.Forms.Button();
            this.btCancelar = new System.Windows.Forms.Button();
            this.txtQunatidade = new NumericBox.NumericBox();
            this.txtAdmin = new NumericBox.NumericBox();
            this.txtJuros = new NumericBox.NumericBox();
            this.txtValor = new NumericBox.NumericBox();
            this.btFiltrar = new System.Windows.Forms.Button();
            this.btCheques = new System.Windows.Forms.Button();
            this.labCET = new System.Windows.Forms.Label();
            this.labDesconto = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtData = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.BtAdicionarCliente = new System.Windows.Forms.Button();
            this.cobClientes = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.LabelLote = new System.Windows.Forms.Label();
            this.tbCheques = new System.Windows.Forms.TabPage();
            this.panelCheque = new System.Windows.Forms.Panel();
            this.btLimpa = new System.Windows.Forms.Button();
            this.btBuscaCh = new System.Windows.Forms.Button();
            this.dtVencimento = new System.Windows.Forms.MaskedTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labVEmitente = new System.Windows.Forms.Label();
            this.labEmitenteInfo = new System.Windows.Forms.Label();
            this.txtVcheque = new NumericBox.NumericBox();
            this.grupoComple = new System.Windows.Forms.GroupBox();
            this.labVInforma = new System.Windows.Forms.Label();
            this.labInformeCh = new System.Windows.Forms.Label();
            this.GrupoLote = new System.Windows.Forms.GroupBox();
            this.btCapa = new System.Windows.Forms.Button();
            this.labInforma = new System.Windows.Forms.Label();
            this.btFinaliza = new System.Windows.Forms.Button();
            this.btCancela = new System.Windows.Forms.Button();
            this.btExcluiCh = new System.Windows.Forms.Button();
            this.btEdit = new System.Windows.Forms.Button();
            this.lbCheques = new System.Windows.Forms.ListBox();
            this.btAdd = new System.Windows.Forms.Button();
            this.txtEmitente = new System.Windows.Forms.TextBox();
            this.txtDocumento = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtNCh = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCC = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtOpercacao = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cobBancos = new System.Windows.Forms.ComboBox();
            this.labNome = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtAgencia = new System.Windows.Forms.MaskedTextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtC1 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtNomeCliente = new System.Windows.Forms.Label();
            this.labNLote = new System.Windows.Forms.Label();
            this.panelTabs = new System.Windows.Forms.Panel();
            this.panelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcone)).BeginInit();
            this.panelTop.SuspendLayout();
            this.panelBarra.SuspendLayout();
            this.panelContainer.SuspendLayout();
            this.barraBusca.SuspendLayout();
            this.barraInicial.SuspendLayout();
            this.panelAbas.SuspendLayout();
            this.tabLote.SuspendLayout();
            this.tbLote.SuspendLayout();
            this.PanelLote.SuspendLayout();
            this.tbCheques.SuspendLayout();
            this.panelCheque.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.grupoComple.SuspendLayout();
            this.GrupoLote.SuspendLayout();
            this.panelTabs.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelLeft
            // 
            this.panelLeft.Size = new System.Drawing.Size(75, 634);
            // 
            // picIcone
            // 
            this.picIcone.Image = null;
            // 
            // panelTop
            // 
            this.panelTop.Size = new System.Drawing.Size(1034, 95);
            // 
            // panelBarra
            // 
            this.panelBarra.Controls.Add(this.barraBusca);
            this.panelBarra.Controls.Add(this.barraInicial);
            this.panelBarra.Size = new System.Drawing.Size(1034, 62);
            // 
            // panelContainer
            // 
            this.panelContainer.Controls.Add(this.panelTabs);
            this.panelContainer.Controls.Add(this.panelAbas);
            this.panelContainer.Size = new System.Drawing.Size(1034, 539);
            // 
            // barraBusca
            // 
            this.barraBusca.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.barraBusca.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btExcluir,
            this.toolStripSeparator3,
            this.toolStripLabel2,
            this.cobResultado,
            this.btEditar,
            this.btImprimirLote,
            this.btDesfazer});
            this.barraBusca.Location = new System.Drawing.Point(0, 31);
            this.barraBusca.Name = "barraBusca";
            this.barraBusca.Size = new System.Drawing.Size(1034, 31);
            this.barraBusca.TabIndex = 6;
            this.barraBusca.Text = "toolStrip2";
            // 
            // btExcluir
            // 
            this.btExcluir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btExcluir.Image")));
            this.btExcluir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btExcluir.Name = "btExcluir";
            this.btExcluir.Size = new System.Drawing.Size(28, 28);
            this.btExcluir.Text = "Exclui o lote";
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(59, 28);
            this.toolStripLabel2.Text = "Resultado";
            // 
            // cobResultado
            // 
            this.cobResultado.Name = "cobResultado";
            this.cobResultado.Size = new System.Drawing.Size(400, 31);
            // 
            // btEditar
            // 
            this.btEditar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btEditar.Image = ((System.Drawing.Image)(resources.GetObject("btEditar.Image")));
            this.btEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btEditar.Name = "btEditar";
            this.btEditar.Size = new System.Drawing.Size(28, 28);
            this.btEditar.Text = "Edita o lote";
            this.btEditar.Click += new System.EventHandler(this.btEditar_Click);
            // 
            // btImprimirLote
            // 
            this.btImprimirLote.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btImprimirLote.Image = ((System.Drawing.Image)(resources.GetObject("btImprimirLote.Image")));
            this.btImprimirLote.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btImprimirLote.Name = "btImprimirLote";
            this.btImprimirLote.Size = new System.Drawing.Size(28, 28);
            this.btImprimirLote.Text = "Imprimir o lote";
            this.btImprimirLote.Click += new System.EventHandler(this.btImprimirLote_Click);
            // 
            // btDesfazer
            // 
            this.btDesfazer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btDesfazer.Image = ((System.Drawing.Image)(resources.GetObject("btDesfazer.Image")));
            this.btDesfazer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btDesfazer.Name = "btDesfazer";
            this.btDesfazer.Size = new System.Drawing.Size(28, 28);
            this.btDesfazer.Text = "Desfaz o filtro";
            this.btDesfazer.Click += new System.EventHandler(this.btDesfazer_Click);
            // 
            // barraInicial
            // 
            this.barraInicial.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.barraInicial.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btNovo,
            this.toolStripSeparator1,
            this.labTermo,
            this.txtTermo,
            this.toolStripLabel1,
            this.cobBusca,
            this.btBusca});
            this.barraInicial.Location = new System.Drawing.Point(0, 0);
            this.barraInicial.Name = "barraInicial";
            this.barraInicial.Size = new System.Drawing.Size(1034, 31);
            this.barraInicial.TabIndex = 5;
            this.barraInicial.Text = "toolStrip1";
            // 
            // btNovo
            // 
            this.btNovo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btNovo.Image = ((System.Drawing.Image)(resources.GetObject("btNovo.Image")));
            this.btNovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btNovo.Name = "btNovo";
            this.btNovo.Size = new System.Drawing.Size(28, 28);
            this.btNovo.Text = "Novo lote";
            this.btNovo.Click += new System.EventHandler(this.btNovo_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // labTermo
            // 
            this.labTermo.Name = "labTermo";
            this.labTermo.Size = new System.Drawing.Size(91, 28);
            this.labTermo.Text = "Termo de busca";
            // 
            // txtTermo
            // 
            this.txtTermo.Name = "txtTermo";
            this.txtTermo.Size = new System.Drawing.Size(200, 31);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(84, 28);
            this.toolStripLabel1.Text = "Tipo de busca:";
            // 
            // cobBusca
            // 
            this.cobBusca.Name = "cobBusca";
            this.cobBusca.Size = new System.Drawing.Size(121, 31);
            // 
            // btBusca
            // 
            this.btBusca.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btBusca.Image = ((System.Drawing.Image)(resources.GetObject("btBusca.Image")));
            this.btBusca.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btBusca.Name = "btBusca";
            this.btBusca.Size = new System.Drawing.Size(28, 28);
            this.btBusca.Text = "Buscar";
            this.btBusca.Click += new System.EventHandler(this.btBusca_Click);
            // 
            // panelAbas
            // 
            this.panelAbas.Controls.Add(this.tabSelect);
            this.panelAbas.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAbas.Location = new System.Drawing.Point(0, 0);
            this.panelAbas.Name = "panelAbas";
            this.panelAbas.Size = new System.Drawing.Size(1034, 27);
            this.panelAbas.TabIndex = 7;
            // 
            // tabSelect
            // 
            this.tabSelect.BaseTabControl = this.tabLote;
            this.tabSelect.Depth = 0;
            this.tabSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabSelect.Location = new System.Drawing.Point(0, 0);
            this.tabSelect.MouseState = MaterialSkin.MouseState.HOVER;
            this.tabSelect.Name = "tabSelect";
            this.tabSelect.Size = new System.Drawing.Size(1034, 27);
            this.tabSelect.TabIndex = 0;
            this.tabSelect.Text = "materialTabSelector1";
            // 
            // tabLote
            // 
            this.tabLote.Controls.Add(this.tbLote);
            this.tabLote.Controls.Add(this.tbCheques);
            this.tabLote.Depth = 0;
            this.tabLote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabLote.Location = new System.Drawing.Point(0, 0);
            this.tabLote.MouseState = MaterialSkin.MouseState.HOVER;
            this.tabLote.Name = "tabLote";
            this.tabLote.SelectedIndex = 0;
            this.tabLote.Size = new System.Drawing.Size(1034, 512);
            this.tabLote.TabIndex = 7;
            // 
            // tbLote
            // 
            this.tbLote.Controls.Add(this.PanelLote);
            this.tbLote.Location = new System.Drawing.Point(4, 30);
            this.tbLote.Name = "tbLote";
            this.tbLote.Padding = new System.Windows.Forms.Padding(3);
            this.tbLote.Size = new System.Drawing.Size(1026, 478);
            this.tbLote.TabIndex = 0;
            this.tbLote.Text = "Lote";
            this.tbLote.UseVisualStyleBackColor = true;
            // 
            // PanelLote
            // 
            this.PanelLote.BackColor = System.Drawing.SystemColors.Control;
            this.PanelLote.Controls.Add(this.btImprimir);
            this.PanelLote.Controls.Add(this.btCancelar);
            this.PanelLote.Controls.Add(this.txtQunatidade);
            this.PanelLote.Controls.Add(this.txtAdmin);
            this.PanelLote.Controls.Add(this.txtJuros);
            this.PanelLote.Controls.Add(this.txtValor);
            this.PanelLote.Controls.Add(this.btFiltrar);
            this.PanelLote.Controls.Add(this.btCheques);
            this.PanelLote.Controls.Add(this.labCET);
            this.PanelLote.Controls.Add(this.labDesconto);
            this.PanelLote.Controls.Add(this.label8);
            this.PanelLote.Controls.Add(this.label7);
            this.PanelLote.Controls.Add(this.txtData);
            this.PanelLote.Controls.Add(this.label5);
            this.PanelLote.Controls.Add(this.label6);
            this.PanelLote.Controls.Add(this.label4);
            this.PanelLote.Controls.Add(this.label3);
            this.PanelLote.Controls.Add(this.label2);
            this.PanelLote.Controls.Add(this.BtAdicionarCliente);
            this.PanelLote.Controls.Add(this.cobClientes);
            this.PanelLote.Controls.Add(this.label1);
            this.PanelLote.Controls.Add(this.LabelLote);
            this.PanelLote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelLote.Font = new System.Drawing.Font("Arial Unicode MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PanelLote.Location = new System.Drawing.Point(3, 3);
            this.PanelLote.Name = "PanelLote";
            this.PanelLote.Size = new System.Drawing.Size(1020, 472);
            this.PanelLote.TabIndex = 5;
            // 
            // btImprimir
            // 
            this.btImprimir.Image = global::SchequesWin.Properties.Resources.imprimir24;
            this.btImprimir.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btImprimir.Location = new System.Drawing.Point(673, 244);
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Size = new System.Drawing.Size(46, 37);
            this.btImprimir.TabIndex = 38;
            this.btImprimir.TabStop = false;
            this.btImprimir.UseVisualStyleBackColor = true;
            this.btImprimir.Click += new System.EventHandler(this.btImprimir_Click);
            // 
            // btCancelar
            // 
            this.btCancelar.Image = global::SchequesWin.Properties.Resources.Excluir_24;
            this.btCancelar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btCancelar.Location = new System.Drawing.Point(16, 244);
            this.btCancelar.Name = "btCancelar";
            this.btCancelar.Size = new System.Drawing.Size(46, 37);
            this.btCancelar.TabIndex = 37;
            this.btCancelar.TabStop = false;
            this.btCancelar.UseVisualStyleBackColor = true;
            this.btCancelar.Click += new System.EventHandler(this.btCancelar_Click);
            // 
            // txtQunatidade
            // 
            this.txtQunatidade.BackColor = System.Drawing.SystemColors.Window;
            this.txtQunatidade.BoxStyle = Util.Style.Normal;
            this.txtQunatidade.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtQunatidade.Location = new System.Drawing.Point(114, 70);
            this.txtQunatidade.Name = "txtQunatidade";
            this.txtQunatidade.NegativeNumberColor = System.Drawing.Color.Red;
            this.txtQunatidade.NoDigitsAfterDecimalSymbol = 0;
            this.txtQunatidade.Size = new System.Drawing.Size(87, 29);
            this.txtQunatidade.TabIndex = 4;
            this.txtQunatidade.Text = "0";
            // 
            // txtAdmin
            // 
            this.txtAdmin.BackColor = System.Drawing.SystemColors.Window;
            this.txtAdmin.BoxStyle = Util.Style.Normal;
            this.txtAdmin.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAdmin.Location = new System.Drawing.Point(332, 113);
            this.txtAdmin.Name = "txtAdmin";
            this.txtAdmin.NegativeNumberColor = System.Drawing.Color.Red;
            this.txtAdmin.Size = new System.Drawing.Size(65, 29);
            this.txtAdmin.TabIndex = 7;
            this.txtAdmin.Text = "0,00";
            // 
            // txtJuros
            // 
            this.txtJuros.BackColor = System.Drawing.SystemColors.Window;
            this.txtJuros.BoxStyle = Util.Style.Normal;
            this.txtJuros.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtJuros.Location = new System.Drawing.Point(116, 110);
            this.txtJuros.Name = "txtJuros";
            this.txtJuros.NegativeNumberColor = System.Drawing.Color.Red;
            this.txtJuros.Size = new System.Drawing.Size(68, 29);
            this.txtJuros.TabIndex = 6;
            this.txtJuros.Text = "0,00";
            // 
            // txtValor
            // 
            this.txtValor.BackColor = System.Drawing.SystemColors.Window;
            this.txtValor.BoxStyle = Util.Style.Normal;
            this.txtValor.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtValor.Location = new System.Drawing.Point(320, 71);
            this.txtValor.Name = "txtValor";
            this.txtValor.NegativeNumberColor = System.Drawing.Color.Red;
            this.txtValor.Size = new System.Drawing.Size(209, 29);
            this.txtValor.TabIndex = 5;
            this.txtValor.Text = "0,00";
            // 
            // btFiltrar
            // 
            this.btFiltrar.Image = ((System.Drawing.Image)(resources.GetObject("btFiltrar.Image")));
            this.btFiltrar.Location = new System.Drawing.Point(579, 36);
            this.btFiltrar.Name = "btFiltrar";
            this.btFiltrar.Size = new System.Drawing.Size(37, 28);
            this.btFiltrar.TabIndex = 3;
            this.btFiltrar.TabStop = false;
            this.btFiltrar.UseVisualStyleBackColor = true;
            this.btFiltrar.Click += new System.EventHandler(this.btFiltrar_Click);
            // 
            // btCheques
            // 
            this.btCheques.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCheques.Image = ((System.Drawing.Image)(resources.GetObject("btCheques.Image")));
            this.btCheques.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCheques.Location = new System.Drawing.Point(497, 171);
            this.btCheques.Name = "btCheques";
            this.btCheques.Size = new System.Drawing.Size(75, 32);
            this.btCheques.TabIndex = 9;
            this.btCheques.Text = "F8";
            this.btCheques.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btCheques.UseVisualStyleBackColor = true;
            this.btCheques.Click += new System.EventHandler(this.btCheques_Click);
            // 
            // labCET
            // 
            this.labCET.AutoSize = true;
            this.labCET.Location = new System.Drawing.Point(210, 183);
            this.labCET.Name = "labCET";
            this.labCET.Size = new System.Drawing.Size(35, 21);
            this.labCET.TabIndex = 17;
            this.labCET.Text = "R$:";
            // 
            // labDesconto
            // 
            this.labDesconto.AutoSize = true;
            this.labDesconto.Location = new System.Drawing.Point(210, 152);
            this.labDesconto.Name = "labDesconto";
            this.labDesconto.Size = new System.Drawing.Size(35, 21);
            this.labDesconto.TabIndex = 16;
            this.labDesconto.Text = "R$:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 183);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(194, 21);
            this.label8.TabIndex = 15;
            this.label8.Text = "CET (Custo Efetivo Total):";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 152);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(154, 21);
            this.label7.TabIndex = 14;
            this.label7.Text = "Valor com desconto:";
            // 
            // txtData
            // 
            this.txtData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtData.Location = new System.Drawing.Point(453, 113);
            this.txtData.Name = "txtData";
            this.txtData.Size = new System.Drawing.Size(120, 29);
            this.txtData.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(398, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 21);
            this.label5.TabIndex = 12;
            this.label5.Text = "Data:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(185, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(152, 21);
            this.label6.TabIndex = 10;
            this.label6.Text = "Taxa Administrativa:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 114);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 21);
            this.label4.TabIndex = 8;
            this.label4.Text = "Taxa de juros:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(216, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 21);
            this.label3.TabIndex = 6;
            this.label3.Text = "Valor do lote:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 21);
            this.label2.TabIndex = 4;
            this.label2.Text = "Quantidade:";
            // 
            // BtAdicionarCliente
            // 
            this.BtAdicionarCliente.Image = global::SchequesWin.Properties.Resources.cliente_24;
            this.BtAdicionarCliente.Location = new System.Drawing.Point(536, 36);
            this.BtAdicionarCliente.Name = "BtAdicionarCliente";
            this.BtAdicionarCliente.Size = new System.Drawing.Size(37, 28);
            this.BtAdicionarCliente.TabIndex = 2;
            this.BtAdicionarCliente.TabStop = false;
            this.BtAdicionarCliente.UseVisualStyleBackColor = true;
            this.BtAdicionarCliente.Click += new System.EventHandler(this.BtAdicionarCliente_Click);
            // 
            // cobClientes
            // 
            this.cobClientes.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.cobClientes.FormattingEnabled = true;
            this.cobClientes.Location = new System.Drawing.Point(80, 36);
            this.cobClientes.Name = "cobClientes";
            this.cobClientes.Size = new System.Drawing.Size(450, 29);
            this.cobClientes.TabIndex = 1;
            this.cobClientes.Leave += new System.EventHandler(this.cobClientes_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cliente:";
            // 
            // LabelLote
            // 
            this.LabelLote.AutoSize = true;
            this.LabelLote.Location = new System.Drawing.Point(12, 8);
            this.LabelLote.Name = "LabelLote";
            this.LabelLote.Size = new System.Drawing.Size(71, 21);
            this.LabelLote.TabIndex = 0;
            this.LabelLote.Text = "Nº Lote: ";
            // 
            // tbCheques
            // 
            this.tbCheques.Controls.Add(this.panelCheque);
            this.tbCheques.Location = new System.Drawing.Point(4, 30);
            this.tbCheques.Name = "tbCheques";
            this.tbCheques.Padding = new System.Windows.Forms.Padding(3);
            this.tbCheques.Size = new System.Drawing.Size(1026, 478);
            this.tbCheques.TabIndex = 1;
            this.tbCheques.Text = "Cheques";
            this.tbCheques.UseVisualStyleBackColor = true;
            // 
            // panelCheque
            // 
            this.panelCheque.BackColor = System.Drawing.SystemColors.Control;
            this.panelCheque.Controls.Add(this.btLimpa);
            this.panelCheque.Controls.Add(this.btBuscaCh);
            this.panelCheque.Controls.Add(this.dtVencimento);
            this.panelCheque.Controls.Add(this.groupBox1);
            this.panelCheque.Controls.Add(this.txtVcheque);
            this.panelCheque.Controls.Add(this.grupoComple);
            this.panelCheque.Controls.Add(this.GrupoLote);
            this.panelCheque.Controls.Add(this.btFinaliza);
            this.panelCheque.Controls.Add(this.btCancela);
            this.panelCheque.Controls.Add(this.btExcluiCh);
            this.panelCheque.Controls.Add(this.btEdit);
            this.panelCheque.Controls.Add(this.lbCheques);
            this.panelCheque.Controls.Add(this.btAdd);
            this.panelCheque.Controls.Add(this.txtEmitente);
            this.panelCheque.Controls.Add(this.txtDocumento);
            this.panelCheque.Controls.Add(this.label20);
            this.panelCheque.Controls.Add(this.label12);
            this.panelCheque.Controls.Add(this.txtNCh);
            this.panelCheque.Controls.Add(this.label11);
            this.panelCheque.Controls.Add(this.txtCC);
            this.panelCheque.Controls.Add(this.label10);
            this.panelCheque.Controls.Add(this.txtOpercacao);
            this.panelCheque.Controls.Add(this.label9);
            this.panelCheque.Controls.Add(this.cobBancos);
            this.panelCheque.Controls.Add(this.labNome);
            this.panelCheque.Controls.Add(this.label13);
            this.panelCheque.Controls.Add(this.txtAgencia);
            this.panelCheque.Controls.Add(this.label14);
            this.panelCheque.Controls.Add(this.label15);
            this.panelCheque.Controls.Add(this.label16);
            this.panelCheque.Controls.Add(this.txtC1);
            this.panelCheque.Controls.Add(this.label17);
            this.panelCheque.Controls.Add(this.txtNomeCliente);
            this.panelCheque.Controls.Add(this.labNLote);
            this.panelCheque.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCheque.Font = new System.Drawing.Font("Arial Unicode MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelCheque.Location = new System.Drawing.Point(3, 3);
            this.panelCheque.Name = "panelCheque";
            this.panelCheque.Size = new System.Drawing.Size(1020, 472);
            this.panelCheque.TabIndex = 6;
            // 
            // btLimpa
            // 
            this.btLimpa.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btLimpa.Image = global::SchequesWin.Properties.Resources.vassoura_24;
            this.btLimpa.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btLimpa.Location = new System.Drawing.Point(518, 190);
            this.btLimpa.Name = "btLimpa";
            this.btLimpa.Size = new System.Drawing.Size(46, 47);
            this.btLimpa.TabIndex = 42;
            this.btLimpa.Text = "F5";
            this.btLimpa.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btLimpa.UseVisualStyleBackColor = true;
            this.btLimpa.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btBuscaCh
            // 
            this.btBuscaCh.Image = global::SchequesWin.Properties.Resources.Lupa_30;
            this.btBuscaCh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btBuscaCh.Location = new System.Drawing.Point(149, 464);
            this.btBuscaCh.Name = "btBuscaCh";
            this.btBuscaCh.Size = new System.Drawing.Size(46, 37);
            this.btBuscaCh.TabIndex = 41;
            this.btBuscaCh.TabStop = false;
            this.btBuscaCh.UseVisualStyleBackColor = true;
            this.btBuscaCh.Click += new System.EventHandler(this.button1_Click);
            // 
            // dtVencimento
            // 
            this.dtVencimento.Location = new System.Drawing.Point(322, 138);
            this.dtVencimento.Mask = "00/00/0000";
            this.dtVencimento.Name = "dtVencimento";
            this.dtVencimento.Size = new System.Drawing.Size(99, 29);
            this.dtVencimento.TabIndex = 19;
            this.dtVencimento.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.dtVencimento.ValidatingType = typeof(System.DateTime);
            this.dtVencimento.Leave += new System.EventHandler(this.dtVencimento_Leave);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.labVEmitente);
            this.groupBox1.Controls.Add(this.labEmitenteInfo);
            this.groupBox1.Location = new System.Drawing.Point(624, 217);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(379, 173);
            this.groupBox1.TabIndex = 40;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Informações do emitente";
            // 
            // labVEmitente
            // 
            this.labVEmitente.AutoSize = true;
            this.labVEmitente.Location = new System.Drawing.Point(255, 23);
            this.labVEmitente.Name = "labVEmitente";
            this.labVEmitente.Size = new System.Drawing.Size(14, 21);
            this.labVEmitente.TabIndex = 3;
            this.labVEmitente.Text = " ";
            this.labVEmitente.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labEmitenteInfo
            // 
            this.labEmitenteInfo.AutoSize = true;
            this.labEmitenteInfo.Location = new System.Drawing.Point(7, 23);
            this.labEmitenteInfo.Name = "labEmitenteInfo";
            this.labEmitenteInfo.Size = new System.Drawing.Size(18, 21);
            this.labEmitenteInfo.TabIndex = 2;
            this.labEmitenteInfo.Text = "  ";
            // 
            // txtVcheque
            // 
            this.txtVcheque.BackColor = System.Drawing.SystemColors.Window;
            this.txtVcheque.BoxStyle = Util.Style.Normal;
            this.txtVcheque.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtVcheque.Location = new System.Drawing.Point(67, 141);
            this.txtVcheque.Name = "txtVcheque";
            this.txtVcheque.NegativeNumberColor = System.Drawing.Color.Red;
            this.txtVcheque.Size = new System.Drawing.Size(143, 29);
            this.txtVcheque.TabIndex = 18;
            this.txtVcheque.Text = "0,00";
            // 
            // grupoComple
            // 
            this.grupoComple.Controls.Add(this.labVInforma);
            this.grupoComple.Controls.Add(this.labInformeCh);
            this.grupoComple.Location = new System.Drawing.Point(622, 15);
            this.grupoComple.Name = "grupoComple";
            this.grupoComple.Size = new System.Drawing.Size(379, 196);
            this.grupoComple.TabIndex = 39;
            this.grupoComple.TabStop = false;
            this.grupoComple.Text = "Informações do cheque";
            // 
            // labVInforma
            // 
            this.labVInforma.AutoSize = true;
            this.labVInforma.Location = new System.Drawing.Point(255, 23);
            this.labVInforma.Name = "labVInforma";
            this.labVInforma.Size = new System.Drawing.Size(14, 21);
            this.labVInforma.TabIndex = 3;
            this.labVInforma.Text = " ";
            this.labVInforma.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // labInformeCh
            // 
            this.labInformeCh.AutoSize = true;
            this.labInformeCh.Location = new System.Drawing.Point(7, 23);
            this.labInformeCh.Name = "labInformeCh";
            this.labInformeCh.Size = new System.Drawing.Size(86, 21);
            this.labInformeCh.TabIndex = 2;
            this.labInformeCh.Text = "Valor Lote:";
            // 
            // GrupoLote
            // 
            this.GrupoLote.Controls.Add(this.btCapa);
            this.GrupoLote.Controls.Add(this.labInforma);
            this.GrupoLote.Location = new System.Drawing.Point(622, 396);
            this.GrupoLote.Name = "GrupoLote";
            this.GrupoLote.Size = new System.Drawing.Size(379, 105);
            this.GrupoLote.TabIndex = 38;
            this.GrupoLote.TabStop = false;
            this.GrupoLote.Text = "Informação do lote";
            // 
            // btCapa
            // 
            this.btCapa.Image = global::SchequesWin.Properties.Resources.desfaz;
            this.btCapa.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btCapa.Location = new System.Drawing.Point(327, 62);
            this.btCapa.Name = "btCapa";
            this.btCapa.Size = new System.Drawing.Size(46, 37);
            this.btCapa.TabIndex = 43;
            this.btCapa.TabStop = false;
            this.btCapa.UseVisualStyleBackColor = true;
            this.btCapa.Click += new System.EventHandler(this.button2_Click);
            // 
            // labInforma
            // 
            this.labInforma.AutoSize = true;
            this.labInforma.Location = new System.Drawing.Point(6, 22);
            this.labInforma.Name = "labInforma";
            this.labInforma.Size = new System.Drawing.Size(86, 21);
            this.labInforma.TabIndex = 1;
            this.labInforma.Text = "Valor Lote:";
            // 
            // btFinaliza
            // 
            this.btFinaliza.Image = global::SchequesWin.Properties.Resources.Lixo_24;
            this.btFinaliza.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btFinaliza.Location = new System.Drawing.Point(570, 464);
            this.btFinaliza.Name = "btFinaliza";
            this.btFinaliza.Size = new System.Drawing.Size(46, 37);
            this.btFinaliza.TabIndex = 37;
            this.btFinaliza.TabStop = false;
            this.btFinaliza.UseVisualStyleBackColor = true;
            this.btFinaliza.Click += new System.EventHandler(this.btFinaliza_Click);
            // 
            // btCancela
            // 
            this.btCancela.Image = global::SchequesWin.Properties.Resources.Excluir_24;
            this.btCancela.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btCancela.Location = new System.Drawing.Point(15, 464);
            this.btCancela.Name = "btCancela";
            this.btCancela.Size = new System.Drawing.Size(46, 37);
            this.btCancela.TabIndex = 36;
            this.btCancela.TabStop = false;
            this.btCancela.UseVisualStyleBackColor = true;
            this.btCancela.Click += new System.EventHandler(this.btCancela_Click);
            // 
            // btExcluiCh
            // 
            this.btExcluiCh.Image = global::SchequesWin.Properties.Resources.Lixo;
            this.btExcluiCh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btExcluiCh.Location = new System.Drawing.Point(431, 464);
            this.btExcluiCh.Name = "btExcluiCh";
            this.btExcluiCh.Size = new System.Drawing.Size(46, 37);
            this.btExcluiCh.TabIndex = 35;
            this.btExcluiCh.TabStop = false;
            this.btExcluiCh.UseVisualStyleBackColor = true;
            // 
            // btEdit
            // 
            this.btEdit.Image = global::SchequesWin.Properties.Resources.Editar_24;
            this.btEdit.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btEdit.Location = new System.Drawing.Point(287, 464);
            this.btEdit.Name = "btEdit";
            this.btEdit.Size = new System.Drawing.Size(46, 37);
            this.btEdit.TabIndex = 34;
            this.btEdit.TabStop = false;
            this.btEdit.UseVisualStyleBackColor = true;
            this.btEdit.Click += new System.EventHandler(this.btEdit_Click);
            // 
            // lbCheques
            // 
            this.lbCheques.FormattingEnabled = true;
            this.lbCheques.ItemHeight = 21;
            this.lbCheques.Location = new System.Drawing.Point(15, 247);
            this.lbCheques.Name = "lbCheques";
            this.lbCheques.Size = new System.Drawing.Size(601, 193);
            this.lbCheques.TabIndex = 33;
            this.lbCheques.TabStop = false;
            // 
            // btAdd
            // 
            this.btAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAdd.Image = ((System.Drawing.Image)(resources.GetObject("btAdd.Image")));
            this.btAdd.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btAdd.Location = new System.Drawing.Point(570, 190);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(46, 47);
            this.btAdd.TabIndex = 22;
            this.btAdd.Text = "F12";
            this.btAdd.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // txtEmitente
            // 
            this.txtEmitente.Location = new System.Drawing.Point(103, 205);
            this.txtEmitente.Name = "txtEmitente";
            this.txtEmitente.Size = new System.Drawing.Size(401, 29);
            this.txtEmitente.TabIndex = 21;
            // 
            // txtDocumento
            // 
            this.txtDocumento.Location = new System.Drawing.Point(103, 173);
            this.txtDocumento.Name = "txtDocumento";
            this.txtDocumento.Size = new System.Drawing.Size(191, 29);
            this.txtDocumento.TabIndex = 20;
            this.txtDocumento.Leave += new System.EventHandler(this.txtDocumento_Leave);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(11, 208);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(76, 21);
            this.label20.TabIndex = 29;
            this.label20.Text = "Emitente:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(9, 176);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(94, 21);
            this.label12.TabIndex = 28;
            this.label12.Text = "CPF/CNPJ:";
            // 
            // txtNCh
            // 
            this.txtNCh.Location = new System.Drawing.Point(518, 106);
            this.txtNCh.Mask = "000000";
            this.txtNCh.Name = "txtNCh";
            this.txtNCh.Size = new System.Drawing.Size(89, 29);
            this.txtNCh.TabIndex = 17;
            this.txtNCh.Leave += new System.EventHandler(this.txtNCh_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(427, 106);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(93, 21);
            this.label11.TabIndex = 25;
            this.label11.Text = "Nº Cheque:";
            // 
            // txtCC
            // 
            this.txtCC.Location = new System.Drawing.Point(323, 106);
            this.txtCC.Mask = "000000";
            this.txtCC.Name = "txtCC";
            this.txtCC.Size = new System.Drawing.Size(98, 29);
            this.txtCC.TabIndex = 16;
            this.txtCC.Leave += new System.EventHandler(this.textBox10_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(283, 109);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 21);
            this.label10.TabIndex = 23;
            this.label10.Text = "C/C";
            // 
            // txtOpercacao
            // 
            this.txtOpercacao.Location = new System.Drawing.Point(222, 105);
            this.txtOpercacao.Mask = "000";
            this.txtOpercacao.Name = "txtOpercacao";
            this.txtOpercacao.Size = new System.Drawing.Size(59, 29);
            this.txtOpercacao.TabIndex = 15;
            this.txtOpercacao.Leave += new System.EventHandler(this.txtOpercacao_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(142, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 21);
            this.label9.TabIndex = 21;
            this.label9.Text = "Operação:";
            // 
            // cobBancos
            // 
            this.cobBancos.FormattingEnabled = true;
            this.cobBancos.Location = new System.Drawing.Point(67, 70);
            this.cobBancos.Name = "cobBancos";
            this.cobBancos.Size = new System.Drawing.Size(540, 29);
            this.cobBancos.TabIndex = 13;
            this.cobBancos.Leave += new System.EventHandler(this.cobBancos_Leave);
            // 
            // labNome
            // 
            this.labNome.AutoSize = true;
            this.labNome.Location = new System.Drawing.Point(218, 8);
            this.labNome.Name = "labNome";
            this.labNome.Size = new System.Drawing.Size(0, 21);
            this.labNome.TabIndex = 19;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(218, 141);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(98, 21);
            this.label13.TabIndex = 12;
            this.label13.Text = "Vencimento:";
            // 
            // txtAgencia
            // 
            this.txtAgencia.Location = new System.Drawing.Point(80, 106);
            this.txtAgencia.Mask = "0000";
            this.txtAgencia.Name = "txtAgencia";
            this.txtAgencia.Size = new System.Drawing.Size(59, 29);
            this.txtAgencia.TabIndex = 14;
            this.txtAgencia.Leave += new System.EventHandler(this.txtAgencia_Leave);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(12, 109);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(72, 21);
            this.label14.TabIndex = 10;
            this.label14.Text = "Agência:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(12, 73);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 21);
            this.label15.TabIndex = 8;
            this.label15.Text = "Banco:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 141);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(51, 21);
            this.label16.TabIndex = 6;
            this.label16.Text = "Valor:";
            // 
            // txtC1
            // 
            this.txtC1.Location = new System.Drawing.Point(126, 35);
            this.txtC1.Name = "txtC1";
            this.txtC1.Size = new System.Drawing.Size(481, 29);
            this.txtC1.TabIndex = 10;
            this.txtC1.Leave += new System.EventHandler(this.txtC1_Leave);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(12, 36);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(103, 21);
            this.label17.TabIndex = 4;
            this.label17.Text = "Barra CMC7:";
            // 
            // txtNomeCliente
            // 
            this.txtNomeCliente.AutoSize = true;
            this.txtNomeCliente.Location = new System.Drawing.Point(156, 8);
            this.txtNomeCliente.Name = "txtNomeCliente";
            this.txtNomeCliente.Size = new System.Drawing.Size(63, 21);
            this.txtNomeCliente.TabIndex = 1;
            this.txtNomeCliente.Text = "Cliente:";
            // 
            // labNLote
            // 
            this.labNLote.AutoSize = true;
            this.labNLote.Location = new System.Drawing.Point(12, 8);
            this.labNLote.Name = "labNLote";
            this.labNLote.Size = new System.Drawing.Size(71, 21);
            this.labNLote.TabIndex = 0;
            this.labNLote.Text = "Nº Lote: ";
            // 
            // panelTabs
            // 
            this.panelTabs.Controls.Add(this.tabLote);
            this.panelTabs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTabs.Location = new System.Drawing.Point(0, 27);
            this.panelTabs.Name = "panelTabs";
            this.panelTabs.Size = new System.Drawing.Size(1034, 512);
            this.panelTabs.TabIndex = 8;
            // 
            // FormLoteCheques
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1109, 634);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormLoteCheques";
            this.Text = "Lotes de cheques";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormLoteCheques_KeyDown);
            this.Resize += new System.EventHandler(this.FormLoteCheques_Resize);
            this.panelLeft.ResumeLayout(false);
            this.panelLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcone)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.panelBarra.ResumeLayout(false);
            this.panelBarra.PerformLayout();
            this.panelContainer.ResumeLayout(false);
            this.barraBusca.ResumeLayout(false);
            this.barraBusca.PerformLayout();
            this.barraInicial.ResumeLayout(false);
            this.barraInicial.PerformLayout();
            this.panelAbas.ResumeLayout(false);
            this.tabLote.ResumeLayout(false);
            this.tbLote.ResumeLayout(false);
            this.PanelLote.ResumeLayout(false);
            this.PanelLote.PerformLayout();
            this.tbCheques.ResumeLayout(false);
            this.panelCheque.ResumeLayout(false);
            this.panelCheque.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grupoComple.ResumeLayout(false);
            this.grupoComple.PerformLayout();
            this.GrupoLote.ResumeLayout(false);
            this.GrupoLote.PerformLayout();
            this.panelTabs.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip barraBusca;
        private System.Windows.Forms.ToolStripButton btExcluir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox cobResultado;
        private System.Windows.Forms.ToolStripButton btEditar;
        private System.Windows.Forms.ToolStripButton btImprimirLote;
        private System.Windows.Forms.ToolStripButton btDesfazer;
        private System.Windows.Forms.ToolStrip barraInicial;
        private System.Windows.Forms.ToolStripButton btNovo;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel labTermo;
        private System.Windows.Forms.ToolStripTextBox txtTermo;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox cobBusca;
        private System.Windows.Forms.ToolStripButton btBusca;
        private System.Windows.Forms.Panel panelTabs;
        private MaterialTabControl tabLote;
        private System.Windows.Forms.TabPage tbLote;
        private System.Windows.Forms.Panel PanelLote;
        private System.Windows.Forms.Button btImprimir;
        private System.Windows.Forms.Button btCancelar;
        private NumericBox.NumericBox txtQunatidade;
        private NumericBox.NumericBox txtAdmin;
        private NumericBox.NumericBox txtJuros;
        private NumericBox.NumericBox txtValor;
        private System.Windows.Forms.Button btFiltrar;
        private System.Windows.Forms.Button btCheques;
        private System.Windows.Forms.Label labCET;
        private System.Windows.Forms.Label labDesconto;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker txtData;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button BtAdicionarCliente;
        private System.Windows.Forms.ComboBox cobClientes;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LabelLote;
        private System.Windows.Forms.TabPage tbCheques;
        private System.Windows.Forms.Panel panelCheque;
        private System.Windows.Forms.Button btLimpa;
        private System.Windows.Forms.Button btBuscaCh;
        private System.Windows.Forms.MaskedTextBox dtVencimento;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labVEmitente;
        private System.Windows.Forms.Label labEmitenteInfo;
        private NumericBox.NumericBox txtVcheque;
        private System.Windows.Forms.GroupBox grupoComple;
        private System.Windows.Forms.Label labVInforma;
        private System.Windows.Forms.Label labInformeCh;
        private System.Windows.Forms.GroupBox GrupoLote;
        private System.Windows.Forms.Button btCapa;
        private System.Windows.Forms.Label labInforma;
        private System.Windows.Forms.Button btFinaliza;
        private System.Windows.Forms.Button btCancela;
        private System.Windows.Forms.Button btExcluiCh;
        private System.Windows.Forms.Button btEdit;
        private System.Windows.Forms.ListBox lbCheques;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.TextBox txtEmitente;
        private System.Windows.Forms.MaskedTextBox txtDocumento;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox txtNCh;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.MaskedTextBox txtCC;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.MaskedTextBox txtOpercacao;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cobBancos;
        private System.Windows.Forms.Label labNome;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox txtAgencia;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtC1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label txtNomeCliente;
        private System.Windows.Forms.Label labNLote;
        private System.Windows.Forms.Panel panelAbas;
        private MaterialTabSelector tabSelect;
    }
}