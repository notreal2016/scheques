﻿namespace SchequesWin.Forms
{
    partial class FormReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormReport));
            this.rpview = new Microsoft.Reporting.WinForms.ReportViewer();
            this.panelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcone)).BeginInit();
            this.panelTop.SuspendLayout();
            this.panelContainer.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelLeft
            // 
            this.panelLeft.Size = new System.Drawing.Size(75, 290);
            // 
            // picIcone
            // 
            this.picIcone.Image = ((System.Drawing.Image)(resources.GetObject("picIcone.Image")));
            // 
            // panelTop
            // 
            this.panelTop.Size = new System.Drawing.Size(541, 95);
            // 
            // panelBarra
            // 
            this.panelBarra.Size = new System.Drawing.Size(541, 62);
            // 
            // labTitulo
            // 
            this.labTitulo.Size = new System.Drawing.Size(194, 22);
            this.labTitulo.Text = "Cadastro de usuário";
            // 
            // panelContainer
            // 
            this.panelContainer.Controls.Add(this.rpview);
            this.panelContainer.Size = new System.Drawing.Size(541, 195);
            // 
            // rpview
            // 
            this.rpview.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpview.Location = new System.Drawing.Point(0, 0);
            this.rpview.Name = "rpview";
            this.rpview.Size = new System.Drawing.Size(541, 195);
            this.rpview.TabIndex = 1;
            this.rpview.ZoomMode = Microsoft.Reporting.WinForms.ZoomMode.PageWidth;
            // 
            // FormReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 290);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormReport";
            this.Text = "Relatorio";
            this.MaximumSizeChanged += new System.EventHandler(this.FormReport_MaximumSizeChanged);
            this.Load += new System.EventHandler(this.FormReport_Load);
            this.panelLeft.ResumeLayout(false);
            this.panelLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcone)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.panelContainer.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpview;
    }
}