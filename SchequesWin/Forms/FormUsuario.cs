﻿
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Scheque.Context;
using Scheque.Entidade;
using Scheque.Service;
namespace SchequesWin.Forms
{

    public partial class FormUsuario: FormModelo
    {
        private UsuarioService _service;
        private Usuario _user;
        public FormUsuario(MDIPrincipal principal)
        {
            this.MdiParent = principal;
            Inicializa();
        }

        

        private void Inicializa()
        {
            InitializeComponent();
            _service = new UsuarioService(((MDIPrincipal)MdiParent).Context);
            cobBusca.Items.Add(TipoBusca.NOME);
        }

        private void btNovo_Click(object sender, System.EventArgs e)
        {
            this.panelContext.Visible = true;
            txtNome.Focus();
            LimpaCampos();
            barraBusca.Enabled = false;
            barraInicial.Enabled = false;
            AtivaCampos(true);
            chbInativo.Enabled = true;
        }

        private void btFinaliza_Click(object sender, System.EventArgs e)
        {
            CriaUsuario();
            try
            {
                _service.Save(_user);
                MessageBox.Show("Usuário cadastrado com sucesso", "Cadastro de usuário", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                this.panelContext.Visible = false;
                LimpaCampos();
                barraBusca.Enabled = true;
                barraInicial.Enabled = true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, "Cadastro de usuário", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtNome.Focus();
            }
        }

        private void LimpaCampos()
        {
            labId.Text = "";
            txtNome.Text = "";
            txtLogin.Text = "";
            txtSenha.Text = "";
            chbInativo.Checked = false;
            _user = new Usuario();
        }

        private void CriaUsuario()
        {
            if (_user == null)
            {
                _user = new Usuario();
            }
            _user.Nome = txtNome.Text;
            _user.Login = txtLogin.Text;
            _user.Senha = txtSenha.Text;
            _user.Ativo = chbInativo.Checked;

    }

        private void btCancela_Click(object sender, EventArgs e)
        {
            this.panelContext.Visible = false;
            barraBusca.Enabled = true;
            barraInicial.Enabled = true;
        }

        private void btBusca_Click(object sender, EventArgs e)
        {
            if (txtTermo.Text.Trim().Equals(""))
            {
                MessageBox.Show("Termo para busca não foi informado.", "Cadastro do usuário", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                txtTermo.Focus();
            }

            try
            {
                List<Usuario> nomes = _service.FindByNome(txtTermo.Text);
                cobResultado.ComboBox.DataSource = null;
                cobResultado.ComboBox.DataSource = nomes;
                cobResultado.ComboBox.SelectedItem = null;
                barraBusca.Visible = true;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                txtTermo.Focus();
            }
            
        }

        private void btDesfazer_Click(object sender, EventArgs e)
        {
            cobResultado.ComboBox.DataSource = null;
            cobResultado.ComboBox.SelectedItem = null;
            barraBusca.Visible = false;
        }

        private void btEditar_Click(object sender, EventArgs e)
        {
            if (cobResultado.ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Deve ser selecionado um usuário para poder edita-lo");
                cobResultado.Focus();
                return;
            }

            _user = (Usuario) cobResultado.ComboBox.SelectedItem;
            //Verificando se é o proprio usuário
            AtivaCampos(_user.Id == ((MDIPrincipal)this.ParentForm).User.Id);
            
            PreencheCampos(_user);
            this.panelContext.Visible = true;
            barraBusca.Enabled = false;
            barraInicial.Enabled = false;
        }

        private void AtivaCampos(bool b)
        {
            txtNome.Enabled = b;
            txtLogin.Enabled = b;
            txtSenha.Enabled = b;
            chbInativo.Enabled = !b;
        }

        private void PreencheCampos(Usuario _user)
        {
            txtNome.Text = _user.Nome;
            txtLogin.Text = _user.Login;
            txtSenha.Text = _user.Senha;
            labId.Text = _user.Id.ToString();
            chbInativo.Checked = _user.Ativo;
        }

        private void btExcluir_Click(object sender, EventArgs e)
        {
            if (cobResultado.ComboBox.SelectedItem == null)
            {
                MessageBox.Show("Para excluir um usuário, deve seleciona-lo primeiro.", Text,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            try
            {
                Usuario user = (Usuario) cobResultado.SelectedItem;
                PreencheCampos(user);
                panelContext.Visible = true;
                panelContext.Enabled = false;
                var resposta =
                    MessageBox.Show(string.Format("Deseja realmente excluir o usuário {0} do sistema?", user.Nome),
                        Text, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resposta == DialogResult.Yes)
                {
                    _service.Remove(user.Id);
                    LimpaCampos();
                    MessageBox.Show("Usuário excluido com sucesso!", Text, MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                panelContext.Enabled = true;
                panelContext.Visible = false;

            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }

}
