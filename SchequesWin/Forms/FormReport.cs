﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Reporting.WinForms;

namespace SchequesWin.Forms
{
    public partial class FormReport : FormModelo
    {
        public FormReport(string path, bool isEmbeddedResource, Dictionary<string, object> dataSources, Dictionary<string, object> reportParameters = null, bool paisagem = false)
        {

            InitializeComponent();
            // path + isEmbeddedResource.
            if (isEmbeddedResource)
                this.rpview.LocalReport.ReportEmbeddedResource = path;
            else
                this.rpview.LocalReport.ReportPath = path;

           
            
            // dataSources.
            foreach (var dataSource in dataSources)
            {
                var reportDataSource = new Microsoft.Reporting.WinForms.ReportDataSource(dataSource.Key, dataSource.Value);
                this.rpview.LocalReport.DataSources.Add(reportDataSource);
            }

            // reportParameters.
            if (reportParameters != null)
            {
                var reportParameterCollection = new List<Microsoft.Reporting.WinForms.ReportParameter>();

                foreach (var parameter in reportParameters)
                {
                    var reportParameter = new Microsoft.Reporting.WinForms.ReportParameter(parameter.Key, parameter.Value.ToString());
                    reportParameterCollection.Add(reportParameter);
                }

                this.rpview.LocalReport.SetParameters(reportParameterCollection);
            }

            if (paisagem) { FormaPaisagem(); }
            else
            {
                rpview.SetPageSettings(new PageSettings()
                {
                    Margins = new Margins()
                    {
                        Left = 3,
                        Bottom = 20,
                        Top = 1,
                        Right = 1
                    }
            });

            }
            rpview.SetDisplayMode(DisplayMode.PrintLayout);
           
        }

        private void FormReport_Load(object sender, EventArgs e)
        {
            
            this.rpview.RefreshReport();
            this.rpview.RefreshReport();

        }

        private void FormaPaisagem()
        {
            System.Drawing.Printing.PageSettings ps = new System.Drawing.Printing.PageSettings();
            ps.Landscape = true;
            ps.PaperSize = new System.Drawing.Printing.PaperSize("A4", 827, 1170);
            ps.PaperSize.RawKind = (int) System.Drawing.Printing.PaperKind.A4;
            ps.Margins = new Margins()
            {
                Left = 3,
                Bottom = 20,
                Top = 1,
                Right = 1
            };
            rpview.SetPageSettings(ps);
            rpview.Refresh();
        }

        public static void ShowReport(string path, bool isEmbeddedResource, Dictionary<string, object> dataSources, Dictionary<string, object> reportParameters = null)
        {
            var formRelatorio = new FormReport(path, isEmbeddedResource, dataSources, reportParameters);
            formRelatorio.Show();
        }

        private void FormReport_MaximumSizeChanged(object sender, EventArgs e)
        {
            rpview.Height = this.Height;
            rpview.Width = this.Width;
        }

        public FormReport()
        {
            InitializeComponent();
        }
    }
}
