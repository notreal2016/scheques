﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using ADVCore.Service;
using Microsoft.VisualBasic;
using Scheque;
using Scheque.Entidade;
using Scheque.Service;

namespace SchequesWin.Forms
{
    public partial class FormEmprestimo : FormModelo
    {
        private Emprestimo _emprestimo;
        private EmprestimoService _service;
        private Cheque _cheque;
        private Conta _conta;
        private int _numeroDoc = 0;
        private List<AbstracDocumento> _documentos;
        public FormEmprestimo()
        {
            Inicializa();
        }

        public FormEmprestimo(MDIPrincipal mdiForm) : base(mdiForm)
        {
            Inicializa();
            this.MdiParent = mdiForm;
            _service = new EmprestimoService(mdiForm.Context);
            cobClientes.DataSource = _service.AllClientes();
            cobClientes.SelectedItem = null;
            tabParcelas.Enabled = false;
            tabEmp.Enabled = true;
            MontaComboBancos();
            MontaComboBusca();

        }

        private void MontaComboBusca()
        {
            cobBusca.Items.Clear();
            cobBusca.Items.Add(TipoBusca.NOME);
            cobBusca.Items.Add(TipoBusca.DATA);
            cobBusca.Items.Add(TipoBusca.LOTE);

        }

        /// <summary>
        /// Monta comboBancos com os dados dos bancos cadastrados no banco de dados.
        /// </summary>
        private void MontaComboBancos()
        {
            cobBanco.DataSource = _service.FindAllBancos();
            cobBanco.SelectedItem = null;
        }

        private void LimpaCampos()
        {
            LimpaDadosCheque();
            txtTermo.Text = "";
            txtAdm.Text = "0";
            txtTxJuros.Text = "0";
            txtValor.Text = "0";
            cobClientes.SelectedItem = null;
            lbDocs.DataSource = null;
            txtNumero.Text = "0";
            tabEmprestimo.SelectedTab = tabEmp;
            _numeroDoc = 0;
        }

        private void LimpaDadosCheque()
        {
            txtAgencia.Text = "";
            txtCheque.Text = "";
            txtCmc7.Text = "";
            txtConta.Text = "";
            txtOperacao.Text = "";
            cobBanco.SelectedItem = null;
        }

        private void Inicializa()
        {
            InitializeComponent();
            picIcone.Image = Icon.ToBitmap();
            labTitulo.Text = Text;
            ExibeTabs(false);

        }

        private void ExibeTabs(bool b)
        {
            tabSelect.Visible = b;
            tabEmprestimo.Visible = b;
            barraBusca.Enabled = !b;
            barraInicial.Enabled = !b;
        }

        private void btCancelar_Click(object sender, EventArgs e)
        {
            var resposta = MessageBox.Show("Deseja cancelar o processo em andamento?", Text, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            if (resposta == DialogResult.Yes)
            {
                ExibeTabs(false);
                tabEmp.Enabled = true;
                tabParcelas.Enabled = false;
                _emprestimo = null;
            }
        }

        private void mMComCh_Click(object sender, EventArgs e)
        {
            ExibeTabs(true);
            tabEmprestimo.SelectedTab = tabEmp;
            rbCh.Checked = true;
            _emprestimo = new Emprestimo();
            _emprestimo.Tipo = TipoEmprestimo.CHEQUES;
            LimpaCampos();
        }

        private void mMComPro_Click(object sender, EventArgs e)
        {
            ExibeTabs(true);
            rbPro.Checked = true;
            _emprestimo = new Emprestimo();
            _emprestimo.Tipo = TipoEmprestimo.PROMISSORIA;
            LimpaCampos();
            

        }

    
        private void mMAntecipa_Click(object sender, EventArgs e)
        {
            ExibeTabs(true);
            rbAnt.Checked = true;
            _emprestimo = new Emprestimo();
            _emprestimo.Tipo = TipoEmprestimo.UNICA;
            LimpaCampos();
            txtNumero.Text = "1";
            txtNumero.Enabled = false;
        }

        private void btCheques_Click(object sender, EventArgs e)
        {
            if (cobClientes.SelectedItem == null)
            {
                MessageBox.Show("É preciso selecionar um cliente para proceguir com o processo de emprestimo.", Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                cobClientes.Focus();
                return;
            }

            String doc = ((Cliente) cobClientes.SelectedItem).Cpfcnpj;

            if (doc == null || doc.Trim().Equals(""))
            {
                var resposta =  MessageBox.Show(
                    "Para a criação de um emprestimo o cliente deve ter no seu cadastro o preenchimento do CPF o CNPJ. Deseja ir ao cadastro do cliente para atualizar os dados?",
                    Text, MessageBoxButtons.YesNo);
                if (resposta == DialogResult.Yes)
                {
                    BtAdicionarCliente_Click(sender, e);
                }
                return;
            }

            if (ValidaDadosEmprestimo()) return;

            CriaEmprestimo();
           
            if (rbPro.Checked && lbDocs.Items.Count ==0)
            {
                MontaPromissorias();
            }

            if (rbCh.Checked && lbDocs.Items.Count == 0)
            {
                MontaCheques();
            }

            if (rbAnt.Checked && lbDocs.Items.Count == 0)
            {
                MontaPromissoriaUnica();
            }

            tabParcelas.Enabled = true;
            tabEmp.Enabled = false;
            tabEmprestimo.SelectedTab = tabParcelas;
        }

        private void MontaPromissoriaUnica()
        {
            List<AbstracDocumento> promissorias = new List<AbstracDocumento>();
            promissorias.Add( new Promissoria()
            {
                Valor = double.Parse( txtValor.Text),
                
            });
            _emprestimo.Documentos = promissorias;
            lbDocs.DataSource = promissorias;
            gbCheque.Enabled = false;
        }

        private void MontaCheques()
        {

            List<AbstracDocumento> cheques = _service.CriaCheque(_emprestimo, rbSAC.Checked, chkFixo.Checked);
            _emprestimo.Documentos = cheques;
            lbDocs.DataSource = cheques;
            gbCheque.Enabled = true;
            txtCmc7.Focus();
        }

        private void CriaEmprestimo()
        {
            _emprestimo.Cliente = (Cliente) cobClientes.SelectedItem;
            _emprestimo.Adiantamento = rbAnt.Checked;
            _emprestimo.DataContrato = DtData.Value;
            _emprestimo.Fixo = chkFixo.Checked;
            _emprestimo.NumeroParcelas = int.Parse(txtNumero.Text);
            _emprestimo.Sac = rbSAC.Checked;
            _emprestimo.TaxaAdministrativa = double.Parse(txtAdm.Text);
            _emprestimo.TaxaJuros = double.Parse(txtTxJuros.Text);
            _emprestimo.TaxaAdministrativa = double.Parse(txtAdm.Text);
            _emprestimo.Valor = double.Parse(txtValor.Text);
            if (lbDocs.DataSource != null)
            {
                _emprestimo.Documentos = (List<AbstracDocumento>)lbDocs.DataSource;
            }

            if (rbAnt.Checked)
            {
                _emprestimo.Tipo = TipoEmprestimo.UNICA;
            }
            if (rbCh.Checked)
            {
                _emprestimo.Tipo = TipoEmprestimo.CHEQUES;
            }
            if (rbPro.Checked)
            {
                _emprestimo.Tipo = TipoEmprestimo.PROMISSORIA;
            }

        }

        private void PreencheCampos()
        {
            switch (_emprestimo.Tipo)
            {
                case TipoEmprestimo.CHEQUES:
                    rbCh.Checked = true;
                    break;
                case TipoEmprestimo.PROMISSORIA:
                    rbPro.Checked = true;
                    break;
                case TipoEmprestimo.UNICA:
                    rbAnt.Checked = true;
                    break;
            }

            cobClientes.SelectedItem = _emprestimo.Cliente;
            DtData.Value = _emprestimo.DataContrato;
            chkFixo.Checked = _emprestimo.Fixo;
            txtNumero.Text = _emprestimo.NumeroParcelas.ToString("N");
            rbSAC.Checked = _emprestimo.Sac;
            rbPrice.Checked = !_emprestimo.Sac;
            txtAdm.Text = _emprestimo.TaxaAdministrativa.ToString("F");
            txtTxJuros.Text = _emprestimo.TaxaJuros.ToString("F");
            txtAdm.Text = _emprestimo.TaxaAdministrativa.ToString("F");
            lbDocs.DataSource = _emprestimo.Documentos;
            txtValor.Text = _emprestimo.Valor.ToString("F");
        }

        private bool ValidaDadosEmprestimo()
        {
            if (cobClientes.SelectedItem == null)
            {
                MessageBox.Show("Não foi selecionado um cliente", Text, MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                cobClientes.Focus();
                return true;
            }

            if (txtValor.Text.Trim().Equals("") || double.Parse(txtValor.Text) <= 0)
            {
                MessageBox.Show("Valor do emprestimo deve ser positivo e maior que zero.", Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                txtValor.Focus();
                return true;
            }

            if (txtNumero.Text.Trim().Equals("") || int.Parse(txtNumero.Text) <= 0)
            {
                MessageBox.Show("O número de parcelas para o emprestimo deve ser informado.", Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                txtNumero.Focus();
                return true;
            }

            if (txtTxJuros.Text.Trim().Equals("") || double.Parse(txtTxJuros.Text) < 0)
            {
                MessageBox.Show("A taxa de juros do emprestimo não pode ser negativa.", Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                txtTxJuros.Focus();
                return true;
            }

            if (txtAdm.Text.Trim().Equals("") || double.Parse(txtAdm.Text) < 0)
            {
                MessageBox.Show("A taxa administrativa não pode conter um valor negatívo", Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Asterisk);
                txtAdm.Focus();
                return true;
            }

            return false;
        }

        private void MontaPromissorias()
        {
            List<AbstracDocumento> promissorias = _service.CriaPromissorias(_emprestimo, rbSAC.Checked, chkFixo.Checked);
            _emprestimo.Documentos = promissorias;
            lbDocs.DataSource = promissorias;
            gbCheque.Enabled = false;
        }

        private void BtAdicionarCliente_Click(object sender, EventArgs e)
        {
            FormCliente childForm = new FormCliente((MDIPrincipal) this.MdiParent , cobClientes);
            childForm.Show();
            childForm.WindowState = FormWindowState.Normal;
            childForm.WindowState = FormWindowState.Maximized;

        }

        private void btFiltrar_Click(object sender, EventArgs e)
        {
            var termo = Interaction.InputBox("Informe o termo que deseja fazer a filtragem dos dados do cliente:",
                "Filtro de cliente", "%", 100, 200);
            cobClientes.DataSource = _service.FindClienteByNome(termo);
            if (cobClientes.Items.Count == 0)
            {
                var resposta = MessageBox.Show("O resultado do filtro foi de zero registros, deseja aplicar um novo filtro?",
                    "Filtro de clientes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (resposta == DialogResult.Yes)
                {
                    btFiltrar_Click(sender, e);
                }
                else
                {
                    this.cobClientes.DataSource = _service.AllClientes();
                    this.cobClientes.SelectedItem = null;
                }
            }
        }

        private void btImprimir_Click(object sender, EventArgs e)
        {
            if (_emprestimo.Id == 0)
            {
                MessageBox.Show(
                    "Não é possível imprimir o processo de imprestimo enquanto o cadastro não for finalizado.", Text,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
        }

        private void btConfirma_Click(object sender, EventArgs e)
        {
            if (rbPro.Checked)
            {
                MontaPromissorias();
            }

            try
            {
                
                _service.Save(_emprestimo);
                MessageBox.Show("Emprestimo cadastrado com sucesso!", Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                tabParcelas.Enabled = false;
                tabEmp.Enabled = true;
                tabEmprestimo.SelectedTab = tabEmp;
                barraInicial.Enabled = true;
                barraBusca.Enabled = true;
                txtNumero.Enabled = true;
                _emprestimo = null;
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btVoltar_Click(object sender, EventArgs e)
        {
            tabParcelas.Enabled = false;
            tabEmp.Enabled = true;
            tabEmprestimo.SelectedTab = tabEmp;

        }

        private void txtCmc7_Leave(object sender, EventArgs e)
        {
            if (txtCmc7.Text.Trim().Equals(""))
            {
                return;
            }
            String cmc7 = ValidaCmc7.RemoveCaracteresEspeciais(txtCmc7.Text);
            
            if (cmc7.Length == 30 && !ValidaCmc7.ValidaCmc(cmc7))
            {
                MessageBox.Show("Codigo CMC7 inválido",Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                txtCmc7.Text = "";
                txtCmc7.Focus();
                return;
            }
            else if (cmc7.Length != 30 && cmc7.Length != 0)
            {
                MessageBox.Show("Erro de leitura, CMC7 está inválido pela leitura ou digitação", Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
                txtCmc7.Text = "";
                txtCmc7.Focus();
            }
                ExtrairDados(cmc7);
                criaConta();
          
        }
        /// <summary>
        /// Método extra os dados do codigo cmc7 repassado e preenche o formulário
        /// </summary>
        /// <param name="cmc7"></param>
        private void ExtrairDados(string cmc7)
        {
            Banco banco = _service.FindBancoByCodBanco(ValidaCmc7.ExtraiCodBanco(cmc7));
            if (banco == null)
            {
                MessageBox.Show("Dados do cheque estão inválidos, tente refazer a leitura ou digite manualmente.",
                    Text, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtCmc7.Text = "";
                txtCmc7.Focus();
                return;
            }

            cobBanco.SelectedItem = banco;
            txtConta.Text = ValidaCmc7.ExtraiNumContaCliente(cmc7);
            txtAgencia.Text = ValidaCmc7.ExtraiCodAgencia(cmc7);
            txtOperacao.Text = ValidaCmc7.ExtraiCodOperadorConta(cmc7);
            txtCheque.Text = ValidaCmc7.ExtraiNumDocumento(cmc7);
        }

        private void txtAgencia_Leave(object sender, EventArgs e)
        {
            Scheque.Uteis.Util.PreencheMascara(txtAgencia.Text, txtAgencia.Mask.Length, '0');
        }

        private void txtOperacao_Leave(object sender, EventArgs e)
        {
            Scheque.Uteis.Util.PreencheMascara(txtOperacao.Text, txtOperacao.Mask.Length, '0');
        }

        private void txtConta_Leave(object sender, EventArgs e)
        {
            Scheque.Uteis.Util.PreencheMascara(txtConta.Text, txtConta.Mask.Length, '0');
        }

        private void txtCheque_Leave(object sender, EventArgs e)
        {
            Scheque.Uteis.Util.PreencheMascara(txtCheque.Text, txtCheque.Mask.Length, '0');
            criaConta();
        }

        private void criaConta()
        {
            if (cobBanco.SelectedItem == null) return;

            String idBanco = ((Banco)cobBanco.SelectedItem).Id;
            _conta = _service.FindContaByDados(idBanco, txtAgencia.Text, txtConta.Text);
            if (_conta == null)
            {
                Proprietario emitente =
                    _service.FindProprietarioByDocumentos(((Cliente) cobClientes.SelectedItem).Cpfcnpj);
                if (emitente == null)
                {
                    emitente = new Proprietario()
                    {
                        CpfCNPJ = ((Cliente)cobClientes.SelectedItem).Cpfcnpj,
                        Nome = ((Cliente)cobClientes.SelectedItem).Nome
                    };
                }
                _conta = new Conta()
                {
                    Agencia = txtAgencia.Text,
                    ContaCorrente = txtConta.Text,
                    Banco = (Banco)cobBanco.SelectedItem,
                    Operacao = txtOperacao.Text,
                    Proprietario = emitente
                         
                };
                _service.SaveConta(_conta);
            }
        }

        private void btAdd_Click(object sender, EventArgs e)
        {
            if (rbCh.Checked)
            {
                _documentos = (List<AbstracDocumento>) lbDocs.DataSource;
                _cheque = (Cheque) _documentos[_numeroDoc];
                _cheque.ContaCheque = _conta;
                _cheque.NumeroCheque = txtCheque.Text;
                _documentos[_numeroDoc] = _cheque;
                lbDocs.DataSource = null;
                lbDocs.Items.Clear();
                lbDocs.DataSource = _documentos;
                lbDocs.Refresh();
                LimpaDadosCheque();
                if (_numeroDoc == _documentos.Count - 1)
                {
                    btConfirma_Click(sender, e);
                    return;
                }
                _numeroDoc += 1;
                txtCmc7.Focus();
            }
        }

        private void FormEmprestimo_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F8:
                    if (tabEmp.Enabled) btCheques_Click(sender, e);
                    break;
                case Keys.F12:
                    if (gbCheque.Enabled) btAdd_Click(sender, e);
                    break;
            }
        }

        private void btBusca_Click(object sender, EventArgs e)
        {
            if (txtTermo.Text.Trim().Equals(""))
            {
                MessageBox.Show("O termo para busca não foi repassado.", Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                txtTermo.Focus();
                return;
            }

            if (cobBusca.SelectedItem == null)
            {
                MessageBox.Show("Não foi selecionado um tipo de busca.", Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                cobBusca.Focus();
                return;
            }

            Buscar((TipoBusca) cobBusca.SelectedItem, txtTermo.Text);
            barraBusca.Visible = true;
            cobBusca.SelectedItem = null;
            cobBusca.Focus();
            txtTermo.Text = "";
        }

        private void Buscar(TipoBusca cobBuscaSelectedItem, String termo)
        {
            cobResultado.ComboBox.DataSource = null;
            cobResultado.SelectedItem = null;
            cobResultado.Items.Clear();
            switch (cobBuscaSelectedItem)
            {
                case TipoBusca.LOTE:
                    if (!ValidaDados.SoNumeros(termo))
                    {
                        MessageBox.Show(
                            "Para busca de um lote só pode ser informado só números positvos e maiores que zero", Text,
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtTermo.Focus();
                        return;
                    }
                    List<Emprestimo> emps = new List<Emprestimo>()
                    {
                        _service.FindById(Int64.Parse(termo))
                    };
                    cobResultado.ComboBox.DataSource = emps;
                    break;
                
                case TipoBusca.DATA:
                    ///Processo ainda a ser implementado
                    break;
                case TipoBusca.NOME:
                    cobResultado.ComboBox.DataSource = _service.FindEmpByNomeCliente(termo);
                    break;
                default:
                    break;

            }
        }

        private void btEditar_Click(object sender, EventArgs e)
        {
            if (cobResultado.SelectedItem == null)
            {
                MessageBox.Show("Para abrir o lote deve ser selecionando um no resultado.", Text, MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                cobResultado.Focus();
                return;
            }
            _emprestimo = (Emprestimo) cobResultado.SelectedItem;
            PreencheCampos();
            ExibeTabs(true);
            tabEmprestimo.SelectedTab = tabEmp;
        }

        private void btDesfazer_Click(object sender, EventArgs e)
        {
            barraBusca.Visible = false;
            txtTermo.Focus();
        }

        private void btExcluir_Click(object sender, EventArgs e)
        {
            if (_emprestimo == null || _emprestimo.Id == 0)
            {
                MessageBox.Show("Não foi selecionando nenhum registro", Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var resposta = MessageBox.Show("Deseja realmente excluir o registro atual?", Text, MessageBoxButtons.YesNo,
                MessageBoxIcon.Question);
            if (resposta == DialogResult.Yes)
            {
                try
                {
                    _service.Remove(_emprestimo);
                }
                catch (Exception exception)
                {
                    MessageBox.Show(exception.Message, Text, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void btParcelas_Click(object sender, EventArgs e)
        {
            if (_emprestimo == null || _emprestimo.Id == 0)
            {
                MessageBox.Show(
                    "Não é possível imprimir as promissórias ou cheques sem que o processo esteja concluido.", Text,
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Form report = new FormReport("SchequesWin.Relatorios.RtpPromissoria.rdlc", true, _service.DadosPromissorias(_emprestimo.Id));
            report.Text = String.Format("Emprestimo {0} de {1}.", _emprestimo.Id, _emprestimo.Cliente.Nome);
            report.MdiParent = this.MdiParent;
            report.Show();
            report.WindowState = FormWindowState.Normal;
            report.WindowState = FormWindowState.Maximized;

        }
    }
}
