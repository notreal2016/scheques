﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SchequesWin.Forms
{
    public partial class FormBuscaProprietario : Form
    {
        public FormBuscaProprietario()
        {
            Inicializa();
            LabTitulo.Text = Text;


        }

        public void Inicializa()
        {
            InitializeComponent();
            
        }
        public FormBuscaProprietario(string labTitulo)
        {
            InitializeComponent();
            this.labTitulo.Text = labTitulo;
            
        }

        public Panel PanelTop
        {
            get { return panelTop; }
            set { panelTop = value; }
        }

        public Label LabTitulo
        {
            get { return labTitulo; }
            set { labTitulo = value; }
        }

        public Panel PanelLeft
        {
            get { return panelLeft; }
            set { panelLeft = value; }
        }

      

        public PictureBox PicIcone
        {
            get { return picIcone; }
            set { picIcone = value; }
        }

        

        private void FormModelo_Resize(object sender, EventArgs e)
        {
            PanelTop.Width = this.Width;
            PanelLeft.Height = this.Height;
            panelContainer.Width = this.Width;
            panelContainer.Height = this.Height;
            panelBarra.Width = PanelTop.Width;
        }

        private void FormBuscaProprietario_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
