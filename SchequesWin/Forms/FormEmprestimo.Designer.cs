﻿namespace SchequesWin.Forms
{
    partial class FormEmprestimo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormEmprestimo));
            this.barraBusca = new System.Windows.Forms.ToolStrip();
            this.btExcluir = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.cobResultado = new System.Windows.Forms.ToolStripComboBox();
            this.btEditar = new System.Windows.Forms.ToolStripButton();
            this.btImprimirLote = new System.Windows.Forms.ToolStripButton();
            this.btDesfazer = new System.Windows.Forms.ToolStripButton();
            this.barraInicial = new System.Windows.Forms.ToolStrip();
            this.btNovo = new System.Windows.Forms.ToolStripSplitButton();
            this.mMComCh = new System.Windows.Forms.ToolStripMenuItem();
            this.mMComPro = new System.Windows.Forms.ToolStripMenuItem();
            this.mMAntecipa = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.labTermo = new System.Windows.Forms.ToolStripLabel();
            this.txtTermo = new System.Windows.Forms.ToolStripTextBox();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.cobBusca = new System.Windows.Forms.ToolStripComboBox();
            this.btBusca = new System.Windows.Forms.ToolStripButton();
            this.panelAbas = new System.Windows.Forms.Panel();
            this.tabSelect = new MaterialSkin.Controls.MaterialTabSelector();
            this.tabEmprestimo = new MaterialSkin.Controls.MaterialTabControl();
            this.tabEmp = new System.Windows.Forms.TabPage();
            this.txtNumero = new NumericBox.NumericBox();
            this.btParcelas = new System.Windows.Forms.Button();
            this.grupModalidade = new System.Windows.Forms.GroupBox();
            this.chkFixo = new System.Windows.Forms.CheckBox();
            this.rbPrice = new System.Windows.Forms.RadioButton();
            this.rbSAC = new System.Windows.Forms.RadioButton();
            this.btImprimir = new System.Windows.Forms.Button();
            this.btCancela1 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbAnt = new System.Windows.Forms.RadioButton();
            this.rbCh = new System.Windows.Forms.RadioButton();
            this.rbPro = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.btFiltrar = new System.Windows.Forms.Button();
            this.btCheques = new System.Windows.Forms.Button();
            this.BtAdicionarCliente = new System.Windows.Forms.Button();
            this.txtAdm = new NumericBox.NumericBox();
            this.txtTxJuros = new NumericBox.NumericBox();
            this.txtValor = new NumericBox.NumericBox();
            this.DtData = new System.Windows.Forms.DateTimePicker();
            this.cobClientes = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabParcelas = new System.Windows.Forms.TabPage();
            this.btVoltar = new System.Windows.Forms.Button();
            this.btCancela = new System.Windows.Forms.Button();
            this.btConfirma = new System.Windows.Forms.Button();
            this.btEdCh = new System.Windows.Forms.Button();
            this.lbDocs = new System.Windows.Forms.ListBox();
            this.gbCheque = new System.Windows.Forms.GroupBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtOperacao = new System.Windows.Forms.MaskedTextBox();
            this.btAdd = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.txtCheque = new System.Windows.Forms.MaskedTextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtConta = new System.Windows.Forms.MaskedTextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtAgencia = new System.Windows.Forms.MaskedTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cobBanco = new System.Windows.Forms.ComboBox();
            this.txtCmc7 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panelTab = new System.Windows.Forms.Panel();
            this.panelLeft.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcone)).BeginInit();
            this.panelTop.SuspendLayout();
            this.panelBarra.SuspendLayout();
            this.panelContainer.SuspendLayout();
            this.barraBusca.SuspendLayout();
            this.barraInicial.SuspendLayout();
            this.panelAbas.SuspendLayout();
            this.tabEmprestimo.SuspendLayout();
            this.tabEmp.SuspendLayout();
            this.grupModalidade.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabParcelas.SuspendLayout();
            this.gbCheque.SuspendLayout();
            this.panelTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelLeft
            // 
            this.panelLeft.Size = new System.Drawing.Size(75, 503);
            // 
            // picIcone
            // 
            this.picIcone.Image = ((System.Drawing.Image)(resources.GetObject("picIcone.Image")));
            // 
            // panelTop
            // 
            this.panelTop.Size = new System.Drawing.Size(934, 95);
            // 
            // panelBarra
            // 
            this.panelBarra.Controls.Add(this.barraBusca);
            this.panelBarra.Controls.Add(this.barraInicial);
            this.panelBarra.Size = new System.Drawing.Size(934, 62);
            // 
            // panelContainer
            // 
            this.panelContainer.Controls.Add(this.panelTab);
            this.panelContainer.Controls.Add(this.panelAbas);
            this.panelContainer.Size = new System.Drawing.Size(934, 408);
            // 
            // barraBusca
            // 
            this.barraBusca.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.barraBusca.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btExcluir,
            this.toolStripSeparator3,
            this.toolStripLabel2,
            this.cobResultado,
            this.btEditar,
            this.btImprimirLote,
            this.btDesfazer});
            this.barraBusca.Location = new System.Drawing.Point(0, 31);
            this.barraBusca.Name = "barraBusca";
            this.barraBusca.Size = new System.Drawing.Size(934, 31);
            this.barraBusca.TabIndex = 8;
            this.barraBusca.Text = "Barra de edição";
            this.barraBusca.Visible = false;
            // 
            // btExcluir
            // 
            this.btExcluir.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btExcluir.Image = ((System.Drawing.Image)(resources.GetObject("btExcluir.Image")));
            this.btExcluir.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btExcluir.Name = "btExcluir";
            this.btExcluir.Size = new System.Drawing.Size(28, 28);
            this.btExcluir.Text = "Exclui o lote";
            this.btExcluir.Click += new System.EventHandler(this.btExcluir_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 31);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(59, 28);
            this.toolStripLabel2.Text = "Resultado";
            // 
            // cobResultado
            // 
            this.cobResultado.Name = "cobResultado";
            this.cobResultado.Size = new System.Drawing.Size(400, 31);
            // 
            // btEditar
            // 
            this.btEditar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btEditar.Image = ((System.Drawing.Image)(resources.GetObject("btEditar.Image")));
            this.btEditar.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btEditar.Name = "btEditar";
            this.btEditar.Size = new System.Drawing.Size(28, 28);
            this.btEditar.Text = "Edita o lote";
            this.btEditar.Click += new System.EventHandler(this.btEditar_Click);
            // 
            // btImprimirLote
            // 
            this.btImprimirLote.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btImprimirLote.Image = ((System.Drawing.Image)(resources.GetObject("btImprimirLote.Image")));
            this.btImprimirLote.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btImprimirLote.Name = "btImprimirLote";
            this.btImprimirLote.Size = new System.Drawing.Size(28, 28);
            this.btImprimirLote.Text = "Imprimir o lote";
            // 
            // btDesfazer
            // 
            this.btDesfazer.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btDesfazer.Image = ((System.Drawing.Image)(resources.GetObject("btDesfazer.Image")));
            this.btDesfazer.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btDesfazer.Name = "btDesfazer";
            this.btDesfazer.Size = new System.Drawing.Size(28, 28);
            this.btDesfazer.Text = "Desfaz o filtro";
            this.btDesfazer.Click += new System.EventHandler(this.btDesfazer_Click);
            // 
            // barraInicial
            // 
            this.barraInicial.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.barraInicial.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btNovo,
            this.toolStripSeparator1,
            this.labTermo,
            this.txtTermo,
            this.toolStripLabel1,
            this.cobBusca,
            this.btBusca});
            this.barraInicial.Location = new System.Drawing.Point(0, 0);
            this.barraInicial.Name = "barraInicial";
            this.barraInicial.Size = new System.Drawing.Size(934, 31);
            this.barraInicial.TabIndex = 7;
            this.barraInicial.Text = "Barra de Inclusão e Busca";
            // 
            // btNovo
            // 
            this.btNovo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btNovo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mMComCh,
            this.mMComPro,
            this.mMAntecipa});
            this.btNovo.Image = global::SchequesWin.Properties.Resources.Novo_24;
            this.btNovo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btNovo.Name = "btNovo";
            this.btNovo.Size = new System.Drawing.Size(40, 28);
            this.btNovo.Text = "Cria um novo emprestimo";
            // 
            // mMComCh
            // 
            this.mMComCh.Name = "mMComCh";
            this.mMComCh.Size = new System.Drawing.Size(170, 22);
            this.mMComCh.Text = "Com Cheques";
            this.mMComCh.Click += new System.EventHandler(this.mMComCh_Click);
            // 
            // mMComPro
            // 
            this.mMComPro.Name = "mMComPro";
            this.mMComPro.Size = new System.Drawing.Size(170, 22);
            this.mMComPro.Text = "Com Promissorias";
            this.mMComPro.Click += new System.EventHandler(this.mMComPro_Click);
            // 
            // mMAntecipa
            // 
            this.mMAntecipa.Name = "mMAntecipa";
            this.mMAntecipa.Size = new System.Drawing.Size(170, 22);
            this.mMAntecipa.Text = "Antecipação";
            this.mMAntecipa.Click += new System.EventHandler(this.mMAntecipa_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 31);
            // 
            // labTermo
            // 
            this.labTermo.Name = "labTermo";
            this.labTermo.Size = new System.Drawing.Size(91, 28);
            this.labTermo.Text = "Termo de busca";
            // 
            // txtTermo
            // 
            this.txtTermo.Name = "txtTermo";
            this.txtTermo.Size = new System.Drawing.Size(200, 31);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(84, 28);
            this.toolStripLabel1.Text = "Tipo de busca:";
            // 
            // cobBusca
            // 
            this.cobBusca.Name = "cobBusca";
            this.cobBusca.Size = new System.Drawing.Size(121, 31);
            // 
            // btBusca
            // 
            this.btBusca.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btBusca.Image = ((System.Drawing.Image)(resources.GetObject("btBusca.Image")));
            this.btBusca.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btBusca.Name = "btBusca";
            this.btBusca.Size = new System.Drawing.Size(28, 28);
            this.btBusca.Text = "Buscar";
            this.btBusca.Click += new System.EventHandler(this.btBusca_Click);
            // 
            // panelAbas
            // 
            this.panelAbas.BackColor = System.Drawing.Color.Transparent;
            this.panelAbas.Controls.Add(this.tabSelect);
            this.panelAbas.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelAbas.Location = new System.Drawing.Point(0, 0);
            this.panelAbas.Name = "panelAbas";
            this.panelAbas.Size = new System.Drawing.Size(934, 29);
            this.panelAbas.TabIndex = 0;
            // 
            // tabSelect
            // 
            this.tabSelect.BaseTabControl = this.tabEmprestimo;
            this.tabSelect.Depth = 0;
            this.tabSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabSelect.Location = new System.Drawing.Point(0, 0);
            this.tabSelect.MouseState = MaterialSkin.MouseState.HOVER;
            this.tabSelect.Name = "tabSelect";
            this.tabSelect.Size = new System.Drawing.Size(934, 29);
            this.tabSelect.TabIndex = 0;
            this.tabSelect.Text = "materialTabSelector1";
            // 
            // tabEmprestimo
            // 
            this.tabEmprestimo.Controls.Add(this.tabEmp);
            this.tabEmprestimo.Controls.Add(this.tabParcelas);
            this.tabEmprestimo.Depth = 0;
            this.tabEmprestimo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabEmprestimo.Location = new System.Drawing.Point(0, 0);
            this.tabEmprestimo.MouseState = MaterialSkin.MouseState.HOVER;
            this.tabEmprestimo.Name = "tabEmprestimo";
            this.tabEmprestimo.SelectedIndex = 0;
            this.tabEmprestimo.Size = new System.Drawing.Size(934, 379);
            this.tabEmprestimo.TabIndex = 1;
            // 
            // tabEmp
            // 
            this.tabEmp.BackColor = System.Drawing.Color.Transparent;
            this.tabEmp.Controls.Add(this.txtNumero);
            this.tabEmp.Controls.Add(this.btParcelas);
            this.tabEmp.Controls.Add(this.grupModalidade);
            this.tabEmp.Controls.Add(this.btImprimir);
            this.tabEmp.Controls.Add(this.btCancela1);
            this.tabEmp.Controls.Add(this.groupBox1);
            this.tabEmp.Controls.Add(this.label7);
            this.tabEmp.Controls.Add(this.btFiltrar);
            this.tabEmp.Controls.Add(this.btCheques);
            this.tabEmp.Controls.Add(this.BtAdicionarCliente);
            this.tabEmp.Controls.Add(this.txtAdm);
            this.tabEmp.Controls.Add(this.txtTxJuros);
            this.tabEmp.Controls.Add(this.txtValor);
            this.tabEmp.Controls.Add(this.DtData);
            this.tabEmp.Controls.Add(this.cobClientes);
            this.tabEmp.Controls.Add(this.label6);
            this.tabEmp.Controls.Add(this.label5);
            this.tabEmp.Controls.Add(this.label4);
            this.tabEmp.Controls.Add(this.label3);
            this.tabEmp.Controls.Add(this.label2);
            this.tabEmp.Controls.Add(this.label1);
            this.tabEmp.Location = new System.Drawing.Point(4, 30);
            this.tabEmp.Name = "tabEmp";
            this.tabEmp.Padding = new System.Windows.Forms.Padding(3);
            this.tabEmp.Size = new System.Drawing.Size(926, 345);
            this.tabEmp.TabIndex = 0;
            this.tabEmp.Text = "Emprestimo";
            // 
            // txtNumero
            // 
            this.txtNumero.BackColor = System.Drawing.SystemColors.Window;
            this.txtNumero.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtNumero.Location = new System.Drawing.Point(108, 113);
            this.txtNumero.Name = "txtNumero";
            this.txtNumero.NegativeNumberColor = System.Drawing.Color.Red;
            this.txtNumero.NoDigitsAfterDecimalSymbol = 0;
            this.txtNumero.Size = new System.Drawing.Size(42, 29);
            this.txtNumero.TabIndex = 3;
            this.txtNumero.Text = "0";
            // 
            // btParcelas
            // 
            this.btParcelas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btParcelas.Image = global::SchequesWin.Properties.Resources.imprimir24;
            this.btParcelas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btParcelas.Location = new System.Drawing.Point(225, 289);
            this.btParcelas.Name = "btParcelas";
            this.btParcelas.Size = new System.Drawing.Size(94, 32);
            this.btParcelas.TabIndex = 53;
            this.btParcelas.TabStop = false;
            this.btParcelas.Text = "Parcelas";
            this.btParcelas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btParcelas.UseVisualStyleBackColor = true;
            this.btParcelas.Click += new System.EventHandler(this.btParcelas_Click);
            // 
            // grupModalidade
            // 
            this.grupModalidade.Controls.Add(this.chkFixo);
            this.grupModalidade.Controls.Add(this.rbPrice);
            this.grupModalidade.Controls.Add(this.rbSAC);
            this.grupModalidade.Location = new System.Drawing.Point(270, 169);
            this.grupModalidade.Name = "grupModalidade";
            this.grupModalidade.Size = new System.Drawing.Size(245, 100);
            this.grupModalidade.TabIndex = 52;
            this.grupModalidade.TabStop = false;
            this.grupModalidade.Text = "Modalidade de calculo";
            // 
            // chkFixo
            // 
            this.chkFixo.AutoSize = true;
            this.chkFixo.Checked = true;
            this.chkFixo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkFixo.Location = new System.Drawing.Point(10, 60);
            this.chkFixo.Name = "chkFixo";
            this.chkFixo.Size = new System.Drawing.Size(81, 25);
            this.chkFixo.TabIndex = 52;
            this.chkFixo.Text = "Dia fixo";
            this.chkFixo.UseVisualStyleBackColor = true;
            // 
            // rbPrice
            // 
            this.rbPrice.AutoSize = true;
            this.rbPrice.Checked = true;
            this.rbPrice.Location = new System.Drawing.Point(135, 28);
            this.rbPrice.Name = "rbPrice";
            this.rbPrice.Size = new System.Drawing.Size(64, 25);
            this.rbPrice.TabIndex = 51;
            this.rbPrice.TabStop = true;
            this.rbPrice.Text = "Price";
            this.rbPrice.UseVisualStyleBackColor = true;
            // 
            // rbSAC
            // 
            this.rbSAC.AutoSize = true;
            this.rbSAC.Location = new System.Drawing.Point(10, 28);
            this.rbSAC.Name = "rbSAC";
            this.rbSAC.Size = new System.Drawing.Size(62, 25);
            this.rbSAC.TabIndex = 50;
            this.rbSAC.Text = "SAC";
            this.rbSAC.UseVisualStyleBackColor = true;
            // 
            // btImprimir
            // 
            this.btImprimir.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btImprimir.Image = global::SchequesWin.Properties.Resources.imprimir24;
            this.btImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btImprimir.Location = new System.Drawing.Point(114, 289);
            this.btImprimir.Name = "btImprimir";
            this.btImprimir.Size = new System.Drawing.Size(94, 32);
            this.btImprimir.TabIndex = 51;
            this.btImprimir.TabStop = false;
            this.btImprimir.Text = "Emprestimo";
            this.btImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btImprimir.UseVisualStyleBackColor = true;
            this.btImprimir.Click += new System.EventHandler(this.btImprimir_Click);
            // 
            // btCancela1
            // 
            this.btCancela1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCancela1.Image = global::SchequesWin.Properties.Resources.Excluir_24;
            this.btCancela1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCancela1.Location = new System.Drawing.Point(11, 289);
            this.btCancela1.Name = "btCancela1";
            this.btCancela1.Size = new System.Drawing.Size(79, 32);
            this.btCancela1.TabIndex = 50;
            this.btCancela1.TabStop = false;
            this.btCancela1.Text = "Cancela";
            this.btCancela1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btCancela1.UseVisualStyleBackColor = true;
            this.btCancela1.Click += new System.EventHandler(this.btCancelar_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rbAnt);
            this.groupBox1.Controls.Add(this.rbCh);
            this.groupBox1.Controls.Add(this.rbPro);
            this.groupBox1.Enabled = false;
            this.groupBox1.Location = new System.Drawing.Point(11, 169);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(253, 100);
            this.groupBox1.TabIndex = 49;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tipo de parcelas:";
            // 
            // rbAnt
            // 
            this.rbAnt.AutoSize = true;
            this.rbAnt.Location = new System.Drawing.Point(24, 53);
            this.rbAnt.Name = "rbAnt";
            this.rbAnt.Size = new System.Drawing.Size(116, 25);
            this.rbAnt.TabIndex = 50;
            this.rbAnt.TabStop = true;
            this.rbAnt.Text = "Antecipação";
            this.rbAnt.UseVisualStyleBackColor = true;
            // 
            // rbCh
            // 
            this.rbCh.AutoSize = true;
            this.rbCh.Location = new System.Drawing.Point(149, 28);
            this.rbCh.Name = "rbCh";
            this.rbCh.Size = new System.Drawing.Size(93, 25);
            this.rbCh.TabIndex = 49;
            this.rbCh.TabStop = true;
            this.rbCh.Text = "Cheques";
            this.rbCh.UseVisualStyleBackColor = true;
            // 
            // rbPro
            // 
            this.rbPro.AutoSize = true;
            this.rbPro.Location = new System.Drawing.Point(24, 28);
            this.rbPro.Name = "rbPro";
            this.rbPro.Size = new System.Drawing.Size(119, 25);
            this.rbPro.TabIndex = 48;
            this.rbPro.TabStop = true;
            this.rbPro.Text = "Promissorias";
            this.rbPro.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(10, 3);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(28, 21);
            this.label7.TabIndex = 48;
            this.label7.Text = "Id:";
            // 
            // btFiltrar
            // 
            this.btFiltrar.Image = ((System.Drawing.Image)(resources.GetObject("btFiltrar.Image")));
            this.btFiltrar.Location = new System.Drawing.Point(564, 35);
            this.btFiltrar.Name = "btFiltrar";
            this.btFiltrar.Size = new System.Drawing.Size(37, 28);
            this.btFiltrar.TabIndex = 40;
            this.btFiltrar.TabStop = false;
            this.btFiltrar.UseVisualStyleBackColor = true;
            this.btFiltrar.Click += new System.EventHandler(this.btFiltrar_Click);
            // 
            // btCheques
            // 
            this.btCheques.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCheques.Image = ((System.Drawing.Image)(resources.GetObject("btCheques.Image")));
            this.btCheques.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCheques.Location = new System.Drawing.Point(376, 289);
            this.btCheques.Name = "btCheques";
            this.btCheques.Size = new System.Drawing.Size(139, 32);
            this.btCheques.TabIndex = 6;
            this.btCheques.Text = "Gerar parcelas (F8)";
            this.btCheques.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btCheques.UseVisualStyleBackColor = true;
            this.btCheques.Click += new System.EventHandler(this.btCheques_Click);
            // 
            // BtAdicionarCliente
            // 
            this.BtAdicionarCliente.Image = global::SchequesWin.Properties.Resources.cliente_24;
            this.BtAdicionarCliente.Location = new System.Drawing.Point(521, 35);
            this.BtAdicionarCliente.Name = "BtAdicionarCliente";
            this.BtAdicionarCliente.Size = new System.Drawing.Size(37, 28);
            this.BtAdicionarCliente.TabIndex = 39;
            this.BtAdicionarCliente.TabStop = false;
            this.BtAdicionarCliente.UseVisualStyleBackColor = true;
            this.BtAdicionarCliente.Click += new System.EventHandler(this.BtAdicionarCliente_Click);
            // 
            // txtAdm
            // 
            this.txtAdm.BackColor = System.Drawing.SystemColors.Window;
            this.txtAdm.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtAdm.Location = new System.Drawing.Point(453, 113);
            this.txtAdm.Name = "txtAdm";
            this.txtAdm.NegativeNumberColor = System.Drawing.Color.Red;
            this.txtAdm.Size = new System.Drawing.Size(62, 29);
            this.txtAdm.TabIndex = 5;
            this.txtAdm.Text = "0,00";
            // 
            // txtTxJuros
            // 
            this.txtTxJuros.BackColor = System.Drawing.SystemColors.Window;
            this.txtTxJuros.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtTxJuros.Location = new System.Drawing.Point(241, 113);
            this.txtTxJuros.Name = "txtTxJuros";
            this.txtTxJuros.NegativeNumberColor = System.Drawing.Color.Red;
            this.txtTxJuros.Size = new System.Drawing.Size(62, 29);
            this.txtTxJuros.TabIndex = 4;
            this.txtTxJuros.Text = "0,00";
            // 
            // txtValor
            // 
            this.txtValor.BackColor = System.Drawing.SystemColors.Window;
            this.txtValor.Cursor = System.Windows.Forms.Cursors.Default;
            this.txtValor.Location = new System.Drawing.Point(76, 69);
            this.txtValor.Name = "txtValor";
            this.txtValor.NegativeNumberColor = System.Drawing.Color.Red;
            this.txtValor.Size = new System.Drawing.Size(128, 29);
            this.txtValor.TabIndex = 1;
            this.txtValor.Text = "0,00";
            // 
            // DtData
            // 
            this.DtData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.DtData.Location = new System.Drawing.Point(347, 69);
            this.DtData.Name = "DtData";
            this.DtData.Size = new System.Drawing.Size(168, 29);
            this.DtData.TabIndex = 2;
            // 
            // cobClientes
            // 
            this.cobClientes.FormattingEnabled = true;
            this.cobClientes.Location = new System.Drawing.Point(76, 34);
            this.cobClientes.Name = "cobClientes";
            this.cobClientes.Size = new System.Drawing.Size(439, 29);
            this.cobClientes.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(309, 116);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(138, 21);
            this.label6.TabIndex = 5;
            this.label6.Text = "Tx. Administrativa:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(156, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 21);
            this.label5.TabIndex = 4;
            this.label5.Text = "Tx. Juros:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(10, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 21);
            this.label4.TabIndex = 3;
            this.label4.Text = "Nº Parcelas:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(10, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 21);
            this.label3.TabIndex = 2;
            this.label3.Text = "Valor :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(210, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 21);
            this.label2.TabIndex = 1;
            this.label2.Text = "Data de contrato:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 21);
            this.label1.TabIndex = 0;
            this.label1.Text = "Cliente:";
            // 
            // tabParcelas
            // 
            this.tabParcelas.BackColor = System.Drawing.Color.Transparent;
            this.tabParcelas.Controls.Add(this.btVoltar);
            this.tabParcelas.Controls.Add(this.btCancela);
            this.tabParcelas.Controls.Add(this.btConfirma);
            this.tabParcelas.Controls.Add(this.btEdCh);
            this.tabParcelas.Controls.Add(this.lbDocs);
            this.tabParcelas.Controls.Add(this.gbCheque);
            this.tabParcelas.Location = new System.Drawing.Point(4, 30);
            this.tabParcelas.Name = "tabParcelas";
            this.tabParcelas.Padding = new System.Windows.Forms.Padding(3);
            this.tabParcelas.Size = new System.Drawing.Size(856, 293);
            this.tabParcelas.TabIndex = 1;
            this.tabParcelas.Text = "Parcelas";
            // 
            // btVoltar
            // 
            this.btVoltar.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btVoltar.Image = global::SchequesWin.Properties.Resources.desfaz;
            this.btVoltar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btVoltar.Location = new System.Drawing.Point(344, 307);
            this.btVoltar.Name = "btVoltar";
            this.btVoltar.Size = new System.Drawing.Size(109, 32);
            this.btVoltar.TabIndex = 17;
            this.btVoltar.TabStop = false;
            this.btVoltar.Text = "Voltar a capa";
            this.btVoltar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btVoltar.UseVisualStyleBackColor = true;
            this.btVoltar.Click += new System.EventHandler(this.btVoltar_Click);
            // 
            // btCancela
            // 
            this.btCancela.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCancela.Image = global::SchequesWin.Properties.Resources.Excluir_24;
            this.btCancela.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btCancela.Location = new System.Drawing.Point(6, 307);
            this.btCancela.Name = "btCancela";
            this.btCancela.Size = new System.Drawing.Size(79, 32);
            this.btCancela.TabIndex = 15;
            this.btCancela.TabStop = false;
            this.btCancela.Text = "Cancela";
            this.btCancela.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btCancela.UseVisualStyleBackColor = true;
            this.btCancela.Click += new System.EventHandler(this.btCancelar_Click);
            // 
            // btConfirma
            // 
            this.btConfirma.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btConfirma.Image = global::SchequesWin.Properties.Resources.Lixo_24;
            this.btConfirma.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btConfirma.Location = new System.Drawing.Point(716, 307);
            this.btConfirma.Name = "btConfirma";
            this.btConfirma.Size = new System.Drawing.Size(82, 32);
            this.btConfirma.TabIndex = 16;
            this.btConfirma.TabStop = false;
            this.btConfirma.Text = "Confirma";
            this.btConfirma.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btConfirma.UseVisualStyleBackColor = true;
            this.btConfirma.Click += new System.EventHandler(this.btConfirma_Click);
            // 
            // btEdCh
            // 
            this.btEdCh.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btEdCh.Image = global::SchequesWin.Properties.Resources.Editar_24;
            this.btEdCh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btEdCh.Location = new System.Drawing.Point(804, 187);
            this.btEdCh.Name = "btEdCh";
            this.btEdCh.Size = new System.Drawing.Size(68, 32);
            this.btEdCh.TabIndex = 14;
            this.btEdCh.TabStop = false;
            this.btEdCh.Text = "Editar";
            this.btEdCh.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btEdCh.UseVisualStyleBackColor = true;
            // 
            // lbDocs
            // 
            this.lbDocs.FormattingEnabled = true;
            this.lbDocs.ItemHeight = 21;
            this.lbDocs.Location = new System.Drawing.Point(7, 187);
            this.lbDocs.Name = "lbDocs";
            this.lbDocs.Size = new System.Drawing.Size(791, 88);
            this.lbDocs.TabIndex = 1;
            // 
            // gbCheque
            // 
            this.gbCheque.Controls.Add(this.label13);
            this.gbCheque.Controls.Add(this.txtOperacao);
            this.gbCheque.Controls.Add(this.btAdd);
            this.gbCheque.Controls.Add(this.label12);
            this.gbCheque.Controls.Add(this.txtCheque);
            this.gbCheque.Controls.Add(this.label11);
            this.gbCheque.Controls.Add(this.txtConta);
            this.gbCheque.Controls.Add(this.label10);
            this.gbCheque.Controls.Add(this.txtAgencia);
            this.gbCheque.Controls.Add(this.label9);
            this.gbCheque.Controls.Add(this.cobBanco);
            this.gbCheque.Controls.Add(this.txtCmc7);
            this.gbCheque.Controls.Add(this.label8);
            this.gbCheque.Location = new System.Drawing.Point(7, 7);
            this.gbCheque.Name = "gbCheque";
            this.gbCheque.Size = new System.Drawing.Size(791, 173);
            this.gbCheque.TabIndex = 0;
            this.gbCheque.TabStop = false;
            this.gbCheque.Text = "Dados do cheque";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(6, 106);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(84, 21);
            this.label13.TabIndex = 13;
            this.label13.Text = "Operação:";
            // 
            // txtOperacao
            // 
            this.txtOperacao.Location = new System.Drawing.Point(110, 103);
            this.txtOperacao.Mask = "000";
            this.txtOperacao.Name = "txtOperacao";
            this.txtOperacao.Size = new System.Drawing.Size(69, 29);
            this.txtOperacao.TabIndex = 10;
            this.txtOperacao.Leave += new System.EventHandler(this.txtOperacao_Leave);
            // 
            // btAdd
            // 
            this.btAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btAdd.Image = ((System.Drawing.Image)(resources.GetObject("btAdd.Image")));
            this.btAdd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btAdd.Location = new System.Drawing.Point(653, 135);
            this.btAdd.Name = "btAdd";
            this.btAdd.Size = new System.Drawing.Size(119, 32);
            this.btAdd.TabIndex = 13;
            this.btAdd.Text = "Adicionar (F12)";
            this.btAdd.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btAdd.UseVisualStyleBackColor = true;
            this.btAdd.Click += new System.EventHandler(this.btAdd_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(354, 106);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(93, 21);
            this.label12.TabIndex = 9;
            this.label12.Text = "Nº Cheque:";
            // 
            // txtCheque
            // 
            this.txtCheque.Location = new System.Drawing.Point(453, 103);
            this.txtCheque.Mask = "000000";
            this.txtCheque.Name = "txtCheque";
            this.txtCheque.Size = new System.Drawing.Size(100, 29);
            this.txtCheque.TabIndex = 12;
            this.txtCheque.Leave += new System.EventHandler(this.txtCheque_Leave);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(185, 106);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 21);
            this.label11.TabIndex = 7;
            this.label11.Text = "Conta:";
            // 
            // txtConta
            // 
            this.txtConta.Location = new System.Drawing.Point(248, 103);
            this.txtConta.Mask = "000000";
            this.txtConta.Name = "txtConta";
            this.txtConta.Size = new System.Drawing.Size(100, 29);
            this.txtConta.TabIndex = 11;
            this.txtConta.Leave += new System.EventHandler(this.txtConta_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(586, 68);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(72, 21);
            this.label10.TabIndex = 5;
            this.label10.Text = "Agência:";
            // 
            // txtAgencia
            // 
            this.txtAgencia.Location = new System.Drawing.Point(672, 65);
            this.txtAgencia.Mask = "0000";
            this.txtAgencia.Name = "txtAgencia";
            this.txtAgencia.Size = new System.Drawing.Size(100, 29);
            this.txtAgencia.TabIndex = 9;
            this.txtAgencia.Leave += new System.EventHandler(this.txtAgencia_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 68);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 21);
            this.label9.TabIndex = 3;
            this.label9.Text = "Banco:";
            // 
            // cobBanco
            // 
            this.cobBanco.FormattingEnabled = true;
            this.cobBanco.Location = new System.Drawing.Point(110, 65);
            this.cobBanco.Name = "cobBanco";
            this.cobBanco.Size = new System.Drawing.Size(470, 29);
            this.cobBanco.TabIndex = 8;
            // 
            // txtCmc7
            // 
            this.txtCmc7.Location = new System.Drawing.Point(110, 29);
            this.txtCmc7.Name = "txtCmc7";
            this.txtCmc7.Size = new System.Drawing.Size(662, 29);
            this.txtCmc7.TabIndex = 7;
            this.txtCmc7.Leave += new System.EventHandler(this.txtCmc7_Leave);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(6, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(98, 21);
            this.label8.TabIndex = 0;
            this.label8.Text = "Cod. CMC7:";
            // 
            // panelTab
            // 
            this.panelTab.BackColor = System.Drawing.Color.Transparent;
            this.panelTab.Controls.Add(this.tabEmprestimo);
            this.panelTab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelTab.Location = new System.Drawing.Point(0, 29);
            this.panelTab.Name = "panelTab";
            this.panelTab.Size = new System.Drawing.Size(934, 379);
            this.panelTab.TabIndex = 1;
            // 
            // FormEmprestimo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 503);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormEmprestimo";
            this.Text = "Cadastro de emprestimos";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormEmprestimo_KeyDown);
            this.panelLeft.ResumeLayout(false);
            this.panelLeft.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picIcone)).EndInit();
            this.panelTop.ResumeLayout(false);
            this.panelTop.PerformLayout();
            this.panelBarra.ResumeLayout(false);
            this.panelBarra.PerformLayout();
            this.panelContainer.ResumeLayout(false);
            this.barraBusca.ResumeLayout(false);
            this.barraBusca.PerformLayout();
            this.barraInicial.ResumeLayout(false);
            this.barraInicial.PerformLayout();
            this.panelAbas.ResumeLayout(false);
            this.tabEmprestimo.ResumeLayout(false);
            this.tabEmp.ResumeLayout(false);
            this.tabEmp.PerformLayout();
            this.grupModalidade.ResumeLayout(false);
            this.grupModalidade.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabParcelas.ResumeLayout(false);
            this.gbCheque.ResumeLayout(false);
            this.gbCheque.PerformLayout();
            this.panelTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStrip barraBusca;
        private System.Windows.Forms.ToolStripButton btExcluir;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripComboBox cobResultado;
        private System.Windows.Forms.ToolStripButton btEditar;
        private System.Windows.Forms.ToolStripButton btImprimirLote;
        private System.Windows.Forms.ToolStripButton btDesfazer;
        private System.Windows.Forms.ToolStrip barraInicial;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel labTermo;
        private System.Windows.Forms.ToolStripTextBox txtTermo;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripComboBox cobBusca;
        private System.Windows.Forms.ToolStripButton btBusca;
        private System.Windows.Forms.ToolStripSplitButton btNovo;
        private System.Windows.Forms.ToolStripMenuItem mMComCh;
        private System.Windows.Forms.ToolStripMenuItem mMComPro;
        private System.Windows.Forms.ToolStripMenuItem mMAntecipa;
        private System.Windows.Forms.Panel panelTab;
        private MaterialSkin.Controls.MaterialTabControl tabEmprestimo;
        private System.Windows.Forms.TabPage tabEmp;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbAnt;
        private System.Windows.Forms.RadioButton rbCh;
        private System.Windows.Forms.RadioButton rbPro;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btFiltrar;
        private System.Windows.Forms.Button btCheques;
        private System.Windows.Forms.Button BtAdicionarCliente;
        private NumericBox.NumericBox txtAdm;
        private NumericBox.NumericBox txtTxJuros;
        private NumericBox.NumericBox txtValor;
        private System.Windows.Forms.DateTimePicker DtData;
        private System.Windows.Forms.ComboBox cobClientes;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabParcelas;
        private System.Windows.Forms.GroupBox gbCheque;
        private System.Windows.Forms.Panel panelAbas;
        private MaterialSkin.Controls.MaterialTabSelector tabSelect;
        private System.Windows.Forms.ListBox lbDocs;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cobBanco;
        private System.Windows.Forms.TextBox txtCmc7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btEdCh;
        private System.Windows.Forms.Button btAdd;
        private System.Windows.Forms.Button btCancela;
        private System.Windows.Forms.Button btConfirma;
        private System.Windows.Forms.Button btImprimir;
        private System.Windows.Forms.Button btCancela1;
        private System.Windows.Forms.GroupBox grupModalidade;
        private System.Windows.Forms.CheckBox chkFixo;
        private System.Windows.Forms.RadioButton rbPrice;
        private System.Windows.Forms.RadioButton rbSAC;
        private System.Windows.Forms.Button btParcelas;
        private NumericBox.NumericBox txtNumero;
        private System.Windows.Forms.Button btVoltar;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox txtCheque;
        private System.Windows.Forms.MaskedTextBox txtConta;
        private System.Windows.Forms.MaskedTextBox txtAgencia;
        private System.Windows.Forms.MaskedTextBox txtOperacao;
    }
}